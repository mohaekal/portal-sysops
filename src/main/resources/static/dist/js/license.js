$("#frm_edit").find(".seeKey").click(function(){
    console.log("prod key")
    if($("#frm_edit").find("[name=productKey]").attr("type")=="password"){
        $("#frm_edit").find("[name=productKey]").attr("type","text")
    }else{
        $("#frm_edit").find("[name=productKey]").attr("type","password")
    }
});

$(document).ready(function(){
    $("#frm_add").validate({
        rules: {
            name: {
                required: true,
            },
            qty: {
                required: true,
            },
            supplierId: {
                required: true,
            },
            licenseCategoryId: {
                required: true,
            },manufacturerId: {
                required: true,
            },supplierId: {
                required: true,
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    $("#frm_edit").validate({
        rules: {
            name: {
                required: true,
            },
            qty: {
                required: true,
            },
            supplierId: {
                required: true,
            },
            categoryId: {
                required: true,
            },manufacturerId: {
                required: true,
            },supplierId: {
                required: true,
            }

        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

})

function submitAdd(button){
    if ($("#frm_add").valid()){


        var formd = new FormData(document.querySelector('#frm_add'));


        $.ajax({
            url: "/api/license",
            type:"post",
            data: formd,
            processData: false,
            contentType: false,
            beforeSend: function () {
                loadingBtn(button);
            },
            success: function (res) {
                if (res.success) {
                    datarow.ajax.reload();
                    $("#frm_add").trigger("reset");
                } else {
                    alert("Sorry data cannot updated");
                }
            },
        })
            .fail(function () {
                alert("Sorry something wrong while processing");
            })
            .always(function () {
                $("#modal_add").modal("hide");
                unloadingBtn(button);
            });
    }
}

function submitUpdate(button){
    if($("#frm_edit").valid()){
        var formd = new FormData(document.querySelector('#frm_edit'));

        $.ajax({
            url: "/api/license",
            type:"put",
            data: formd,
            processData: false,
            contentType: false,
            beforeSend: function () {
                loadingBtn(button);
            },
            success: function (res) {
                if (res.success) {
                    datarow.ajax.reload();
                } else {
                    alert("Sorry data cannot updated");
                }
            },
        })
            .fail(function () {
                alert("Sorry something wrong while processing");
            })
            .always(function () {
                $("#loading").hide();
                $("#datas").show();
                $("#modal_edit").modal("hide");
                unloadingBtn(button);
            });
    }
}

var datarow = $("#tbl_data").DataTable({
    ajax: {
        type:"get",
        url: "/api/license",
    },
    paging: true,
    lengthChange: true,
    searching: true,
    info: true,
    ordering:false,
    autoWidth: false,
    responsive: true,
    columns: [
        { data: 'id' },
        { data: 'name' },
        { data: 'qty' },
        { data: 'category' },
        { data: 'supplier' },
        { data: 'manufacturer' }
    ],
    columnDefs: [
        {
            targets: 0,
            render: function (data, type, row, meta) {
                var btnDelete = '';
                if (privilege.filter(e=>e.authority=='ROLE_LICENSE').length>0){
                    btnDelete= `<button onclick="deleteId(this,'`+row.id+`')" class="btn btn-outline-danger btn-flat btn-sm"><i class="fa fa-trash"></i></button> `
                }
                return `<button type="button" class="btn btn-outline-primary btn-flat btn-sm " onclick="func_update('`+row.id+`')"><i class="fa fa-external-link-alt"> `+row.id+`</i></button> `+btnDelete;
            },
        }],
    initComplete: function () {
        var thisTable = this;
        var rowFilter = $('<tr class="filter"></tr>').appendTo(
            $(this.api().table().header())
        );

        this.api()
            .columns()
            .every(function () {
                var column = this;
                switch (column.index()) {
                    case 3:
                    case 4:
                    case 5:
                        var select = $(
                            `<select class="form-control form-control-sm form-filter kt-input" title="Select" data-col-index="` +
                            column.index() +
                            `"><option value="">Select</option></select>`
                        )
                            .appendTo($("<th>").appendTo(rowFilter))
                            .on("change", function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                column
                                    .search(val ? "^" + val + "$" : "", true, false)
                                    // .search(val)
                                    .draw();
                            });

                        column.data().unique().sort().each(function (d, j) {
                            select.append(
                                '<option value="' + d + '">' + d + "</option>"
                            );
                        });

                        break;
                    case 7:
                        var select = $(
                            `<select class="form-control form-control-sm form-filter kt-input" title="Select" data-col-index="` +
                            column.index() +
                            `"><option value="">Select</option></select>`
                        )
                            .appendTo($("<th>").appendTo(rowFilter))
                            .on("change", function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                column
                                    .search(val ? "^" + val + "$" : "", true, false)
                                    // .search(val)
                                    .draw();
                            });

                        column.data().unique().sort().each(function (d, j) {

                            var dd = (d==1)?'Expired':'Active';
                            select.append(
                                '<option value="' + dd + '">' + dd + "</option>"
                            );
                        });

                        break;
                    case 1:
                    case 2:
                        var input = $(
                            `<input class="form-control form-control-sm form-filter " title="Input" data-col-index="` +
                            column.index() +
                            `"/>`
                        )
                            .appendTo($("<th>").appendTo(rowFilter))
                            .on("keyup change", function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                column
                                    // .search(val ? "^" + val + "$" : "", true, false)
                                    .search(val)
                                    .draw();
                            });
                        break;
                    default:
                        var select = $("").appendTo($("<th>").appendTo(rowFilter));
                        break;
                }
            });

        var hideSearchColumnResponsive = function () {
            thisTable
                .api()
                .columns()
                .every(function () {
                    var column = this;
                    if (column.responsiveHidden()) {
                        $(".filter").find("th").eq(column.index()).show();
                    } else {
                        $(".filter").find("th").eq(column.index()).hide();
                    }
                });
        };

        // init on datatable load
        hideSearchColumnResponsive();
        // recheck on window resize
        window.onresize = hideSearchColumnResponsive;
    }
});

function deleteId(button,id) {
    var conf = confirm("Are you sure to delete this item ?");
    if(conf){
        $.ajax({
            url: "/api/license",
            type: "delete",
            data: {id:id},
            beforeSend: function () {
                loadingBtn(button);
            },
            success: function (res) {
                if (res.success) {
                    datarow.ajax.reload();
                } else {
                    alert("Sorry data cannot updated");
                }
            },
        }).fail(function () {
            alert("Sorry something wrong while processing");
        }).always(function () {
            unloadingBtn(button);
        });
    }
}

$('.dpicker').daterangepicker({
    singleDatePicker: true,
    locale: {
        format: 'YYYY-MM-DD'
    }
});

function func_update(id){
    var setid = datarow.rows().data().filter(e=>e.id==id)[0];
    var frm = $("#frm_edit");
    frm.find("[name=id]").val(setid.id);
    frm.find("[name=name]").val(setid.name);
    frm.find("[name=qty]").val(setid.qty);
    frm.find("[name=productKey]").val(setid.productKey);
    frm.find("[name=licenseCategoryId]").val(setid.licenseCategoryId);
    frm.find("[name=licenseCategoryId]").trigger("change");
    frm.find("[name=manufacturerId]").val(setid.manufacturerId);
    frm.find("[name=manufacturerId]").trigger("change");
    frm.find("[name=supplierId]").val(setid.supplierId);
    frm.find("[name=supplierId]").trigger("change");
    frm.find("[name=licenseToName]").val(setid.licenseToName);
    frm.find("[name=licenseToEmail]").val(setid.licenseToEmail);
    frm.find("[name=password]").val(setid.password);
    frm.find("[name=prNumber]").val(setid.prNumber);
    frm.find("[name=purchaseCost]").val(setid.purchaseCost);
    frm.find("[name=purchaseDate]").val(setid.purchaseDateStr);
    frm.find("[name=expDate]").val(setid.expDateString);
    frm.find("[name=notes]").val(setid.notes);
    $("#downloadLicenseUrl").hide();
    if (setid.licenseFileId!=null){
        $("#downloadLicenseUrl").attr("href","/asset-management/license/download/file?id="+setid.licenseFileId);
        $("#downloadLicenseUrl").show();
    }
    $("#modal_edit").modal('show');
}


function loadingBtn(el){
    $(el).prop('disabled', true);
    $(el).find(".fa-circle-notch").show();
}
function unloadingBtn(el){
    $(el).prop('disabled', false);
    $(el).find(".fa-circle-notch").hide();
}