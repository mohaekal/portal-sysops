

var datatables = $('#datas').DataTable({
    "paging": true,
    "lengthChange": true,
    "searching": true,
    "ordering": false,
    "info": true,
    "autoWidth": false,
    "responsive": true,
    ajax: {
        url:'/api/dci/master-group-variable',
    },
    columns: [
        { data: 'id' },
        { data: 'groupNm' },
        { data: 'groupDesc' },
        { data: 'active' }
    ],
    columnDefs: [
        {
            targets: 0,
            visible: privilege.filter(e=>e.authority=='ROLE_MGMIDDCIGROUPVARIABLE').length>0,
            render: function (data, type, row, meta) {
                return `<button type="button" onclick="modaledit('`
                    +row.id+`','`
                    +row.groupNm+`','`
                    +row.groupDesc+`','`
                    +row.active+`'
                )" class="btn btn-outline-primary btn-sm" ><i class="fa fa-external-link-alt"></i> `+row.id+`</button>`;
            },
        },
        {
            targets: -1,
            render: function (data, type, row, meta) {
                return (row.active)?'<b style="color: green">Active</b>':'<b style="color: red">Inactive</b>';
            },
        }
    ],
});


function modaledit(id,gn,gd,active) {
    var setid = datatables.rows().data().filter(e=>e.id==id)[0];

    var frm = $("#frm_edit");

    frm.find("[name=id]").val(setid.id);
    frm.find("[name=groupNm]").val(setid.groupNm);
    frm.find("[name=groupDesc]").val(setid.groupDesc);
    frm.find("[name=active][value=1]").prop("checked",setid.active);
    frm.find("[name=active][value=0]").prop("checked",!setid.active);

    $("#modaledit").modal("show");
}



function add(){
    $.ajax({
        url: "/api/dci/master-group-variable/add",
        data: $("#frm_add").serialize(),
        beforeSend:function(){

            $("#modaladd").modal("hide");
            $("#loading").show();
            $("#datas").hide();
        },
        success:function(res){
            if(res.success){
                datatables.ajax.reload();
                alert("Successfully added, ID is "+res.data.id);
            }else{
                alert("Sorry data cannot updated")
            }
        }
    }).fail(function(){
        alert("Sorry something wrong while processing")
    })
        .always(function(){
            $("#loading").hide();
            $("#datas").show();
        })
}



function update(){
    $.ajax({
        url: "/api/dci/master-group-variable/update",
        data: $("#frm_edit").serialize(),
        beforeSend:function(){

            $("#modaledit").modal("hide");
            $("#loading").show();
            $("#datas").hide();
        },
        success:function(res){
            if(res.success){
                datatables.ajax.reload();
            }else{
                alert("Sorry data cannot updated")
            }
        }
    }).fail(function(){
        alert("Sorry something wrong while processing")
    })
        .always(function(){
            $("#loading").hide();
            $("#datas").show();
        })
}
