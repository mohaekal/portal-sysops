
var datatables =$("#datas").DataTable({
    paging: true,
    lengthChange: true,
    searching: true,
    ordering: false,
    info: true,
    autoWidth: false,
    responsive: true,
    ajax: {
        url: "/api/dci/master-device",
    },
    columns: [
        { data: "id" },
        { data: "deviceName" },
        { data: "deviceDesc" },
        { data: "macAddress" },
        { data: "ip" },
    ],
    columnDefs: [
        {
            targets: 0,
            visible: privilege.filter(e=>e.authority=='ROLE_MGMIDDCIDEVICE').length>0,
            render: function (data, type, row, meta) {
                return (
                    `<button type="button" onclick="modaledit('` +
                    row.id +
                    `','` +
                    row.deviceName +
                    `','` +
                    row.deviceDesc +
                    `','` +
                    row.macAddress +
                    `','` +
                    row.ip +
                    `'
          )" class="btn btn-outline-primary btn-sm" ><i class="fa fa-external-link-alt"></i> ` +
                    row.id +
                    `</button>`
                );
            },
        },
    ],
});


function modaledit(id,dn,dd,ma,ip) {
    $("#frm_edit").find("input[name=id]").val(id);
    $("#frm_edit").find("input[name=deviceName]").val(dn);
    $("#frm_edit").find("input[name=deviceDesc]").val(dd);
    $("#frm_edit").find("input[name=macAddress]").val(ma);
    $("#frm_edit").find("input[name=ip]").val(ip);
    $("#modaledit").modal("show");
}


function add(){
    $.ajax({
        url: "/api/dci/master-device/add",
        data: $("#frm_add").serialize(),
        beforeSend:function(){

            $("#modaladd").modal("hide");
            $("#loading").show();
            $("#datas").hide();
        },
        success:function(res){
            if(res.success){
                datatables.ajax.reload();
                alert("Successfully added, ID is "+res.data.id);
            }else{
                alert("Sorry data cannot updated")
            }
        }
    }).fail(function(){
        alert("Sorry something wrong while processing")
    })
        .always(function(){
            $("#loading").hide();
            $("#datas").show();
        })
}



function update(){
    $.ajax({
        url: "/api/dci/master-device/update",
        data: $("#frm_edit").serialize(),
        beforeSend:function(){

            $("#modaledit").modal("hide");
            $("#loading").show();
            $("#datas").hide();
        },
        success:function(res){
            if(res.success){
                datatables.ajax.reload();
            }else{
                alert("Sorry data cannot updated")
            }
        }
    }).fail(function(){
        alert("Sorry something wrong while processing")
    })
        .always(function(){
            $("#loading").hide();
            $("#datas").show();
        })
}
