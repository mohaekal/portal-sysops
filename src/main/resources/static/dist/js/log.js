
var datarow = $("#tbl_data").DataTable({
    ajax: {
        url: "/api/user-management/log",
    },
    paging: true,
    lengthChange: true,
    searching: true,
    info: true,
    ordering:false,
    autoWidth: false,
    responsive: true,

    columns: [
        { data: 'id' },
        { data: 'email' },
        { data: 'description' },
        { data: 'createDate' }
    ],
    columnDefs: [
        {
            targets: 2,
            render: function (data, type, row, meta) {
                return `<a href="#" onclick="seeChanges(`+row.id+`)">`+row.description+`</a>`;
            },
        },{
            targets: -1,
            render: function (data, type, row, meta) {
                return moment(row.createDate).calendar();
            },
        }],

    initComplete: function () {
        var thisTable = this;
        var rowFilter = $('<tr class="filter"></tr>').appendTo(
            $(this.api().table().header())
        );

        this.api()
            .columns()
            .every(function () {
                var column = this;
                switch (column.index()) {
                    case 1:
                    case 2:
                        var input = $(
                            `<input class="form-control form-control-sm form-filter " title="Input" data-col-index="` +
                            column.index() +
                            `"/>`
                        )
                            .appendTo($("<th>").appendTo(rowFilter))
                            .on("keyup change", function () {
                                var val = $(this).val();

                                column
                                    // .search(val ? "^" + val + "$" : "", true, false)
                                    .search(val)
                                    .draw();
                            });
                        break;
                    default:
                        var select = $("").appendTo($("<th>").appendTo(rowFilter));
                        break;
                }
            });

        var hideSearchColumnResponsive = function () {
            thisTable
                .api()
                .columns()
                .every(function () {
                    var column = this;
                    if (column.responsiveHidden()) {
                        $(".filter").find("th").eq(column.index()).show();
                    } else {
                        $(".filter").find("th").eq(column.index()).hide();
                    }
                });
        };

        // init on datatable load
        hideSearchColumnResponsive();
        // recheck on window resize
        window.onresize = hideSearchColumnResponsive;
    },
});

function seeChanges(id) {
    $("#before").empty();
    $("#after").empty();

    $("#before").html("<h1>Before</h1>");
    $("#after").html("<h1>After</h1>");

    var setid = datarow.rows().data().filter(e=>e.id==id)[0];
    let beforechanges = setid.beforechanges;

    const bf = jsonview.create(beforechanges);
    jsonview.render(bf, document.querySelector('#before'));
    jsonview.expand(bf);

    let afterchanges = setid.afterchanges;

    const tf = jsonview.create(afterchanges);
    jsonview.render(tf, document.querySelector('#after'));
    jsonview.expand(tf);

    $("#modal_different").modal("show");
}

