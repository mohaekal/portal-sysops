

var stompClient = null;


function connect() {
    var socket = new SockJS('/websocket-dbiot');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        // console.log('Connected: ' + frame);
        stompClient.subscribe('/data-stream/iot', function (response) {
            $.each(JSON.parse(response.body).data,function (index,value) {
                datatables.$("#varid"+value.variableId).text(value.value);
                datatables.$("#time"+value.variableId).text(value.convertedDatetime);
            });
        });
    });
}

$(document).ready(function(){
    connect();
})
