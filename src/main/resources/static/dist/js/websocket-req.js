

var stompClient = null;


function connect() {
    var socket = new SockJS('/websocket-request');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        // console.log('Connected: ' + frame);
        stompClient.subscribe('/request/task', function (response) {
            datarow.ajax.reload();
            dataclosedrow.ajax.reload();
            callreq();
            // $.each(JSON.parse(response.body).data,function (index,value) {
            //
            // });
        });
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    console.log("Disconnected");
}

function sendReq(jsonstring) {
    stompClient.send("/app/new-task", {}, jsonstring);
}

$(document).ready(function(){
    connect();
})
