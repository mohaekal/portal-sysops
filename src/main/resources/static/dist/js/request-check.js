
$(document).ready(function () {
    callreq();
})


function callreq() {
    $.ajax({
        url: "/api/request/vm-cctv-nms/notification",
        type:"get",
        beforeSend: function () {
        },
        success: function (res) {
            if (res.success ){
                try{
                    if(res.data.length > 0){
                        $("#menu_request").show();
                        $("#menu_request").text("!");
                        $("#menu_request_cctvnms").show();
                        $("#menu_request_cctvnms").text(res.data.length);
                    }else{
                        $("#menu_request").hide();
                        $("#menu_request_cctvnms").hide();
                    }
                }catch (e) {
                    $("#menu_request").hide();
                    $("#menu_request_cctvnms").hide();
                }
            }else {
                $("#menu_request").hide();
                $("#menu_request_cctvnms").hide();
            }
        },
    }).fail(function(){
        $("#menu_request").hide();
        $("#menu_request_cctvnms").hide();
    });

    $.ajax({
        url: "/api/request/vm-sysops/notification",
        type:"get",
        beforeSend: function () {
        },
        success: function (res) {
            if (res.success ){
                try{
                    if(res.data.length > 0){
                        $("#menu_request").show();
                        $("#menu_request").text("!");
                        $("#menu_request_sysops").show();
                        $("#menu_request_sysops").text(res.data.length);
                    }else{
                        $("#menu_request").hide();
                        $("#menu_request_sysops").hide();
                    }
                }catch (e) {
                    $("#menu_request").hide();
                    $("#menu_request_sysops").hide();
                }
            }else {
                $("#menu_request").hide();
                $("#menu_request_sysops").hide();
            }
        },
    }).fail(function(){
        $("#menu_request").hide();
        $("#menu_request_sysops").hide();
    });
}

setInterval(function () {
    callreq()
}, 60000);