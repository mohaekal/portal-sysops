

$(document).ready(function(){
    $("#frm_add").validate({
        rules: {
            name: {
                required: true
            },
            email: {
                required: true,
                email:true,
            },
            password:{
                required:true,
                minlength: 4
            },
            role:{
                required:true,
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    $("#frm_edit").validate({
        rules: {

            name: {
                required: true
            },
            email: {
                required: true,
                email:true,
            },
            role:{
                required:true,
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

});

$("#btn_submit_add").click(function(){
    if($("#frm_add").valid()){
        add();
    }
});

function add(button) {
    $.ajax({
        url: "/api/user-management/user",
        type:"post",
        data: $("#frm_add").serialize(),
        beforeSend: function () {
            loadingBtn(button);
        },
        success: function (res) {
            if (res.success) {
                datarow.ajax.reload();
                $("#frm_add").trigger("reset");
            } else {
                alert("Sorry data cannot updated");
            }
        },
    })
        .fail(function () {
            alert("Sorry something wrong while processing");
        })
        .always(function () {
            $("#modal_add").modal("hide");
            unloadingBtn(button);
        });
}

function edit(button,id){
    var setid = datarow.rows().data().filter(e=>e.id==id)[0];
    var frm = $("#frm_edit");
    frm.trigger("reset");
    frm.find("[name=id]").val(setid.id);
    frm.find("[name=name]").val(setid.name);
    frm.find("[name=email]").val(setid.email);
    frm.find("[name=role]").val(setid.role);
    frm.find("[name=role]").trigger("change");

    frm.find("[name=enable][value=1]").prop("checked",setid.enable);
    frm.find("[name=enable][value=0]").prop("checked",!setid.enable);
    $("#modal_edit").modal('show');


}

$("#btn_submit_edit").click(function (e) {
    if($("#frm_edit").valid()){
        submitEdit(this);
    }
})

function submitEdit(button){
    $.ajax({
        url: "/api/user-management/user",
        type:"put",
        data: $("#frm_edit").serialize(),
        beforeSend: function () {
            loadingBtn(button);
        },
        success: function (res) {
            if (res.success) {
                datarow.ajax.reload();
            } else {
                alert("Sorry data cannot updated");
            }
        },
    })
        .fail(function () {
            alert("Sorry something wrong while processing");
        })
        .always(function () {
            $("#loading").hide();
            $("#datas").show();
            $("#modal_edit").modal("hide");
            unloadingBtn(button);
        });
}


function deleteId(button,id) {
    var conf = confirm("Are you sure to delete this item ?");
    if(conf){
        $.ajax({
            url: "/api/user-management/user",
            type: "delete",
            data: {id:id},
            beforeSend: function () {
                loadingBtn(button);
            },
            success: function (res) {
                if (res.success) {
                    datarow.ajax.reload();
                } else {
                    alert("Sorry data cannot updated");
                }
            },
        }).fail(function () {
            alert("Sorry something wrong while processing");
        }).always(function () {
            unloadingBtn(button);
        });
    }

}



var datarow = $("#tbl_asset").DataTable({
    ajax: {
        url: "/api/user-management/user",
    },
    paging: true,
    lengthChange: true,
    searching: true,
    info: true,
    ordering:false,
    autoWidth: false,
    responsive: true,

    columns: [
        { data: 'id' },
        { data: 'name' },
        { data: 'email' },
        { data: 'enable' }
    ],
    columnDefs: [
        {
            targets: 0,
            render: function (data, type, row, meta) {
                return `<button type="button" class="btn btn-primary btn-sm " onclick="edit(this,'`+row.id+`')"><i class="fa fa-external-link-alt"><i class="fa fa-pencil"></i> Edit</i></button>
              <button onclick="deleteId(this,'`+row.id+`')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>`;
            },
        },{
            targets: -1,
            render: function (data, type, row, meta) {
                if (row.enable){
                    return '<b style="color: green">Yes</b>'
                }else {
                    return '<b style="color: red">No</b>'
                }
            },
        }],

    initComplete: function () {
        var thisTable = this;
        var rowFilter = $('<tr class="filter"></tr>').appendTo(
            $(this.api().table().header())
        );

        this.api()
            .columns()
            .every(function () {
                var column = this;
                switch (column.index()) {
                    case 1:
                    case 2:
                        var input = $(
                            `<input class="form-control form-control-sm form-filter " title="Input" data-col-index="` +
                            column.index() +
                            `"/>`
                        )
                            .appendTo($("<th>").appendTo(rowFilter))
                            .on("keyup change", function () {
                                var val = $(this).val();

                                column
                                    // .search(val ? "^" + val + "$" : "", true, false)
                                    .search(val)
                                    .draw();
                            });
                        break;
                    default:
                        var select = $("").appendTo($("<th>").appendTo(rowFilter));
                        break;
                }
            });

        var hideSearchColumnResponsive = function () {
            thisTable
                .api()
                .columns()
                .every(function () {
                    var column = this;
                    if (column.responsiveHidden()) {
                        $(".filter").find("th").eq(column.index()).show();
                    } else {
                        $(".filter").find("th").eq(column.index()).hide();
                    }
                });
        };

        // init on datatable load
        hideSearchColumnResponsive();
        // recheck on window resize
        window.onresize = hideSearchColumnResponsive;
    },
});



function loadingBtn(el){
    $(el).prop('disabled', true);
    $(el).find(".fa-circle-notch").show();
}
function unloadingBtn(el){
    $(el).prop('disabled', false);
    $(el).find(".fa-circle-notch").hide();
}