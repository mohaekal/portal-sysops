

var datatables = $('#datas').DataTable({
    "paging": true,
    "lengthChange": true,
    "searching": true,
    "ordering": false,
    "info": true,
    "autoWidth": false,
    "responsive": true,
    ajax: {
        url:'/api/h2/master-group-variable',
    },
    columns: [
        { data: 'id' },
        { data: 'groupNm' },
        { data: 'groupDesc' }
    ],
    columnDefs: [
        {
            targets: 0,
            visible: privilege.filter(e=>e.authority=='ROLE_MGMIDH2GROUPVARIABLE').length>0,
            render: function (data, type, row, meta) {
                return `<button type="button" onclick="modaledit('`
                    +row.id+`','`
                    +row.groupNm+`','`
                    +row.groupDesc+`'
            )" class="btn btn-outline-primary btn-sm" ><i class="fa fa-external-link-alt"></i> `+row.id+`</button>`;
            },
        },
    ],
});


function modaledit(id,gn,gd) {
    $("#frm_edit").find("input[name=id]").val(id);
    $("#frm_edit").find("input[name=groupNm]").val(gn);
    $("#frm_edit").find("input[name=groupDesc]").val(gd);
    $("#modaledit").modal("show");
}



function add(){
    $.ajax({
        url: "/api/h2/master-group-variable/add",
        data: $("#frm_add").serialize(),
        beforeSend:function(){

            $("#modaladd").modal("hide");
            $("#loading").show();
            $("#datas").hide();
        },
        success:function(res){
            if(res.success){
                datatables.ajax.reload();
                alert("Successfully added, ID is "+res.data.id);
            }else{
                alert("Sorry data cannot updated")
            }
        }
    }).fail(function(){
        alert("Sorry something wrong while processing")
    })
        .always(function(){
            $("#loading").hide();
            $("#datas").show();
        })
}



function update(){
    $.ajax({
        url: "/api/h2/master-group-variable/update",
        data: $("#frm_edit").serialize(),
        beforeSend:function(){

            $("#modaledit").modal("hide");
            $("#loading").show();
            $("#datas").hide();
        },
        success:function(res){
            if(res.success){
                datatables.ajax.reload();
            }else{
                alert("Sorry data cannot updated")
            }
        }
    }).fail(function(){
        alert("Sorry something wrong while processing")
    })
        .always(function(){
            $("#loading").hide();
            $("#datas").show();
        })
}
