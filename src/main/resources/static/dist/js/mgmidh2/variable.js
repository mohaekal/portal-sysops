
var devices;
var groupvariables;
var buildings;
var levels;
$.ajax({
    url: "/api/h2/select2-device",
    success: function (res) {
        devices = res.data;
        $(".select2device").select2({
            data: devices,
        });
    },
});

$.ajax({
    url: "/api/h2/select2-group-variable",
    success: function (res) {
        groupvariables = res.data;
        $(".select2groupvariable").select2({
            data: groupvariables,
        });
    },
});

$.ajax({
    url: "/api/h2/select2-building",
    success: function (res) {
        buildings = res.data;
        $(".select2building").select2({
            data: buildings,
        });
    },
});

$.ajax({
    url: "/api/h2/select2-level",
    success: function (res) {
        levels = res.data;
        $(".select2level").select2({
            data: levels,
        });
    },
});


<!-- initialize datatables -->

var datatables = $("#datas").DataTable({
    paging: true,
    lengthChange: true,
    searching: true,
    ordering: false,
    info: true,
    autoWidth: false,
    responsive: true,
    ajax: {
        url: "/api/h2/master-variable",
    },
    columns: [
        { data: "id" },
        { data: "variableName" },
        { data: "variableAttribute" },
        { data: "deviceName" },
        { data: "groupNm" },
        { data: "buildingId" },
        { data: "levelName" },
        { data: "macAddress" },
        { data: "type" },
        { data: "maxvaluee" },
        { data: "measurement" },
        { data: "lastUpdate" },
    ],
    columnDefs: [
        {
            targets: 0,
            visible: privilege.filter(e=>e.authority=='ROLE_MGMIDH2VARIABLE').length>0,
            render: function (data, type, row, meta) {
                return (
                    `<button type="button" onclick="modaledit('` +row.id+
                    `'
          )" class="btn btn-outline-primary btn-sm" ><i class="fa fa-external-link-alt"></i> ` +
                    row.id +
                    `</button>`
                );
            },
        },
    ],
    initComplete: function () {
        var thisTable = this;
        var rowFilter = $('<tr class="filter"></tr>').appendTo(
            $(this.api().table().header())
        );

        this.api()
            .columns()
            .every(function () {
                var column = this;

                switch (column.index()) {
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        var select = $(
                            `<select class="form-control form-control-sm form-filter kt-input" title="Select" data-col-index="` +
                            column.index() +
                            `">
                                      <option value="">Select</option></select>`
                        )
                            .appendTo($("<th>").appendTo(rowFilter))
                            .on("change", function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                column
                                    .search(val ? "^" + val + "$" : "", true, false)
                                    // .search(val)
                                    .draw();
                            });

                        column
                            .data()
                            .unique()
                            .sort()
                            .each(function (d, j) {
                                // console.log(select);
                                if (
                                    !select.find(`option[value="` + d + `"]`).val() &&
                                    $.trim(d) != ""
                                ) {
                                    select.append(
                                        '<option value="' + d + '">' + d + "</option>"
                                    );
                                }
                            });

                        break;
                    default:
                        var select = $("").appendTo($("<th>").appendTo(rowFilter));
                        break;
                }
            });

        var hideSearchColumnResponsive = function () {
            thisTable
                .api()
                .columns()
                .every(function () {
                    var column = this;
                    if (column.responsiveHidden()) {
                        $(".filter").find("th").eq(column.index()).show();
                    } else {
                        $(".filter").find("th").eq(column.index()).hide();
                    }
                });
        };

        // init on datatable load
        hideSearchColumnResponsive();
        // recheck on window resize
        window.onresize = hideSearchColumnResponsive;
    },
});


<!-- initialize current value for modal edit -->

function modaledit(id) {
    var obj = datatables.rows().data().filter(e=>e.id==id)[0];
    $("#frm_edit").find("input[name=id]").val(obj.id);
    $("#frm_edit").find("input[name=variableName]").val(obj.variableName);
    $("#frm_edit").find("input[name=variableAttribute]").val(obj.variableAttribute);
    $("#frm_edit").find("select[name=groupId]").val(obj.groupId);
    $("#frm_edit").find("select[name=groupId]").trigger("change.select2");
    $("#frm_edit").find("select[name=deviceId]").val(obj.deviceId);
    $("#frm_edit").find("select[name=deviceId]").trigger("change.select2");
    $("#frm_edit").find("select[name=buildingId]").val(obj.buildingIdx);
    $("#frm_edit").find("select[name=buildingId]").trigger("change.select2");
    $("#frm_edit").find("select[name=levelId]").val(obj.levelId);
    $("#frm_edit").find("input[name=measurement]").val(obj.measurement);
    $("#frm_edit").find("select[name=levelId]").trigger("change.select2");
    $("#frm_edit_ubidots").find("input[name=macAddress]").val(obj.macAddress);
    $("#frm_edit_ubidots").find("input[name=variableId]").val(obj.id);
    $("#frm_edit_ubidots").find("input[name=type]").val(obj.type);
    $("#frm_edit_ubidots").find("input[name=maxvaluee]").val(obj.maxvaluee);
    $("#modaledit").modal("show");
}


<!-- ajax for add new data -->

function add() {
    $.ajax({
        url: "/api/h2/master-variable/add",
        data: $("#frm_add").serialize(),
        beforeSend: function () {
            $("#modaladd").modal("hide");
            $("#loading").show();
            $("#datas").hide();
        },
        success: function (res) {
            if (res.success) {
                add_ubidots(res.data.id).then(function (resu) {
                    if (resu) {
                        datatables.ajax.reload();
                        alert("Successfully added, variable ID is " + res.data.id);
                    }
                });
            } else {
                alert("Sorry data cannot updated");
            }
        },
    })
        .fail(function () {
            alert("Sorry something wrong while processing");
        })
        .always(function () {
            $("#loading").hide();
            $("#datas").show();
        });
}


<!-- ajax for update data -->

function update() {
    $.ajax({
        url: "/api/h2/master-variable/update",
        data: $("#frm_edit").serialize(),
        beforeSend: function () {
            $("#modaledit").modal("hide");
            $("#loading").show();
            $("#datas").hide();
        },
        success: function (res) {
            if (res.success) {
                save_ubidots(res.data.id).then(function (resu) {
                    if (resu) {
                        datatables.ajax.reload();
                        alert("Successfully updated");
                    }
                });
            } else {
                alert("Sorry data cannot updated");
            }
        },
    })
        .fail(function () {
            alert("Sorry something wrong while processing");
        })
        .always(function () {
            $("#loading").hide();
            $("#datas").show();
        });
}


<!-- sub ajax for add ubidots -->

function add_ubidots(varid) {
    return $.ajax({
        url: "/api/h2/master-ubidots/save",
        data: $("#frm_add_ubidots").serialize() + "&variableId=" + varid,
        success: function (res) {
            return true;
        },
    }).fail(function () {
        alert("Sorry something wrong while processing ubidots data ");
        return false;
    });
}


<!-- sub ajax for update ubidots -->

function save_ubidots(varid) {
    return $.ajax({
        url: "/api/h2/master-ubidots/save",
        data: $("#frm_edit_ubidots").serialize(),
        success: function (res) {
            return true;
        },
    }).fail(function () {
        alert("Sorry something wrong while processing ubidots data ");
        return false;
    });
}

