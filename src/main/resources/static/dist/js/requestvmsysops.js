$(document).ready(function(){
    $("#frm_add").validate({
        rules: {
            licenseId: {
                required: true
            },
            socket: {
                required: true
            },
            cpu: {
                required: true
            },
            memory: {
                required: true
            },
            description: {
                required: true
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    $("#frm_edit").validate({
        rules: {
            licenseId: {
                required: true
            },
            socket: {
                required: true
            },
            cpu: {
                required: true
            },
            memory: {
                required: true
            },
            description: {
                required: true
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

})
function saveDraft(button) {
    $.ajax({
        url: "/api/request/vm-sysops/save-draft",
        type:"post",
        data: $("#frm_add").serialize(),
        beforeSend: function () {
            loadingBtn(button);
        },
        success: function (res) {
            if (res.success) {
                datarow.ajax.reload();
                callreq();
                $("#frm_add").trigger("reset");
            } else {
                alert("Sorry data cannot updated");
            }
        },
    })
        .fail(function () {
            alert("Sorry something wrong while processing");
        })
        .always(function () {
            $("#modal_add").modal("hide");
            unloadingBtn(button);
        });
}

function prepared(button) {
    if($("#frm_add").valid()){
        var c = confirm("You will not be able to change this form after this action. Are you sure to Prepared ? ");
        if (c){
            $.ajax({
                url: "/api/request/vm-sysops/prepared",
                type:"post",
                data: $("#frm_add").serialize(),
                beforeSend: function () {
                    loadingBtn(button);
                },
                success: function (res) {
                    if (res.success) {
                        datarow.ajax.reload();
                        callreq();
                        sendReq(res);
                        $("#frm_add").trigger("reset");
                    } else {
                        alert("Sorry data cannot updated");
                    }
                },
            })
                .fail(function () {
                    alert("Sorry something wrong while processing");
                })
                .always(function () {
                    $("#modal_add").modal("hide");
                    unloadingBtn(button);
                });
        }
    }

}

function saveDraftUpdate(button) {
    $.ajax({
        url: "/api/request/vm-sysops/save-draft",
        type:"put",
        data: $("#frm_edit").serialize(),
        beforeSend: function () {
            loadingBtn(button);
        },
        success: function (res) {
            if (res.success) {
                datarow.ajax.reload();
                callreq();
                $("#frm_edit").trigger("reset");
            } else {
                alert("Sorry data cannot updated");
            }
        },
    })
        .fail(function () {
            alert("Sorry something wrong while processing");
        })
        .always(function () {
            $("#modal_edit").modal("hide");
            unloadingBtn(button);
        });
}

function preparedUpdate(button) {
    if($("#frm_edit").valid()){
        var c = confirm("You will not be able to change this form after this action. Are you sure to Prepared ? ");
        if (c){
            $.ajax({
                url: "/api/request/vm-sysops/prepared",
                type:"put",
                data: $("#frm_edit").serialize(),
                beforeSend: function () {
                    loadingBtn(button);
                },
                success: function (res) {
                    if (res.success) {
                        datarow.ajax.reload();
                        callreq();
                        sendReq(res);
                        $("#frm_edit").trigger("reset");
                    } else {
                        alert("Sorry data cannot updated");
                    }
                },
            })
                .fail(function () {
                    alert("Sorry something wrong while processing");
                })
                .always(function () {
                    $("#modal_edit").modal("hide");
                    unloadingBtn(button);
                });
        }
    }

}

function approvement(button,isapprove) {
    var url = "/api/request/vm-sysops/reject-request";
    if (isapprove){
        url = "/api/request/vm-sysops/approve-request";
    }
    $.ajax({
        url: url,
        type:"put",
        data: {approvementId:$("#frm_edit").find("[name=approvementId]").val(),reason:$("#frm_reason").find("[name=reason]").val()},
        beforeSend: function () {
            loadingBtn(button);
        },
        success: function (res) {
            if (res.success) {
                datarow.ajax.reload();
                callreq();
                sendReq(res);
                $("#frm_edit").trigger("reset");
            } else {
                alert("Sorry data cannot updated");
            }
        },
    })
        .fail(function () {
            alert("Sorry something wrong while processing");
        })
        .always(function () {
            $("#modal_edit").modal("hide");
            $("#modal_reason").modal("hide");

            unloadingBtn(button);
        });
}

function approveBtnRow(button,approvementId){
    $.ajax({
        url: "/api/request/vm-sysops/approve-request",
        type:"put",
        data: {approvementId:approvementId},
        beforeSend: function () {
            loadingBtn(button);
        },
        success: function (res) {
            if (res.success) {
                datarow.ajax.reload();
                callreq();
                sendReq(res);
                $("#frm_edit").trigger("reset");
            } else {
                alert("Sorry data cannot updated");
            }
        },
    })
        .fail(function () {
            alert("Sorry something wrong while processing");
        })
        .always(function () {
            $("#modal_edit").modal("hide");
            $("#modal_reason").modal("hide");
            $("#modal_terminate").modal("hide");
            unloadingBtn(button);
        });
}

function rejectBtnRow(button){
    $.ajax({
        url: "/api/request/vm-sysops/reject-request",
        type:"put",
        data: {approvementId:$("#frm_reason").find("[name=approvementId]").val(),reason:$("#frm_reason").find("[name=reason]").val()},
        beforeSend: function () {
            loadingBtn(button);
        },
        success: function (res) {
            if (res.success) {
                datarow.ajax.reload();
                callreq();
                sendReq(res);
                $("#frm_edit").trigger("reset");
            } else {
                alert("Sorry data cannot updated");
            }
        },
    })
        .fail(function () {
            alert("Sorry something wrong while processing");
        })
        .always(function () {
            $("#modal_edit").modal("hide");
            $("#modal_reason").modal("hide");
            $("#modal_terminate").modal("hide");
            unloadingBtn(button);
        });
}

function approveEvidenceBtnRow(button,approvementId){
    $.ajax({
        url: "/api/request/vm-sysops/approve-evidence",
        type:"put",
        data: {approvementId:approvementId},
        beforeSend: function () {
            loadingBtn(button);
        },
        success: function (res) {
            if (res.success) {
                datarow.ajax.reload();
                callreq();
                sendReq(res);
                $("#frm_reason_evidence").trigger("reset");
            } else {
                alert("Sorry data cannot updated");
            }
        },
    })
        .fail(function () {
            alert("Sorry something wrong while processing");
        })
        .always(function () {
            unloadingBtn(button);
        });
}

function rejectEvidenceBtnRow(button){
    $.ajax({
        url: "/api/request/vm-sysops/reject-evidence",
        type:"put",
        data: {approvementId:$("#frm_reason_evidence").find("[name=approvementId]").val(),reason:$("#frm_reason_evidence").find("[name=reason]").val()},
        beforeSend: function () {
            loadingBtn(button);
        },
        success: function (res) {
            if (res.success) {
                datarow.ajax.reload();
                callreq();
                sendReq(res);
                $("#frm_reason_evidence").trigger("reset");
            } else {
                alert("Sorry data cannot updated");
            }
        },
    })
        .fail(function () {
            alert("Sorry something wrong while processing");
        })
        .always(function () {
            $("#modal_edit").modal("hide");
            $("#modal_evidence").modal("hide");
            $("#modal_reason_evidence").modal("hide");

            unloadingBtn(button);
        });
}

function showreason(approvementId) {
    $("#frm_reason").find("[name=approvementId]").val(approvementId);
    $("#modal_reason").modal("show");
}

function showreasonevidence(approvementId) {
    $("#frm_reason_evidence").find("[name=approvementId]").val(approvementId);
    $("#modal_reason_evidence").modal("show");
}

function showevidencefile(id) {
    $("#btnModalDownloadEvidence").removeAttr("href");
    $("#btnModalDownloadEvidence").attr("href","/request/vm-sysops/download/file?id="+id)

    $("#iframeevidence").removeAttr("src");
    $("#iframeevidence").attr("src","/request/vm-sysops/show/file?id="+id);
    // document.getElementById("iframeevidence").contentDocument.location.reload();
    var iframe = document.getElementById("iframeevidence");
    iframe.src = iframe.src;
    $("#modal_file_evidence").modal("show");
}

function submitevidence(button) {

    var formd = new FormData(document.querySelector("#frm_evidence"));

    $.ajax({
        url: "/api/request/vm-sysops/evidence",
        type:"put",
        data: formd,
        processData: false,
        contentType: false,
        beforeSend: function () {
            loadingBtn(button);
        },
        success: function (res) {
            if (res.success) {
                datarow.ajax.reload();
                callreq();
                sendReq(res);
                $("#frm_evidence").trigger("reset");
            } else {
                alert("Sorry data cannot updated");
            }
        },
    })
        .fail(function () {
            alert("Sorry something wrong while processing");
        })
        .always(function () {
            $("#modal_evidence").modal("hide");

            unloadingBtn(button);
        });
}

function approvementEvidence(button,isapprove) {
    var url = "/api/request/vm-sysops/reject-evidence";
    if (isapprove){
        url = "/api/request/vm-sysops/approve-evidence";
    }
    $.ajax({
        url: url,
        type:"put",
        data: {approvementId:$("#frm_evidence").find("[name=approvementId]").val(),reason:$("#frm_reason_evidence").find("[name=reason]").val()},
        beforeSend: function () {
            loadingBtn(button);
        },
        success: function (res) {
            if (res.success) {
                datarow.ajax.reload();
                callreq();
                sendReq(res);
                $("#frm_evidence").trigger("reset");
            } else {
                alert("Sorry data cannot updated");
            }
        },
    })
        .fail(function () {
            alert("Sorry something wrong while processing");
        })
        .always(function () {
            $("#modal_evidence").modal("hide");
            $("#modal_reason_evidence").modal("hide");

            unloadingBtn(button);
        });
}

function showdetail(button,reqId) {

    var setid = datarow.rows().data().filter(e=>e.requestId==reqId)[0];
    var frm = $("#frm_edit");

    if (setid.requestType=='MODIFICATION'){
        frm.find("[name=name]").parent().show();
    }else {
        frm.find("[name=name]").parent().hide();
    }

    frm.find("[name=id]").val(setid.id);
    frm.find("[name=requestId]").val(setid.requestId);
    frm.find("[name=name]").val(setid.name);
    frm.find("[name=socket]").val(setid.socket);
    frm.find("[name=cpu]").val(setid.cpu);
    frm.find("[name=memory]").val(setid.memory);
    frm.find("[name=storage]").val(setid.storage);
    frm.find("[name=licenseId]").val(setid.licenseId);
    frm.find("[name=licenseId]").trigger('change.select2');
    frm.find("[name=description]").val(setid.description);
    frm.find("[name=remark]").val(setid.remark);
    frm.find("[name=approvementId]").val(setid.approvementId);

    $("#frm_reason").find("[name=approvementId]").val(setid.approvementId);


    switch(setid.progressid){
        case 1:

            $(".btnaction").hide();
            $("#btnSaveDraft").show();
            $("#btnPrepared").show();
            break;

        case 2:
            $(".btnaction").hide();
            $("#btnApprove").show();
            $("#btnReject").show();
            break;
        default:
            $(".btnaction").hide();
    }

    $("#modal_edit").modal('show');
}

function showUpgrade(button,reqId) {

    var setid = datarow.rows().data().filter(e=>e.requestId==reqId)[0];
    var frm = $("#frm_upgrade");
    frm.find("[name=id]").val(setid.id);
    frm.find("[name=requestId]").val(setid.requestId);
    frm.find("[name=name]").val(setid.name);
    frm.find("[name=socket]").val(setid.socket);
    frm.find("[name=cpu]").val(setid.cpu);
    frm.find("[name=memory]").val(setid.memory);
    frm.find("[name=storage]").val(setid.storage);
    frm.find("[name=remark]").val(setid.remark);
    $("#frm_reason").find("[name=approvementId]").val(setid.approvementId);

    switch(setid.progressid){
        case 1:

            $(".btnaction").hide();
            $("#btnSaveDraft").show();
            $("#btnPrepared").show();
            break;

        case 2:
            $(".btnaction").hide();
            $("#btnApprove").show();
            $("#btnReject").show();
            break;
        default:
            $(".btnaction").hide();
    }

    $("#modal_upgrade").modal('show');
}

function detailevidence(button,reqId) {
    if (datarow.rows().data().filter(e=>e.requestId==reqId).length>0){
        var setid = datarow.rows().data().filter(e=>e.requestId==reqId)[0];
    }else{
        var setid = dataclosedrow.rows().data().filter(e=>e.requestId==reqId)[0];
    }
    var frm = $("#frm_evidence");
    frm.find("[name=id]").val(setid.id);
    frm.find("[name=requestId]").val(setid.requestId);
    frm.find("[name=requestType]").val(setid.requestType);
    frm.find("[name=approvementId]").val(setid.approvementId);
    frm.find("[name=socket]").val(setid.socket);
    frm.find("[name=name]").val(setid.name);
    frm.find("[name=ip]").val(setid.ip);
    frm.find("[name=ips]").val(setid.ips);
    frm.find("[name=ips]").trigger("change");
    frm.find("[name=cpu]").val(setid.cpu);
    frm.find("[name=memory]").val(setid.memory);
    frm.find("[name=storage]").val(setid.storage);
    frm.find("[name=licenseId]").val(setid.licenseId);
    frm.find("[name=licenseId]").trigger('change.select2');
    frm.find("[name=assetId]").val(setid.assetId);
    frm.find("[name=assetId]").trigger('change.select2');
    frm.find("[name=state]").val(setid.state);
    frm.find("[name=state]").trigger('change.select2');
    frm.find("[name=description]").val(setid.description);
    frm.find("[name=backup][value=1]").prop("checked",setid.backup);
    frm.find("[name=backup][value=0]").prop("checked",!setid.backup);
    frm.find("[name=highAvailbility][value=1]").prop("checked",setid.highAvailbility);
    frm.find("[name=highAvailbility][value=0]").prop("checked",!setid.highAvailbility);
    frm.find("[name=priority]").val(setid.priority);
    frm.find("[name=priority]").trigger('change.select2');
    frm.find("[name=avSpanPort][value=1]").prop("checked",setid.avSpanPort);
    frm.find("[name=avSpanPort][value=0]").prop("checked",!setid.avSpanPort);
    frm.find("[name=avOssec][value=1]").prop("checked",setid.avOssec);
    frm.find("[name=avOssec][value=0]").prop("checked",!setid.avOssec);
    frm.find("[name=antivirus]").val(setid.antivirus);
    frm.find("[name=antivirusInstalled][value=1]").prop("checked",setid.antivirusInstalled);
    frm.find("[name=antivirusInstalled][value=0]").prop("checked",!setid.antivirusInstalled);
    frm.find("[name=prtgIcmp][value=1]").prop("checked",setid.prtgIcmp);
    frm.find("[name=prtgIcmp][value=0]").prop("checked",!setid.prtgIcmp);
    frm.find("[name=prtgService][value=1]").prop("checked",setid.prtgService);
    frm.find("[name=prtgService][value=0]").prop("checked",!setid.prtgService);
    frm.find("[name=prtgServiceRemark]").val(setid.prtgServiceRemark);
    frm.find("[name=grafana][value=1]").prop("checked",setid.grafana);
    frm.find("[name=grafana][value=0]").prop("checked",!setid.grafana);
    frm.find("[name=evidenceRemark]").val(setid.evidenceRemark);


    $("#frm_reason_evidence").find("[name=approvementId]").val(setid.approvementId);

    $.ajax({
        type: 'GET',
        url: '/api/ips',
        data: {vmid: setid.id },
        success:function(response){
            $('#editIps').val(null);
            $('#editIps').val(null);
            $('#editIps').trigger('change');
            // create the option and append to Select2
            response.data.forEach(function (el,i) {
                console.log(el)
                var option = new Option(el.ip, el.id, true, true);
                $("#editIps").append(option).trigger('change');
                // manually trigger the `select2:select` event
                $("#editIps").trigger({
                    type: 'select2:select',
                    params: {
                        data: el
                    }
                });
            })

            $('.selectipsModalEdit').select2({
                ajax: {
                    url: '/api/ips/select2',
                    type:"get",
                    data: {vmid:setid.id},
                    processResults: function (response) {
                        return {
                            results: response.data
                        };
                    }
                }
            });
        }
    });

    switch(setid.progressid){
        case 1:
        case 2:
            break;
        case 3:

            $(".btnaction").hide();
            $("#btnSaveEvidence").show();
            $(".evidenceform").hide();
            $("#fileevidenceform").show();
            $(".noteslimitfile").show();
            break;

        case 4:
            $(".btnaction").hide();
            $("#btnApproveEvidence").show();
            $("#btnRejectEvidence").show();
            $(".evidenceform").hide();
            $("#evidencebutton").empty()
            if (setid.evidenceFiles.length>0){

                setid.evidenceFiles.forEach(function (el, i) {
                    var c = i+1;
                    $("#evidencebutton").append(`<button type="button" onclick="showevidencefile(`+el+`)" class="btn btn-outline-dark dark btn-flat"> Evidence `+c+`</button>`)
                });
                $("#evidencebutton").show();
            }
            break;
        case 5:
            $(".btnaction").hide();
            $(".evidenceformgroup").hide();
            $("#evidencebutton").empty()
            break;

        default:
            $(".btnaction").hide();

    }
    $("#modal_evidence").modal('show');

}


function showTerminate(button,reqId) {
    if (datarow.rows().data().filter(e=>e.requestId==reqId).length>0){
        var setid = datarow.rows().data().filter(e=>e.requestId==reqId)[0];
    }else{
        var setid = dataclosedrow.rows().data().filter(e=>e.requestId==reqId)[0];
    }
    var frm = $("#frm_terminate");
    frm.find("[name=id]").val(setid.id);
    frm.find("[name=requestId]").val(setid.requestId);
    frm.find("[name=requestType]").val(setid.requestType);
    frm.find("[name=approvementId]").val(setid.approvementId);
    frm.find("[name=socket]").val(setid.socket);
    frm.find("[name=name]").val(setid.name);
    frm.find("[name=ip]").val(setid.ip);
    frm.find("[name=ips]").val(setid.ips);
    frm.find("[name=ips]").trigger("change");
    frm.find("[name=cpu]").val(setid.cpu);
    frm.find("[name=memory]").val(setid.memory);
    frm.find("[name=storage]").val(setid.storage);
    frm.find("[name=licenseId]").val(setid.licenseId);
    frm.find("[name=licenseId]").trigger('change.select2');
    frm.find("[name=assetId]").val(setid.assetId);
    frm.find("[name=assetId]").trigger('change.select2');
    frm.find("[name=state]").val(setid.state);
    frm.find("[name=state]").trigger('change.select2');
    frm.find("[name=description]").val(setid.description);
    frm.find("[name=backup][value=1]").prop("checked",setid.backup);
    frm.find("[name=backup][value=0]").prop("checked",!setid.backup);
    frm.find("[name=highAvailbility][value=1]").prop("checked",setid.highAvailbility);
    frm.find("[name=highAvailbility][value=0]").prop("checked",!setid.highAvailbility);
    frm.find("[name=priority]").val(setid.priority);
    frm.find("[name=priority]").trigger('change.select2');
    frm.find("[name=avSpanPort][value=1]").prop("checked",setid.avSpanPort);
    frm.find("[name=avSpanPort][value=0]").prop("checked",!setid.avSpanPort);
    frm.find("[name=avOssec][value=1]").prop("checked",setid.avOssec);
    frm.find("[name=avOssec][value=0]").prop("checked",!setid.avOssec);
    frm.find("[name=antivirus]").val(setid.antivirus);
    frm.find("[name=antivirusInstalled][value=1]").prop("checked",setid.antivirusInstalled);
    frm.find("[name=antivirusInstalled][value=0]").prop("checked",!setid.antivirusInstalled);
    frm.find("[name=prtgIcmp][value=1]").prop("checked",setid.prtgIcmp);
    frm.find("[name=prtgIcmp][value=0]").prop("checked",!setid.prtgIcmp);
    frm.find("[name=prtgService][value=1]").prop("checked",setid.prtgService);
    frm.find("[name=prtgService][value=0]").prop("checked",!setid.prtgService);
    frm.find("[name=prtgServiceRemark]").val(setid.prtgServiceRemark);
    frm.find("[name=grafana][value=1]").prop("checked",setid.grafana);
    frm.find("[name=grafana][value=0]").prop("checked",!setid.grafana);
    frm.find("[name=remark]").val(setid.remark);
    $("#frm_reason").find("[name=approvementId]").val(setid.approvementId);

    $.ajax({
        type: 'GET',
        url: '/api/ips',
        data: {vmid: setid.id },
        success:function(response){
            $('#TerminateeditIps').val(null);
            $('#TerminateeditIps').val(null);
            $('#TerminateeditIps').trigger('change');
            // create the option and append to Select2
            response.data.forEach(function (el,i) {
                console.log(el)
                var option = new Option(el.ip, el.id, true, true);
                $("#TerminateeditIps").append(option).trigger('change');
                // manually trigger the `select2:select` event
                $("#TerminateeditIps").trigger({
                    type: 'select2:select',
                    params: {
                        data: el
                    }
                });
            })

            $('.selectipsModalEdit').select2({
                ajax: {
                    url: '/api/ips/select2',
                    type:"get",
                    data: {vmid:setid.id},
                    processResults: function (response) {
                        return {
                            results: response.data
                        };
                    }
                }
            });
        }
    });

    switch(setid.progressid){
        case 1:

            $(".btnaction").hide();
            $("#btnPreparedTerminate").show();
            break;

        case 2:
            $(".btnaction").hide();
            $("#btnApproveTerminate").show();
            $("#btnRejectTerminate").show();
            break;
        default:
            $(".btnaction").hide();
    }

    $("#modal_terminate").modal('show');

}

function submitTerminate(button){
    var id = $("#frm_terminate").find("[name=id]").val();
    var note = $("#frm_terminate").find("[name=remark]").val();
    var nt = "";
    if(note){
        nt="&remark="+note;
    }

    $.ajax({
        url: "/api/request/vm-sysops/terminate/prepared",
        type:"put",
        data: $("#frm_terminate").serialize(),
        beforeSend: function () {
            loadingBtn(button);
        },
        success: function (res) {
            if (res.success) {
                datarow.ajax.reload();

                callreq();
                sendReq(res);
            } else {
                alert(res.data);
            }
        },
    })
        .fail(function () {
            alert("Sorry something wrong while processing");
        })
        .always(function () {
            $("#modal_terminate").modal("hide");
            unloadingBtn(button);
        });
}

function deleteid(button,id) {
    var c = confirm("Are you sure to remove this item ? ");
    if (c){
        $.ajax({
            url: "/api/request/vm-sysops",
            type:"delete",
            data: {requestid:id},
            beforeSend: function () {
                loadingBtn(button);
            },
            success: function (res) {
                if (res.success) {
                    datarow.ajax.reload();
                    callreq();
                    sendReq(res);
                    $("#frm_add").trigger("reset");
                } else {
                    alert("Sorry data cannot updated");
                }
            },
        })
            .fail(function () {
                alert("Sorry something wrong while processing");
            })
            .always(function () {
                $("#modal_add").modal("hide");
                unloadingBtn(button);
            });
    }

}

function showhistory(button,reqId) {



    $.ajax({
        url: "/api/request/vm-sysops/history",
        type:"get",
        data: {id:reqId },
        beforeSend: function () {
            $("#historyy").empty();
            loadingBtn(button);
        },
        success: function (res) {
            if (res.success){
                var a=``;
                res.data.forEach(function(el,i) {

                    var approvement = '';
                    if (el.approvement.length > 0){
                        var fillappr = ''
                        el.approvement.forEach(function (ell,ii) {
                            var isapprove = '<strong style="color: #ff0000">Reject</strong> - '+ell.reason;
                            if(ell.approve){
                                isapprove = '<strong style="color: green">Approve</strong>';
                            }
                            if (ell.approve==null){
                                isapprove = ``
                            }

                            var datess = (ell.updateDate!=null)?' <i>'+moment(ell.updateDate).calendar()+'</i>':'';
                            fillappr=fillappr+`<div>`+ell.userString+` : `+isapprove+datess+` </div>`
                        })
                        approvement = `<div class="timeline-body">`+fillappr+`</div>`;
                    }

                    a=a+`<div>
                            <i class="`+iconforprogress(el.progressId)+`"></i>
                            <div class="timeline-item">
                                <span class="time"><i class="fas fa-clock"></i> `+moment(el.progressTime).calendar()+`</span>
                                <h3 class="timeline-header">`+el.progressName+`</h3>
                                `+approvement+`
                            </div>
                        </div>`;
                })
                a=a+`<div>
                        <i class="fas fa-clock bg-gray"></i>
                    </div>`;
            }else {
                alert("Sorry data is null");
            }
            $("#historyy").html(a)
        },
    })
        .fail(function () {
            alert("Sorry something wrong while processing");
        })
        .always(function () {
            $("#modal_edit").modal("hide");
            unloadingBtn(button);
        });

    $("#modal_history").modal("show");
}

function submitUpgrade(button){
    $.ajax({
        url: "/api/request/vm-sysops/upgrade/prepared",
        type:"put",
        data: $("#frm_upgrade").serialize(),
        beforeSend: function () {
            loadingBtn(button);
        },
        success: function (res) {
            if (res.success) {
                datarow.ajax.reload();
                callreq();
                sendReq(res);
            } else {
                alert("Sorry data cannot updated");
            }
        },
    })
        .fail(function () {
            alert("Sorry something wrong while processing");
        })
        .always(function () {
            $("#modal_upgrade").modal("hide");
            unloadingBtn(button);
        });
}

function iconforprogress(id) {
    switch (id) {
        case 1 :
            return `fas fa-edit bg-white`;
            break;
        case 2 :
            return `fas fa-paper-plane bg-yellow`;
            break;
        case 3 :
            return `fas fa-clipboard-list bg-blue`;
            break;
        case 4 :
            return `fas fa-clipboard-check bg-info`;
        case 5 :
            return `fas fa-check-circle bg-green`;
            break;
    }
}

var datarow = $("#tbl_data").DataTable({
    ajax: {
        type:"get",
        url: "/api/request/vm-sysops",
    },
    paging: true,
    lengthChange: true,
    searching: true,
    info: true,
    ordering:true,
    "order": [[ 0, "desc" ]],
    autoWidth: false,
    responsive: true,
    processing:true,

    columns: [
        { data: 'requestId' },
        { data: 'requestDate' },
        { data: 'progress' },
        { data: 'license' },
        { data: 'description' },
        { data: 'socket' },
        { data: 'cpu' },
        { data: 'memory' },
        { data: 'storage' },
        { data: 'requestBy' },
        { data: 'requestType' },
        { data: 'requestId' }
    ],
    columnDefs: [
        {
            targets: 1,
            "orderable":false,
            render:function (data,type,row,meta) {
                return moment(row.requestDate).format("YYYY-MM-DD HH:mm")
            }
        },
        {
            targets: 11,
            "orderable": false,
            render: function (data, type, row, meta) {
                var btnDelete = ' <button onclick="deleteid(this,'+row.requestId+')" class="btn btn-danger btn-sm" title="Remove"><i class="fa fa-trash"></i></button>';
                var btnViewDetail = ` <button type="button" class="btn btn-primary btn-sm " onclick="showdetail(this,'`+row.requestId+`')" title="View Detail"><i class="fa fa-eye"></i></button> `;
                var btnViewDetailEvidence = ` <button type="button" class="btn btn-primary btn-sm " onclick="detailevidence(this,'`+row.requestId+`')" title="View Detail"><i class="fa fa-eye"></i></button> `;
                var btnHistory = ` <button type="button" class="btn btn-outline-info btn-sm " onclick="showhistory(this,'`+row.requestId+`')" title="History"><i class="fas fa-history"></i></button> `;
                var btnApprove = ' <button onclick="approveBtnRow(this,'+row.approvementId+')" class="btn btn-outline-success btn-flat btn-sm" title="Approve"><i class="fa fa-check"></i></button>';
                var btnReject = ` <button type="button" class="btn btn-outline-danger btn-flat btn-sm " onclick="showreason(`+row.approvementId+`)" title="Reject"><i class="fa fa-times"></i></button> `;
                var btnApproveEvidence = ' <button onclick="approveEvidenceBtnRow(this,'+row.approvementId+')" class="btn btn-outline-success btn-flat btn-sm" title="Approve"><i class="fa fa-check"></i></button>';
                var btnRejectEvidence = ` <button type="button" class="btn btn-outline-danger btn-flat btn-sm " onclick="showreasonevidence(`+row.approvementId+`)" title="Reject"><i class="fa fa-times"></i></button> `;
                var btnModalUpgrade = ` <button type="button" class="btn btn-primary btn-sm " onclick="showUpgrade(this,'`+row.requestId+`')" title="View Detail"><i class="fa fa-eye"></i></button> `;
                var btnModalTerminate = ` <button type="button" class="btn btn-primary btn-sm " onclick="showTerminate(this,'`+row.requestId+`')" title="View Detail"><i class="fa fa-eye"></i></button> `;



                var detailbtn = (row.requestType=="TERMINATE")?btnModalTerminate:btnViewDetail;

                switch (row.progressid) {
                    case 1:
                        if(row.requestType=='MODIFICATION'){
                            return btnModalUpgrade+btnDelete+btnHistory;
                        }else if(row.requestType=='TERMINATE'){
                            return btnModalTerminate+btnDelete+btnHistory;
                        }else {
                            return btnViewDetail+btnDelete+btnHistory;
                        }
                        break;
                    case 2:
                        if (row.approvementId!=null){
                            return detailbtn+btnApprove+btnReject+btnHistory;
                        }else {
                            return detailbtn+btnHistory;
                        }
                        break;
                    case 3:

                        if (privilege.filter(e=>e.authority=='ROLE_VMSYSOPS').length>0){

                            return btnViewDetailEvidence+btnHistory;
                        }else {

                            return detailbtn+btnHistory;
                        }
                        break;
                    case 4:

                        if ( row.approvementId!=null){
                            return btnViewDetailEvidence+btnApproveEvidence+btnRejectEvidence+btnHistory;
                        }else {
                            return btnViewDetailEvidence+btnHistory;
                        }
                        break;
                }
            },
        }]
});

var dataclosedrow = $("#tbl_closed_data").DataTable({
    ajax: {
        type:"get",
        url: "/api/request/vm-sysops/closed",
    },
    paging: true,
    lengthChange: true,
    searching: true,
    info: true,
    ordering:true,
    "order": [[ 0, "desc" ]],
    autoWidth: false,
    responsive: true,
    processing:true,

    columns: [
        { data: 'requestId' },
        { data: 'requestDate' },
        { data: 'progress' },
        { data: 'license' },
        { data: 'description' },
        { data: 'socket' },
        { data: 'cpu' },
        { data: 'memory' },
        { data: 'storage' },
        { data: 'requestBy' },
        { data: 'requestType' },
        { data: 'requestId' }
    ],
    columnDefs: [

        {
            targets: 1,
            "orderable":false,
            render:function (data,type,row,meta) {
                return moment(row.requestDate).format("YYYY-MM-DD HH:mm")
            }
        },
        {
            targets: 11,
            "orderable": false,
            render: function (data, type, row, meta) {
                var btnHistory = `<button type="button" class="btn btn-outline-info btn-sm " onclick="showhistory(this,'`+row.requestId+`')" title="History"><i class="fas fa-history"></i></button> `;
                var btnViewDetail= `<button type="button" class="btn btn-primary btn-sm " onclick="detailevidence(this,'`+row.requestId+`')" title="View Detail"><i class="fa fa-eye"></i></button> `;
                return btnViewDetail+btnHistory;
            },
        }]
});

$('.selectips').select2({
    ajax: {
        url: '/api/ips/select2',
        type:"get",
        processResults: function (response) {
            return {
                results: response.data
            };
        }
    }
});

$('.selectipsModalEdit').select2();

function addNewIP() {
    $("frm_add_ip").trigger("reset")
    $("#modal_add_ip").modal("show");
}

function submitAddIP(button) {

    var frm = $("#frm_add_ip");
    if(frm.valid()){
        $.ajax({
            url: "/api/ips",
            type:"post",
            data: frm.serialize(),
            beforeSend: function () {
                loadingBtn(button);
            },
            success: function (res) {
                if (res.success) {

                } else {
                    alert("Sorry data cannot updated");
                }
            },
        }).fail(function () {
            alert("Sorry something wrong while processing");
        }).always(function () {
            $("#modal_add_ip").modal("hide");
            unloadingBtn(button);
        });

    }
}