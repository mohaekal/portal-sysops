$(document).ready(function(){
    $("#frm_add").validate({
        rules: {
            name: {
                required: true,
            },
            accessoryCategoryId: {
                required: true,
            },
            supplierId: {
                required: true,
            },
            manufacturerId: {
                required: true,
            },accessoryStatusId: {
                required: true,
            },supplierId: {
                required: true,
            },qty: {
                required: true,
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    $("#frm_edit").validate({
        rules: {
            name: {
                required: true,
            },
            accessoryCategoryId: {
                required: true,
            },
            supplierId: {
                required: true,
            },
            manufacturerId: {
                required: true,
            },accessoryStatusId: {
                required: true,
            },supplierId: {
                required: true,
            },qty: {
                required: true,
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

})
function submitAdd(button){
    if($("#frm_add").valid()){$.ajax({
        url: "/api/accessory",
        type:"post",
        data: $("#frm_add").serialize(),
        beforeSend: function () {
            loadingBtn(button);
        },
        success: function (res) {
            if (res.success) {
                datarow.ajax.reload();
                $("#frm_add").trigger("reset");
            } else {
                alert("Sorry data cannot updated");
            }
        },
    }).fail(function () {
        alert("Sorry something wrong while processing");
    }).always(function () {
        $("#loading").hide();
        $("#modal_add").modal("hide");
        unloadingBtn(button);
    });
    }
}

function submitUpdate(button){
    if($("#frm_edit").valid()){
        $.ajax({
            url: "/api/accessory",
            type:"put",
            data: $("#frm_edit").serialize(),
            beforeSend: function () {
                loadingBtn(button);
            },
            success: function (res) {
                if (res.success) {
                    datarow.ajax.reload();
                } else {
                    alert("Sorry data cannot updated");
                }
            },
        })
            .fail(function () {
                alert("Sorry something wrong while processing");
            })
            .always(function () {
                $("#loading").hide();
                $("#modal_edit").modal("hide");
                unloadingBtn(button);
            });
    }
}
function checkout(button){
    if($("#frm_checkout").valid()){
        $.ajax({
            url: "/api/accessory/checkout",
            type:"post",
            data: $("#frm_checkout").serialize(),
            beforeSend: function () {
                loadingBtn(button);
            },
            success: function (res) {
                if (res.success) {
                    datarow.ajax.reload();
                } else {
                    alert("Sorry data cannot updated");
                }
            },
        })
            .fail(function () {
                alert("Sorry something wrong while processing");
            })
            .always(function () {
                $("#loading").hide();
                $("#modal_checkout").modal("hide");
                unloadingBtn(button);
            });
    }
}
var datarow = $("#tbl_data").DataTable({
    ajax: {
        type:"get",
        url: "/api/accessory",
    },
    paging: true,
    lengthChange: true,
    searching: true,
    info: true,
    ordering:false,
    autoWidth: false,
    responsive: true,
    columns: [
        { data: 'id' },
        { data: 'name' },
        { data: 'assetTag' },
        { data: 'status' },
        { data: 'category' },
        { data: 'supplier' },
        { data: 'manufacturer' },
        { data: 'location' },
        { data: 'room' },
        { data: 'owner' },
        { data: 'model' },
        { data: 'prNumber' },
        { data: 'purchaseDateStr' },
        { data: 'purchaseCost' },
        { data: 'usedBy' }
    ],
    columnDefs: [
        {
            targets: 0,
            render: function (data, type, row, meta) {
                var btnDelete = '';
                var btnUse  = '';

                if(privilege.filter(e=>e.authority=='ROLE_ACCESSORY').length>0){
                    btnDelete= ` <button onclick="deleteId(this,'`+row.id+`')" class="btn btn-outline-danger btn-flat btn-sm"><i class="fa fa-trash"></i></button> `;
                    btnUse =` <button type="button" class="btn btn-outline-primary btn-flat btn-sm " onclick="use('`+row.id+`')"><i class="fa fa-user"> </i> USE</button> `
                }

                return `<button type="button" class="btn btn-outline-primary btn-flat btn-sm " onclick="func_update('`+row.id+`')"><i class="fa fa-external-link-alt"> `+row.id+`</i></button>`+btnDelete+btnUse;
            },
        }
    ]
});
function func_update(id){
    var setid = datarow.rows().data().filter(e=>e.id==id)[0];
    var frm = $("#frm_edit");
    frm.find("[name=id]").val(setid.id);
    frm.find("[name=name]").val(setid.name);
    frm.find("[name=assetTag]").val(setid.assetTag);
    frm.find("[name=accessoryCategoryId]").val(setid.accessoryCategoryId);
    frm.find("[name=accessoryCategoryId]").trigger("change");
    frm.find("[name=accessoryStatusId]").val(setid.accessoryStatusId);
    frm.find("[name=accessoryStatusId]").trigger("change");
    frm.find("[name=supplierId]").val(setid.supplierId);
    frm.find("[name=supplierId]").trigger("change");
    frm.find("[name=manufacturerId]").val(setid.manufacturerId);
    frm.find("[name=manufacturerId]").trigger("change");
    frm.find("[name=locationId]").val(setid.locationId);
    frm.find("[name=locationId]").trigger("change");
    frm.find("[name=roomId]").val(setid.roomId);
    frm.find("[name=roomId]").trigger("change");
    frm.find("[name=owner]").val(setid.owner);
    frm.find("[name=model]").val(setid.model);
    frm.find("[name=prNumber]").val(setid.prNumber);
    frm.find("[name=purchaseCost]").val(setid.purchaseCost);
    frm.find("[name=purchaseDate]").val(setid.purchaseDateStr);
    frm.find("[name=qty]").val(setid.qty);
    $("#modal_edit").modal('show');
}
function use(id){
    var setid = datarow.rows().data().filter(e=>e.id==id)[0];
    var frm = $("#frm_checkout");
    frm.trigger("reset");
    frm.find("[name=accessoryId]").val(setid.id);
    frm.find("#accname").val(setid.name);
    $("#modal_checkout").modal('show');
}

function deleteId(button,id) {
    var conf = confirm("Are you sure to delete this item ?");
    if(conf){
        $.ajax({
            url: "/api/accessory",
            type: "delete",
            data: {id:id},
            beforeSend: function () {
                loadingBtn(button);
            },
            success: function (res) {
                if (res.success) {
                    datarow.ajax.reload();
                } else {
                    alert("Sorry data cannot updated");
                }
            },
        }).fail(function () {
            alert("Sorry something wrong while processing");
        }).always(function () {
            unloadingBtn(button);
        });
    }
}


$('.dpicker').daterangepicker({
    singleDatePicker: true,
    locale: {
        format: 'YYYY-MM-DD'
    }
});



function loadingBtn(el){
    $(el).prop('disabled', true);
    $(el).find(".fa-circle-notch").show();
}
function unloadingBtn(el){
    $(el).prop('disabled', false);
    $(el).find(".fa-circle-notch").hide();
}