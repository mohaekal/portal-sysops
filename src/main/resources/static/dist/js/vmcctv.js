

$(document).ready(function(){
    $("#frm_add").validate({
        rules: {
            name: {
                required: true
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    $("#frm_edit").validate({
        rules: {
            name: {
                required: true,
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    $("#frm_add_ip").validate({
        rules: {
            ip: {
                required: true,
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

})

var datarow = $("#tbl_asset").DataTable({
    ajax: {
        type:"get",
        url: "/api/vm/cctv",
    },
    paging: true,
    lengthChange: true,
    searching: true,
    info: true,
    ordering:true,
    "order": [[ 0, "desc" ]],
    autoWidth: false,
    responsive: true,

    columns: [
        { data: 'id' },
        { data: 'id' },
        { data: 'name' },
        { data: 'ipsStr' },
        { data: 'license' },
        { data: 'state' },
        { data: 'socket' },
        { data: 'cpu' },
        { data: 'memory' },
        { data: 'storage' },
        { data: 'assetIPHost' },
        { data: 'description' },
        { data: 'backup' },
        { data: 'highAvailbility' },
        { data: 'priority' },
        { data: 'avSpanPort' },
        { data: 'avOssec' },
        { data: 'antivirusInstalled' },
        { data: 'prtgIcmp' },
        { data: 'prtgService' },
        { data: 'grafana' },
        { data: 'company' },
    ],
    columnDefs: [
        {
            targets: 0,
            visible: false
        },
        {
            targets: 1,
            "orderable": false,
            render: function (data, type, row, meta) {
                var btnDelete = ' <button onclick="deleteId(this,'+row.id+')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button> ';
                var btnEdit = `<button type="button" class="btn btn-primary btn-sm " onclick="edit(this,'`+row.id+`')"><i class="fa fa-external-link-alt"> `+row.id+`</i></button> `;
                var btnReqUpgrade = `<button type="button" class="btn btn-warning btn-sm " onclick="upgrade(this,'`+row.id+`')"> Upgrade</button> `;
                var btnRequestTerminate = `<button type="button" class="btn btn-danger btn-sm " onclick="modalTerminate(this,'`+row.id+`')"> Terminate</button> `;
                if (privilege.filter(e=>e.authority=='ROLE_VMCCTV').length>0) {
                    return btnEdit+btnDelete+btnReqUpgrade+btnRequestTerminate ;
                }
                return btnEdit+btnReqUpgrade+btnRequestTerminate;
            },
        },
        {
            targets: 6,
            visible:true,
        },
        {
            targets: 7,
            visible:true,
        },
        {
            targets: 8,
            visible:true,
        },
        {
            targets: 9,
            visible:true,
        },
        {
            targets: 10,
            "orderable": false,
            render: function (data, type, row, meta) {
                return `<a href="#" onclick="showAssetDetail(`+row.assetId+`)" > `+row.assetIPHost+` <i class="fa fa-external-link-alt"></i></a>`;
            },
        },
        {
            targets: 12,
            "orderable": false,
            "className": "text-center",
            render: function (data, type, row, meta) {
                return (row.backup)?'<i class="fa fa-check" style="color: green"></i>':'<i class="fa fa-times" style="color:red;"></i>';
            },
        },
        {
            targets: 13,
            "orderable": false,
            "className": "text-center",
            render: function (data, type, row, meta) {
                return (row.highAvailbility)?'<i class="fa fa-check" style="color: green"></i>':'<i class="fa fa-times" style="color:red;"></i>';
            },
        },
        {
            targets: 15,
            "orderable": false,
            "className": "text-center",
            render: function (data, type, row, meta) {
                return (row.avSpanPort)?'<i class="fa fa-check" style="color: green"></i>':'<i class="fa fa-times" style="color:red;"></i>';
            },
        },
        {
            targets: 16,
            "orderable": false,
            "className": "text-center",
            render: function (data, type, row, meta) {
                return (row.avOssec)?'<i class="fa fa-check" style="color: green"></i>':'<i class="fa fa-times" style="color:red;"></i>';
            },
        },
        {
            targets: 17,
            "orderable": false,
            "className": "text-center",
            render: function (data, type, row, meta) {
                return (row.antivirusInstalled)?'<i class="fa fa-check" style="color: green"></i>':'<i class="fa fa-times" style="color:red;"></i>';
            },
        },
        {
            targets: 18,
            "orderable": false,
            "className": "text-center",
            render: function (data, type, row, meta) {
                return (row.prtgIcmp)?'<i class="fa fa-check" style="color: green"></i>':'<i class="fa fa-times" style="color:red;"></i>';
            },
        },
        {
            targets: 19,
            "orderable": false,
            "className": "text-center",
            render: function (data, type, row, meta) {
                return (row.prtgService)?'<i class="fa fa-check" style="color: green"></i>':'<i class="fa fa-times" style="color:red;"></i>';
            },
        },
        {
            targets: 20,
            "orderable": false,
            "className": "text-center",
            render: function (data, type, row, meta) {
                return (row.grafana)?'<i class="fa fa-check" style="color: green"></i>':'<i class="fa fa-times" style="color:red;"></i>';
            },
        }],
    initComplete: function () {
        var thisTable = this;
        var rowFilter = $('<tr class="filter"></tr>').appendTo(
            $(this.api().table().header())
        );

        this.api()
            .columns()
            .every(function () {
                var column = this;
                switch (column.index()) {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                        var input = $(
                            `<input class="form-control form-control-sm form-filter " title="Input" data-col-index="` +
                            column.index() +
                            `"/>`
                        )
                            .appendTo($("<th>").appendTo(rowFilter))
                            .on("keyup change", function () {
                                var val = $(this).val();

                                column
                                    // .search(val ? "^" + val + "$" : "", true, false)
                                    .search(val)
                                    .draw();
                            });
                        break;

                    case 14:

                        var select = $(
                            `<select class="form-control form-control-sm form-filter kt-input select2select" title="Select" data-col-index="` +
                            column.index() +
                            `"><option value="">Select</option></select>`
                        )
                            .appendTo($("<th>").appendTo(rowFilter))
                            .on("change", function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                column
                                    .search(val ? "^" + val + "$" : "", true, false)
                                    // .search(val)
                                    .draw();
                            });

                        column.data().unique().sort().each(function (d, j) {
                            select.append(
                                '<option value="' + d + '">' + d + "</option>"
                            );
                        });

                        break;
                    default:
                        var select = $("").appendTo($("<th>").appendTo(rowFilter));
                        break;
                }
            });

        var hideSearchColumnResponsive = function () {
            thisTable
                .api()
                .columns()
                .every(function () {
                    var column = this;
                    if (column.responsiveHidden()) {
                        $(".filter").find("th").eq(column.index()).show();
                    } else {
                        $(".filter").find("th").eq(column.index()).hide();
                    }
                });
        };

        // init on datatable load
        hideSearchColumnResponsive();
        // recheck on window resize
        window.onresize = hideSearchColumnResponsive;
    },
});

$("#btn_submit_add").click(function(){
    if($("#frm_add").valid()){
        add();
    }
});

function add(button) {
    $.ajax({
        url: "/api/vm/cctv",
        type:"post",
        data: $("#frm_add").serialize(),
        beforeSend: function () {
            loadingBtn(button);
        },
        success: function (res) {
            if (res.success) {
                datarow.ajax.reload();
                $("#frm_add").trigger("reset");
                $("#frm_add").find("select").trigger("change");
            } else {
                alert("Sorry data cannot updated");
            }
        },
    })
        .fail(function () {
            alert("Sorry something wrong while processing");
        })
        .always(function () {
            $("#modal_add").modal("hide");
            unloadingBtn(button);
        });
}

$("#btn_submit_edit").click(function (e) {
    if($("#frm_edit").valid()){
        submitEdit(this);
    }
})

function edit(button,id){
    var setid = datarow.rows().data().filter(e=>e.id==id)[0];
    var frm = $("#frm_edit");
    frm.find("[name=id]").val(setid.id);
    frm.find("[name=socket]").val(setid.socket);
    frm.find("[name=name]").val(setid.name);
    frm.find("[name=ip]").val(setid.ip);
    frm.find("[name=ips]").val(setid.ips);
    frm.find("[name=ips]").trigger("change");
    frm.find("[name=cpu]").val(setid.cpu);
    frm.find("[name=memory]").val(setid.memory);
    frm.find("[name=storage]").val(setid.storage);
    frm.find("[name=licenseId]").val(setid.licenseId);
    frm.find("[name=licenseId]").trigger('change.select2');
    frm.find("[name=assetId]").val(setid.assetId);
    frm.find("[name=assetId]").trigger('change.select2');
    frm.find("[name=state]").val(setid.state);
    frm.find("[name=state]").trigger('change.select2');
    frm.find("[name=description]").val(setid.description);
    frm.find("[name=backup][value=1]").prop("checked",setid.backup);
    frm.find("[name=backup][value=0]").prop("checked",!setid.backup);
    frm.find("[name=highAvailbility][value=1]").prop("checked",setid.highAvailbility);
    frm.find("[name=highAvailbility][value=0]").prop("checked",!setid.highAvailbility);
    frm.find("[name=priority]").val(setid.priority);
    frm.find("[name=priority]").trigger('change.select2');
    frm.find("[name=avSpanPort][value=1]").prop("checked",setid.avSpanPort);
    frm.find("[name=avSpanPort][value=0]").prop("checked",!setid.avSpanPort);
    frm.find("[name=avOssec][value=1]").prop("checked",setid.avOssec);
    frm.find("[name=avOssec][value=0]").prop("checked",!setid.avOssec);
    frm.find("[name=antivirus]").val(setid.antivirus);
    frm.find("[name=antivirusInstalled][value=1]").prop("checked",setid.antivirusInstalled);
    frm.find("[name=antivirusInstalled][value=0]").prop("checked",!setid.antivirusInstalled);
    frm.find("[name=prtgIcmp][value=1]").prop("checked",setid.prtgIcmp);
    frm.find("[name=prtgIcmp][value=0]").prop("checked",!setid.prtgIcmp);
    frm.find("[name=prtgService][value=1]").prop("checked",setid.prtgService);
    frm.find("[name=prtgService][value=0]").prop("checked",!setid.prtgService);
    frm.find("[name=prtgServiceRemark]").val(setid.prtgServiceRemark);
    frm.find("[name=grafana][value=1]").prop("checked",setid.grafana);
    frm.find("[name=grafana][value=0]").prop("checked",!setid.grafana);


    frm.find("[name=companyId]").val(setid.companyId);
    frm.find("[name=companyId]").trigger('change.select2');

    $.ajax({
        type: 'GET',
        url: '/api/ips',
        data: {vmid:id},
        success:function(response){
            $('#editIps').val(null);
            $('#editIps').val(null);
            $('#editIps').trigger('change');
            // create the option and append to Select2
            response.data.forEach(function (el,i) {
                console.log(el)
                var option = new Option(el.ip, el.id, true, true);
                $("#editIps").append(option).trigger('change');
                // manually trigger the `select2:select` event
                $("#editIps").trigger({
                    type: 'select2:select',
                    params: {
                        data: el
                    }
                });
            })

            $('.selectipsModalEdit').select2({
                ajax: {
                    url: '/api/ips/select2',
                    type:"get",
                    data: {vmid:id},
                    processResults: function (response) {
                        return {
                            results: response.data
                        };
                    }
                }
            });
        }
    });

    $("#modal_edit").modal('show');

}

function submitEdit(button){
    $.ajax({
        url: "/api/vm/cctv",
        type:"put",
        data: $("#frm_edit").serialize(),
        beforeSend: function () {
            loadingBtn(button);
        },
        success: function (res) {
            if (res.success) {
                datarow.ajax.reload();
            } else {
                alert("Sorry data cannot updated");
            }
        },
    })
        .fail(function () {
            alert("Sorry something wrong while processing");
        })
        .always(function () {
            $("#loading").hide();
            $("#datas").show();
            $("#modal_edit").modal("hide");
            unloadingBtn(button);
        });
}


function upgrade(button,id){
    var setid = datarow.rows().data().filter(e=>e.id==id)[0];
    var frm = $("#frm_upgrade");
    frm.find("[name=id]").val(setid.id);
    frm.find("[name=socket]").val(setid.socket);
    frm.find("[name=name]").val(setid.name);
    frm.find("[name=cpu]").val(setid.cpu);
    frm.find("[name=memory]").val(setid.memory);
    frm.find("[name=storage]").val(setid.storage);
    frm.find("[name=remark]").val(setid.remark);

    $.ajax({
        type: 'GET',
        url: '/api/ips',
        data: {vmid:id},
        success:function(response){
            $('#editIps').val(null);
            $('#editIps').val(null);
            $('#editIps').trigger('change');
            // create the option and append to Select2
            response.data.forEach(function (el,i) {
                console.log(el)
                var option = new Option(el.ip, el.id, true, true);
                $("#editIps").append(option).trigger('change');
                // manually trigger the `select2:select` event
                $("#editIps").trigger({
                    type: 'select2:select',
                    params: {
                        data: el
                    }
                });
            })

            $('.selectipsModalEdit').select2({
                ajax: {
                    url: '/api/ips/select2',
                    type:"get",
                    data: {vmid:id},
                    processResults: function (response) {
                        return {
                            results: response.data
                        };
                    }
                }
            });
        }
    });

    $("#modal_upgrade").modal('show');

}

function submitUpgrade(button){
    $.ajax({
        url: "/api/request/vm-cctv-nms/upgrade/prepared",
        type:"post",
        data: $("#frm_upgrade").serialize(),
        beforeSend: function () {
            loadingBtn(button);
        },
        success: function (res) {
            if (res.success) {
                // datarow.ajax.reload();
                toastr.success('Request Upgrade VM Sent')
                window.location.href = "/request/vm-cctv-nms"
            } else {
                alert(res.data);
            }
        },
    })
    .fail(function () {
        alert("Sorry something wrong while processing");
    })
    .always(function () {
        $("#modal_upgrade").modal("hide");
        unloadingBtn(button);
    });
}

function modalTerminate(button,id) {
    var frm = $("#frm_req_terminate");
    frm.find("[name=id").val(id);
    $("#modal_req_terminate").modal("show");
}

function submitTerminate(button){
    var id = $("#frm_req_terminate").find("[name=id]").val();
    var note = $("#frm_req_terminate").find("[name=remark]").val();
    var nt = "";
    if(note){
        nt="&remark="+note;
    }

    $.ajax({
        url: "/api/request/vm-cctv-nms/terminate/prepared?id="+id+nt,
        type:"post",
        beforeSend: function () {
            loadingBtn(button);
        },
        success: function (res) {
            if (res.success) {
                // datarow.ajax.reload();
                toastr.success('Request Terminate VM Sent')
                window.location.href = "/request/vm-cctv-nms"
            } else {
                alert(res.data);
            }
        },
    })
        .fail(function () {
            alert("Sorry something wrong while processing");
        })
        .always(function () {
            $("#modal_req_terminate").modal("hide");
            unloadingBtn(button);
        });
}

function deleteId(button,id) {
    var conf = confirm("Are you sure to delete this item ?");
    if(conf){
        $.ajax({
            url: "/api/vm/cctv",
            type: "delete",
            data: {id:id},
            beforeSend: function () {
                loadingBtn(button);
            },
            success: function (res) {
                if (res.success) {
                    datarow.ajax.reload();
                } else {
                    alert("Sorry data cannot updated");
                }
            },
        }).fail(function () {
            alert("Sorry something wrong while processing");
        }).always(function () {
            unloadingBtn(button);
        });
    }

}
$('.dpicker').daterangepicker({
    singleDatePicker: true,
    locale: {
        format: 'YYYY-MM-DD'
    }
});




$('.selectips').select2({
    ajax: {
        url: '/api/ips/select2',
        type:"get",
        processResults: function (response) {
            return {
                results: response.data
            };
        }
    }
});

$('.selectipsModalEdit').select2();





function addNewIP() {
    $("frm_add_ip").trigger("reset")
    $("#modal_add_ip").modal("show");
}

function submitAddIP(button) {

    var frm = $("#frm_add_ip");
    if(frm.valid()){
        $.ajax({
            url: "/api/ips",
            type:"post",
            data: frm.serialize(),
            beforeSend: function () {
                loadingBtn(button);
            },
            success: function (res) {
                if (res.success) {

                } else {
                    alert("Sorry data cannot updated");
                }
            },
        }).fail(function () {
            alert("Sorry something wrong while processing");
        }).always(function () {
            $("#modal_add_ip").modal("hide");
            unloadingBtn(button);
        });

    }
}

function showAssetDetail(id){
    $.ajax({
        url:"/api/asset/findById",
        data:{id:id},
        type:"get",
        success: function (res) {
            $("#assetTag").text(res.data.assetTag);
            $("#assetName").text(res.data.assetName);
            $("#category").text(res.data.assetCategory);
            $("#status").text(res.data.assetStatus);
            $("#ipManagement").text(res.data.ipManagement);
            $("#ipHost").text(res.data.ipHost);
            $("#prNumber").text(res.data.prNumber);
            $("#purchaseDate").text(res.data.purchaseDateStr);
            $("#purchaseCost").text(res.data.purchaseCost);
            $("#warranty").text(res.data.warranty);
            $("#location").text(res.data.location);
            $("#room").text(res.data.room);
            $("#notes").text(res.data.notes);
            $("#modal_show_asset").modal("show");
        }
    })
}