

$(document).ready(function(){
    $("#frm_add").validate({
        rules: {
            name: {
                required: true,
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    $("#frm_edit").validate({
        rules: {

            name: {
                required: true,
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

})
$("#btn_submit_add").click(function(){
    if($("#frm_add").valid){
        add()();
    }
});

function add(button) {
    $.ajax({
        url: "/api/user-management/access",
        type:"post",
        data: $("#frm_add").serialize(),
        beforeSend: function () {
            loadingBtn(button);
        },
        success: function (res) {
            if (res.success) {
                datarow.ajax.reload();
                $("#frm_add").trigger("reset");
            } else {
                alert("Sorry data cannot updated");
            }
        },
    })
        .fail(function () {
            alert("Sorry something wrong while processing");
        })
        .always(function () {
            $("#modal_add").modal("hide");
            unloadingBtn(button);
        });
}

function edit(button,id){
    var setid = datarow.rows().data().filter(e=>e.id==id)[0];
    console.log(setid);
    var frm = $("#frm_edit");
    frm.find("[name=id]").val(setid.id);
    frm.find("[name=name]").val(setid.name);
    frm.find("[name=description]").val(setid.description);
    $("#modal_edit").modal('show');


}

$("#btn_submit_edit").click(function (e) {
    if($("#frm_edit").valid){
        submitEdit(this);
    }
})

function submitEdit(button){
    $.ajax({
        url: "/api/user-management/access",
        type:"put",
        data: $("#frm_edit").serialize(),
        beforeSend: function () {
            loadingBtn(button);
        },
        success: function (res) {
            if (res.success) {
                datarow.ajax.reload();
            } else {
                alert("Sorry data cannot updated");
            }
        },
    })
        .fail(function () {
            alert("Sorry something wrong while processing");
        })
        .always(function () {
            $("#loading").hide();
            $("#datas").show();
            $("#modal_edit").modal("hide");
            unloadingBtn(button);
        });
}


function deleteId(button,id) {
    var conf = confirm("Are you sure to delete this item ?");
    if(conf){
        $.ajax({
            url: "/api/user-management/access",
            type: "delete",
            data: {id:id},
            beforeSend: function () {
                loadingBtn(button);
            },
            success: function (res) {
                if (res.success) {
                    datarow.ajax.reload();
                } else {
                    alert("Sorry data cannot updated");
                }
            },
        }).fail(function () {
            alert("Sorry something wrong while processing");
        }).always(function () {
            unloadingBtn(button);
        });
    }

}

$("#select2role").on('change',function(e){
    $("#covertable").show();
    datarow.ajax.url("/api/user-management/role-access/get-access-by-role?roleid="+$(this).val()).load();
})

var datarow = $("#tbl_asset").DataTable({
    ajax: {
        url: "/api/user-management/role-access/get-access-by-role",
    },
    paging: true,
    lengthChange: true,
    searching: true,
    info: true,
    ordering:false,
    autoWidth: false,
    responsive: true,

    columns: [
        { data: 'id' },
        { data: 'name' },
        { data: 'description' }
    ],
    columnDefs: [
        {
            "width": "88px",
            targets: 0,
            render: function (data, type, row, meta) {
                var c = '';
                if(row.used==1){
                    c = 'checked'
                }
                return `<div class="icheck-danger d-inline">
                            <input type="checkbox" id="accessid`+row.id+`" onclick="roleaccesscheck(this,'`+row.id+`')"`+c+`>
                            <label for="accessid`+row.id+`">
                            </label>
                          </div>`;
            },
        },],

    initComplete: function () {
        var thisTable = this;
        var rowFilter = $('<tr class="filter"></tr>').appendTo(
            $(this.api().table().header())
        );

        this.api()
            .columns()
            .every(function () {
                var column = this;
                switch (column.index()) {
                    case 1:
                    case 2:
                        var input = $(
                            `<input class="form-control form-control-sm form-filter " title="Input" data-col-index="` +
                            column.index() +
                            `"/>`
                        )
                            .appendTo($("<th>").appendTo(rowFilter))
                            .on("keyup change", function () {
                                var val = $(this).val();

                                column
                                    // .search(val ? "^" + val + "$" : "", true, false)
                                    .search(val)
                                    .draw();
                            });
                        break;
                    default:
                        var select = $("").appendTo($("<th>").appendTo(rowFilter));
                        break;
                }
            });

        var hideSearchColumnResponsive = function () {
            thisTable
                .api()
                .columns()
                .every(function () {
                    var column = this;
                    if (column.responsiveHidden()) {
                        $(".filter").find("th").eq(column.index()).show();
                    } else {
                        $(".filter").find("th").eq(column.index()).hide();
                    }
                });
        };

        // init on datatable load
        hideSearchColumnResponsive();
        // recheck on window resize
        window.onresize = hideSearchColumnResponsive;
    },
});


function roleaccesscheck(e,accessid){
    var overlaycontent = $("#overlaycontent");
    $.ajax({
        url:"/api/user-management/role-access/update-role-access",
        type:"put",
        data:{accessId:accessid,roleId:$("#select2role").val(),enable:$(e).is(":checked")},
        beforeSend:function(){
            overlaycontent.show();
        },
        success:function(){
            overlaycontent.hide();
        }
    }).always(function(){
        overlaycontent.hide();
        datatables.ajax.reload();
    })
}


function loadingBtn(el){
    $(el).prop('disabled', true);
    $(el).find(".fa-circle-notch").show();
}
function unloadingBtn(el){
    $(el).prop('disabled', false);
    $(el).find(".fa-circle-notch").hide();
}