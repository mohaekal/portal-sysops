
$(document).ready(function(){
    $("#frm_add").validate({
        rules: {
            name: {
                required: true,
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    $("#frm_edit").validate({
        rules: {
            name: {
                required: true,
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

})


function submitAdd(button){
    if ($("#frm_add").valid()){
        $.ajax({
            url: "/api/remote-access",
            type:"post",
            data: $("#frm_add").serialize(),
            beforeSend: function () {
                loadingBtn(button);
            },
            success: function (res) {
                if (res.success) {
                    datarow.ajax.reload();
                    $("#frm_add").trigger("reset");
                } else {
                    alert("Sorry data cannot updated");
                }
            },
        })
            .fail(function () {
                alert("Sorry something wrong while processing");
            })
            .always(function () {
                $("#modal_add").modal("hide");
                unloadingBtn(button);
            });
    }
}

function submitUpdate(button){
    if($("#frm_edit").valid()){
        $.ajax({
            url: "/api/remote-access",
            type:"put",
            data: $("#frm_edit").serialize(),
            beforeSend: function () {
                loadingBtn(button);
            },
            success: function (res) {
                if (res.success) {
                    datarow.ajax.reload();
                } else {
                    alert("Sorry data cannot updated");
                }
            },
        })
            .fail(function () {
                alert("Sorry something wrong while processing");
            })
            .always(function () {
                $("#loading").hide();
                $("#datas").show();
                $("#modal_edit").modal("hide");
                unloadingBtn(button);
            });
    }
}

function deleteId(button,id) {
    var conf = confirm("Are you sure to delete this item ?");
    if(conf){
        $.ajax({
            url: "/api/remote-access",
            type: "delete",
            data: {id:id},
            beforeSend: function () {
                loadingBtn(button);
            },
            success: function (res) {
                if (res.success) {
                    datarow.ajax.reload();
                } else {
                    alert("Sorry data cannot updated");
                }
            },
        }).fail(function () {
            alert("Sorry something wrong while processing");
        }).always(function () {
            unloadingBtn(button);
        });
    }
}

var datarow = $("#tbl_data").DataTable({
    ajax: {
        type:"get",
        url: "/api/remote-access",
    },
    paging: true,
    lengthChange: true,
    searching: true,
    info: true,
    ordering:false,
    autoWidth: false,
    responsive: true,
    columns: [
        { data: 'id' },
        { data: 'ip' },
        { data: 'port' },
        { data: 'application' },
        { data: 'location' },
        { data: 'room' },
        { data: 'user' },
        { data: 'purpose' }
    ],
    columnDefs: [
        {
            targets: 0,
            "width":"100px",
            render: function (data, type, row, meta) {
                var btnDelete = '';
                if (privilege.filter(e=>e.authority=='ROLE_REMOTEACCESS').length>0){
                    btnDelete= `<button onclick="deleteId(this,'`+row.id+`')" class="btn btn-outline-danger btn-flat btn-sm"><i class="fa fa-trash"></i></button> `;
                }
                return `<button type="button" class="btn btn-outline-primary btn-flat btn-sm " onclick="func_update('`+row.id+`')"><i class="fa fa-external-link-alt"> `+row.id+`</i></button>`+btnDelete;
            },
        }],initComplete: function () {
        var thisTable = this;
        var rowFilter = $('<tr class="filter"></tr>').appendTo(
            $(this.api().table().header())
        );

        this.api()
            .columns()
            .every(function () {
                var column = this;
                switch (column.index()) {
                    case 4:
                    case 5:
                        var select = $(
                            `<select class="form-control form-control-sm form-filter kt-input" title="Select" data-col-index="` +
                            column.index() +
                            `"><option value="">Select</option></select>`
                        )
                            .appendTo($("<th>").appendTo(rowFilter))
                            .on("change", function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                column
                                    .search(val ? "^" + val + "$" : "", true, false)
                                    // .search(val)
                                    .draw();
                            });

                        column.data().unique().sort().each(function (d, j) {
                            select.append(
                                '<option value="' + d + '">' + d + "</option>"
                            );
                        });

                        break;
                    case 1:
                    case 2:
                    case 3:
                    case 6:
                    case 7:
                        var input = $(
                            `<input class="form-control form-control-sm form-filter " title="Input" data-col-index="` +
                            column.index() +
                            `"/>`
                        )
                            .appendTo($("<th>").appendTo(rowFilter))
                            .on("keyup change", function () {
                                // var val = $.fn.dataTable.util.escapeRegex($(this).val());
                                var val = $(this).val();
                                column
                                    // .search(val ? "^" + val + "$" : "", true, false)
                                    .search(val)
                                    .draw();
                            });
                        break;
                    default:
                        var select = $("").appendTo($("<th>").appendTo(rowFilter));
                        break;
                }
            });

        var hideSearchColumnResponsive = function () {
            thisTable
                .api()
                .columns()
                .every(function () {
                    var column = this;
                    if (column.responsiveHidden()) {
                        $(".filter").find("th").eq(column.index()).show();
                    } else {
                        $(".filter").find("th").eq(column.index()).hide();
                    }
                });
        };

        // init on datatable load
        hideSearchColumnResponsive();
        // recheck on window resize
        window.onresize = hideSearchColumnResponsive;
    },
});

function func_update(id){
    var setid = datarow.rows().data().filter(e=>e.id==id)[0];
    var frm = $("#frm_edit");
    frm.find("[name=id]").val(setid.id);
    frm.find("[name=ip]").val(setid.ip);
    frm.find("[name=port]").val(setid.port);
    frm.find("[name=application]").val(setid.application);
    frm.find("[name=user]").val(setid.user);
    frm.find("[name=password]").val(setid.password);
    frm.find("[name=purpose]").val(setid.purpose);
    frm.find("[name=locationId]").val(setid.locationId);
    frm.find("[name=locationId]").trigger('change.select2');
    frm.find("[name=roomId]").val(setid.roomId);
    frm.find("[name=roomId]").trigger('change.select2');
    $("#modal_edit").modal('show');
}

function loadingBtn(el){
    $(el).prop('disabled', true);
    $(el).find(".fa-circle-notch").show();
}
function unloadingBtn(el){
    $(el).prop('disabled', false);
    $(el).find(".fa-circle-notch").hide();
}