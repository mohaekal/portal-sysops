$(document).ready(function(){
    $("#frm_add").validate({
        rules: {
            name: {
                required: true,
            },
            assetName: {
                required: true,
            },
            assetStatusId: {
                required: true,
            },purchaseDate: {
                required: true,
            },supplierId: {
                required: true,
            },orderNumber: {
                required: true,
            },purchaseCost: {
                required: true,
            },warranty: {
                required: true,
            },notes: {
                required: true,
            },locationId: {
                required: true,
            },categoryId: {
                required: true,
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    $("#frm_edit").validate({
        rules: {
            name: {
                required: true,
            },
            assetTag: {
                required: true,
            },
            assetName: {
                required: true,
            },
            assetStatusId: {
                required: true,
            },purchaseDate: {
                required: true,
            },supplierId: {
                required: true,
            },orderNumber: {
                required: true,
            },purchaseCost: {
                required: true,
            },warranty: {
                required: true,
            },notes: {
                required: true,
            },locationId: {
                required: true,
            },categoryId: {
                required: true,
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

})

function submitAdd(button){
    if ($("#frm_add").valid()){
        $.ajax({
            url: "/api/asset",
            type:"post",
            data: $("#frm_add").serialize(),
            beforeSend: function () {
                loadingBtn(button);
            },
            success: function (res) {
                if (res.success) {
                    datarow.ajax.reload();
                    $("#frm_add").trigger("reset");
                } else {
                    alert("Sorry data cannot updated");
                }
            },
        })
            .fail(function () {
                alert("Sorry something wrong while processing");
            })
            .always(function () {
                $("#modal_add").modal("hide");
                unloadingBtn(button);
            });
    }
}

function submitUpdate(button){
    if($("#frm_edit").valid()){
        $.ajax({
            url: "/api/asset",
            type:"put",
            data: $("#frm_edit").serialize(),
            beforeSend: function () {
                loadingBtn(button);
            },
            success: function (res) {
                if (res.success) {
                    datarow.ajax.reload();
                } else {
                    alert("Sorry data cannot updated");
                }
            },
        })
            .fail(function () {
                alert("Sorry something wrong while processing");
            })
            .always(function () {
                $("#loading").hide();
                $("#datas").show();
                $("#modal_edit").modal("hide");
                unloadingBtn(button);
            });
    }
}

var datarow = $("#tbl_data").DataTable({
    ajax: {
        type:"get",
        url: "/api/asset",
    },
    paging: true,
    lengthChange: true,
    searching: true,
    info: true,
    ordering:true,
    "order": [[ 0, "desc" ]],
    autoWidth: false,
    responsive: true,
    "pageLength": 100,
    columns: [
        { data: 'id' },
        { data: 'id' },
        { data: 'assetTag' },
        { data: 'assetName' },
        { data: 'assetCategory' },
        { data: 'assetStatus' },
        { data: 'socket' },
        { data: 'core' },
        { data: 'type' },
        { data: 'memory' },
        { data: 'storage' },
        { data: 'ipHost' },
        { data: 'location' },
        { data: 'room' },
        { data: 'priority' },
        { data: 'backup' },
        { data: 'highAvailbility' },
        { data: 'avSpanport' },
        { data: 'avOssec' },
        { data: 'antivirus' },
        { data: 'prtgIcmp' },
        { data: 'prtgService' },
        { data: 'grafana' },
        { data: 'company' },
    ],
    columnDefs: [
        {
            "width": "150px",
            targets: 0,
            visible:false
        },
        {
            targets: 1,
            "orderable": false,
            render: function (data, type, row, meta) {
                var btnDelete = '';
                if (privilege.filter(e=>e.authority=='ROLE_ASSET').length>0){
                    btnDelete= `<button onclick="deleteId(this,'`+row.id+`')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button> `
                }
                return `<button type="button" class="btn btn-outline-primary btn-flat btn-sm " onclick="func_update('`+row.id+`')"><i class="fa fa-external-link-alt"> `+row.id+`</i></button> `+btnDelete;
            }
        },{
            "width": "150px",
            targets: 6,
            visible:false
        },{
            "width": "150px",
            targets: 7,
            visible:false
        },{
            "width": "150px",
            targets: 8,
            visible:false
        },{
            "width": "150px",
            targets: 9,
            visible:false
        },{
            "width": "150px",
            targets: 10,
            visible:false
        },
        {
            targets: 15,
            "orderable": false,
            "className": "text-center",
            render: function (data, type, row, meta) {
                switch (row.backup) {
                    case 'Yes' :
                        return '<i class="fa fa-check" style="color: green"></i>';
                        break;
                    case 'No' :
                        return '<i class="fa fa-times" style="color:red;"></i>';
                        break;
                    default:
                        return 'N/A';
                }
            },
        },
        {
            targets: 16,
            "orderable": false,
            "className": "text-center",
            render: function (data, type, row, meta) {
                switch (row.highAvailbility) {
                    case 'Yes' :
                        return '<i class="fa fa-check" style="color: green"></i>';
                        break;
                    case 'No' :
                        return '<i class="fa fa-times" style="color:red;"></i>';
                        break;
                    default:
                        return 'N/A';
                }
            },
        },
        {
            targets: 17,
            "orderable": false,
            "className": "text-center",
            render: function (data, type, row, meta) {
                switch (row.avSpanport) {
                    case 'Yes' :
                        return '<i class="fa fa-check" style="color: green"></i>';
                        break;
                    case 'No' :
                        return '<i class="fa fa-times" style="color:red;"></i>';
                        break;
                    default:
                        return 'N/A';
                }
            },
        },
        {
            targets: 18,
            "orderable": false,
            "className": "text-center",
            render: function (data, type, row, meta) {
                switch (row.avOssec) {
                    case 'Yes' :
                        return '<i class="fa fa-check" style="color: green"></i>';
                        break;
                    case 'No' :
                        return '<i class="fa fa-times" style="color:red;"></i>';
                        break;
                    default:
                        return 'N/A';
                }
            },
        },
        {
            targets: 19,
            "orderable": false,
            "className": "text-center",
            render: function (data, type, row, meta) {
                switch (row.antivirus) {
                    case 'Yes' :
                        return '<i class="fa fa-check" style="color: green"></i>';
                        break;
                    case 'No' :
                        return '<i class="fa fa-times" style="color:red;"></i>';
                        break;
                    default:
                        return 'N/A';
                }
            },
        },
        {
            targets: 20,
            "orderable": false,
            "className": "text-center",
            render: function (data, type, row, meta) {
                switch (row.prtgIcmp) {
                    case 'Yes' :
                        return '<i class="fa fa-check" style="color: green"></i>';
                        break;
                    case 'No' :
                        return '<i class="fa fa-times" style="color:red;"></i>';
                        break;
                    default:
                        return 'N/A';
                }
            },
        },
        {
            targets: 21,
            "orderable": false,
            "className": "text-center",
            render: function (data, type, row, meta) {
                switch (row.prtgService) {
                    case 'Yes' :
                        return '<i class="fa fa-check" style="color: green"></i>';
                        break;
                    case 'No' :
                        return '<i class="fa fa-times" style="color:red;"></i>';
                        break;
                    default:
                        return 'N/A';
                }
            },
        },
        {
            targets: 22,
            "orderable": false,
            "className": "text-center",
            render: function (data, type, row, meta) {
                switch (row.grafana) {
                    case 'Yes' :
                        return '<i class="fa fa-check" style="color: green"></i>';
                        break;
                    case 'No' :
                        return '<i class="fa fa-times" style="color:red;"></i>';
                        break;
                    default:
                        return 'N/A';
                }
            },
        },
    ],
    initComplete: function () {
        var thisTable = this;
        var rowFilter = $('<tr class="filter"></tr>').appendTo(
            $(this.api().table().header())
        );

        this.api()
            .columns()
            .every(function () {
                var column = this;
                switch (column.index()) {
                    case 4:
                    case 5:
                    case 12:
                    case 13:
                    case 14:
                        var select = $(
                            `<select class="form-control form-control-sm form-filter select2select" title="Select" data-col-index="` +
                            column.index() +
                            `"><option value="">Select</option></select>`
                        )
                            .appendTo($("<th>").appendTo(rowFilter))
                            .on("change", function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                column
                                    .search(val ? "^" + val + "$" : "", true, false)
                                    // .search(val)
                                    .draw();
                            });

                        column.data().unique().sort().each(function (d, j) {
                            select.append(
                                '<option value="' + d + '">' + d + "</option>"
                            );
                        });

                        break;
                    case 2:
                    case 3:
                    case 11:
                        var input = $(
                            `<input class="form-control form-control-sm form-filter " title="Input" data-col-index="` +
                            column.index() +
                            `"/>`
                        )
                            .appendTo($("<th>").appendTo(rowFilter))
                            .on("keyup change", function () {
                                var val = $(this).val();

                                column
                                    // .search(val ? "^" + val + "$" : "", true, false)
                                    .search(val)
                                    .draw();
                            });
                        break;
                    default:
                        var select = $("").appendTo($("<th>").appendTo(rowFilter));
                        break;
                }
            });

        var hideSearchColumnResponsive = function () {
            thisTable
                .api()
                .columns()
                .every(function () {
                    var column = this;
                    if (column.responsiveHidden()) {
                        $(".filter").find("th").eq(column.index()).show();
                    } else {
                        $(".filter").find("th").eq(column.index()).hide();
                    }
                });
        };

        // init on datatable load
        hideSearchColumnResponsive();
        // recheck on window resize
        window.onresize = hideSearchColumnResponsive;
    },
});

function deleteId(button,id) {
    var conf = confirm("Are you sure to delete this item ?");
    if(conf){
        $.ajax({
            url: "/api/asset",
            type: "delete",
            data: {id:id},
            beforeSend: function () {
                loadingBtn(button);
            },
            success: function (res) {
                if (res.success) {
                    datarow.ajax.reload();
                } else {
                    alert("Sorry data cannot updated");
                }
            },
        }).fail(function () {
            alert("Sorry something wrong while processing");
        }).always(function () {
            unloadingBtn(button);
        });
    }

}

$('.dpicker').daterangepicker({
    singleDatePicker: true,
    locale: {
        format: 'YYYY-MM-DD'
    }
});
function func_update(id){
    var setid = datarow.rows().data().filter(e=>e.id==id)[0];
    var frm = $("#frm_edit");
    frm.find("[name=id]").val(setid.id);
    frm.find("[name=assetTag]").val(setid.assetTag);
    frm.find("[name=assetName]").val(setid.assetName);
    frm.find("[name=assetStatusId]").val(setid.assetStatusId);
    frm.find("[name=assetStatusId]").trigger("change");
    frm.find("[name=purchaseDate]").val(setid.purchaseDateStr);
    frm.find("[name=assetCategoryId]").val(setid.assetCategoryId);
    frm.find("[name=assetCategoryId]").trigger("change");

    frm.find("[name=socket]").val(setid.socket);
    frm.find("[name=core]").val(setid.core);
    frm.find("[name=type]").val(setid.type);
    frm.find("[name=storage]").val(setid.storage);
    frm.find("[name=memory]").val(setid.memory);

    frm.find("[name=ipManagement]").val(setid.ipManagement);
    frm.find("[name=userManagement]").val(setid.userManagement);
    frm.find("[name=passwordManagement]").val(setid.passwordManagement);
    frm.find("[name=ipHost]").val(setid.ipHost);
    frm.find("[name=userHost]").val(setid.userHost);
    frm.find("[name=passwordHost]").val(setid.passwordHost);

    frm.find("[name=prNumber]").val(setid.prNumber);
    frm.find("[name=purchaseCost]").val(setid.purchaseCost);
    frm.find("[name=warranty]").val(setid.warranty);
    frm.find("[name=notes]").val(setid.notes);
    frm.find("[name=locationId]").val(setid.locationId);
    frm.find("[name=locationId]").trigger("change");
    frm.find("[name=roomId]").val(setid.roomId);
    frm.find("[name=roomId]").trigger("change");
    frm.find("[name=backup][value=Yes]").prop("checked",(setid.backup=='Yes'));
    frm.find("[name=backup][value=No]").prop("checked",(setid.backup=='No'));
    frm.find("[name=backup][value='N/A']").prop("checked",(setid.backup=='N/A'));
    frm.find("[name=highAvailbility][value=Yes]").prop("checked",(setid.highAvailbility=='Yes'));
    frm.find("[name=highAvailbility][value=No]").prop("checked",(setid.highAvailbility=='No'));
    frm.find("[name=highAvailbility][value='N/A']").prop("checked",(setid.highAvailbility=='N/A'));
    frm.find("[name=priority]").val(setid.priority);
    frm.find("[name=priority]").trigger('change.select2');
    frm.find("[name=avSpanport][value=Yes]").prop("checked",(setid.avSpanport=='Yes'));
    frm.find("[name=avSpanport][value=No]").prop("checked",(setid.avSpanport=='No'));
    frm.find("[name=avSpanport][value='N/A']").prop("checked",(setid.avSpanport=='N/A'));
    frm.find("[name=avOssec][value=Yes]").prop("checked",(setid.avOssec=='Yes'));
    frm.find("[name=avOssec][value=No]").prop("checked",(setid.avOssec=='No'));
    frm.find("[name=avOssec][value='N/A']").prop("checked",(setid.avOssec=='N/A'));
    frm.find("[name=antivirus][value=Yes]").prop("checked",(setid.antivirus=='Yes'));
    frm.find("[name=antivirus][value=No]").prop("checked",(setid.antivirus=='No'));
    frm.find("[name=antivirus][value='N/A']").prop("checked",(setid.antivirus=='N/A'));
    frm.find("[name=prtgIcmp][value=Yes]").prop("checked",(setid.prtgIcmp=='Yes'));
    frm.find("[name=prtgIcmp][value=No]").prop("checked",(setid.prtgIcmp=='No'));
    frm.find("[name=prtgIcmp][value='N/A']").prop("checked",(setid.prtgIcmp=='N/A'));
    frm.find("[name=prtgService][value=Yes]").prop("checked",(setid.prtgService=='Yes'));
    frm.find("[name=prtgService][value=No]").prop("checked",(setid.prtgService=='No'));
    frm.find("[name=prtgService][value='N/A']").prop("checked",(setid.prtgService=='N/A'));
    frm.find("[name=prtgServiceRemark]").val(setid.prtgServiceRemark);
    frm.find("[name=grafana][value=Yes]").prop("checked",(setid.grafana=='Yes'));
    frm.find("[name=grafana][value=No]").prop("checked",(setid.grafana=='No'));
    frm.find("[name=grafana][value='N/A']").prop("checked",(setid.grafana=='N/A'));


    frm.find("[name=companyId]").val(setid.companyId);
    frm.find("[name=companyId]").trigger("change");
    $("#modal_edit").modal('show');
}


function loadingBtn(el){
    $(el).prop('disabled', true);
    $(el).find(".fa-circle-notch").show();
}
function unloadingBtn(el){
    $(el).prop('disabled', false);
    $(el).find(".fa-circle-notch").hide();
}