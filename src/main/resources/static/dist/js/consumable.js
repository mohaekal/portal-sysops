


$(document).ready(function(){
    $("#frm_add").validate({
        rules: {
            name: {
                required: true,
            },
            consumableCategoryId: {
                required: true,
            },
            manufacturerId: {
                required: true,
            },accessoryStatusId: {
                required: true,
            },qty: {
                required: true,
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    $("#frm_edit").validate({
        rules: {
            name: {
                required: true,
            },
            consumableCategoryId: {
                required: true,
            },
            supplierId: {
                required: true,
            },
            manufacturerId: {
                required: true,
            },qty: {
                required: true,
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

})
function submitAdd(button){
    if ($("#frm_add").valid()){
        $.ajax({
            url: "/api/consumable",
            type:"post",
            data: $("#frm_add").serialize(),
            beforeSend: function () {
                loadingBtn(button);
            },
            success: function (res) {
                if (res.success) {
                    datarow.ajax.reload();
                    $("#frm_add").trigger("reset");
                } else {
                    alert("Sorry data cannot updated");
                }
            },
        })
            .fail(function () {
                alert("Sorry something wrong while processing");
            })
            .always(function () {
                $("#modal_add").modal("hide");
                unloadingBtn(button);
            });
    }
}

function submitUpdate(button){
    if($("#frm_edit").valid()){
        $.ajax({
            url: "/api/consumable",
            type:"put",
            data: $("#frm_edit").serialize(),
            beforeSend: function () {
                loadingBtn(button);
            },
            success: function (res) {
                if (res.success) {
                    datarow.ajax.reload();
                } else {
                    alert("Sorry data cannot updated");
                }
            },
        })
            .fail(function () {
                alert("Sorry something wrong while processing");
            })
            .always(function () {
                $("#loading").hide();
                $("#datas").show();
                $("#modal_edit").modal("hide");
                unloadingBtn(button);
            });
    }
}

function submitConsume(button){
    if($("#frm_consume").valid()){
        $.ajax({
            url: "/api/consumable/consume",
            type:"post",
            data: $("#frm_consume").serialize(),
            beforeSend: function () {
                loadingBtn(button);
            },
            success: function (res) {
                if (res.success) {
                    datarow.ajax.reload();
                } else {
                    alert("Sorry data cannot updated");
                }
            },
        }).fail(function () {
            alert("Sorry something wrong while processing");
        }).always(function () {
            $("#loading").hide();
            $("#modal_consume").modal("hide");
            unloadingBtn(button);
        });
    }
}


function deleteId(button,id) {
    var conf = confirm("Are you sure to delete this item ?");
    if(conf){
        $.ajax({
            url: "/api/consumable",
            type: "delete",
            data: {id:id},
            beforeSend: function () {
                loadingBtn(button);
            },
            success: function (res) {
                if (res.success) {
                    datarow.ajax.reload();
                } else {
                    alert("Sorry data cannot updated");
                }
            },
        }).fail(function () {
            alert("Sorry something wrong while processing");
        }).always(function () {
            unloadingBtn(button);
        });
    }
}


var datarow = $("#tbl_data").DataTable({
    ajax: {
        type:"get",
        url: "/api/consumable",
    },
    paging: true,
    lengthChange: true,
    searching: true,
    info: true,
    ordering:false,
    autoWidth: false,
    responsive: true,
    columns: [
        { data: 'id' },
        { data: 'name' },
        { data: 'category' },
        { data: 'manufacturer' },
        { data: 'location' },
        { data: 'room' },
        { data: 'model' },
        { data: 'prNumber' },
        { data: 'purchaseDateString' },
        { data: 'purchaseCost' },
        { data: 'qty' },
        { data: 'qtyUsed' }
    ],
    columnDefs: [
        {
            targets: 0,
            render: function (data, type, row, meta) {

                var btnDelete = '';
                var btnUse = '';
                if (privilege.filter(e=>e.authority=='ROLE_CONSUMABLE').length>0){
                    btnDelete= `<button onclick="deleteId(this,'`+row.id+`')" class="btn btn-outline-danger btn-flat btn-sm"><i class="fa fa-trash"></i></button> `;
                    btnUse = ` <button type="button" class="btn btn-primary btn-sm " onclick="use('`+row.id+`')"><i class="fa fa-share"></i></button> `
                }

                return `<button type="button" class="btn btn-primary btn-sm " onclick="func_update('`+row.id+`')"><i class="fa fa-external-link-alt"> `+row.id+`</i></button> `+btnDelete+btnUse+`
              <button type="button" class="btn btn-warning btn-sm " onclick="showdetail('`+row.id+`')">Detail</button>`;
            },
        },
        {
            targets: 11,
            render: function (data, type, row, meta) {
                return row.qty-row.qtyUsed;
            },
        }],
    initComplete: function () {
        var thisTable = this;
        var rowFilter = $('<tr class="filter"></tr>').appendTo(
            $(this.api().table().header())
        );

        this.api()
            .columns()
            .every(function () {
                var column = this;
                switch (column.index()) {
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        var select = $(
                            `<select class="form-control form-control-sm form-filter kt-input" title="Select" data-col-index="` +
                            column.index() +
                            `"><option value="">Select</option></select>`
                        )
                            .appendTo($("<th>").appendTo(rowFilter))
                            .on("change", function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                column
                                    .search(val ? "^" + val + "$" : "", true, false)
                                    // .search(val)
                                    .draw();
                            });

                        column.data().unique().sort().each(function (d, j) {
                            select.append(
                                '<option value="' + d + '">' + d + "</option>"
                            );
                        });

                        break;
                    case 1:
                    case 6:
                    case 7:
                        var input = $(
                            `<input class="form-control form-control-sm form-filter " title="Input" data-col-index="` +
                            column.index() +
                            `"/>`
                        )
                            .appendTo($("<th>").appendTo(rowFilter))
                            .on("keyup change", function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                column
                                    // .search(val ? "^" + val + "$" : "", true, false)
                                    .search(val)
                                    .draw();
                            });
                        break;
                    default:
                        var select = $("").appendTo($("<th>").appendTo(rowFilter));
                        break;
                }
            });

        var hideSearchColumnResponsive = function () {
            thisTable
                .api()
                .columns()
                .every(function () {
                    var column = this;
                    if (column.responsiveHidden()) {
                        $(".filter").find("th").eq(column.index()).show();
                    } else {
                        $(".filter").find("th").eq(column.index()).hide();
                    }
                });
        };

        // init on datatable load
        hideSearchColumnResponsive();
        // recheck on window resize
        window.onresize = hideSearchColumnResponsive;
    },
});

$('.dpicker').daterangepicker({
    singleDatePicker: true,
    locale: {
        format: 'YYYY-MM-DD'
    }
});


function func_update(id){
    var setid = datarow.rows().data().filter(e=>e.id==id)[0];
    var frm = $("#frm_edit");
    frm.find("[name=id]").val(setid.id);
    frm.find("[name=name]").val(setid.name);
    frm.find("[name=consumableCategoryId]").val(setid.consumableCategoryId);
    frm.find("[name=consumableCategoryId]").trigger("change");
    frm.find("[name=manufacturerId]").val(setid.manufacturerId);
    frm.find("[name=manufacturerId]").trigger("change");
    frm.find("[name=locationId]").val(setid.locationId);
    frm.find("[name=locationId]").trigger("change");
    frm.find("[name=roomId]").val(setid.roomId);
    frm.find("[name=roomId]").trigger("change");
    frm.find("[name=model]").val(setid.model);
    frm.find("[name=prNumber]").val(setid.prNumber);
    frm.find("[name=purchaseCost]").val(setid.purchaseCost);
    frm.find("[name=purchaseDate]").val(setid.purchaseDateString);
    frm.find("[name=qty]").val(setid.qty);
    $("#modal_edit").modal('show');
}




function use(id){
    var setid = datarow.rows().data().filter(e=>e.id==id)[0];
    var frm = $("#frm_consume");
    frm.find("[name=consumableId]").val(setid.id);
    frm.find("[name=qty]").val(setid.qty);
    $("#modal_consume").modal('show');
}



function showdetail(id){
    tbldetail.ajax.url('/api/consumable/detail?consumableId='+id).load();
    $('#modal_detail').modal('show');
}

var tbldetail = $("#tbl_detail").DataTable({
    paging: true,
    lengthChange: true,
    searching: true,
    ordering: false,
    info: true,
    autoWidth: false,
    processing:true,
    responsive: true, "pageLength": 100,
    ajax: {
        url: "/api/consumable/detail",
        type:"get"
    },
    columns: [
        { data: 'id' },
        { data: 'id' },
        { data: 'note' },
        { data: 'qty' }
    ],
    columnDefs: [
        {
            targets: 0,
            render: function (data, type, row, meta) {
                var btn = '';
                if (privilege.filter(e=>e.authority=='ROLE_CONSUMABLE').length>0){
                    btn = `<button type="button" class="btn btn-danger btn-sm " onclick="deletedetail('`+row.id+`')">Remove</button>`
                }

                return btn;
            },
        }
    ]
});

function deletedetail(id){
    $.ajax({
        url:"/api/consumable/detail",
        data:{id:id},
        type:"delete",
        success:function(res){
            // tbldetail.ajax.reload();
            location.reload();
        }
    }).fail(function(){
        alert('Remove fail');
    })
}

function loadingBtn(el){
    $(el).prop('disabled', true);
    $(el).find(".fa-circle-notch").show();
}
function unloadingBtn(el){
    $(el).prop('disabled', false);
    $(el).find(".fa-circle-notch").hide();
}