package com.dciindonesia.portalsysops.controller;

import com.dciindonesia.portalsysops.service.HttpConn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.IOException;

@Controller
@CrossOrigin
public class Alert {

        @GetMapping("/alert")
        public String windowsperf(){

            return "alert";
        }

    }
