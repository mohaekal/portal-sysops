package com.dciindonesia.portalsysops.controller.managementid.edge.api;

import com.dciindonesia.portalsysops.entity.mgmtidedge.LogChange;
import com.dciindonesia.portalsysops.entity.mgmtidedge.MsVariable;
import com.dciindonesia.portalsysops.model.Response;
import com.dciindonesia.portalsysops.repository.mgmtidedge.LogChangeEdgeRepo;
import com.dciindonesia.portalsysops.repository.mgmtiddci.ManagementIdConfigDCIRepo;
import com.dciindonesia.portalsysops.repository.mgmtidedge.MsVariableEdgeRepo;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/api/edge")
public class MsVariableApiEDGE {

    private static final Logger logger = LoggerFactory.getLogger("edge");

    @Autowired
    private MsVariableEdgeRepo msVariableRepo;
    @Autowired
    private LogChangeEdgeRepo logChangeRepo;
    @Autowired
    private ManagementIdConfigDCIRepo managementIdConfigRepo;

    @Autowired
    private UserManagementService userManagementService;
    private Gson gson = new Gson();

    @GetMapping("/master-variable")
    public Response getListMasterVariable(HttpServletRequest request){
//        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,null,null,"View Variable EDGE Menu");
        return new Response(true,msVariableRepo.findJoinAllMapped()) ;
    }

    @GetMapping("/master-variable/add")
    public Response addMsVar(HttpServletRequest request,@ModelAttribute("msvariable") MsVariable mv) throws JsonProcessingException {

        LogChange lc = new LogChange();
        lc.setLogAfter(gson.toJson(mv));
        logChangeRepo.save(lc);

        MsVariable res = msVariableRepo.save(mv);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,null,obj.writeValueAsString(res),"Add New Variable EDGE : "+res.getVariableName());
        return new Response((res!=null),res) ;
    }

    @GetMapping("/master-variable/update")
    public Response updateMsVar(HttpServletRequest request,@ModelAttribute("msvariable")  MsVariable mv) throws JsonProcessingException {
        LogChange lc = new LogChange();

//        Before
        MsVariable existingVariable =  msVariableRepo.findById(mv.getId()).orElse(null) ;
        MsVariable getBefore = existingVariable;
        lc.setLogBefore(gson.toJson(existingVariable));

//        After
        if (managementIdConfigRepo.findById(1).orElse(null).getValue().equals("1")){
            existingVariable.setVariableName(mv.getVariableName());
        }

        existingVariable.setBuildingId(mv.getBuildingId());
        existingVariable.setGroupId(mv.getGroupId());
        existingVariable.setVariableAttribute(mv.getVariableAttribute());
        existingVariable.setDeviceId(mv.getDeviceId());
        existingVariable.setLevelId(mv.getLevelId());
        existingVariable.setMeasurement(mv.getMeasurement());
        lc.setLogAfter(gson.toJson(existingVariable));
//        save log
        logChangeRepo.save(lc);

//        save data
        MsVariable res = msVariableRepo.save(existingVariable);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,obj.writeValueAsString(getBefore),obj.writeValueAsString(res),"Changes Variable EDGE : "+res.getVariableName());
        return new Response((res!=null),res) ;
    }







}
