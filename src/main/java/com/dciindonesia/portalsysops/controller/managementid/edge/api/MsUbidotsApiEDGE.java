package com.dciindonesia.portalsysops.controller.managementid.edge.api;

import com.dciindonesia.portalsysops.entity.mgmtidedge.LogChange;
import com.dciindonesia.portalsysops.entity.mgmtidedge.MsUbidots;
import com.dciindonesia.portalsysops.model.Response;
import com.dciindonesia.portalsysops.repository.mgmtidedge.LogChangeEdgeRepo;
import com.dciindonesia.portalsysops.repository.mgmtidedge.MsUbidotsEdgeRepo;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping(value = "/api/edge")
public class MsUbidotsApiEDGE {

    private static final Logger logger = LoggerFactory.getLogger(MsVariableApiEDGE.class);

    @Autowired
    private MsUbidotsEdgeRepo msUbidotsRepo;
    @Autowired
    private LogChangeEdgeRepo logChangeRepo;

    private Gson gson = new Gson();

    @GetMapping("/master-ubidots")
    public Response getListMasterUbidots(){

        return new Response(true,msUbidotsRepo.findAll() ) ;
    }

    @GetMapping("/master-ubidots/save")
    public Response saveMsUbidots(@ModelAttribute("msubidots") MsUbidots m){
        List<MsUbidots> s = msUbidotsRepo.findByVariableId(m.getVariableId());
        if (s.size() > 0){
            return updateMsUbidots(m);
        }else {
            return addMsUbidots(m);
        }
    }

    @GetMapping("/master-ubidots/add")
    public Response addMsUbidots(@ModelAttribute("msubidots") MsUbidots m){
        LogChange lc = new LogChange();
        lc.setLogAfter(gson.toJson(m));
        logChangeRepo.save(lc);

        MsUbidots res = msUbidotsRepo.save(m);
        return new Response((res!=null),res) ;

    }

    @GetMapping("/master-ubidots/update")
    public Response updateMsUbidots(@ModelAttribute("msubidots") MsUbidots m){

        LogChange lc = new LogChange();

//        Before
        MsUbidots existingData =  msUbidotsRepo.findByVariableId(m.getVariableId()).get(0) ;
        lc.setLogBefore(gson.toJson(existingData));

//        After
        existingData.setMacAddress(m.getMacAddress());
        existingData.setType(m.getType());
        existingData.setMaxvaluee(m.getMaxvaluee());
        existingData.setVariableId(m.getVariableId());
        existingData.setLastUpdate(LocalDateTime.now());
        lc.setLogAfter(gson.toJson(existingData));
//        save log
        logChangeRepo.save(lc);

//        save data
        MsUbidots res = msUbidotsRepo.save(existingData);
        return new Response((res!=null),res) ;
    }
}
