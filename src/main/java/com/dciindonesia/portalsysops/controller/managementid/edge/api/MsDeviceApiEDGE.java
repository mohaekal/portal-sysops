package com.dciindonesia.portalsysops.controller.managementid.edge.api;

import com.dciindonesia.portalsysops.config.JwtTokenForDeviceEdge;
import com.dciindonesia.portalsysops.entity.mgmtidedge.LogChange;
import com.dciindonesia.portalsysops.entity.mgmtidedge.MsDevice;
import com.dciindonesia.portalsysops.model.Response;
import com.dciindonesia.portalsysops.repository.mgmtidedge.LogChangeEdgeRepo;
import com.dciindonesia.portalsysops.repository.mgmtidedge.MsDeviceEdgeRepo;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Comparator;
import java.util.List;


@RestController
@RequestMapping(value = "/api/edge")
public class MsDeviceApiEDGE {

    private static final Logger logger = LoggerFactory.getLogger("devicedci");

    @Autowired
    private JwtTokenForDeviceEdge jwtTokenForDeviceEdge;
    @Autowired
    private MsDeviceEdgeRepo msDeviceEdgeRepo;
    @Autowired
    private LogChangeEdgeRepo logChangeEdgeRepo;

    private Gson gson = new Gson();

    @Autowired
    private UserManagementService userManagementService;
    @GetMapping("/master-device")
    public Response getListMasterDevice(HttpServletRequest request){

        List<MsDevice> g = msDeviceEdgeRepo.findAll();
        g.sort(Comparator.comparing(MsDevice::getId).reversed());
        userManagementService.track(request,null,null,"View Device Menu EDGE ");

        return new Response(true, g) ;
    }

    @PostMapping("/generate-token")
    public Response getToken(Long id){
        try {
            return new Response(true, jwtTokenForDeviceEdge.generateToken(String.valueOf(id)));
        }catch (Exception e){
            return new Response(false,e.getMessage());
        }
    }

    @GetMapping("/master-device/add")
    public Response addMsDevice(HttpServletRequest request,@ModelAttribute("msdevice") MsDevice m) throws JsonProcessingException {
        LogChange lc = new LogChange();
        lc.setLogAfter(gson.toJson(m));
        logChangeEdgeRepo.save(lc);

        MsDevice res = msDeviceEdgeRepo.save(m);
        res.setToken(jwtTokenForDeviceEdge.generateToken(String.valueOf(res.getId())));
        com.dciindonesia.portalsysops.entity.mgmtidedge.MsDevice res2 =msDeviceEdgeRepo.save(res);
        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,null,obj.writeValueAsString(res),"Add New Device EDGE : "+res.getDeviceName());

        return new Response((res2!=null),res2) ;

    }

    @GetMapping("/master-device/update")
    public Response updateMsDevice(HttpServletRequest request,@ModelAttribute("msdevice") MsDevice m) throws JsonProcessingException {

        LogChange lc = new LogChange();

//        Before
        MsDevice existingData =  msDeviceEdgeRepo.findById(m.getId()).orElse(null) ;
        MsDevice getBefore =  existingData;
        lc.setLogBefore(gson.toJson(existingData));

//        After
        existingData.setDeviceName(m.getDeviceName());
        existingData.setDeviceDesc(m.getDeviceDesc());
        existingData.setMacAddress(m.getMacAddress());
        existingData.setIp(m.getIp());
        existingData.setToken(m.getToken());
        lc.setLogAfter(gson.toJson(existingData));
//        save log
        logChangeEdgeRepo.save(lc);

//        save data
        MsDevice res = msDeviceEdgeRepo.save(existingData);
        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,obj.writeValueAsString(getBefore),obj.writeValueAsString(res),"Changes Device EDGE : "+res.getDeviceName());


        return new Response((res!=null),res) ;
    }

}
