package com.dciindonesia.portalsysops.controller.managementid.h2.api;

import com.dciindonesia.portalsysops.config.JwtTokenForDeviceH2;
import com.dciindonesia.portalsysops.entity.mgmtidh2.LogChange;
import com.dciindonesia.portalsysops.entity.mgmtidh2.MsDevice;
import com.dciindonesia.portalsysops.model.Response;
import com.dciindonesia.portalsysops.repository.mgmtidh2.LogChangeH2Repo;
import com.dciindonesia.portalsysops.repository.mgmtidh2.MsDeviceH2Repo;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Comparator;
import java.util.List;


@RestController
@RequestMapping(value = "/api/h2")
public class MsDeviceApiH2 {

    private static final Logger logger = LoggerFactory.getLogger("devicedci");
    @Autowired
    private JwtTokenForDeviceH2 jwtTokenForDeviceH2;
    @Autowired
    private MsDeviceH2Repo msDeviceH2Repo;
    @Autowired
    private LogChangeH2Repo logChangeH2Repo;

    private Gson gson = new Gson();
    @Autowired
    private UserManagementService userManagementService;

    @GetMapping("/master-device")
    public Response getListMasterDevice(HttpServletRequest request){

        List<MsDevice> g = msDeviceH2Repo.findAll();
        g.sort(Comparator.comparing(MsDevice::getId).reversed());
//        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,null,null,"View Device Menu H2");
        return new Response(true, g) ;
    }

    @PostMapping("/generate-token")
    public Response getToken(Long id){
        try {
            return new Response(true, jwtTokenForDeviceH2.generateToken(String.valueOf(id)));
        }catch (Exception e){
            return new Response(false,e.getMessage());
        }
    }

    @GetMapping("/master-device/add")
    public Response addMsDevice(HttpServletRequest request,@ModelAttribute("msdevice") MsDevice m) throws JsonProcessingException {
        LogChange lc = new LogChange();
        lc.setLogAfter(gson.toJson(m));
        logChangeH2Repo.save(lc);
        MsDevice res = msDeviceH2Repo.save(m);
        res.setToken(jwtTokenForDeviceH2.generateToken(String.valueOf(res.getId())));
        MsDevice res2 =msDeviceH2Repo.save(res);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,null,obj.writeValueAsString(res),"Add New Device H2 : "+res.getDeviceName());
        return new Response((res!=null),res2) ;

    }

    @GetMapping("/master-device/update")
    public Response updateMsDevice(HttpServletRequest request,@ModelAttribute("msdevice") MsDevice m) throws JsonProcessingException {

        LogChange lc = new LogChange();

//        Before
        MsDevice existingData =  msDeviceH2Repo.findById(m.getId()).orElse(null) ;
        MsDevice getBefore = existingData;
        lc.setLogBefore(gson.toJson(existingData));

//        After
        existingData.setDeviceName(m.getDeviceName());
        existingData.setDeviceDesc(m.getDeviceDesc());
        existingData.setMacAddress(m.getMacAddress());
        existingData.setIp(m.getIp());
        existingData.setToken(m.getToken());
        lc.setLogAfter(gson.toJson(existingData));
//        save log
        logChangeH2Repo.save(lc);

//        save data
        MsDevice res = msDeviceH2Repo.save(existingData);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,obj.writeValueAsString(getBefore),obj.writeValueAsString(res),"Changes Device H2 : "+res.getDeviceName());
        return new Response((res!=null),res) ;
    }

}
