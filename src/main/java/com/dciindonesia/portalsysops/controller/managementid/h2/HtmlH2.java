package com.dciindonesia.portalsysops.controller.managementid.h2;

import com.dciindonesia.portalsysops.repository.mgmtiddci.ManagementIdConfigDCIRepo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/management-id/h2")
public class HtmlH2 {

    private static final Logger logger = LoggerFactory.getLogger("html");

    @Autowired
    private ManagementIdConfigDCIRepo managementIdConfigRepo;

    @GetMapping("/variable")
    public String variable(Model model) throws JsonProcessingException {
        String val = managementIdConfigRepo.findById(1).orElse(null).getValue();
        ObjectMapper obj = new ObjectMapper();
        model.addAttribute("privilege",obj.writeValueAsString(
                SecurityContextHolder.getContext().getAuthentication().getAuthorities()));
        model.addAttribute("variable_name_can_edit",val );
        return "managementid/h2/variable";
    }

    @GetMapping("/group-variable")
    public String groupvariable(Model model) throws JsonProcessingException {

        ObjectMapper obj = new ObjectMapper();
        model.addAttribute("privilege",obj.writeValueAsString(
                SecurityContextHolder.getContext().getAuthentication().getAuthorities()));
        return "managementid/h2/groupvariable";
    }

    @GetMapping("/device")
    public String device(Model model) throws JsonProcessingException {

        ObjectMapper obj = new ObjectMapper();
        model.addAttribute("privilege",obj.writeValueAsString(
                SecurityContextHolder.getContext().getAuthentication().getAuthorities()));
        return "managementid/h2/device";
    }


    @GetMapping("/level")
    public String level(Model model) throws JsonProcessingException {

        ObjectMapper obj = new ObjectMapper();
        model.addAttribute("privilege",obj.writeValueAsString(
                SecurityContextHolder.getContext().getAuthentication().getAuthorities()));
        return "managementid/h2/level";
    }
}
