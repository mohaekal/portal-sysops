package com.dciindonesia.portalsysops.controller.managementid.h2.api;

import com.dciindonesia.portalsysops.entity.mgmtidh2.LogChange;
import com.dciindonesia.portalsysops.entity.mgmtidh2.MsGroupVariable;
import com.dciindonesia.portalsysops.model.Response;
import com.dciindonesia.portalsysops.repository.mgmtidh2.LogChangeH2Repo;
import com.dciindonesia.portalsysops.repository.mgmtidh2.MsGroupVariableH2Repo;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Comparator;
import java.util.List;


@RestController
@RequestMapping(value = "/api/h2")
public class MsGroupVariableApiH2 {

    private static final Logger logger = LoggerFactory.getLogger("groupvardci");

    @Autowired
    private MsGroupVariableH2Repo msGroupVariableRepo;
    @Autowired
    private LogChangeH2Repo logChangeRepo;
    @Autowired
    private UserManagementService userManagementService;

    private Gson gson = new Gson();
    @GetMapping("/master-group-variable")
    public Response getListMasterGroupVariable(HttpServletRequest request){

        List<MsGroupVariable> g = msGroupVariableRepo.findAll();
        g.sort(Comparator.comparing(MsGroupVariable::getId).reversed());

//        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,null,null,"View Group Variable Menu H2 ");
        return new Response(true, g) ;
    }

    @GetMapping("/master-group-variable/add")
    public Response addMsGroupVar(HttpServletRequest request,@ModelAttribute("msgroupvariable") MsGroupVariable m) throws JsonProcessingException {

        LogChange lc = new LogChange();
        lc.setLogAfter(gson.toJson(m));
        logChangeRepo.save(lc);

        MsGroupVariable res = msGroupVariableRepo.save(m);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,null,obj.writeValueAsString(res),"Add New Group Variable H2 : "+res.getGroupNm());
        return new Response((res!=null),res) ;


    }

    @GetMapping("/master-group-variable/update")
    public Response updateMsGroupVar(HttpServletRequest request,@ModelAttribute("msgroupvariable") MsGroupVariable m) throws JsonProcessingException {

        LogChange lc = new LogChange();

//        Before
        MsGroupVariable existingData =  msGroupVariableRepo.findById(m.getId()).orElse(null) ;
        MsGroupVariable getBefore =existingData;
        lc.setLogBefore(gson.toJson(existingData));

//        After
        existingData.setGroupNm(m.getGroupNm());
        existingData.setGroupDesc(m.getGroupDesc());
        lc.setLogAfter(gson.toJson(existingData));
//        save log
        logChangeRepo.save(lc);

//        save data
        MsGroupVariable res = msGroupVariableRepo.save(existingData);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,obj.writeValueAsString(getBefore),obj.writeValueAsString(res),"Changes Group Variable H2 : "+res.getGroupNm());
        return new Response((res!=null),res) ;

    }
}
