package com.dciindonesia.portalsysops.controller.managementid.dci.api;

import com.dciindonesia.portalsysops.entity.mgmtiddci.LogChange;
import com.dciindonesia.portalsysops.entity.mgmtiddci.MsDevice;
import com.dciindonesia.portalsysops.model.Response;
import com.dciindonesia.portalsysops.repository.mgmtiddci.LogChangeDCIRepo;
import com.dciindonesia.portalsysops.repository.mgmtiddci.MsDeviceDCIRepo;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Comparator;
import java.util.List;


@RestController
@RequestMapping(value = "/api/dci")
public class MsDeviceApiDCI {

    private static final Logger logger = LoggerFactory.getLogger("devicedci");

    @Autowired
    private MsDeviceDCIRepo msDeviceDCIRepo;
    @Autowired
    private LogChangeDCIRepo logChangeDCIRepo;

    private Gson gson = new Gson();
    @Autowired
    private UserManagementService userManagementService;
    @GetMapping("/master-device")
    public Response getListMasterDevice(HttpServletRequest request ){

        List<MsDevice> g = msDeviceDCIRepo.findAll();
        g.sort(Comparator.comparing(MsDevice::getId).reversed());
//        ObjectMapper obj  = new ObjectMapper();
        userManagementService.track(request,null,null,"View Device Menu DCI ");

        return new Response(true, g) ;
    }

    @GetMapping("/master-device/add")
    public Response addMsDevice(HttpServletRequest request,@ModelAttribute("msdevice") MsDevice m) throws JsonProcessingException {
        LogChange lc = new LogChange();
        lc.setLogAfter(gson.toJson(m));
        logChangeDCIRepo.save(lc);
        MsDevice res = msDeviceDCIRepo.save(m);

        ObjectMapper obj =  new ObjectMapper();
        userManagementService.track(request,null,obj.writeValueAsString(res),"Add New Device DCI : "+ res.getDeviceName());

        return new Response((res!=null),res) ;

    }

    @GetMapping("/master-device/update")
    public Response updateMsDevice(HttpServletRequest request,@ModelAttribute("msdevice") MsDevice m) throws JsonProcessingException {

        LogChange lc = new LogChange();

//        Before
        MsDevice existingData =  msDeviceDCIRepo.findById(m.getId()).orElse(null) ;
        MsDevice getBefore = existingData;
        lc.setLogBefore(gson.toJson(existingData));

//        After
        existingData.setDeviceName(m.getDeviceName());
        existingData.setDeviceDesc(m.getDeviceDesc());
        existingData.setMacAddress(m.getMacAddress());
        existingData.setIp(m.getIp());
        lc.setLogAfter(gson.toJson(existingData));
//        save log
        logChangeDCIRepo.save(lc);

//        save data
        MsDevice res = msDeviceDCIRepo.save(existingData);

        ObjectMapper obj =  new ObjectMapper();
        userManagementService.track(request,obj.writeValueAsString(getBefore),obj.writeValueAsString(res),"Changes Device DCI : "+ res.getDeviceName());

        return new Response((res!=null),res) ;
    }

}
