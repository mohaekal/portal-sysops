package com.dciindonesia.portalsysops.controller.managementid.h2.api;

import com.dciindonesia.portalsysops.entity.mgmtidh2.LogChange;
import com.dciindonesia.portalsysops.entity.mgmtidh2.MsLevel;
import com.dciindonesia.portalsysops.model.Response;
import com.dciindonesia.portalsysops.repository.mgmtidh2.LogChangeH2Repo;
import com.dciindonesia.portalsysops.repository.mgmtidh2.MsLevelH2Repo;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Comparator;
import java.util.List;


@RestController
@RequestMapping(value = "/api/h2")
public class MsLevelApiH2 {

    private static final Logger logger = LoggerFactory.getLogger(MsVariableApiH2.class);

    @Autowired
    private LogChangeH2Repo logChangeRepo;


    @Autowired
    private MsLevelH2Repo msLevelRepo;
    @Autowired
    private UserManagementService userManagementService;

    private Gson gson = new Gson();

    @GetMapping("/master-level")
    public Response getListMasterLevel(HttpServletRequest request){

        List<MsLevel> g = msLevelRepo.findAll();
        g.sort(Comparator.comparing(MsLevel::getId).reversed());
//        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,null,null,"View Level Menu H2");
        return new Response(true,g ) ;
    }

    @GetMapping("/master-level/add")
    public Response addMsLevel(HttpServletRequest request,@ModelAttribute("mslevel") MsLevel m) throws JsonProcessingException {
        LogChange lc = new LogChange();
        lc.setLogAfter(gson.toJson(m));
        logChangeRepo.save(lc);

        MsLevel res = msLevelRepo.save(m);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,null,obj.writeValueAsString(res),"Add New Level H2 : "+res.getLevelName());
        return new Response((res!=null),res) ;

    }

    @GetMapping("/master-level/update")
    public Response updateMsDevice(HttpServletRequest request,@ModelAttribute("mslevel") MsLevel m) throws JsonProcessingException {

        LogChange lc = new LogChange();

//        Before
        MsLevel existingData =  msLevelRepo.findById(m.getId()).orElse(null) ;
        MsLevel getBefore = existingData;
        lc.setLogBefore(gson.toJson(existingData));

//        After
        existingData.setLevelName(m.getLevelName());
        lc.setLogAfter(gson.toJson(existingData));
//        save log
        logChangeRepo.save(lc);

//        save data
        MsLevel res = msLevelRepo.save(existingData);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,obj.writeValueAsString(getBefore),obj.writeValueAsString(res),"Changes Level H2 : "+res.getLevelName());
        return new Response((res!=null),res) ;
    }
}
