package com.dciindonesia.portalsysops.controller.managementid.h2.api;

import com.dciindonesia.portalsysops.entity.mgmtidh2.MsBuilding;
import com.dciindonesia.portalsysops.entity.mgmtidh2.MsDevice;
import com.dciindonesia.portalsysops.entity.mgmtidh2.MsGroupVariable;
import com.dciindonesia.portalsysops.entity.mgmtidh2.MsLevel;
import com.dciindonesia.portalsysops.model.Response;
import com.dciindonesia.portalsysops.model.Select2Response;
import com.dciindonesia.portalsysops.repository.mgmtidh2.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping(value = "/api/h2")
public class Select2ApiH2 {
    @Autowired
    private MsVariableH2Repo msVariableRepo;
    @Autowired
    private MsDeviceH2Repo msDeviceRepo;
    @Autowired
    private MsGroupVariableH2Repo msGroupVariableRepo;
    @Autowired
    private MsBuildingH2Repo msBuildingRepo;
    @Autowired
    private MsLevelH2Repo msLevelRepo;
    @Autowired
    private MsUbidotsH2Repo msUbidotsRepo;

    @GetMapping("/select2-group-variable")
    public Response getGroupVariableSelect2(){
        List<Select2Response> srs = new ArrayList<>();
        for (MsGroupVariable gr: msGroupVariableRepo.findAll()
        ) {
            srs.add(new Select2Response(gr.getId(),gr.getGroupNm()));
        }
        return new Response(true,srs);
    }

    @GetMapping("/select2-device")
    public Response getDeviceSelect2(){
        List<Select2Response> srs = new ArrayList<>();
        for (MsDevice gr: msDeviceRepo.findAll()
        ) {
            srs.add(new Select2Response(gr.getId(),gr.getDeviceName()));
        }
        return new Response(true,srs);
    }

    @GetMapping("/select2-building")
    public Response getBuildingSelect2(){
        List<Select2Response> srs = new ArrayList<>();
        for (MsBuilding gr: msBuildingRepo.findAll().stream().filter(c->c.getActive().equals(true)).collect(Collectors.toList())
        ) {
            srs.add(new Select2Response(gr.getId(),gr.getBuildingId()));
        }
        return new Response(true,srs);
    }

    @GetMapping("/select2-level")
    public Response getLevelSelect2(){
        List<Select2Response> srs = new ArrayList<>();
        for (MsLevel gr: msLevelRepo.findAll()
        ) {
            srs.add(new Select2Response(gr.getId(),gr.getLevelName()));
        }
        return new Response(true,srs);
    }
}
