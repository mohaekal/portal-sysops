package com.dciindonesia.portalsysops.controller.api;

import com.dciindonesia.portalsysops.model.ElasticResponse;
import com.dciindonesia.portalsysops.model.Response;
import com.dciindonesia.portalsysops.model.elastic.Source;
import com.dciindonesia.portalsysops.service.LastValueElastic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class LoadData {

    @Autowired
    LastValueElastic lastValueElastic;

    @GetMapping("/api/get-last-value")
    public Response data() throws IOException {

        List<Source> source = lastValueElastic.get().hits.hits.stream()
                .map(e->new Source(e))
                .collect(Collectors.toList());
        return new Response(true, source);
    }
}
