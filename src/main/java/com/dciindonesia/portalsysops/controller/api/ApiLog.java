package com.dciindonesia.portalsysops.controller.api;

import com.dciindonesia.portalsysops.model.InterfaceUser;
import com.dciindonesia.portalsysops.model.Response;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.repository.portalmanagement.LogsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/user-management/log")
public class ApiLog {


    @Autowired
    private LogsRepo logsRepo;


    @GetMapping("")
    public Response getAllActive(){
        List<InterfaceUser> data = logsRepo.findAllUser();
        return new Response(true, data);
    }



}
