package com.dciindonesia.portalsysops.controller.api;

import com.dciindonesia.portalsysops.entity.assetmgmt.LogConsumable;
import com.dciindonesia.portalsysops.entity.assetmgmt.MsConsumable;
import com.dciindonesia.portalsysops.model.Response;
import com.dciindonesia.portalsysops.repository.assetmgmt.LogConsumableRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsConsumableRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/consumable")
public class ApiConsumable {

    @Autowired
    private LogConsumableRepo logConsumableRepo;

    @Autowired
    private MsUserRepo msUserRepo;

    @Autowired
    private MsConsumableRepo msConsumableRepo;

    @Autowired
    private UserManagementService userManagementService;

    @GetMapping
    public Response getAllData(HttpServletRequest request){


        userManagementService.track(request,null,null,"View Consumable ");

        return new Response(true,msConsumableRepo.findJoinActive());
    }

    @PostMapping
    public Response postData(HttpServletRequest request, MsConsumable msConsumable) throws JsonProcessingException {
        String email = request.getUserPrincipal().getName();
        msConsumable.setActive(true);
        msConsumable.setCreateDate(LocalDateTime.now());
        msConsumable.setCreateBy(msUserRepo.findByEmail(email).getId());
        MsConsumable sv = msConsumableRepo.save(msConsumable);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,null,obj.writeValueAsString(sv),"Add New Consumable "+sv.getName());
        return new Response(true,sv);
    }

    @PutMapping
    public  Response updateData(HttpServletRequest request, MsConsumable msConsumable) throws JsonProcessingException {

        MsConsumable getdt = msConsumableRepo.findById(msConsumable.getId()).get();
        MsConsumable getBefore = getdt;
        msConsumable.setCreateDate(getdt.getCreateDate());
        msConsumable.setCreateBy(getdt.getCreateBy());
        msConsumable.setActive(true);
        msConsumable.setUpdateDate(LocalDateTime.now());
        msConsumable.setUpdateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
        MsConsumable sv = msConsumableRepo.save(msConsumable);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,obj.writeValueAsString(getBefore),obj.writeValueAsString(sv),"Consumable consume "+sv.getName());
        return new Response(true,sv);
    }

    @DeleteMapping
    public Response deleteData(HttpServletRequest request , MsConsumable msConsumable){
        MsConsumable getdt = msConsumableRepo.findById(msConsumable.getId()).get();
        getdt.setActive(false);
        getdt.setUpdateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
        getdt.setUpdateDate(LocalDateTime.now());
        MsConsumable sv = msConsumableRepo.save(getdt);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,null,null,"Delete consumable "+sv.getName());
        return new Response(true,sv);
    }

    @PostMapping("/consume")
    public Response consumes(HttpServletRequest request,Long consumableId, String note, Integer qty) throws JsonProcessingException {

        MsConsumable ma = msConsumableRepo.findById(consumableId).orElse(null);
        List<LogConsumable> lcs = logConsumableRepo.findByConsumableId(consumableId);

        Integer qtyUsed = 0 ;
        for (LogConsumable lc:lcs
        ) {
            qtyUsed=qtyUsed+lc.getQty();
        }

        if (ma.getQty()-qtyUsed-qty >= 0 ){
            LogConsumable newdata = new LogConsumable();
            newdata.setConsumableId(consumableId);
            newdata.setActive(true);
            newdata.setCreateDate(LocalDateTime.now());
            newdata.setQty(qty);
            newdata.setNote(note);
            LogConsumable sv = logConsumableRepo.save(newdata);

            ObjectMapper obj = new ObjectMapper();
            userManagementService.track(request,null,obj.writeValueAsString(sv),"Consumable consume "+sv.getNote());
            return new Response(true,sv);
        }
        return new Response( false,"Qty is not enough");
    }

    @GetMapping("/detail")
    public Response getAll(HttpServletRequest request, Long consumableId){
        List<LogConsumable> data =  logConsumableRepo.findByConsumableId(consumableId);

        return new Response(true,data);
    };

    @DeleteMapping("/detail")
    public Response delete(HttpServletRequest request, Long id){

        LogConsumable data =  logConsumableRepo.findById(id).orElse(null);
        data.setActive(false);
        data.setUpdateDate(LocalDateTime.now());
        data.setUpdateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());

        return new Response(true,logConsumableRepo.save(data));
    };
}
