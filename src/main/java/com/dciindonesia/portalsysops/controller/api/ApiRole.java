package com.dciindonesia.portalsysops.controller.api;

import com.dciindonesia.portalsysops.entity.portalmanagement.MsAccess;
import com.dciindonesia.portalsysops.entity.portalmanagement.MsRole;
import com.dciindonesia.portalsysops.model.Response;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.repository.portalmanagement.MsRoleRepo;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@RestController
@RequestMapping("/api/user-management/role")
public class ApiRole {
        @Autowired
        private MsRoleRepo msRoleRepo;

        @Autowired
        private MsUserRepo msUserRepo;

        @Autowired
        private UserManagementService userManagementService;


        @GetMapping("")
        public Response getAllActive(HttpServletRequest request){
            List<MsRole> data = msRoleRepo.findAll();
            Collections.sort(data, Comparator.comparingLong(MsRole::getId).reversed());
            userManagementService.track(request,"-","-","View Role Menu");
            return new Response(true, data);
        }

        @PostMapping("")
        public Response store(HttpServletRequest request, MsRole msRole){
            try {
                msRole.setActive(true);
                msRole.setCreateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
                msRole.setCreateDate(LocalDateTime.now());

                ObjectMapper obj = new ObjectMapper();
                userManagementService.track(request,"-",obj.writeValueAsString(msRole),"Add Role "+msRole.getName());
                return new Response(true,msRoleRepo.save(msRole));
            }catch (Exception e){
                return new Response(false,null);
            }
        }

        @PutMapping("")
        public Response update(HttpServletRequest request, MsRole msRole){
            try {

                MsRole getExist = msRoleRepo.findById(msRole.getId()).get();
                MsRole getBefore = getExist;
                getExist.setName(msRole.getName());
                getExist.setUpdateDate(LocalDateTime.now());
                getExist.setUpdateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
                MsRole sv = msRoleRepo.save(getExist);

                ObjectMapper obj = new ObjectMapper();
                userManagementService.track(request,obj.writeValueAsString(getBefore),obj.writeValueAsString(sv),"Changes Role : "+msRole.getName());

                return new Response(true,sv);
            }catch (Exception e){
                return new Response(false, e.getMessage());
            }
        }

        @DeleteMapping("")
        public Response delete(HttpServletRequest request, MsRole msRole) throws JsonProcessingException {
            MsRole getExist = msRoleRepo.findById(msRole.getId()).get();
            MsRole getBefore = getExist;
            getExist.setActive(false);
            MsRole sv = msRoleRepo.save(getExist);
            ObjectMapper obj = new ObjectMapper();
            userManagementService.track(request,obj.writeValueAsString(getBefore),obj.writeValueAsString(sv),"Delete User "+getExist.getName());

            return new Response(true,sv);
        }
    }
