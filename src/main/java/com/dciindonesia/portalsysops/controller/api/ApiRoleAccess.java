package com.dciindonesia.portalsysops.controller.api;

import com.dciindonesia.portalsysops.entity.portalmanagement.MsAccess;
import com.dciindonesia.portalsysops.entity.portalmanagement.MsRole;
import com.dciindonesia.portalsysops.entity.portalmanagement.RoleAccess;
import com.dciindonesia.portalsysops.model.InterfaceAccess;
import com.dciindonesia.portalsysops.model.Response;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.repository.portalmanagement.MsAccessRepo;
import com.dciindonesia.portalsysops.repository.portalmanagement.MsRoleRepo;
import com.dciindonesia.portalsysops.repository.portalmanagement.RoleAccessRepo;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@RestController
@RequestMapping("/api/user-management/role-access")
public class ApiRoleAccess {
    @Autowired
    private MsRoleRepo msRoleRepo;

    @Autowired
    private MsAccessRepo msAccessRepo;

    @Autowired
    private RoleAccessRepo roleAccessRepo;

    @Autowired
    private MsUserRepo msUserRepo;

    @Autowired
    private UserManagementService userManagementService;

    @GetMapping("/get-role")
    public Response getRoleActive(){

        List<MsRole> roles =msRoleRepo.findAllActive();

        return new Response(true, roles);
    }

    @GetMapping("/get-access-by-role")
    public Response getAllActive(HttpServletRequest request, Long roleid){
        List<InterfaceAccess> accesses = msAccessRepo.findAllAccessByRole(roleid);

        userManagementService.track(request,"-","-","View Role Access Menu");
        return new Response(true, accesses);
    }

    @PutMapping("/update-role-access")
    public Response updateAccess(HttpServletRequest request, Long roleId, Long accessId,Boolean enable ){
        try {
            List<RoleAccess> ra = roleAccessRepo.findRoleAccessByRoleIdAndAccessId(roleId,accessId);
            if (enable && ra.size()==0){
                RoleAccess newRoleAccess = new RoleAccess();
                newRoleAccess.setAccessId(accessId);
                newRoleAccess.setRoleId(roleId);
                newRoleAccess.setCreateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
                newRoleAccess.setCreateDate(LocalDateTime.now());
                RoleAccess raz = roleAccessRepo.save(newRoleAccess);

                ObjectMapper obj = new ObjectMapper();
                userManagementService.track(request,"-",obj.writeValueAsString(raz),"Changes Role Access ");

                return new Response(true,"Access Added");
            }else{
                roleAccessRepo.deleteAll(ra);
                return new Response(true,"Access Removed");
            }
        }catch (Exception e){
            return new Response(false,e.getMessage());
        }
    }

}
