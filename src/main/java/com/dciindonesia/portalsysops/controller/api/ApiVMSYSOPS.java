package com.dciindonesia.portalsysops.controller.api;

import com.dciindonesia.portalsysops.entity.assetmgmt.IpVMDetail;
import com.dciindonesia.portalsysops.entity.assetmgmt.IpVm;
import com.dciindonesia.portalsysops.entity.assetmgmt.MsVM;
import com.dciindonesia.portalsysops.model.RequestMsVM;
import com.dciindonesia.portalsysops.model.Response;
import com.dciindonesia.portalsysops.repository.assetmgmt.IpVMDetailRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.IpVMRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsVMRepo;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/vm/sysops")
public class ApiVMSYSOPS {

    @Autowired
    private MsVMRepo msVMRepo;

    @Autowired
    private MsUserRepo msUserRepo;

    @Autowired
    private IpVMDetailRepo ipVMDetailRepo;

    @Autowired
    private IpVMRepo ipVMRepo;
    @Autowired
    private UserManagementService userManagementService;


    @GetMapping("")
    public Response get(HttpServletRequest request){

        List<IpVMDetail> ipdet = ipVMDetailRepo.findAll();
        List<IpVm> msip = ipVMRepo.findAll();

        List<RequestMsVM> reqsVm = msVMRepo.findByCategoryId(Long.valueOf(1)).stream().map(e->new RequestMsVM(e,ipdet,msip)).collect(Collectors.toList());
        userManagementService.track(request,"-","-","View VM SYSOPS");
        return new Response(true, reqsVm );
    }

    @PostMapping("")
    public Response store(HttpServletRequest request, RequestMsVM requestMsVM){
        try {
            MsVM msVM = new MsVM(requestMsVM);
            msVM.setCreateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
            msVM.setCreateDate(LocalDateTime.now());
            msVM.setActive(true);
            msVM.setCategoryId(Long.valueOf(1));
            MsVM setMsVm = msVMRepo.save(msVM);
            List<IpVMDetail> ipdet = ipVMDetailRepo.findAllByvmid(setMsVm.getId());
            ipVMDetailRepo.deleteAll(ipdet);
            try {
                for (Long ip:requestMsVM.getIps()
                ) {
                    IpVMDetail newIp = new IpVMDetail();
                    newIp.setVmId(setMsVm.getId());
                    newIp.setIpVmId(ip);
                    newIp.setCreateDate(LocalDateTime.now());
                    newIp.setCreateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
                    ipVMDetailRepo.save(newIp);
                }
            }catch (Exception e){}

            ObjectMapper obj = new ObjectMapper();

            userManagementService.track(request,"-",obj.writeValueAsString(setMsVm),"Add VM SYSOPS");

            return new Response(true, setMsVm);
        }catch (Exception e){
            return new Response(false,e.getMessage());
        }
    }


    @PutMapping("")
    public Response update(HttpServletRequest request, RequestMsVM requestMsVM){
        try {
            MsVM msVM = new MsVM(requestMsVM);
            MsVM existmsVM = msVMRepo.findById(requestMsVM.getId()).get();

            msVM.setCreateBy(existmsVM.getCreateBy());
            msVM.setCreateDate(existmsVM.getCreateDate());
            msVM.setUpdateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
            msVM.setUpdateDate(LocalDateTime.now());
            msVM.setActive(true);
            msVM.setCategoryId(Long.valueOf(1));
            MsVM setMsVm = msVMRepo.save(msVM);

            List<IpVMDetail> ipdet = ipVMDetailRepo.findAllByvmid(setMsVm.getId());
            ipVMDetailRepo.deleteAll(ipdet);
            try {
                for (Long ip:requestMsVM.getIps()
                ) {
                    IpVMDetail newIp = new IpVMDetail();
                    newIp.setIpVmId(ip);
                    newIp.setVmId(setMsVm.getId());
                    newIp.setCreateDate(LocalDateTime.now());
                    newIp.setCreateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
                    ipVMDetailRepo.save(newIp);
                }
            }catch (Exception e){}


            ObjectMapper obj = new ObjectMapper();
            userManagementService.track(request,obj.writeValueAsString(existmsVM),obj.writeValueAsString(setMsVm),"Changes VM SYSOPS");

            return new Response(true,setMsVm );
        }catch (Exception e){
            return new Response(false,e.getMessage());
        }
    }

    @DeleteMapping("")
    public Response delete(HttpServletRequest request, Long id){
        try {

            MsVM existmsVM = msVMRepo.findById(id).get();

            existmsVM.setUpdateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
            existmsVM.setUpdateDate(LocalDateTime.now());
            existmsVM.setActive(false);
            MsVM setMsVm = msVMRepo.save(existmsVM);

            List<IpVMDetail> ipdet = ipVMDetailRepo.findAllByvmid(setMsVm.getId());
            ipVMDetailRepo.deleteAll(ipdet);

            userManagementService.track(request,"-","-","Delete VM SYSOPS");

            return new Response(true,setMsVm );
        }catch (Exception e){
            return new Response(false,e.getMessage());
        }
    }
}
