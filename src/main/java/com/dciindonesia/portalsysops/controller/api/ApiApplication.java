package com.dciindonesia.portalsysops.controller.api;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsApplication;
import com.dciindonesia.portalsysops.model.Response;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsApplicationRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@RestController
@RequestMapping("/api/application")
public class ApiApplication {

    @Autowired
    private MsApplicationRepo msApplicationRepo;

    @Autowired
    private UserManagementService userManagementService;

    @Autowired
    private MsUserRepo msUserRepo;
    @GetMapping
    public Response getAllData(HttpServletRequest request){
        userManagementService.track(request,"-","-","View Application Menu ");
        return  new Response(true,msApplicationRepo.findJoinActive());
    }

    @PostMapping
    public Response postData(HttpServletRequest request, MsApplication msApplication) throws JsonProcessingException {

        msApplication.setActive(true);
        msApplication.setCreateDate(LocalDateTime.now());
        msApplication.setCreateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
        MsApplication sv = msApplicationRepo.save(msApplication);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,"-",obj.writeValueAsString(sv),"Add New Application "+sv.getName());

        return new Response(true,sv);
    }

    @PutMapping
    public Response updateData(HttpServletRequest request,MsApplication msApplication) throws JsonProcessingException {

        MsApplication getdt = msApplicationRepo.findById(msApplication.getId()).get();
        MsApplication getBefore = getdt;
        msApplication.setCreateDate(getdt.getCreateDate());
        msApplication.setCreateBy(getdt.getCreateBy());
        msApplication.setActive(true);
        msApplication.setUpdateDate(LocalDateTime.now());
        msApplication.setUpdateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
        MsApplication sv= msApplicationRepo.save(msApplication);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,obj.writeValueAsString(getBefore),obj.writeValueAsString(sv),"Changes Application "+sv.getName());

        return new Response(true,sv);
    }

    @DeleteMapping
    public Response deleteData(HttpServletRequest request,MsApplication msApplication){
        String email = request.getUserPrincipal().getName();
        MsApplication getdt = msApplicationRepo.findById(msApplication.getId()).get();
        getdt.setActive(false);
        getdt.setUpdateBy(msUserRepo.findByEmail(email).getId());
        getdt.setUpdateDate(LocalDateTime.now());
        MsApplication sv = msApplicationRepo.save(getdt);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,"-","-","Delete Application "+sv.getName());

        return new Response(true,sv);
    }

}
