package com.dciindonesia.portalsysops.controller.api;

import com.dciindonesia.portalsysops.entity.assetmgmt.LicenseFile;
import com.dciindonesia.portalsysops.entity.assetmgmt.MsLicense;
import com.dciindonesia.portalsysops.model.RequestLicense;
import com.dciindonesia.portalsysops.model.Response;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsLicenseRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.service.LicenseStorageFile;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.time.LocalDateTime;

@RestController
@RequestMapping("/api/license")
public class ApiLicense {


    @Autowired
    private MsUserRepo msUserRepo;
    private Logger log = LoggerFactory.getLogger("API License");

    @Autowired
    private UserManagementService userManagementService;

    @Autowired
    private MsLicenseRepo msLicenseRepo;

    @Autowired
    private LicenseStorageFile licenseStorageFile;

    @GetMapping("")
    private Response getAllData(HttpServletRequest request){

        userManagementService.track(request,null,null,"View License");

        return new Response(true,msLicenseRepo.getDetail());
    }

    @PostMapping("")
    private Response postData(HttpServletRequest request, RequestLicense requestLicense) throws IOException {

        MsLicense msLicense = new MsLicense(requestLicense);


        msLicense.setActive(true);
        msLicense.setCreateDate(LocalDateTime.now());
        msLicense.setCreateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
        MsLicense svx = msLicenseRepo.save(msLicense);

        LicenseFile lf = licenseStorageFile.store(requestLicense.getLicenseDoc(),svx.getId());
        svx.setLicenseFileId(lf.getId());
        MsLicense sv = msLicenseRepo.save(svx);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,null,obj.writeValueAsString(sv),"Add New License "+sv.getName());

        return new Response(true,sv);
    }

    @PutMapping("")
    public Response updateData(HttpServletRequest request,RequestLicense requestLicense) throws IOException {

        MsLicense msLicense = new MsLicense(requestLicense);

        MsLicense getdt = msLicenseRepo.findById(msLicense.getId()).get();
        MsLicense getBefore = getdt;

        msLicense.setCreateDate(getdt.getCreateDate());
        msLicense.setCreateBy(getdt.getCreateBy());
        msLicense.setActive(true);
        msLicense.setUpdateDate(LocalDateTime.now());
        msLicense.setUpdateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
        MsLicense svx = msLicenseRepo.save(msLicense);

        LicenseFile lf = licenseStorageFile.store(requestLicense.getLicenseDoc(),svx.getId());
        svx.setLicenseFileId(lf.getId());
        MsLicense sv = msLicenseRepo.save(svx);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,obj.writeValueAsString(getBefore),obj.writeValueAsString(sv),"Changes License "+sv.getName());
        return new Response(true,sv);
    }



    @DeleteMapping("")
    public Response deleteData(HttpServletRequest request,MsLicense msLicense){

        MsLicense getdt = msLicenseRepo.findById(msLicense.getId()).get();
        getdt.setActive(false);
        getdt.setUpdateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
        getdt.setUpdateDate(LocalDateTime.now());
        MsLicense sv =msLicenseRepo.save(getdt);

        userManagementService.track(request,null,null,"Delete License");

        return new Response(true, sv);
    }
}
