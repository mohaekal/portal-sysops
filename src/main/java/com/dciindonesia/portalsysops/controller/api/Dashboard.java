package com.dciindonesia.portalsysops.controller.api;


import com.dciindonesia.portalsysops.model.Response;
import com.dciindonesia.portalsysops.repository.assetmgmt.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Dashboard {

    @Autowired
    private MsAssetRepo msAssetRepo;

    @Autowired
    private MsLicenseRepo msLicenseRepo;

    @Autowired
    private MsConsumableRepo msConsumableRepo;

    @Autowired
    private MsAccessoryRepo msAccessoryRepo;

    @GetMapping("/api/summary/asset")
    public Response getAsset(){

        return new Response(true,msAssetRepo.findAllActive().size());

    }
    @GetMapping("/api/summary/license")
    public Response getLicense(){

        return new Response(true,msLicenseRepo.findAllActive().size());
    }
    @GetMapping("/api/summary/consumable")
    public Response getConsumable(){

        return new Response(true,msConsumableRepo.findAllActive().size());
    }
    @GetMapping("/api/summary/accessory")
    public Response getAccessory(){

        return new Response(true,msAccessoryRepo.findAllActive().size());
    }


}
