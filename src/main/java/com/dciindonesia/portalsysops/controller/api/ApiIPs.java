package com.dciindonesia.portalsysops.controller.api;

import com.dciindonesia.portalsysops.entity.assetmgmt.IpVMDetail;
import com.dciindonesia.portalsysops.entity.assetmgmt.IpVm;
import com.dciindonesia.portalsysops.entity.assetmgmt.MsUser;
import com.dciindonesia.portalsysops.model.Response;
import com.dciindonesia.portalsysops.model.Select2;
import com.dciindonesia.portalsysops.repository.assetmgmt.IpVMDetailRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.IpVMRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/ips")
public class ApiIPs {


    @Autowired
    private IpVMRepo ipVMRepo;

    @Autowired
    private IpVMDetailRepo ipVMDetailRepo;
    @Autowired
    private MsUserRepo msUserRepo;

    private Logger log = LoggerFactory.getLogger("API IPs");
    @GetMapping("/select2")
    public Response getAllActiveSelect(String q,Long vmid){

        try {
            if (vmid!=null){
                log.info(vmid+" vmid");
                if (q!=null){
                    return new Response(true, ipVMRepo.findAllActiveUnUsedByVMId(vmid).stream().filter(f->f.getIp().contains(q))
                            .map(e->new Select2(e.getId(),e.getIp())).collect(Collectors.toList()));
                }

                return new Response(true, ipVMRepo.findAllActiveUnUsedByVMId(vmid).stream().map(e->new Select2(e.getId(),e.getIp())).collect(Collectors.toList()));
            }

            if (q!=null){
                return new Response(true, ipVMRepo.findAllActiveUnUsed().stream().filter(f->f.getIp().contains(q))
                        .map(e->new Select2(e.getId(),e.getIp())).collect(Collectors.toList()));
            }
            return new Response(true, ipVMRepo.findAllActiveUnUsed().stream().map(e->new Select2(e.getId(),e.getIp())).collect(Collectors.toList()));
        }catch (Exception e){
            return new Response(false,null);
        }
    }


    @GetMapping("")
    public Response getAllActive(Long vmid){

        try {
            List<IpVMDetail> ipdet = ipVMDetailRepo.findAllByvmid(vmid);
            List<IpVm> ip = ipVMRepo.findAllActive().stream().filter(e->ipdet.stream().anyMatch(f->f.getIpVmId().equals(e.getId()))).collect(Collectors.toList());
            return new Response(true,ip);
        }catch (Exception e){
            return new Response(false,null);
        }
    }

    @PostMapping("")
    public Response store(HttpServletRequest request, String ip){
        try {
            List<IpVm> ips = ipVMRepo.findByIP(ip);
            if (ips.size()==0){
                IpVm ipVm = new IpVm();
                ipVm.setActive(true);
                ipVm.setCreateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
                ipVm.setCreateDate(LocalDateTime.now());
                ipVm.setIp(ip);
                return new Response(true, ipVMRepo.save(ipVm));
            }else if(!ips.get(0).getActive()){
                IpVm ipv = ips.get(0);
                ipv.setActive(true);
                return new Response(true, ipVMRepo.save(ipv));
            }else {
                return new Response(false, "Sorry, Your IP already created ");
            }
        }catch (Exception e){
            return new Response(false,null);
        }

    }
}
