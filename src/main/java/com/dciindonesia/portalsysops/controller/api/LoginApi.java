package com.dciindonesia.portalsysops.controller.api;

import com.dciindonesia.portalsysops.model.Response;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/api/login")
public class LoginApi {

    private Logger log = LoggerFactory.getLogger("api Login");
    @Autowired
    private MsUserRepo msUserRepo;

    @GetMapping("/authenticate")
    public Response userdetailauth(HttpServletRequest request){
        try {

//            log.info(request.getUserPrincipal().getName());
            return new Response(true, msUserRepo.findByEmail(request.getUserPrincipal().getName()));
        }catch (Exception e){
            return new Response(false, null);
        }
    }
}
