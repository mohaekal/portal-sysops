package com.dciindonesia.portalsysops.controller.api;

import com.dciindonesia.portalsysops.entity.assetmgmt.LogAccessory;
import com.dciindonesia.portalsysops.entity.assetmgmt.MsAccessory;
import com.dciindonesia.portalsysops.model.Response;
import com.dciindonesia.portalsysops.repository.assetmgmt.LogAccessoryRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsAccessoryRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@RestController
@RequestMapping("/api/accessory")
public class ApiAccessory {

    @Autowired
    private MsUserRepo msUserRepo;

    @Autowired
    private MsAccessoryRepo msAccessoryRepo;

    @Autowired
    private UserManagementService userManagementService;
    @Autowired
    private LogAccessoryRepo logAccessoryRepo;

    @GetMapping("")
    public Response getAllData(HttpServletRequest request){
        userManagementService.track(request,null,null,"View Accessory Menu ");

        return new Response(true,msAccessoryRepo.findJoinActive());
    }

    @PostMapping("")
    public Response postData(HttpServletRequest request, MsAccessory msAccessory) throws JsonProcessingException {

        msAccessory.setActive(true);
        msAccessory.setCreateDate(LocalDateTime.now());
        msAccessory.setCreateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
        MsAccessory sv = msAccessoryRepo.save(msAccessory);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,"-",obj.writeValueAsString(sv),"Add Accessory "+sv.getName());

        return new Response(true,sv);
    }

    @PutMapping
    public Response updateData(HttpServletRequest request, MsAccessory msAccessory) throws JsonProcessingException {

        MsAccessory getdt = msAccessoryRepo.findById(msAccessory.getId()).get();
        MsAccessory getBefore = getdt;
        msAccessory.setCreateDate(getdt.getCreateDate());
        msAccessory.setCreateBy(getdt.getCreateBy());
        msAccessory.setActive(true);
        msAccessory.setUpdateDate(LocalDateTime.now());
        msAccessory.setUpdateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
        MsAccessory sv =  msAccessoryRepo.save(msAccessory);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,obj.writeValueAsString(getBefore),obj.writeValueAsString(sv),"Changes Accessory "+sv.getName());

        return new Response(true,sv);
    }

    @DeleteMapping
    public Response deleteData(HttpServletRequest request, MsAccessory msAccessory) throws JsonProcessingException {

        MsAccessory getdt = msAccessoryRepo.findById(msAccessory.getId()).get();
        MsAccessory getBefore = getdt;
        getdt.setActive(false);
        getdt.setUpdateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
        getdt.setUpdateDate(LocalDateTime.now());
        MsAccessory sv = msAccessoryRepo.save(getdt);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,obj.writeValueAsString(getBefore),obj.writeValueAsString(sv),"Delete Accessory "+sv.getName());

        return new Response(true,sv);
    }

    @PostMapping("/checkout")
    public Response checkout(HttpServletRequest request,Long accessoryId,String usedBy) throws JsonProcessingException {

        LogAccessory newdata = new LogAccessory();
        newdata.setAccessoryId(accessoryId);
        newdata.setUsedBy(usedBy);
        newdata.setCreateDate(LocalDateTime.now());
        LogAccessory newLog = logAccessoryRepo.save(newdata);

        MsAccessory ma = msAccessoryRepo.findById(accessoryId).orElse(null);
        ma.setLogId( newLog.getId() );
        ma.setUpdateBy( msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId() );
        ma.setUpdateDate( LocalDateTime.now() );
        MsAccessory sv =   msAccessoryRepo.save(ma);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,"-",obj.writeValueAsString(newLog),"Use Accessory ");

        return new Response(true,newLog);
    }



}
