package com.dciindonesia.portalsysops.controller.api;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsUser;
import com.dciindonesia.portalsysops.entity.assetmgmt.MsVM;
import com.dciindonesia.portalsysops.entity.portalmanagement.UserRole;
import com.dciindonesia.portalsysops.model.RequestUser;
import com.dciindonesia.portalsysops.model.Response;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.repository.portalmanagement.UserRoleRepo;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/user-management/user")
public class ApiUser {

    @Autowired
    private MsUserRepo msUserRepo;

    @Autowired
    private UserManagementService userManagementService;

    @Autowired
    private UserRoleRepo userRoleRepo;
    private Logger log = LoggerFactory.getLogger("APIUser");
    @GetMapping("")
    public Response getAllActive(HttpServletRequest request){
        List<MsUser> data = msUserRepo.findAll();
        Collections.sort(data,Comparator.comparing(MsUser::getName));

        List<UserRole> dataROle = userRoleRepo.findAll();

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,"-","-","View User Menu");
        return new Response(true, data.stream().map(e->new RequestUser(e,dataROle)).collect(Collectors.toList()));
    }

    @PostMapping("")
    @PreAuthorize("hasAnyRole({'USER'})")
    public Response store(HttpServletRequest request, HttpSession session, RequestUser requestUser){
        try {

            ObjectMapper obj = new ObjectMapper();
            userManagementService.track(request,"-",obj.writeValueAsString(requestUser),"Add User "+requestUser.getEmail()+" - "+requestUser.getName());
            return new Response(true,userManagementService.addUser(requestUser,request));
        }catch (Exception e){
            return new Response(false,null);
        }

    }

    @PutMapping("")
    @PreAuthorize("hasAnyRole({'USER'})")
    public Response update(HttpServletRequest request, RequestUser requestUser){
        try {
            com.dciindonesia.portalsysops.entity.portalmanagement.MsUser chuser = userManagementService.updateUser(requestUser,request);

            ObjectMapper obj = new ObjectMapper();
            userManagementService.track(request,obj.writeValueAsString(chuser),obj.writeValueAsString(requestUser),"Changes User "+requestUser.getEmail()+" - "+requestUser.getName());

            return new Response(true,new ArrayList<>());
        }catch (Exception e){
            return new Response(false,null);
        }
    }

    @PutMapping("/change-profile")
    public Response updateProfile(HttpServletRequest request, RequestUser requestUser){
        try {
            requestUser.setId(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
            MsUser chuser = userManagementService.changeProfile(requestUser);
            ObjectMapper obj = new ObjectMapper();
            userManagementService.track(request,obj.writeValueAsString(chuser),obj.writeValueAsString(requestUser),"Changes Profile ");

            return new Response(true,new ArrayList<>());
        }catch (Exception e){
            return new Response(false,null);
        }
    }

    @DeleteMapping("")
    @PreAuthorize("hasAnyRole({'USER'})")
    public Response delete(HttpServletRequest request, RequestUser requestUser){
        try {
            com.dciindonesia.portalsysops.entity.portalmanagement.MsUser chuser = userManagementService.removeUser(requestUser,request);
            ObjectMapper obj = new ObjectMapper();
            userManagementService.track(request,obj.writeValueAsString(chuser),obj.writeValueAsString(requestUser),"Delete User "+requestUser.getEmail()+" - "+requestUser.getName());

            return new Response(true,new ArrayList<>());
        }catch (Exception e){
            return new Response(false,null);
        }
    }
}
