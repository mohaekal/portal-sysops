package com.dciindonesia.portalsysops.controller.api;

import com.dciindonesia.portalsysops.entity.assetmgmt.*;
import com.dciindonesia.portalsysops.model.InterfaceAsset;
import com.dciindonesia.portalsysops.model.RequestMsVM;
import com.dciindonesia.portalsysops.model.Response;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsAssetCategoryRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsAssetRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.service.AssetTagGenerate;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/asset")
public class ApiAsset {

    @Autowired
    private AssetTagGenerate assetTagGenerate;

    @Autowired
    private MsAssetRepo msAssetRepo;

    @Autowired
    private MsUserRepo msUserRepo;

    @Autowired
    private UserManagementService userManagementService;

    @GetMapping("")
    public Response get(HttpServletRequest request){

        List<String> privileges = SecurityContextHolder.getContext().getAuthentication().getAuthorities() .stream().filter(e->e.toString().equals("ROLE_ASSET")).map(e->e.toString()).collect(Collectors.toList());

        Collection<InterfaceAsset> show = msAssetRepo.getAssetDetailDataSensitive();
        if(privileges.size()>0){

            show = msAssetRepo.getAssetDetail();
        }


        userManagementService.track(request,"-","-","View Asset Menu ");

        return new Response(true, show );
    }

    @PostMapping("")
    public Response store(HttpServletRequest request, MsAsset msAsset) throws JsonProcessingException {

        try {

            msAsset.setActive(true);
            msAsset.setCreateDate(LocalDateTime.now());
            msAsset.setCreateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
            MsAsset sv = msAssetRepo.save(msAsset);

            ObjectMapper obj = new ObjectMapper();
            userManagementService.track(request,"-",obj.writeValueAsString(sv),"Add New Asset "+sv.getAssetName());

            return new Response(true, sv);
        }catch (Exception e){
            return new Response(false,e.getMessage());
        }
    }


    @PutMapping("")
    public Response update(HttpServletRequest request, MsAsset msAsset){
        try {

            MsAsset existData = msAssetRepo.findById(msAsset.getId()).get();
            MsAsset getBefore = existData;

            msAsset.setActive(existData.getActive());
            msAsset.setCreateBy(existData.getCreateBy());
            msAsset.setCreateDate(existData.getCreateDate());
            msAsset.setUpdateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
            msAsset.setUpdateDate(LocalDateTime.now());

            MsAsset sv = msAssetRepo.save(msAsset);

            ObjectMapper obj = new ObjectMapper();
            userManagementService.track(request,obj.writeValueAsString(getBefore),obj.writeValueAsString(sv),"Changes Asset "+sv.getAssetName());

            return new Response(true,sv );
        }catch (Exception e){
            return new Response(false,e.getMessage());
        }
    }

    @DeleteMapping("")
    public Response delete(HttpServletRequest request, MsAsset msAsset){
        try {
            MsAsset getdt = msAssetRepo.findById(msAsset.getId()).get();
            getdt.setActive(false);
            getdt.setUpdateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
            getdt.setUpdateDate(LocalDateTime.now());
            MsAsset sv = msAssetRepo.save(getdt);

            userManagementService.track(request,null,null,"Delete Asset "+sv.getAssetName());

            return new Response(true,sv );
        }catch (Exception e){
            return new Response(false,e.getMessage());
        }
    }

    ///////////////

    @GetMapping("/get-asset-tag")
    public Response getassettag(HttpServletRequest request, Integer categoryid){


        return new Response(true,assetTagGenerate.generateassettag(categoryid));
    };

    @GetMapping("/findById")
    public Response getById(HttpServletRequest request, Long id){
        InterfaceAsset a = msAssetRepo.getAssetDetailById(id);
        return new Response(true,a);
    };


}
