package com.dciindonesia.portalsysops.controller.api;

import com.dciindonesia.portalsysops.entity.assetmgmt.*;
import com.dciindonesia.portalsysops.entity.portalmanagement.MsUser;
import com.dciindonesia.portalsysops.model.History;
import com.dciindonesia.portalsysops.model.RequestMsVM;
import com.dciindonesia.portalsysops.model.Response;
import com.dciindonesia.portalsysops.repository.assetmgmt.*;
import com.dciindonesia.portalsysops.repository.portalmanagement.MsUserPortalRepo;
import com.dciindonesia.portalsysops.service.EvidenceStorageFiles;
import com.dciindonesia.portalsysops.service.HistoricalRequestVM;
import com.dciindonesia.portalsysops.service.Telegram;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/request/vm-sysops")
@PreAuthorize("hasAnyRole({'REQUEST_VM_SYSOPS'})")
public class ApiRequestVMSYSOPS {

    private Logger log = LoggerFactory.getLogger("Req VM SYSOPS");
    @Autowired
    private TrRequestVmRepo trRequestVmRepo;
    @Autowired
    private TrRequestProgressRepo trRequestProgressRepo;
    @Autowired
    private MsUserRepo msUserRepo;
    @Autowired
    private MsVMRepo msVMRepo;
    @Autowired
    private UserManagementService userManagementService;
    @Autowired
    private MsUserPortalRepo msUserPortalRepo;
    @Autowired
    private IpVMDetailRepo ipVMDetailRepo;
    @Autowired
    private IpVMRepo ipVMRepo;
    @Autowired
    private TrApprovementRequestRepo trApprovementRequestRepo;
    @Autowired
    private EvidenceStorageFiles evidenceStorageFiles;
    @Autowired
    private HistoricalRequestVM historicalRequestVM;
    @Autowired
    private Telegram telegram;
    @Autowired
    private TrVmUpdateRepo trVmUpdateRepo;

    @GetMapping()
    public Response getAllData(HttpServletRequest request){

//        List<String> privileges = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
//                .stream().filter(e->e.toString().equals("ROLE_REQUEST_VM_SYSOPS")).map(e->e.toString()).collect(Collectors.toList());

        List<IpVMDetail> ipdet = ipVMDetailRepo.findAll();
        List<IpVm> msip = ipVMRepo.findAll();
        List<TrEvidenceFile> fileid = evidenceStorageFiles.getFileId();
        List<TrVmUpdate> modifReq = trVmUpdateRepo.findAllActive();

        MsUser user = msUserPortalRepo.findByEmail(request.getUserPrincipal().getName());

        List<RequestMsVM> reqsVm = msVMRepo.findRequestAllActive(Long.valueOf(1),user.getId(),1).stream()
                .map(e->new RequestMsVM(e,ipdet,msip))
                .collect(Collectors.toList());

        List<RequestMsVM> reqVM2 = reqsVm.stream().map(
                f->{
                    f.setEvidenceFiles(fileid.stream()
                            .filter(filidd->filidd.getRequestProgressId().equals(f.getRequestProgressId()))
                            .map(h->h.getId()).collect(Collectors.toList()));

                    if(modifReq.stream().anyMatch(m->m.getRequestId().equals(f.getRequestId()))){
                        TrVmUpdate getMatchReqId = modifReq.stream().filter(m->m.getRequestId().equals(f.getRequestId())).findFirst().get();
                        if ((getMatchReqId.getCore() != null && !getMatchReqId.getCore().trim().equals(""))) {
                            f.setCpu(getMatchReqId.getCore());
                        }

                        if ((getMatchReqId.getSocket() != null)) {
                            f.setSocket(getMatchReqId.getSocket());
                        }

                        if ((getMatchReqId.getStorage() != null && !getMatchReqId.getStorage().trim().equals(""))) {
                            f.setStorage(getMatchReqId.getStorage());
                        }

                        if ((getMatchReqId.getMemory() != null && !getMatchReqId.getMemory().trim().equals(""))) {
                            f.setMemory(getMatchReqId.getMemory());
                        }
                    }
                    return f;
                }
        ).collect(Collectors.toList());

        userManagementService.track(request,"-","-","View Request Menu SYSOPS");

        return new Response(true, reqVM2 );
    }

    @GetMapping("/notification")
    public Response getNotif(HttpServletRequest request){

        MsUser user = msUserPortalRepo.findByEmail(request.getUserPrincipal().getName());
        List<RequestMsVM> reqsVm = msVMRepo.findRequestByCategoryId(Long.valueOf(1),user.getId(),1).stream()
                .map(e->new RequestMsVM(e,null,null))
                .collect(Collectors.toList());


        return new Response(true, reqsVm );
    }

    @GetMapping("/closed")
    public Response getAllClosedData(HttpServletRequest request){

        MsUser user = msUserPortalRepo.findByEmail(request.getUserPrincipal().getName());
        List<String> privileges = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
                .stream().filter(e->e.toString().equals("ROLE_REQUEST_VM_SYSOPS")).map(e->e.toString()).collect(Collectors.toList());

        List<IpVMDetail> ipdet = ipVMDetailRepo.findAll();
        List<IpVm> msip = ipVMRepo.findAll();
        List<TrEvidenceFile> fileid = evidenceStorageFiles.getFileId();

        List<RequestMsVM> reqsVm = msVMRepo.findAllClosed(Long.valueOf(1),user.getId()).stream()
                .map(e->new RequestMsVM(e,ipdet,msip))
                .collect(Collectors.toList());

        List<RequestMsVM> reqVM2 = reqsVm.stream().map(
                f->{f.setEvidenceFiles(
                        fileid.stream()
                                .filter(g->g.getRequestProgressId().equals(f.getRequestProgressId()))
                                .map(h->h.getId()).collect(Collectors.toList())
                ); return f;}
        ).collect(Collectors.toList());

        userManagementService.track(request,"-","-","View Request Menu SYSOPS ");

        return new Response(true, reqVM2 );
    }

    @GetMapping("/history")
    public Response getHistoryByReqId(Long id){

        try{
            return new Response(true,historicalRequestVM.getHistoryByRequestId(id));
        }catch (Exception e){
            return new Response(false,e.getMessage()+" "+e.getCause() );
        }
    }

    @PostMapping("/save-draft")
    public Response storeDraft(HttpServletRequest request,RequestMsVM requestMsVM) throws JsonProcessingException {
        MsVM msVM = new MsVM(requestMsVM);
        TrRequestVm getReq = this.store(request,msVM);
        getReq.setRemark(requestMsVM.getRemark());
        trRequestVmRepo.save(getReq);

        return new Response(true, getReq);
    }

    @Autowired
    private MsLicenseRepo msLicenseRepo;
    @PostMapping("/prepared")
    public Response storePrepared(HttpServletRequest request,RequestMsVM requestMsVM) throws JsonProcessingException {
        try {

            MsUser user = msUserPortalRepo.findByEmail(request.getUserPrincipal().getName());
            MsVM msVM = new MsVM(requestMsVM);
            TrRequestVm getReq = this.store(request,msVM);

            TrRequestProgress trProg = new TrRequestProgress();
            trProg.setActive(true);
            trProg.setCreateBy(user.getId());
            trProg.setCreateDate(LocalDateTime.now());
            trProg.setProgressId(Long.valueOf(2));
            trProg.setRequestId(getReq.getId());
            TrRequestProgress newProgress = trRequestProgressRepo.save(trProg);

            getReq.setRequestProgressId(newProgress.getId());
            getReq.setRemark(requestMsVM.getRemark());
            TrRequestVm setReqVM = trRequestVmRepo.save(getReq);

            telegram.send(user.getName()+" submit Request ID : "+setReqVM.getId()+"<br/>"
                    +"Description : "+msVM.getDescription()+"<br/>"
                    +"CPU : "+msVM.getSocket()+" socket, "+msVM.getCpu()+" core "+"<br/>"
                    +"Memory : "+msVM.getMemory()+"GB <br/>"
                    +"Storage : "+msVM.getStorage()+"GB <br/>"
                    +"Guest OS : "+msLicenseRepo.findById(msVM.getLicenseId()).get().getName()
            );
            setApproverTask( request,newProgress.getId() );

            ObjectMapper obj = new ObjectMapper();

            userManagementService.track(request,"-",obj.writeValueAsString(setReqVM),"Request VM SYSOPS (Prepared) "+"[Req ID:"+setReqVM.getId()+"]");

            return new Response(true,setReqVM);
        }catch (Exception e){
            return new Response(false,e.getMessage()+" "+e.getCause());
        }
    }


    @PutMapping("/save-draft")
    public Response updateDraft(HttpServletRequest request, RequestMsVM requestMsVM){
        try {

            MsVM existvm = msVMRepo.findById(requestMsVM.getId()).get();
            MsVM msVM = new MsVM(requestMsVM);
            msVM.setCreateDate(existvm.getCreateDate());
            msVM.setCreateBy(existvm.getCreateBy());
            msVM.setUpdateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
            msVM.setUpdateDate(LocalDateTime.now());
            msVM.setActive(false);
            msVM.setCategoryId(Long.valueOf(1));
            msVM.setCompanyId(Long.valueOf(1));
            MsVM setMsVm = msVMRepo.save(msVM);

            ObjectMapper obj = new ObjectMapper();

            TrRequestVm getReq = trRequestVmRepo.findById(requestMsVM.getRequestId()).get();
            getReq.setRemark(requestMsVM.getRemark());
            trRequestVmRepo.save(getReq);

            userManagementService.track(request,obj.writeValueAsString(msVM),obj.writeValueAsString(setMsVm),"Request VM SYSOPS (Save Draft) "+"[Req ID:"+requestMsVM.getRequestId()+"]");

            return new Response(true,setMsVm);
        }catch (Exception e){
            return new Response(false,null);
        }

    }

    @PutMapping("/prepared")
    public Response updatePrepared(HttpServletRequest request, RequestMsVM requestMsVM){
        try {

            MsUser user = msUserPortalRepo.findByEmail(request.getUserPrincipal().getName());

            MsVM existvm = msVMRepo.findById(requestMsVM.getId()).get();
            MsVM msVM = new MsVM(requestMsVM);
            msVM.setCreateBy(existvm.getCreateBy());
            msVM.setCreateDate(existvm.getCreateDate());
            msVM.setUpdateBy(user.getId());
            msVM.setUpdateDate(LocalDateTime.now());
            msVM.setActive(false);
            msVM.setCategoryId(Long.valueOf(1));
            msVM.setCompanyId(Long.valueOf(1));
            MsVM setMsVm = msVMRepo.save(msVM);

            TrRequestVm getReq = trRequestVmRepo.findById(requestMsVM.getRequestId()).get();

            TrRequestProgress trProg = new TrRequestProgress();
            trProg.setActive(true);
            trProg.setCreateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
            trProg.setCreateDate(LocalDateTime.now());
            trProg.setProgressId(Long.valueOf(2));
            trProg.setRequestId(getReq.getId());
            TrRequestProgress newProgress = trRequestProgressRepo.save(trProg);

            getReq.setRequestProgressId(newProgress.getId());
            getReq.setRemark(requestMsVM.getRemark());
            TrRequestVm setReqVM = trRequestVmRepo.save(getReq);

            telegram.send(user.getName()+" submit Request ID : "+setReqVM.getId()+"<br/>"
                    +"Description : "+msVM.getDescription()+"<br/>"
                    +"CPU : "+msVM.getSocket()+" socket, "+msVM.getCpu()+" core "+"<br/>"
                    +"Memory : "+msVM.getMemory()+"GB <br/>"
                    +"Storage : "+msVM.getStorage()+"GB <br/>"
                    +"Guest OS : "+msLicenseRepo.findById(msVM.getLicenseId()).get().getName()
            );

            setApproverTask( request,newProgress.getId() );

            ObjectMapper obj = new ObjectMapper();

            userManagementService.track(request,"-",obj.writeValueAsString(setReqVM),"Request VM SYSOPS (Prepared) "+"[Req ID:"+setReqVM.getId()+"]");

            return new Response(true,setReqVM);
        }catch (Exception e){
            return new Response(false,null);
        }

    }

    @PutMapping("/approve-request")

    public Response updateApproveRequest(HttpServletRequest request, Long approvementId){
        try {

            MsUser user = msUserPortalRepo.findByEmail(request.getUserPrincipal().getName());

            TrApprovementRequest approvement =  trApprovementRequestRepo.findById(approvementId).get();
            approvement.setApprove(true);
            approvement.setUpdateBy(user.getId());
            approvement.setUpdateDate(LocalDateTime.now());
            trApprovementRequestRepo.save(approvement);

            TrRequestProgress reqProgress = trRequestProgressRepo.findById(approvement.getRequestProgressId()).get();

            TrRequestVm reqParent = trRequestVmRepo.findById(reqProgress.getRequestId()).get();


            Boolean isaprove= isApprove(request, reqProgress.getId() );

            if(isaprove){

                TrRequestProgress trProg = new TrRequestProgress();
                trProg.setActive(true);
                trProg.setCreateBy(user.getId());
                trProg.setCreateDate(LocalDateTime.now());
                trProg.setProgressId(Long.valueOf(3));
                trProg.setRequestId(reqParent.getId());
                TrRequestProgress newProgress = trRequestProgressRepo.save(trProg);

                reqParent.setRequestProgressId(newProgress.getId());
                TrRequestVm setReqVM = trRequestVmRepo.save(reqParent);

                setEvidenceTask(request,newProgress.getId());

            }

            telegram.send("Request ID "+reqParent.getId()+" APPROVED by "+user.getName());
            userManagementService.track(request,"-","-","Request VM SYSOPS (Request Approved) "+"[Req ID:"+reqParent.getId()+"]");
            return new Response(true,null);
        }catch (Exception e){
            return new Response(false,null);
        }

    }

    @PutMapping("/reject-request")

    public Response updateRejectRequest(HttpServletRequest request, Long approvementId, String reason){
        try {

            MsUser user = msUserPortalRepo.findByEmail(request.getUserPrincipal().getName());

            TrApprovementRequest approvement =  trApprovementRequestRepo.findById(approvementId).get();
            approvement.setReason(reason);
            approvement.setApprove(false);
            approvement.setUpdateDate(LocalDateTime.now());
            approvement.setUpdateBy(user.getId());
            trApprovementRequestRepo.save(approvement);


            TrRequestProgress reqProgress = trRequestProgressRepo.findById(approvement.getRequestProgressId()).get();

            TrRequestVm reqParent = trRequestVmRepo.findById(reqProgress.getRequestId()).get();

            if(reqParent.getRequestProgressId().equals(reqProgress.getId())){

                TrRequestProgress trProg = new TrRequestProgress();
                trProg.setActive(true);
                trProg.setCreateBy(user.getId());
                trProg.setCreateDate(LocalDateTime.now());
                trProg.setProgressId(Long.valueOf(1));
                trProg.setRequestId(reqProgress.getRequestId());
                TrRequestProgress newProgress = trRequestProgressRepo.save(trProg);

                reqParent.setRequestProgressId(newProgress.getId());
                TrRequestVm setReqVM = trRequestVmRepo.save(reqParent);

                telegram.send("Request ID "+reqParent.getId()+" REJECTED by "+user.getName() +"("+reason+")");
            };
            userManagementService.track(request,"-","-","Request VM SYSOPS (Request Rejected) "+"[Req ID:"+reqParent.getId()+"]");

            return new Response(true,null);
        }catch (Exception e){
            return new Response(false,null);
        }
    }

    @PutMapping("/evidence")

    public Response updateEvidence(HttpServletRequest request, RequestMsVM requestMsVM){
        try {

            MsUser user = msUserPortalRepo.findByEmail(request.getUserPrincipal().getName());

            Long cekreqprog = trRequestProgressRepo.findById(trRequestVmRepo.findById(requestMsVM.getRequestId()).get().getRequestProgressId()).get().getProgressId();

            if (cekreqprog!=Long.valueOf(3)){
                return new Response(false,"action expire");
            }

            MsVM msVM = new MsVM(requestMsVM);
            MsVM existmsVM = msVMRepo.findById(requestMsVM.getId()).get();

            msVM.setCreateBy(existmsVM.getCreateBy());
            msVM.setCreateDate(existmsVM.getCreateDate());
            msVM.setUpdateBy(user.getId());
            msVM.setUpdateDate(LocalDateTime.now());
            msVM.setActive(existmsVM.getActive());
            msVM.setCategoryId(existmsVM.getCategoryId());
            msVM.setCompanyId(existmsVM.getCompanyId());
            if(requestMsVM.getRequestType().equals("MODIFICATION")){
                msVM.setStorage(existmsVM.getStorage());
                msVM.setMemory(existmsVM.getMemory());
                msVM.setSocket(existmsVM.getSocket());
                msVM.setCpu((existmsVM.getCpu()));
            }


            MsVM setMsVm = msVMRepo.save(msVM);

            List<IpVMDetail> ipdet = ipVMDetailRepo.findAllByvmid(setMsVm.getId());
            ipVMDetailRepo.deleteAll(ipdet);
            try {

                if(!requestMsVM.getRequestType().equals("TERMINATE")){
                    for (Long ip:requestMsVM.getIps()
                    ) {
                        IpVMDetail newIp = new IpVMDetail();
                        newIp.setIpVmId(ip);
                        newIp.setVmId(setMsVm.getId());
                        newIp.setCreateDate(LocalDateTime.now());
                        newIp.setCreateBy(user.getId());
                        ipVMDetailRepo.save(newIp);
                    }
                }

            }catch (Exception e){}

            TrRequestVm getReq = trRequestVmRepo.findById(requestMsVM.getRequestId()).get();

            TrRequestProgress trProg = new TrRequestProgress();
            trProg.setActive(true);
            trProg.setCreateBy(user.getId());
            trProg.setCreateDate(LocalDateTime.now());
            trProg.setProgressId(Long.valueOf(4));
            trProg.setRequestId(getReq.getId());
            trProg.setRemark(requestMsVM.getEvidenceRemark());
            TrRequestProgress newProgress = trRequestProgressRepo.save(trProg);

            if (requestMsVM.getEvidenceFileData()[0].getSize()>Long.valueOf(0)){
                evidenceStorageFiles.store(requestMsVM.getEvidenceFileData(),trProg.getId());
            }

            getReq.setRequestProgressId(newProgress.getId());
            TrRequestVm setReqVM = trRequestVmRepo.save(getReq);

            telegram.send(user.getName()+" Submit Evidence Request ID : "+setReqVM.getId());
            setApproverTask( request,newProgress.getId() );

            ObjectMapper obj = new ObjectMapper();

            userManagementService.track(request,"-",obj.writeValueAsString(setReqVM),"Request VM SYSOPS (Evidence Prepared) "+"[Req ID:"+setReqVM.getId()+"]");

            return new Response(true,setReqVM);
        }catch (Exception e){
            return new Response(false,e.getMessage()+" - "+e.getCause());
        }

    }


    @PutMapping("/approve-evidence")

    public Response updateApproveEvidence(HttpServletRequest request, Long approvementId){


        try {
            MsUser user = msUserPortalRepo.findByEmail(request.getUserPrincipal().getName());

            TrApprovementRequest approvement =  trApprovementRequestRepo.findById(approvementId).get();
            approvement.setApprove(true);
            approvement.setUpdateBy(user.getId());
            approvement.setUpdateDate(LocalDateTime.now());
            trApprovementRequestRepo.save(approvement);

            TrRequestProgress reqProgress = trRequestProgressRepo.findById(approvement.getRequestProgressId()).get();

            TrRequestVm reqParent = trRequestVmRepo.findById(reqProgress.getRequestId()).get();

            Boolean isaprove= isApprove(request, reqProgress.getId() );

            if(isaprove){

                TrRequestProgress trProg = new TrRequestProgress();
                trProg.setActive(true);
                trProg.setCreateBy(user.getId());
                trProg.setCreateDate(LocalDateTime.now());
                trProg.setProgressId(Long.valueOf(5));
                trProg.setRequestId(reqParent.getId());
                TrRequestProgress newProgress = trRequestProgressRepo.save(trProg);

                reqParent.setRequestProgressId(newProgress.getId());
                reqParent.setActive(false);
                TrRequestVm setReqVM = trRequestVmRepo.save(reqParent);

                MsVM getvm = msVMRepo.findById(reqParent.getVmId()).get();
                switch (reqParent.getRequestType()){
                    case "NEW":

                        getvm.setActive(true);
                        break;

                    case "MODIFICATION":
                        TrVmUpdate trVmUpdate = trVmUpdateRepo.findByRequestId(reqParent.getId());
                        trVmUpdate.setActive(false);
                        trVmUpdateRepo.save(trVmUpdate);
                        getvm.setCpu(trVmUpdate.getCore());
                        getvm.setSocket(trVmUpdate.getSocket());
                        getvm.setMemory(trVmUpdate.getMemory());
                        getvm.setStorage(trVmUpdate.getStorage());
                        break;
                    case "TERMINATE":

                        getvm.setActive(false);
                        break;
                }

                msVMRepo.save(getvm);
            }
            userManagementService.track(request,"-","-","Request VM SYSOPS (Evidence Approved) "+"[Req ID:"+reqParent.getId()+"]");

            telegram.send("Request ID "+reqParent.getId()+" EVIDENCE APPROVED by "+user.getName());
            return new Response(true,null);
        }catch (Exception e){
            return new Response(false,null);
        }
    }



    @PutMapping("/reject-evidence")

    public Response update(HttpServletRequest request, Long approvementId, String reason){
        try {

            MsUser user = msUserPortalRepo.findByEmail(request.getUserPrincipal().getName());

            TrApprovementRequest approvement =  trApprovementRequestRepo.findById(approvementId).get();
            approvement.setReason(reason);
            approvement.setApprove(false);
            approvement.setUpdateDate(LocalDateTime.now());
            approvement.setUpdateBy(user.getId());
            trApprovementRequestRepo.save(approvement);


            TrRequestProgress reqProgress = trRequestProgressRepo.findById(approvement.getRequestProgressId()).get();

            TrRequestVm reqParent = trRequestVmRepo.findById(reqProgress.getRequestId()).get();

            if(reqParent.getRequestProgressId().equals(reqProgress.getId())){

                TrRequestProgress trProg = new TrRequestProgress();
                trProg.setActive(true);
                trProg.setCreateBy(user.getId());
                trProg.setCreateDate(LocalDateTime.now());
                trProg.setProgressId(Long.valueOf(3));
                trProg.setRequestId(reqProgress.getRequestId());
                TrRequestProgress newProgress = trRequestProgressRepo.save(trProg);

                reqParent.setRequestProgressId(newProgress.getId());
                TrRequestVm setReqVM = trRequestVmRepo.save(reqParent);

                telegram.send("Request ID "+reqParent.getId()+" EVIDENCE REJECTED by "+user.getName());
            };
            userManagementService.track(request,"-","-","Request VM SYSOPS (Evidence Rejected) "+"[Req ID:"+reqParent.getId()+"]");
            return new Response(true,null);
        }catch (Exception e){
            return new Response(false,e.getMessage()+" "+e.getCause());
        }
    }


    @DeleteMapping()
    public Response delete(HttpServletRequest request, Long requestid){
        TrRequestVm getreq = trRequestVmRepo.findById(requestid).get();
        if (trRequestProgressRepo.findById(getreq.getRequestProgressId()).get().getProgressId().equals(Long.valueOf(1))){
            getreq.setActive(false);
            trRequestVmRepo.save(getreq);
            userManagementService.track(request,"-","-","Remove VM SYSOPS  "+"[Req ID:"+requestid+"]");
            return new Response(true,null);
        }
        return new Response(false,null);
    }

    @PostMapping("/upgrade/prepared")
    public Response upgradeVM(HttpServletRequest request, RequestMsVM msVM){

        try {
            if (trVmUpdateRepo.findByVmId(msVM.getId()).size() > 0 ){
                return new Response(false,"Sorry, this VM is in progress");
            }

            MsUser user = msUserPortalRepo.findByEmail(request.getUserPrincipal().getName());

//            SET Request
            TrRequestVm trReq = new TrRequestVm();
            trReq.setActive(true);
            trReq.setCreateBy(user.getId());
            trReq.setCreateDate(LocalDateTime.now());
            trReq.setVmId(msVM.getId());
            trReq.setRequestType("MODIFICATION");
            trReq.setRemark(msVM.getRemark());
            TrRequestVm newReq = trRequestVmRepo.save(trReq);

//            SET PROGRESS
            TrRequestProgress trProg = new TrRequestProgress();
            trProg.setActive(true);
            trProg.setCreateBy(user.getId());
            trProg.setCreateDate(LocalDateTime.now());
            trProg.setProgressId(Long.valueOf(2));
            trProg.setRequestId(newReq.getId());
            TrRequestProgress newProgress = trRequestProgressRepo.save(trProg);

//            UPDATE REQUEST
            newReq.setRequestProgressId(newProgress.getId());
            TrRequestVm setReqVM = trRequestVmRepo.save(newReq);

//            SET VM REQUIREMENT
            TrVmUpdate tvUpdate = new TrVmUpdate();
            tvUpdate.setCore(msVM.getCpu());
            tvUpdate.setSocket(msVM.getSocket());
            tvUpdate.setStorage(msVM.getStorage());
            tvUpdate.setMemory(msVM.getMemory());
            tvUpdate.setRequestId(setReqVM.getId());
            tvUpdate.setCreateBy(user.getId());
            tvUpdate.setCreateDate(LocalDateTime.now());
            trVmUpdateRepo.save(tvUpdate);

            telegram.send(user.getName()+" submit Request Upgrade/Modification VM " +"<br/>"
                    +"ID : "+setReqVM.getId()+"<br/>"
                    +"Description : "+msVM.getDescription()+"<br/>"
                    +"CPU : "+msVM.getSocket()+" socket, "+msVM.getCpu()+" core "+"<br/>"
                    +"Memory : "+msVM.getMemory()+"GB <br/>"
                    +"Storage : "+msVM.getStorage()+"GB <br/>"
                    +"Note : "+msVM.getRemark()
            );
            setApproverTask( request,newProgress.getId() );

            ObjectMapper obj = new ObjectMapper();

            userManagementService.track(request,"-",obj.writeValueAsString(setReqVM),"Request Upgrade/Modification VM SYSOPS (Prepared) "+"[Req ID:"+setReqVM.getId()+"]");

            return new Response(true,setReqVM);
        }catch (Exception e){
            return new Response(false,null);
        }
    }

    @PutMapping("/upgrade/prepared")
    public Response reupgradeVM(HttpServletRequest request, RequestMsVM msVM){

        try {

            MsUser user = msUserPortalRepo.findByEmail(request.getUserPrincipal().getName());

//            SET Request
            TrRequestVm trReq = trRequestVmRepo.findById(msVM.getRequestId()).get();

//            SET PROGRESS
            TrRequestProgress trProg = new TrRequestProgress();
            trProg.setActive(true);
            trProg.setCreateBy(user.getId());
            trProg.setCreateDate(LocalDateTime.now());
            trProg.setProgressId(Long.valueOf(2));
            trProg.setRequestId(trReq.getId());
            TrRequestProgress newProgress = trRequestProgressRepo.save(trProg);

//            UPDATE REQUEST
            trReq.setRequestProgressId(newProgress.getId());
            trReq.setRemark(msVM.getRemark());
            TrRequestVm setReqVM = trRequestVmRepo.save(trReq);

//            SET VM REQUIREMENT
            TrVmUpdate tvUpdate = trVmUpdateRepo.findByRequestId(trReq.getId());
            tvUpdate.setCore(msVM.getCpu());
            tvUpdate.setSocket(msVM.getSocket());
            tvUpdate.setStorage(msVM.getStorage());
            tvUpdate.setMemory(msVM.getMemory());
            tvUpdate.setRequestId(setReqVM.getId());
            tvUpdate.setUpdateBy(user.getId());
            tvUpdate.setUpdateDate(LocalDateTime.now());
            trVmUpdateRepo.save(tvUpdate);

            telegram.send(user.getName()+" submit Request Upgrade/Modification VM " +"<br/>"
                    +"ID : "+setReqVM.getId()+"<br/>"
                    +"Description : "+msVM.getDescription()+"<br/>"
                    +"CPU : "+msVM.getSocket()+" socket, "+msVM.getCpu()+" core "+"<br/>"
                    +"Memory : "+msVM.getMemory()+"GB <br/>"
                    +"Storage : "+msVM.getStorage()+"GB <br/>"
                    +"Note : "+msVM.getRemark()
            );
            setApproverTask( request,newProgress.getId() );

            ObjectMapper obj = new ObjectMapper();

            userManagementService.track(request,"-",obj.writeValueAsString(setReqVM),"Request Upgrade/Modification VM SYSOPS (Prepared) "+"[Req ID:"+setReqVM.getId()+"]");

            return new Response(true,setReqVM);
        }catch (Exception e){
            return new Response(false,e.getMessage()+""+e.getCause());
        }
    }


    @PostMapping("/terminate/prepared")
    public Response terminateVM(HttpServletRequest request, RequestMsVM msVM){

        try {
            if (trRequestVmRepo.findByVmId(msVM.getId()).size() > 0 ){
                return new Response(false,"Sorry, this VM is in progress");
            }

            MsUser user = msUserPortalRepo.findByEmail(request.getUserPrincipal().getName());

//            SET Request
            TrRequestVm trReq = new TrRequestVm();
            trReq.setActive(true);
            trReq.setCreateBy(user.getId());
            trReq.setCreateDate(LocalDateTime.now());
            trReq.setVmId(msVM.getId());
            trReq.setRequestType("TERMINATE");
            trReq.setRemark(msVM.getRemark());
            TrRequestVm newReq = trRequestVmRepo.save(trReq);

//            SET PROGRESS
            TrRequestProgress trProg = new TrRequestProgress();
            trProg.setActive(true);
            trProg.setCreateBy(user.getId());
            trProg.setCreateDate(LocalDateTime.now());
            trProg.setProgressId(Long.valueOf(2));
            trProg.setRequestId(newReq.getId());
            TrRequestProgress newProgress = trRequestProgressRepo.save(trProg);

//            UPDATE REQUEST
            newReq.setRequestProgressId(newProgress.getId());
            TrRequestVm setReqVM = trRequestVmRepo.save(newReq);

            telegram.send(user.getName()+" submit Request Terminate VM " +"<br/>"
                    +"ID : "+setReqVM.getId()+"<br/>"
            );
            setApproverTask( request,newProgress.getId() );

            ObjectMapper obj = new ObjectMapper();

            userManagementService.track(request,"-",obj.writeValueAsString(setReqVM),"Request Terminate VM SYSOPS (Prepared) "+"[Req ID:"+setReqVM.getId()+"]");

            return new Response(true,setReqVM);
        }catch (Exception e){
            return new Response(false,null);
        }
    }


    @PutMapping("/terminate/prepared")
    public Response reterminateVM(HttpServletRequest request, RequestMsVM msVM){

        try {
            MsUser user = msUserPortalRepo.findByEmail(request.getUserPrincipal().getName());

//            SET Request
            TrRequestVm trReq = trRequestVmRepo.findById(msVM.getRequestId()).get();

//            SET PROGRESS
            TrRequestProgress trProg = new TrRequestProgress();
            trProg.setActive(true);
            trProg.setCreateBy(user.getId());
            trProg.setCreateDate(LocalDateTime.now());
            trProg.setProgressId(Long.valueOf(2));
            trProg.setRequestId(trReq.getId());
            TrRequestProgress newProgress = trRequestProgressRepo.save(trProg);

//            UPDATE REQUEST
            trReq.setRemark(msVM.getRemark());
            trReq.setRequestProgressId(newProgress.getId());
            TrRequestVm setReqVM = trRequestVmRepo.save(trReq);

            telegram.send(user.getName()+" submit Request Terminate VM " +"<br/>"
                    +"ID : "+setReqVM.getId()+"<br/>"
            );
            setApproverTask( request,newProgress.getId() );

            ObjectMapper obj = new ObjectMapper();

            userManagementService.track(request,"-",obj.writeValueAsString(setReqVM),"Request Terminate VM SYSOPS (Prepared) "+"[Req ID:"+setReqVM.getId()+"]");

            return new Response(true,setReqVM);
        }catch (Exception e){
            return new Response(false,e.getMessage());
        }
    }




    ///////

    public TrRequestVm store(HttpServletRequest request,MsVM msVM)  {

        try {
            msVM.setCreateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
            msVM.setCreateDate(LocalDateTime.now());
            msVM.setActive(false);
            msVM.setCategoryId(Long.valueOf(1));
            msVM.setCompanyId(Long.valueOf(1));
            MsVM setMsVm = msVMRepo.save(msVM);

            TrRequestVm trReq = new TrRequestVm();
            trReq.setActive(true);
            trReq.setCreateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
            trReq.setCreateDate(LocalDateTime.now());
            trReq.setVmId(setMsVm.getId());
            TrRequestVm newReq = trRequestVmRepo.save(trReq);

            TrRequestProgress trProg = new TrRequestProgress();
            trProg.setActive(true);
            trProg.setCreateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
            trProg.setCreateDate(LocalDateTime.now());
            trProg.setProgressId(Long.valueOf(1));
            trProg.setRequestId(newReq.getId());
            TrRequestProgress newProgress = trRequestProgressRepo.save(trProg);

            newReq.setRequestProgressId(newProgress.getId());
            trRequestVmRepo.save(newReq);

            ObjectMapper obj = new ObjectMapper();

            userManagementService.track(request,"-",obj.writeValueAsString(setMsVm),"Request VM SYSOPS (Save Draft) "+"[Req ID:"+newReq.getId()+"]");

            return newReq;
        }catch (Exception e){
            log.info(e.getMessage()+"="+e.getCause());
            return null;
        }


    }


    public void setApproverTask(HttpServletRequest request,Long requestProgressID){
        List<MsUser> approvers = msUserPortalRepo.findApproverByAccesID(Long.valueOf(51));
        List<TrApprovementRequest> trApprovers = new ArrayList<>();

        String whos = "And Need Approval by : ";
        for (MsUser approver:approvers
        ) {
            whos=whos+"<br/>"+approver.getName();
            TrApprovementRequest trs = new TrApprovementRequest();
            trs.setActive(true);
            trs.setCreateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
            trs.setCreateDate(LocalDateTime.now());
            trs.setUserId(approver.getId());
            trs.setRequestProgressId(requestProgressID);
            trApprovers.add(trs);
        }

        telegram.send(whos);
        trApprovementRequestRepo.saveAll(trApprovers);
    }
    @Autowired
    private TrEvidenceTaskRepo trEvidenceTaskRepo;


    public void setEvidenceTask(HttpServletRequest request,Long requestProgressID){
        List<MsUser> users = msUserPortalRepo.findApproverByAccesID(Long.valueOf(11));
        List<TrEvidenceTask> trEvidenceTasks = new ArrayList<>();
        for (MsUser user:users
        ) {
            TrEvidenceTask trs = new TrEvidenceTask();
            trs.setActive(true);
            trs.setCreateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
            trs.setCreateDate(LocalDateTime.now());
            trs.setUserId(user.getId());
            trs.setRequestProgressId(requestProgressID);
            trEvidenceTasks.add(trs);
        }
        trEvidenceTaskRepo.saveAll(trEvidenceTasks);
    }

    public Boolean isApprove(HttpServletRequest request,Long requestProgressID){

        List<TrApprovementRequest> trsAppr = trApprovementRequestRepo.findByReqProgressId(requestProgressID);

        if (trsAppr.stream().anyMatch(e->e.getApprove()==null)){
            return false;
        };

        return trsAppr.stream().allMatch(e->e.getApprove());
    }
}

