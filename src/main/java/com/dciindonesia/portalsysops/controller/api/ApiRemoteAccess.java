package com.dciindonesia.portalsysops.controller.api;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsRemoteAccess;
import com.dciindonesia.portalsysops.model.Response;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsRemoteAccessRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@RestController
@RequestMapping("/api/remote-access")
public class ApiRemoteAccess {

    @Autowired
    private MsRemoteAccessRepo msRemoteAccessRepo;

    @Autowired
    private MsUserRepo msUserRepo;

    @Autowired
    private UserManagementService userManagementService;

    @GetMapping
    public Response getAllData(HttpServletRequest request){

        userManagementService.track(request,null,null,"View Remote Access Menu ");
        return new Response(true,msRemoteAccessRepo.findAllActive());
    }


    @PostMapping
    public Response storeData(HttpServletRequest request,MsRemoteAccess msRemoteAccess) throws JsonProcessingException {

        msRemoteAccess.setActive(true);
        msRemoteAccess.setCreateDate(LocalDateTime.now());
        msRemoteAccess.setCreateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
        MsRemoteAccess sv = msRemoteAccessRepo.save(msRemoteAccess);

        ObjectMapper obj  = new ObjectMapper();
        userManagementService.track(request,null,obj.writeValueAsString(sv),"Add New Remote Access "+sv.getPurpose());
        return new Response(true,sv);
    }

    @PutMapping
    public Response updateData(HttpServletRequest request,MsRemoteAccess msRemoteAccess) throws JsonProcessingException {

        MsRemoteAccess getdt = msRemoteAccessRepo.findById(msRemoteAccess.getId()).get();
        MsRemoteAccess getBefore = getdt;

        msRemoteAccess.setCreateDate(getdt.getCreateDate());
        msRemoteAccess.setCreateBy(getdt.getCreateBy());
        msRemoteAccess.setActive(true);
        msRemoteAccess.setUpdateDate(LocalDateTime.now());
        msRemoteAccess.setUpdateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
        MsRemoteAccess sv = msRemoteAccessRepo.save(msRemoteAccess);

        ObjectMapper obj  = new ObjectMapper();
        userManagementService.track(request,obj.writeValueAsString(getBefore),obj.writeValueAsString(sv),"Changes Remote Access "+sv.getPurpose());
        return new Response(true,sv);

    }


    @DeleteMapping
    public Response deleteData(HttpServletRequest request,MsRemoteAccess msRemoteAccess) throws JsonProcessingException {
        MsRemoteAccess getdt = msRemoteAccessRepo.findById(msRemoteAccess.getId()).get();
        getdt.setActive(false);
        getdt.setUpdateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
        getdt.setUpdateDate(LocalDateTime.now());
        MsRemoteAccess sv = msRemoteAccessRepo.save(getdt);

        ObjectMapper obj  = new ObjectMapper();
        userManagementService.track(request,null,obj.writeValueAsString(sv),"Delete Remote Access "+sv.getPurpose());
        return  new Response(true,sv);
    }

}
