package com.dciindonesia.portalsysops.controller.api;

import com.dciindonesia.portalsysops.entity.portalmanagement.MsAccess;
import com.dciindonesia.portalsysops.model.Response;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.repository.portalmanagement.MsAccessRepo;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@RestController
@RequestMapping("/api/user-management/access")
public class ApiAccess {

    @Autowired
    private MsAccessRepo msAccessRepo;

    @Autowired
    private MsUserRepo msUserRepo;

    @Autowired
    private UserManagementService userManagementService;


    @GetMapping("")
    public Response getAllActive( HttpServletRequest request){
        List<MsAccess> data = msAccessRepo.findAll();
        Collections.sort(data, Comparator.comparingLong(MsAccess::getId).reversed());
        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,null,null,"View Access Menu ");

        return new Response(true, data);
    }

    @PostMapping("")
    public Response store(HttpServletRequest request, MsAccess msAccess){
        try {
            msAccess.setActive(true);
            msAccess.setCreateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
            msAccess.setCreateDate(LocalDateTime.now());
            MsAccess access = msAccessRepo.save(msAccess);
            ObjectMapper obj = new ObjectMapper();
            userManagementService.track(request,null,obj.writeValueAsString(access),"Add New Access ID : "+msAccess.getName());

            return new Response(true,access);
        }catch (Exception e){
            return new Response(false,null);
        }
    }

    @PutMapping("")
    public Response update(HttpServletRequest request, MsAccess msAccess){
        try {

            MsAccess getExist = msAccessRepo.findById(msAccess.getId()).get();
            MsAccess getBefore = getExist;
            getExist.setName(msAccess.getName());
            getExist.setDescription(msAccess.getDescription());
            MsAccess svAccess = msAccessRepo.save(getExist);
            ObjectMapper obj = new ObjectMapper();
            userManagementService.track(request,obj.writeValueAsString(getBefore),obj.writeValueAsString(svAccess),"Changes Access ID : "+msAccess.getName());
            return new Response(true,svAccess);
        }catch (Exception e){
            return new Response(false, e.getMessage());
        }
    }

    @DeleteMapping("")
    public Response delete(HttpServletRequest request, MsAccess msAccess){
        MsAccess getExist = msAccessRepo.findById(msAccess.getId()).get();
        getExist.setActive(false);
        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,null,null,"Delete Access ID : "+getExist.getName());
        return new Response(true,msAccessRepo.save(getExist));
    }
}
