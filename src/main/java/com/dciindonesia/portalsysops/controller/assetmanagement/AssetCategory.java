package com.dciindonesia.portalsysops.controller.assetmanagement;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsAssetCategory;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsAssetCategoryRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Controller
@RequestMapping(value = "/asset-management/master-data/asset/category")
public class AssetCategory {


    @Autowired
    private MsAssetCategoryRepo msAssetCategoryRepo;
    @Autowired
    private MsUserRepo msUserRepo;

    @Autowired
    private UserManagementService userManagementService;
    @GetMapping("")
    public String getAll(HttpServletRequest request, Model model) throws JsonProcessingException {
        ObjectMapper obj  = new ObjectMapper();
        model.addAttribute("data", obj.writeValueAsString(msAssetCategoryRepo.findAllActive()) );

        userManagementService.track(request,null,null,"View Asset Category");


        return "assetcategory";
    };

    @PostMapping("")
    public String postData(HttpServletRequest request,MsAssetCategory msAssetCategory) throws JsonProcessingException {

        String email = request.getUserPrincipal().getName();
        MsAssetCategory newid =new MsAssetCategory();
        newid.setName(msAssetCategory.getName());
        newid.setSlug(msAssetCategory.getSlug());
        newid.setActive(true);
        newid.setCreateDate(LocalDateTime.now());
        newid.setCreateBy(msUserRepo.findByEmail(email).getId());
        MsAssetCategory sv = msAssetCategoryRepo.save(newid);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,null,obj.writeValueAsString(sv),"Add New Asset Category "+sv.getName());

        return "redirect:/asset-management/master-data/asset/category";
    }

    @PostMapping("/edit")
    public String putData(HttpServletRequest request,MsAssetCategory msAssetCategory) throws JsonProcessingException {

        request.getUserPrincipal().getName();
        MsAssetCategory getdt = msAssetCategoryRepo.findById(msAssetCategory.getId()).get();
        MsAssetCategory getBefore = getdt;
        getdt.setName(msAssetCategory.getName());
        getdt.setSlug(msAssetCategory.getSlug());
        MsAssetCategory sv = msAssetCategoryRepo.save(getdt);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,obj.writeValueAsString(getBefore),obj.writeValueAsString(sv),"Changes Asset Category "+sv.getName());


        return "redirect:/asset-management/master-data/asset/category";
    }

    @GetMapping("/delete")
    public String deleteData(HttpServletRequest request,MsAssetCategory msAssetCategory){
        request.getUserPrincipal().getName();
        MsAssetCategory getdt = msAssetCategoryRepo.findById(msAssetCategory.getId()).get();
        getdt.setActive(false);
        MsAssetCategory sv = msAssetCategoryRepo.save(getdt);

        userManagementService.track(request,null,null,"Delete Asset Category "+sv.getName());


        return "redirect:/asset-management/master-data/asset/category";
    }

}
