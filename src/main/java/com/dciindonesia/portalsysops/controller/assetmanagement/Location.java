package com.dciindonesia.portalsysops.controller.assetmanagement;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsLocation;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsLocationRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Controller
@RequestMapping(value = "/asset-management/master-data/location")
public class Location {

    @Autowired
    private MsLocationRepo msLocationRepo;
    @Autowired
    private MsUserRepo msUserRepo;
    @Autowired
    private UserManagementService userManagementService;
    @GetMapping("")
    public String getAll(HttpServletRequest request,Model model) throws JsonProcessingException {
        ObjectMapper obj  = new ObjectMapper();
        model.addAttribute("privilege",obj.writeValueAsString(
                SecurityContextHolder.getContext().getAuthentication().getAuthorities()));
        model.addAttribute("data", obj.writeValueAsString(msLocationRepo.findAllActive()) );

        userManagementService.track(request,null,null,"View Location Menu ");

        return "location";
    };

    @PostMapping("")
    public String postData(HttpServletRequest request, MsLocation msLocation) throws JsonProcessingException {

        String email = request.getUserPrincipal().getName();
        MsLocation newid =new MsLocation();
        newid.setName(msLocation.getName());
        newid.setActive(true);
        newid.setCreateDate(LocalDateTime.now());
        newid.setCreateBy(msUserRepo.findByEmail(email).getId());
        MsLocation sv  = msLocationRepo.save(newid);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,null,obj.writeValueAsString(sv),"Add New Location "+sv.getName());

        return "redirect:/asset-management/master-data/location";
    }

    @PostMapping("/edit")
    public String putData(HttpServletRequest request,MsLocation msLocation) throws JsonProcessingException {

        request.getUserPrincipal().getName();
        MsLocation getdt = msLocationRepo.findById(msLocation.getId()).get();
        MsLocation getBefore = getdt;
        getdt.setName(msLocation.getName());
        MsLocation sv = msLocationRepo.save(getdt);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,obj.writeValueAsString(getBefore),obj.writeValueAsString(sv),"Changes Location "+sv.getName());

        return "redirect:/asset-management/master-data/location";
    }

    @GetMapping("/delete")
    public String deleteData(HttpServletRequest request,MsLocation msLocation){
        request.getUserPrincipal().getName();
        MsLocation getdt = msLocationRepo.findById(msLocation.getId()).get();
        getdt.setActive(false);
        MsLocation sv = msLocationRepo.save(getdt);

        userManagementService.track(request,null,null,"Delete Location "+sv.getName());

        return "redirect:/asset-management/master-data/location";
    }
}
