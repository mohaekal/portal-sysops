package com.dciindonesia.portalsysops.controller.assetmanagement;

import com.dciindonesia.portalsysops.entity.assetmgmt.LogAccessory;
import com.dciindonesia.portalsysops.entity.assetmgmt.MsAccessory;
import com.dciindonesia.portalsysops.repository.assetmgmt.*;
import com.dciindonesia.portalsysops.service.AccessoryAssetTagGenerate;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Controller
@RequestMapping(value = "/asset-management/accessory")
public class Accessory {
    @Autowired
    private MsAccessoryRepo msAccessoryRepo;

    @Autowired
    private MsSupplierRepo msSupplierRepo;
    @Autowired
    private MsManufacturerRepo msManufacturerRepo;
    @Autowired
    private MsAccessoryCategoryRepo msAccessoryCategoryRepo;
    @Autowired
    private MsUserRepo msUserRepo;
    @Autowired
    private MsLocationRepo msLocationRepo;
    @Autowired
    private MsRoomRepo msRoomRepo;
    @Autowired
    private MsAccessoryStatusRepo msAccessoryStatusRepo;

    @Autowired
    private LogAccessoryRepo logAccessoryRepo;

    @Autowired
    private UserManagementService userManagementService;

    private Logger log = LoggerFactory.getLogger("go");
    @GetMapping("")
    public String getAll(HttpServletRequest request, Model model) throws JsonProcessingException {
        ObjectMapper obj  = new ObjectMapper();
        model.addAttribute("privilege",obj.writeValueAsString(
                SecurityContextHolder.getContext().getAuthentication().getAuthorities()));
        model.addAttribute("suppliers",msSupplierRepo.findAllActive());
        model.addAttribute("categories",msAccessoryCategoryRepo.findAllActive());
        model.addAttribute("manufacturers",msManufacturerRepo.findAllActive());
        model.addAttribute("locations",msLocationRepo.findAllActive());
        model.addAttribute("rooms",msRoomRepo.findAllActive());
        model.addAttribute("statuses",msAccessoryStatusRepo.findAllActive());
        return "accessory";
    };
}
