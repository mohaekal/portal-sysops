package com.dciindonesia.portalsysops.controller.assetmanagement;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsRemoteAccess;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsLocationRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsRemoteAccessRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsRoomRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Controller
@RequestMapping(value = "/asset-management/remote-access")
public class RemoteAccess {


    @Autowired
    private MsRemoteAccessRepo msRemoteAccessRepo;
    @Autowired
    private MsLocationRepo msLocationRepo;
    @Autowired
    private MsRoomRepo msRoomRepo;
    @Autowired
    private MsUserRepo msUserRepo;
    @Autowired
    private UserManagementService userManagementService;

    private Logger log = LoggerFactory.getLogger("remoteaccess");
    @GetMapping("")
    public String getAll(HttpServletRequest request,Model model) throws JsonProcessingException {
        ObjectMapper obj  = new ObjectMapper();
        model.addAttribute("privilege",obj.writeValueAsString(
                SecurityContextHolder.getContext().getAuthentication().getAuthorities()));

        model.addAttribute("locations",msLocationRepo.findAllActive());
        model.addAttribute("rooms",msRoomRepo.findAllActive());

        return "remoteaccess";
    };

}
