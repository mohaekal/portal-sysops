package com.dciindonesia.portalsysops.controller.assetmanagement;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsConsumable;
import com.dciindonesia.portalsysops.entity.assetmgmt.MsConsumableCategory;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsConsumableCategoryRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Controller
@RequestMapping(value = "/asset-management/master-data/consumable/category")
public class ConsumableCategory {
    
    @Autowired
    private MsConsumableCategoryRepo msConsumableCategoryRepo;

    @Autowired
    private MsUserRepo msUserRepo;

    @Autowired
    private UserManagementService userManagementService;

    @GetMapping("")
    public String getAll(HttpServletRequest request,Model model) throws JsonProcessingException {
        ObjectMapper obj  = new ObjectMapper();
        model.addAttribute("data", obj.writeValueAsString(msConsumableCategoryRepo.findAllActive()) );

        userManagementService.track(request,null,null,"View Consumable");

        return "consumablecategory";
    };

    @PostMapping("")
    public String postData(HttpServletRequest request, MsConsumableCategory msConsumableCategory) throws JsonProcessingException {

        String email = request.getUserPrincipal().getName();
        MsConsumableCategory newid =new MsConsumableCategory();
        newid.setName(msConsumableCategory.getName());
        newid.setActive(true);
        newid.setCreateDate(LocalDateTime.now());
        newid.setCreateBy(msUserRepo.findByEmail(email).getId());
        MsConsumableCategory sv =  msConsumableCategoryRepo.save(newid);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,null,obj.writeValueAsString(sv),"Add New Consumable "+sv.getName());

        return "redirect:/asset-management/master-data/consumable/category";
    }

    @PostMapping("/edit")
    public String putData(HttpServletRequest request,MsConsumableCategory msConsumableCategory) throws JsonProcessingException {

        request.getUserPrincipal().getName();
        MsConsumableCategory getdt = msConsumableCategoryRepo.findById(msConsumableCategory.getId()).get();
        MsConsumableCategory getBefore = getdt;
        getdt.setName(msConsumableCategory.getName());
        MsConsumableCategory sv = msConsumableCategoryRepo.save(getdt);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,obj.writeValueAsString(getBefore),obj.writeValueAsString(sv),"Changes Consumable "+sv.getName());


        return "redirect:/asset-management/master-data/consumable/category";
    }

    @GetMapping("/delete")
    public String deleteData(HttpServletRequest request,MsConsumableCategory msConsumableCategory){
        request.getUserPrincipal().getName();
        MsConsumableCategory getdt = msConsumableCategoryRepo.findById(msConsumableCategory.getId()).get();
        getdt.setActive(false);
        MsConsumableCategory sv = msConsumableCategoryRepo.save(getdt);

        userManagementService.track(request,null,null,"Delete Consumable "+sv.getName());

        return "redirect:/asset-management/master-data/consumable/category";
    }
}
