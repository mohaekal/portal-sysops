package com.dciindonesia.portalsysops.controller.assetmanagement;

import com.dciindonesia.portalsysops.entity.assetmgmt.LogConsumable;
import com.dciindonesia.portalsysops.entity.assetmgmt.MsConsumable;
import com.dciindonesia.portalsysops.repository.assetmgmt.*;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;

@Controller
@RequestMapping(value = "/asset-management/consumable")
public class Consumable {
    @Autowired
    private MsConsumableRepo msConsumableRepo;
    @Autowired
    private MsManufacturerRepo msManufacturerRepo;
    @Autowired
    private MsConsumableCategoryRepo msConsumableCategoryRepo;
    @Autowired
    private MsLocationRepo msLocationRepo;
    @Autowired
    private MsRoomRepo msRoomRepo;
    private Logger log = LoggerFactory.getLogger("Consumable");


    @GetMapping("")
    public String getAll(HttpServletRequest request,Model model) throws JsonProcessingException {
        ObjectMapper obj  = new ObjectMapper();
        model.addAttribute("privilege",obj.writeValueAsString(
                SecurityContextHolder.getContext().getAuthentication().getAuthorities()));
        model.addAttribute("categories",msConsumableCategoryRepo.findAllActive());
        model.addAttribute("manufacturers",msManufacturerRepo.findAllActive());
        model.addAttribute("locations",msLocationRepo.findAllActive());
        model.addAttribute("rooms",msRoomRepo.findAllActive());
        return "consumable";
    };
}
