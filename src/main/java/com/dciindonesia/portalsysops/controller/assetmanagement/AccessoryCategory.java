package com.dciindonesia.portalsysops.controller.assetmanagement;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsAccessoryCategory;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsAccessoryCategoryRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Controller
@RequestMapping(value = "/asset-management/master-data/accessory/category")
public class AccessoryCategory {
    @Autowired
    private MsAccessoryCategoryRepo msAccessoryCategoryRepo;
    @Autowired
    private MsUserRepo msUserRepo;
    @Autowired
    private UserManagementService userManagementService;

    @GetMapping("")
    public String getAll(HttpServletRequest request, Model model) throws JsonProcessingException {
        ObjectMapper obj  = new ObjectMapper();
        model.addAttribute("data", obj.writeValueAsString(msAccessoryCategoryRepo.findAllActive()) );

        userManagementService.track(request,null,null,"View Accessory Category");
        return "accessorycategory";
    };

    @PostMapping("")
    public String postData(HttpServletRequest request, MsAccessoryCategory msAccessoryCategory) throws JsonProcessingException {

        String email = request.getUserPrincipal().getName();
        msAccessoryCategory.setActive(true);
        msAccessoryCategory.setCreateDate(LocalDateTime.now());
        msAccessoryCategory.setCreateBy(msUserRepo.findByEmail(email).getId());
        MsAccessoryCategory sv = msAccessoryCategoryRepo.save(msAccessoryCategory);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,null,obj.writeValueAsString(sv),"Add Accessory Category "+sv.getName());

        return "redirect:/asset-management/master-data/accessory/category";
    }

    @PostMapping("/edit")
    public String putData(HttpServletRequest request,MsAccessoryCategory msAccessoryCategory) throws JsonProcessingException {

        request.getUserPrincipal().getName();
        MsAccessoryCategory getdt = msAccessoryCategoryRepo.findById(msAccessoryCategory.getId()).get();
        MsAccessoryCategory getBefore = getdt;
        getdt.setName(msAccessoryCategory.getName());
        getdt.setSlug(msAccessoryCategory.getSlug());
        MsAccessoryCategory sv =msAccessoryCategoryRepo.save(getdt);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,obj.writeValueAsString(getBefore),obj.writeValueAsString(sv),"Changes Accessory Category "+sv.getName());

        return "redirect:/asset-management/master-data/accessory/category";
    }

    @GetMapping("/delete")
    public String deleteData(HttpServletRequest request,MsAccessoryCategory msAccessoryCategory) throws JsonProcessingException {
        request.getUserPrincipal().getName();
        MsAccessoryCategory getdt = msAccessoryCategoryRepo.findById(msAccessoryCategory.getId()).get();
        getdt.setActive(false);
        MsAccessoryCategory sv = msAccessoryCategoryRepo.save(getdt);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,"-",obj.writeValueAsString(sv),"Delete Accessory Category "+msAccessoryCategory.getName());

        return "redirect:/asset-management/master-data/accessory/category";
    }
}
