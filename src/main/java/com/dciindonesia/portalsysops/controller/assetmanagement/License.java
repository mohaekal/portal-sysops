package com.dciindonesia.portalsysops.controller.assetmanagement;

import com.dciindonesia.portalsysops.entity.assetmgmt.LicenseFile;
import com.dciindonesia.portalsysops.entity.assetmgmt.MsLicense;
import com.dciindonesia.portalsysops.repository.assetmgmt.*;
import com.dciindonesia.portalsysops.service.LicenseStorageFile;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Controller
@RequestMapping(value = "/asset-management/license")
public class License {
    @Autowired
    private MsLicenseRepo msLicenseRepo;

    @Autowired
    private MsSupplierRepo msSupplierRepo;
    @Autowired
    private MsManufacturerRepo msManufacturerRepo;
    @Autowired
    private MsLicenseCategoryRepo msLicenseCategoryRepo;
    @Autowired
    private MsUserRepo msUserRepo;
    private Logger log = LoggerFactory.getLogger("go");

    @Autowired
    private UserManagementService userManagementService;

    @Autowired
    private LicenseStorageFile licenseStorageFile;
    @GetMapping("")
    public String getAll(HttpServletRequest request, Model model) throws JsonProcessingException {
        ObjectMapper obj  = new ObjectMapper();
        model.addAttribute("privilege",obj.writeValueAsString(
                SecurityContextHolder.getContext().getAuthentication().getAuthorities()));
        model.addAttribute("suppliers",msSupplierRepo.findAllActive());
        model.addAttribute("categories",msLicenseCategoryRepo.findAllActive());
        model.addAttribute("manufacturers",msManufacturerRepo.findAllActive());

        return "license";
    };

    @GetMapping("/download/file")
    public ResponseEntity<byte[]> getFile(Long id) {
        LicenseFile fileDB = licenseStorageFile.getFile(id);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileDB.getName() + "\"")
                .body(fileDB.getData());
    }

}
