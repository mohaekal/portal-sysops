package com.dciindonesia.portalsysops.controller.assetmanagement;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsRoom;
import com.dciindonesia.portalsysops.entity.assetmgmt.MsSupplier;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsRoomRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsSupplierRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Controller
@RequestMapping(value = "/asset-management/master-data/room")
public class Room {

    @Autowired
    private MsRoomRepo msRoomRepo;
    @Autowired
    private MsUserRepo msUserRepo;
    @Autowired
    private UserManagementService userManagementService;

    @GetMapping("")
    public String getAll(HttpServletRequest request,Model model) throws JsonProcessingException {
        ObjectMapper obj  = new ObjectMapper();
        model.addAttribute("privilege",obj.writeValueAsString(
                SecurityContextHolder.getContext().getAuthentication().getAuthorities()));
        model.addAttribute("data", obj.writeValueAsString(msRoomRepo.findAllActive()) );

        userManagementService.track(request,null,null,"View Room Menu");


        return "room";
    };

    @PostMapping("")
    public String postData(HttpServletRequest request, MsRoom msRoom) throws JsonProcessingException {

        String email = request.getUserPrincipal().getName();
        MsRoom newid =new MsRoom();
        newid.setName(msRoom.getName());
        newid.setActive(true);
        newid.setCreateDate(LocalDateTime.now());
        newid.setCreateBy(msUserRepo.findByEmail(email).getId());
        MsRoom sv = msRoomRepo.save(newid);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,null,obj.writeValueAsString(sv),"Add New Room "+sv.getName());

        return "redirect:/asset-management/master-data/room";
    }

    @PostMapping("/edit")
    public String putData(HttpServletRequest request,MsRoom msRoom) throws JsonProcessingException {

        request.getUserPrincipal().getName();
        MsRoom getdt = msRoomRepo.findById(msRoom.getId()).get();
        MsRoom getBefore = getdt;

        getdt.setName(msRoom.getName());
        MsRoom sv =  msRoomRepo.save(getdt);
        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,obj.writeValueAsString(getBefore),obj.writeValueAsString(sv),"Changes Room "+sv.getName());

        return "redirect:/asset-management/master-data/room";
    }

    @GetMapping("/delete")
    public String deleteData(HttpServletRequest request,MsRoom msRoom){
        request.getUserPrincipal().getName();
        MsRoom getdt = msRoomRepo.findById(msRoom.getId()).get();
        getdt.setActive(false);
        MsRoom sv = msRoomRepo.save(getdt);

        userManagementService.track(request,null,null,"Delete Room "+sv.getName());

        return "redirect:/asset-management/master-data/room";
    }
}
