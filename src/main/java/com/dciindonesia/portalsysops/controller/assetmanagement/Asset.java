package com.dciindonesia.portalsysops.controller.assetmanagement;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsAsset;
import com.dciindonesia.portalsysops.repository.assetmgmt.*;
import com.dciindonesia.portalsysops.service.AssetTagGenerate;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Controller
@RequestMapping("/asset-management/asset")
public class Asset {
    @Autowired
    private MsAssetRepo msAssetRepo;
    @Autowired
    private MsAssetCategoryRepo msAssetCategoryRepo;
    @Autowired
    private MsAssetStatusRepo msAssetStatusRepo;
    @Autowired
    private MsLocationRepo msLocationRepo;
    @Autowired
    private MsRoomRepo msRoomRepo;
    @Autowired
    private MsUserRepo msUserRepo;
    @Autowired
    private MsCompanyRepo msCompanyRepo;

    @Autowired
    private UserManagementService userManagementService;

    @GetMapping("")
    public String getAll(HttpServletRequest request,Model model) throws JsonProcessingException {


        ObjectMapper obj  = new ObjectMapper();
        model.addAttribute("privilege",obj.writeValueAsString(
                SecurityContextHolder.getContext().getAuthentication().getAuthorities()));
        model.addAttribute("categories",msAssetCategoryRepo.findAllActive());
        model.addAttribute("statuses",msAssetStatusRepo.findAllActive());
        model.addAttribute("rooms",msRoomRepo.findAllActive());
        model.addAttribute("locations",msLocationRepo.findAllActive());
        model.addAttribute("companies",msCompanyRepo.findAllActive());


        return "asset";
    };
}
