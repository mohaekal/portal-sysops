package com.dciindonesia.portalsysops.controller.assetmanagement;

import com.dciindonesia.portalsysops.repository.assetmgmt.MsAssetRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsCompanyRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsLicenseRepo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/asset-management/vm")
public class VM {

    @Autowired
    private MsLicenseRepo msLicenseRepo;

    @Autowired
    private MsAssetRepo msAssetRepo;

    @Autowired
    private MsCompanyRepo msCompanyRepo;
    @GetMapping("/sysops")
    public String getAll(Model model) throws JsonProcessingException {
        ObjectMapper obj  = new ObjectMapper();
        model.addAttribute("privilege",obj.writeValueAsString(
                SecurityContextHolder.getContext().getAuthentication().getAuthorities()));
        model.addAttribute("licenses",msLicenseRepo.findAllActive());
        model.addAttribute("assets", msAssetRepo.findAllActive());
        model.addAttribute("companies",msCompanyRepo.findAllActive());
        return "vmsysops";
    };

    @GetMapping("/cctv-nms")
    public String getCCtv(Model model) throws JsonProcessingException {
        ObjectMapper obj  = new ObjectMapper();
        model.addAttribute("privilege",obj.writeValueAsString(
                SecurityContextHolder.getContext().getAuthentication().getAuthorities()));
        model.addAttribute("licenses",msLicenseRepo.findAllActive());
        model.addAttribute("assets", msAssetRepo.findAllActive());
        model.addAttribute("companies",msCompanyRepo.findAllActive());
        return "vmcctv";
    };

}
