package com.dciindonesia.portalsysops.controller.assetmanagement;

import com.dciindonesia.portalsysops.entity.assetmgmt.LicenseFile;
import com.dciindonesia.portalsysops.entity.assetmgmt.TrEvidenceFile;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsAssetRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsLicenseRepo;
import com.dciindonesia.portalsysops.service.EvidenceStorageFiles;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/request/vm-cctv-nms")
public class RequestVMCCTVNMS {

    @Autowired
    private MsLicenseRepo msLicenseRepo;

    @Autowired
    private MsAssetRepo msAssetRepo;

    @Autowired
    private EvidenceStorageFiles evidenceStorageFiles;

    @GetMapping
    public String getdata(Model model) throws JsonProcessingException {

        ObjectMapper obj  = new ObjectMapper();
        model.addAttribute("privilege",obj.writeValueAsString(
                SecurityContextHolder.getContext().getAuthentication().getAuthorities()));
        model.addAttribute("licenses",msLicenseRepo.findAllActive());
        model.addAttribute("assets", msAssetRepo.findAllActive());

        return "requestvmcctv";
    }

    @GetMapping("/download/file")
    public ResponseEntity<byte[]> getFile(Long id) {
        TrEvidenceFile fileDB = evidenceStorageFiles.getFilee(id);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileDB.getName() + "\"")
                .body(fileDB.getData());
    }

    @GetMapping("/show/file")
    @CrossOrigin
    public ResponseEntity<byte[]> showFile(Long id) {
        TrEvidenceFile fileDB = evidenceStorageFiles.getFilee(id);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_TYPE, String.valueOf(MediaType.parseMediaType(fileDB.getType())))
                .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + fileDB.getName() + "\"")
                .header(HttpHeaders.CACHE_CONTROL,"must-revalidate, post-check=0, pre-check=0")
                .body(fileDB.getData());
    }
}
