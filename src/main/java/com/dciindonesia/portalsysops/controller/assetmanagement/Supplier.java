package com.dciindonesia.portalsysops.controller.assetmanagement;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsSupplier;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsSupplierRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Controller
@RequestMapping(value = "/asset-management/master-data/supplier")
public class Supplier {

    @Autowired
    private MsSupplierRepo msSupplierRepo;
    @Autowired
    private MsUserRepo msUserRepo;
    @Autowired
    private UserManagementService userManagementService;

    @GetMapping("")
    public String getAll(HttpServletRequest request, Model model) throws JsonProcessingException {
        ObjectMapper obj  = new ObjectMapper();
        model.addAttribute("privilege",obj.writeValueAsString(
                SecurityContextHolder.getContext().getAuthentication().getAuthorities()));
        model.addAttribute("data", obj.writeValueAsString(msSupplierRepo.findAllActive()) );

        userManagementService.track(request,null,null,"View Supplier Menu");

        return "supplier";
    };

    @PostMapping("")
    public String postData(HttpServletRequest request, MsSupplier msSupplier) throws JsonProcessingException {

        String email = request.getUserPrincipal().getName();
        MsSupplier newid =new MsSupplier();
        newid.setName(msSupplier.getName());
        newid.setActive(true);
        newid.setCreateDate(LocalDateTime.now());
        newid.setCreateBy(msUserRepo.findByEmail(email).getId());
        MsSupplier sv = msSupplierRepo.save(newid);

        ObjectMapper obj   = new ObjectMapper();
        userManagementService.track(request,null,obj.writeValueAsString(sv),"Add New Supplier "+sv.getName());

        return "redirect:/asset-management/master-data/supplier";
    }

    @PostMapping("/edit")
    public String putData(HttpServletRequest request,MsSupplier msSupplier) throws JsonProcessingException {

        request.getUserPrincipal().getName();
        MsSupplier getdt = msSupplierRepo.findById(msSupplier.getId()).get();
        MsSupplier getBefore = getdt;

        getdt.setName(msSupplier.getName());
        MsSupplier sv = msSupplierRepo.save(getdt);

        ObjectMapper obj   = new ObjectMapper();
        userManagementService.track(request,obj.writeValueAsString(getBefore),obj.writeValueAsString(sv),"Changes Supplier "+sv.getName());

        return "redirect:/asset-management/master-data/supplier";
    }

    @GetMapping("/delete")
    public String deleteData(HttpServletRequest request,MsSupplier msSupplier){
        request.getUserPrincipal().getName();
        MsSupplier getdt = msSupplierRepo.findById(msSupplier.getId()).get();
        getdt.setActive(false);
        MsSupplier sv = msSupplierRepo.save(getdt);
        userManagementService.track(request,null,null,"Delete Supplier "+sv.getName());


        return "redirect:/asset-management/master-data/supplier";
    }
}
