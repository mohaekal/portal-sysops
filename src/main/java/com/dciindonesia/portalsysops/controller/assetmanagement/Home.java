package com.dciindonesia.portalsysops.controller.assetmanagement;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@CrossOrigin
public class Home {
    @GetMapping("/")
    public String homeroot(){

        return "dashboard";
    }
}
