package com.dciindonesia.portalsysops.controller.assetmanagement;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsApplicationStatus;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsApplicationStatusRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Controller
@RequestMapping(value = "/asset-management/master-data/application/status")
public class ApplicationStatus {

    @Autowired
    private MsApplicationStatusRepo msApplicationStatusRepo;
    @Autowired
    private MsUserRepo msUserRepo;
    @Autowired
    private UserManagementService userManagementService;

    @GetMapping("")
    public String getAll(HttpServletRequest request,Model model) throws JsonProcessingException {
        ObjectMapper obj  = new ObjectMapper();
        model.addAttribute("data", obj.writeValueAsString(msApplicationStatusRepo.findAllActive()) );

        userManagementService.track(request,"-","-","View Application Status Menu ");

        return "applicationstatus";
    };

    @PostMapping("")
    public String postData(HttpServletRequest request, MsApplicationStatus msApplicationStatus) throws JsonProcessingException {

        String email = request.getUserPrincipal().getName();
        MsApplicationStatus newid =new MsApplicationStatus();
        newid.setName(msApplicationStatus.getName());
        newid.setActive(true);
        newid.setCreateDate(LocalDateTime.now());
        newid.setCreateBy(msUserRepo.findByEmail(email).getId());
        MsApplicationStatus sv = msApplicationStatusRepo.save(newid);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,"-",obj.writeValueAsString(sv),"Add New Application Status "+sv.getName());

        return "redirect:/asset-management/master-data/application/status";
    }

    @PostMapping("/edit")
    public String putData(HttpServletRequest request,MsApplicationStatus msApplicationStatus) throws JsonProcessingException {

        request.getUserPrincipal().getName();

        MsApplicationStatus getdt = msApplicationStatusRepo.findById(msApplicationStatus.getId()).get();
        MsApplicationStatus getBefore = getdt;

        getdt.setName(msApplicationStatus.getName());
        MsApplicationStatus sv = msApplicationStatusRepo.save(getdt);


        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,obj.writeValueAsString(getBefore),obj.writeValueAsString(sv),"Changes Application Status "+sv.getName());

        return "redirect:/asset-management/master-data/application/status";
    }

    @GetMapping("/delete")
    public String deleteData(HttpServletRequest request,MsApplicationStatus msApplicationStatus){
        request.getUserPrincipal().getName();
        MsApplicationStatus getdt = msApplicationStatusRepo.findById(msApplicationStatus.getId()).get();
        getdt.setActive(false);
        MsApplicationStatus sv = msApplicationStatusRepo.save(getdt);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request, "-","-","Delete Application Status "+sv.getName());


        return "redirect:/asset-management/master-data/application/status";
    }
}
