package com.dciindonesia.portalsysops.controller.assetmanagement;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsManufacturer;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsManufacturerRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Controller
@RequestMapping(value = "/asset-management/master-data/manufacturer")
public class Manufacturer {

    @Autowired
    private MsManufacturerRepo msManufacturerRepo;
    @Autowired
    private MsUserRepo msUserRepo;
    @Autowired
    private UserManagementService userManagementService;
    @GetMapping("")
    public String getAll(HttpServletRequest request,Model model) throws JsonProcessingException {
        ObjectMapper obj  = new ObjectMapper();
        model.addAttribute("privilege",obj.writeValueAsString(
                SecurityContextHolder.getContext().getAuthentication().getAuthorities()));
        model.addAttribute("data", obj.writeValueAsString(msManufacturerRepo.findAllActive()) );

        userManagementService.track(request,null,null,"View Manufacturer Menu ");

        return "manufacturer";
    };

    @PostMapping("")
    public String postData(HttpServletRequest request, MsManufacturer msManufacturer) throws JsonProcessingException {

        String email = request.getUserPrincipal().getName();
        MsManufacturer newid =new MsManufacturer();
        newid.setName(msManufacturer.getName());
        newid.setActive(true);
        newid.setCreateDate(LocalDateTime.now());
        newid.setCreateBy(msUserRepo.findByEmail(email).getId());
        MsManufacturer sv = msManufacturerRepo.save(newid);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,null,obj.writeValueAsString(sv),"Add New Manufacturer "+sv.getName());

        return "redirect:/asset-management/master-data/manufacturer";
    }

    @PostMapping("/edit")
    public String putData(HttpServletRequest request,MsManufacturer msManufacturer) throws JsonProcessingException {

        request.getUserPrincipal().getName();
        MsManufacturer getdt = msManufacturerRepo.findById(msManufacturer.getId()).get();
        MsManufacturer getBefore  = getdt;
        getdt.setName(msManufacturer.getName());
        MsManufacturer sv = msManufacturerRepo.save(getdt);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,obj.writeValueAsString(getBefore),obj.writeValueAsString(sv),"Changes Manufacturer "+sv.getName());


        return "redirect:/asset-management/master-data/manufacturer";
    }

    @GetMapping("/delete")
    public String deleteData(HttpServletRequest request,MsManufacturer msManufacturer) throws JsonProcessingException {
        request.getUserPrincipal().getName();
        MsManufacturer getdt = msManufacturerRepo.findById(msManufacturer.getId()).get();
        getdt.setActive(false);
        MsManufacturer sv = msManufacturerRepo.save(getdt);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,null,obj.writeValueAsString(sv),"Delete Manufacturer "+sv.getName());

        return "redirect:/asset-management/master-data/manufacturer";
    }
}
