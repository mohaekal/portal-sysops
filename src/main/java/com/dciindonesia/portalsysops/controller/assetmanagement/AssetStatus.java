package com.dciindonesia.portalsysops.controller.assetmanagement;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsAssetStatus;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsAssetStatusRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Controller
@RequestMapping(value = "/asset-management/master-data/asset/status")
public class AssetStatus {

    @Autowired
    private MsAssetStatusRepo msAssetStatusRepo;
    @Autowired
    private MsUserRepo msUserRepo;
    @Autowired
    private UserManagementService userManagementService;

    @GetMapping("")
    public String getAll(HttpServletRequest request, Model model) throws JsonProcessingException {
        ObjectMapper obj  = new ObjectMapper();
        model.addAttribute("data", obj.writeValueAsString(msAssetStatusRepo.findAllActive()) );

        userManagementService.track(request,null,null,"View Asset Status ");

        return "assetstatus";
    };

    @PostMapping("")
    public String postData(HttpServletRequest request, MsAssetStatus msAssetStatus) throws JsonProcessingException {

        String email = request.getUserPrincipal().getName();
        MsAssetStatus newid =new MsAssetStatus();
        newid.setName(msAssetStatus.getName());
        newid.setActive(true);
        newid.setCreateDate(LocalDateTime.now());
        newid.setCreateBy(msUserRepo.findByEmail(email).getId());
        MsAssetStatus sv = msAssetStatusRepo.save(newid);

        ObjectMapper obj= new ObjectMapper();
        userManagementService.track(request,null,obj.writeValueAsString(sv),"Add New Asset Status "+sv.getName());

        return "redirect:/asset-management/master-data/asset/status";
    }

    @PostMapping("/edit")
    public String putData(HttpServletRequest request,MsAssetStatus msAssetStatus) throws JsonProcessingException {

        request.getUserPrincipal().getName();
        MsAssetStatus getdt = msAssetStatusRepo.findById(msAssetStatus.getId()).get();
        MsAssetStatus getBefore = getdt;
        getdt.setName(msAssetStatus.getName());
        MsAssetStatus sv = msAssetStatusRepo.save(getdt);

        ObjectMapper obj= new ObjectMapper();
        userManagementService.track(request,obj.writeValueAsString(getBefore),obj.writeValueAsString(sv),"Changes Asset Status "+sv.getName());

        return "redirect:/asset-management/master-data/asset/status";
    }

    @GetMapping("/delete")
    public String deleteData(HttpServletRequest request,MsAssetStatus msAssetStatus){
        request.getUserPrincipal().getName();
        MsAssetStatus getdt = msAssetStatusRepo.findById(msAssetStatus.getId()).get();
        getdt.setActive(false);
        MsAssetStatus sv = msAssetStatusRepo.save(getdt);

        userManagementService.track(request,null,null,"Delete Asset Status "+sv.getName());

        return "redirect:/asset-management/master-data/asset/status";
    }
}
