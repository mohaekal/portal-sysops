package com.dciindonesia.portalsysops.controller.assetmanagement;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsLicense;
import com.dciindonesia.portalsysops.entity.assetmgmt.MsLicenseCategory;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsLicenseCategoryRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Controller
@RequestMapping(value = "/asset-management/master-data/license/category")
public class LicenseCategory {
    @Autowired
    private MsLicenseCategoryRepo msLicenseCategoryRepo;

    @Autowired
    private MsUserRepo msUserRepo;
    @Autowired
    private UserManagementService userManagementService ;

    @GetMapping("")
    public String getAll(HttpServletRequest request, Model model) throws JsonProcessingException {
        ObjectMapper obj  = new ObjectMapper();
        model.addAttribute("data", obj.writeValueAsString(msLicenseCategoryRepo.findAllActive()) );

        userManagementService.track(request,null,null,"View License Category");

        return "licensecategory";
    };

    @PostMapping("")
    public String postData(HttpServletRequest request, MsLicenseCategory msLicenseCategory) throws JsonProcessingException {

        String email = request.getUserPrincipal().getName();
        MsLicenseCategory newid =new MsLicenseCategory();
        newid.setName(msLicenseCategory.getName());
        newid.setActive(true);
        newid.setCreateDate(LocalDateTime.now());
        newid.setCreateBy(msUserRepo.findByEmail(email).getId());
        MsLicenseCategory sv = msLicenseCategoryRepo.save(newid);
        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,null,obj.writeValueAsString(sv),"Add New License Category "+sv.getName());
        return "redirect:/asset-management/master-data/license/category";
    }

    @PostMapping("/edit")
    public String putData(HttpServletRequest request,MsLicenseCategory msLicenseCategory) throws JsonProcessingException {

        request.getUserPrincipal().getName();
        MsLicenseCategory getdt = msLicenseCategoryRepo.findById(msLicenseCategory.getId()).get();
        MsLicenseCategory getBefore = getdt;
        getdt.setName(msLicenseCategory.getName());
        MsLicenseCategory sv = msLicenseCategoryRepo.save(getdt);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,obj.writeValueAsString(getBefore),obj.writeValueAsString(sv),"Changes License Category "+sv.getName());

        return "redirect:/asset-management/master-data/license/category";
    }

    @GetMapping("/delete")
    public String deleteData(HttpServletRequest request,MsLicenseCategory msLicenseCategory){
        request.getUserPrincipal().getName();
        MsLicenseCategory getdt = msLicenseCategoryRepo.findById(msLicenseCategory.getId()).get();
        getdt.setActive(false);
        msLicenseCategoryRepo.save(getdt);

        userManagementService.track(request,null,null,"Delete Licence Category ");

        return "redirect:/asset-management/master-data/license/category";
    }
}
