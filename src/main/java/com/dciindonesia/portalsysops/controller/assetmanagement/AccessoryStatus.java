package com.dciindonesia.portalsysops.controller.assetmanagement;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsAccessoryStatus;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsAccessoryStatusRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Controller
@RequestMapping(value = "/asset-management/master-data/accessory/status")
public class AccessoryStatus {

    @Autowired
    private MsAccessoryStatusRepo msAccessoryStatusRepo;
    @Autowired
    private MsUserRepo msUserRepo;
    @Autowired
    private UserManagementService userManagementService;

    @GetMapping("")
    public String getAll(HttpServletRequest request,Model model) throws JsonProcessingException {
        ObjectMapper obj  = new ObjectMapper();
        model.addAttribute("data", obj.writeValueAsString(msAccessoryStatusRepo.findAllActive()) );
        userManagementService.track(request,"-","-","View Accessory Status");

        return "accessorystatus";
    };

    @PostMapping("")
    public String postData(HttpServletRequest request, MsAccessoryStatus msAccessoryStatus) throws JsonProcessingException {

        String email = request.getUserPrincipal().getName();
        MsAccessoryStatus newid =new MsAccessoryStatus();
        newid.setName(msAccessoryStatus.getName());
        newid.setActive(true);
        newid.setCreateDate(LocalDateTime.now());
        newid.setCreateBy(msUserRepo.findByEmail(email).getId());
        MsAccessoryStatus sv=msAccessoryStatusRepo.save(newid);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,"-",obj.writeValueAsString(sv),"Add New Accessory Status "+sv.getName());

        return "redirect:/asset-management/master-data/accessory/status";
    }

    @PostMapping("/edit")
    public String putData(HttpServletRequest request,MsAccessoryStatus msAccessoryStatus) throws JsonProcessingException {

        request.getUserPrincipal().getName();
        MsAccessoryStatus getdt = msAccessoryStatusRepo.findById(msAccessoryStatus.getId()).get();
        MsAccessoryStatus getBefore = getdt;
        getdt.setName(msAccessoryStatus.getName());
        MsAccessoryStatus sv = msAccessoryStatusRepo.save(getdt);


        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,obj.writeValueAsString(getBefore),obj.writeValueAsString(sv),"Changes Accessory Status "+sv.getName());

        return "redirect:/asset-management/master-data/accessory/status";
    }

    @GetMapping("/delete")
    public String deleteData(HttpServletRequest request,MsAccessoryStatus msAccessoryStatus) throws JsonProcessingException {
        request.getUserPrincipal().getName();
        MsAccessoryStatus getdt = msAccessoryStatusRepo.findById(msAccessoryStatus.getId()).get();
        getdt.setActive(false);
        MsAccessoryStatus sv = msAccessoryStatusRepo.save(getdt);

        ObjectMapper obj = new ObjectMapper();
        userManagementService.track(request,"-",obj.writeValueAsString(sv),"Delete Accessory Status "+sv.getName());


        return "redirect:/asset-management/master-data/accessory/status";
    }
}
