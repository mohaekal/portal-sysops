package com.dciindonesia.portalsysops.controller.assetmanagement;

import com.dciindonesia.portalsysops.entity.assetmgmt.IpVMDetail;
import com.dciindonesia.portalsysops.entity.assetmgmt.IpVm;
import com.dciindonesia.portalsysops.entity.assetmgmt.MsVM;
import com.dciindonesia.portalsysops.model.RequestMsVM;
import com.dciindonesia.portalsysops.repository.assetmgmt.*;
import com.dciindonesia.portalsysops.service.ExportReport;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class DownloadReport {

    @Autowired
    private ExportReport export;

    @Autowired
    private MsAssetRepo msAssetRepo;
    @Autowired
    private UserManagementService userManagementService;

    private Logger log = LoggerFactory.getLogger("Download Report");

    @GetMapping("/export-asset")
    public void asset(HttpServletResponse response, String from, String to, HttpServletRequest request) throws IOException, ParseException {

        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=asset_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);
        List<String> privileges = SecurityContextHolder.getContext().getAuthentication().getAuthorities() .stream().filter(e->e.toString().equals("ROLE_ASSET")).map(e->e.toString()).collect(Collectors.toList());

        Collection<List<String>> a = msAssetRepo.getAssetDetail().stream().map(item->{
            List<String> newitem = new ArrayList<>();
            newitem.add(item.getAssetName());
            newitem.add(item.getAssetTag());
            newitem.add(item.getAssetStatus());
            newitem.add(item.getAssetCategory());
            newitem.add(item.getLocation());
            newitem.add(item.getRoom());
            newitem.add(item.getIpHost());
            if (privileges.size()>0)
            {
                newitem.add(item.getUserHost());
                newitem.add(item.getPasswordHost());
            }
            newitem.add(item.getIpManagement());
            if (privileges.size()>0)
            {
                newitem.add(item.getUserManagement());
                newitem.add(item.getPasswordManagement());
            }
            newitem.add(item.getNotes());
            newitem.add(item.getPrNumber());
            newitem.add(item.getPurchaseDate().toString());
            newitem.add(String.valueOf(item.getPurchaseCost()));
            newitem.add(String.valueOf(item.getWarranty()));
            newitem.add(item.getExpDateString());
            newitem.add(item.getSupplier());
            newitem.add(item.getBackup());
            newitem.add(item.getHighAvailbility());
            newitem.add(item.getPriority());
            newitem.add(item.getAntivirus());
            newitem.add(item.getAvSpanport());
            newitem.add(item.getAvOssec());
            newitem.add(item.getPrtgIcmp());
            newitem.add(item.getPrtgService());
            return newitem;
        }).collect(Collectors.toList());

        List<String> headers = new ArrayList<>();
        headers.add("Asset Name");
        headers.add("Asset Tag");
        headers.add("Status");
        headers.add("Category");
        headers.add("Building");
        headers.add("Room");
        headers.add("IP Host");
        if (privileges.size()>0)
        {
            headers.add("User Host");
            headers.add("Password Host");
        }
        headers.add("IP Management");
        if (privileges.size()>0)
        {
            headers.add("User Management");
            headers.add("Password Management");
        }
        headers.add("Notes");
        headers.add("PR Number");
        headers.add("Purchase Date");
        headers.add("Cost");
        headers.add("Warranty (month)");
        headers.add("Exp Date");
        headers.add("Supplier");
        headers.add("Backup");
        headers.add("High Availbility");
        headers.add("Priority");
        headers.add("Antivirus");
        headers.add("AV Span Port");
        headers.add("AV Ossec");
        headers.add("PRTG ICMP");
        headers.add("PRTG Service");

        userManagementService.track(request,null,null,"Download ReportAsset");


        export.init(a,headers,request.getUserPrincipal().getName()).export(response);
    }

    @Autowired
    private MsLicenseRepo msLicenseRepo;

    @Autowired
    private MsVMRepo msVMRepo;

    @Autowired
    private IpVMDetailRepo ipVMDetailRepo;

    @Autowired
    private IpVMRepo ipVMRepo;

    @GetMapping("/export-vm-cctv")
    public void vmCCTV(HttpServletResponse response, String from, String to, HttpServletRequest request) throws IOException {
        List<IpVMDetail> ipdet = ipVMDetailRepo.findAll();
        List<IpVm> msip = ipVMRepo.findAll();


        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=vmsysops_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);

        Collection<List<String>> a = msVMRepo.findByCategoryId(Long.valueOf(2)).stream().map(e->new RequestMsVM(e,ipdet,msip)).collect(Collectors.toList()).stream().map(item->{
            List<String> newitem = new ArrayList<>();
            newitem.add(String.valueOf(item.getId()));
            newitem.add(item.getName());
            newitem.add(String.valueOf(item.getIpsStr()));
            newitem.add(String.valueOf(item.getSocket()));
            newitem.add(item.getCpu());
            newitem.add(item.getMemory());
            newitem.add(item.getStorage());
            newitem.add(item.getLicense());
            newitem.add(item.getAssetIPHost());
            newitem.add(item.getState());
            newitem.add(item.getDescription());
            newitem.add(item.getBackup()?"Yes":"No");
            newitem.add(item.getHighAvailbility()?"Yes":"No");
            newitem.add(item.getPriority());
            newitem.add(item.getAvSpanPort()?"Yes":"No");
            newitem.add(item.getAvOssec()?"Yes":"No");
            newitem.add(item.getAntivirusInstalled()?"Yes":"No");
            newitem.add(item.getPrtgIcmp()?"Yes":"No");
            newitem.add(item.getPrtgService()?"Yes":"No");
            newitem.add(item.getGrafana()?"Yes":"No");


            return newitem;
        }).collect(Collectors.toList());

        List<String> headers = new ArrayList<>();
        headers.add("ID");
        headers.add("Nama");
        headers.add("IP");
        headers.add("CPU Socket");
        headers.add("CPU Core");
        headers.add("Memory (GB)");
        headers.add("Storage (GB)");
        headers.add("Guest OS");
        headers.add("IP Host");
        headers.add("State");
        headers.add("Description");
        headers.add("Backup");
        headers.add("High Availbility");
        headers.add("Priority");
        headers.add("Alien Vault - Span Port");
        headers.add("Alien Vault - Ossec");
        headers.add("Antivirus Installed");
        headers.add("Alert PRTG ICMP");
        headers.add("Alert PRTG Services");
        headers.add("Alert Grafana");

        userManagementService.track(request,null,null,"Download Report VM CCTV+NMS");
        export.init(a,headers,request.getUserPrincipal().getName()).export(response);

    }

    @GetMapping("/export-vm-sysops")
    public void vmSYSOPS(HttpServletResponse response, String from, String to, HttpServletRequest request) throws IOException {
        List<IpVMDetail> ipdet = ipVMDetailRepo.findAll();
        List<IpVm> msip = ipVMRepo.findAll();


        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=vmsysops_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);

        Collection<List<String>> a = msVMRepo.findByCategoryId(Long.valueOf(1)).stream().map(e->new RequestMsVM(e,ipdet,msip)).collect(Collectors.toList()).stream().map(item->{
            List<String> newitem = new ArrayList<>();
            newitem.add(String.valueOf(item.getId()));
            newitem.add(item.getName());
            newitem.add(String.valueOf(item.getIpsStr()));
            newitem.add(String.valueOf(item.getSocket()));
            newitem.add(item.getCpu());
            newitem.add(item.getMemory());
            newitem.add(item.getStorage());
            newitem.add(item.getLicense());
            newitem.add(item.getAssetIPHost());
            newitem.add(item.getState());
            newitem.add(item.getDescription());
            newitem.add(item.getBackup()?"Yes":"No");
            newitem.add(item.getHighAvailbility()?"Yes":"No");
            newitem.add(item.getPriority());
            newitem.add(item.getAvSpanPort()?"Yes":"No");
            newitem.add(item.getAvOssec()?"Yes":"No");
            newitem.add(item.getAntivirusInstalled()?"Yes":"No");
            newitem.add(item.getPrtgIcmp()?"Yes":"No");
            newitem.add(item.getPrtgService()?"Yes":"No");
            newitem.add(item.getGrafana()?"Yes":"No");


            return newitem;
        }).collect(Collectors.toList());

        List<String> headers = new ArrayList<>();
        headers.add("ID");
        headers.add("Nama");
        headers.add("IP");
        headers.add("CPU Socket");
        headers.add("CPU Core");
        headers.add("Memory (GB)");
        headers.add("Storage (GB)");
        headers.add("Guest OS");
        headers.add("IP Host");
        headers.add("State");
        headers.add("Description");
        headers.add("Backup");
        headers.add("High Availbility");
        headers.add("Priority");
        headers.add("Alien Vault - Span Port");
        headers.add("Alien Vault - Ossec");
        headers.add("Antivirus Installed");
        headers.add("Alert PRTG ICMP");
        headers.add("Alert PRTG Services");
        headers.add("Alert Grafana");

        userManagementService.track(request,null,null,"Download Report VM CCTV+NMS");
        export.init(a,headers,request.getUserPrincipal().getName()).export(response);

    }


    @GetMapping("/export-license")
    public void license(HttpServletResponse response, String from, String to, HttpServletRequest request) throws IOException, ParseException {

        List<String> privileges = SecurityContextHolder.getContext().getAuthentication().getAuthorities() .stream().filter(e->e.toString().equals("ROLE_ASSET")).map(e->e.toString()).collect(Collectors.toList());

        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=license_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);

        Collection<List<String>> a = msLicenseRepo.getDetail().stream().map(item->{
            List<String> newitem = new ArrayList<>();
            newitem.add(item.getName());
            if (privileges.size()>0){

                newitem.add(item.getProductKey());
            }
            newitem.add(String.valueOf(item.getQty()));
            newitem.add(item.getCategory());
            newitem.add(item.getLicenseToName());
            newitem.add(item.getLicenseToEmail());
            newitem.add(item.getNotes());
            newitem.add(item.getExpDateString());
            newitem.add(item.getManufacturer());
            newitem.add(item.getSupplier());
            newitem.add(item.getPrNumber());
            newitem.add(item.getPurchaseDateStr());
            newitem.add(String.valueOf(item.getPurchaseCost()));
            return newitem;
        }).collect(Collectors.toList());

        List<String> headers = new ArrayList<>();
        headers.add("License Name");
        if (privileges.size()>0){
            headers.add("Product Key");
        }
        headers.add("Qty");
        headers.add("Category");
        headers.add("License To Name");
        headers.add("License To Email");
        headers.add("Notes");
        headers.add("Exp Date");
        headers.add("Manufacturer");
        headers.add("Supplier");
        headers.add("PR Number");
        headers.add("Purchase Date");
        headers.add("Purchase Cost");

        userManagementService.track(request,null,null,"Download ReportLicense");
        export.init(a,headers,request.getUserPrincipal().getName()).export(response);
    }

    @Autowired
    private MsAccessoryRepo msAccessoryRepo;

    @GetMapping("/export-accessory")
    public void accessory(HttpServletResponse response, String from, String to, HttpServletRequest request) throws IOException, ParseException {


        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=accessory_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);

        Collection<List<String>> a = msAccessoryRepo.findJoinActive().stream().map(item->{
            List<String> newitem = new ArrayList<>();
            newitem.add(item.getName());
            newitem.add(item.getAssetTag());
            newitem.add(item.getCategory());
            newitem.add(item.getStatus());
            newitem.add(item.getModel());
            newitem.add(item.getOwner());
            newitem.add(item.getLocation());
            newitem.add(item.getRoom());
            newitem.add(item.getPrNumber());
            newitem.add(item.getPurchaseDate().toString());
            newitem.add(String.valueOf(item.getPurchaseCost()));
            newitem.add(String.valueOf(item.getQty()));
            newitem.add(item.getManufacturer());
            newitem.add(item.getSupplier());
            return newitem;
        }).collect(Collectors.toList());

        List<String> headers = new ArrayList<>();
        headers.add("Name");
        headers.add("Asset Tag");
        headers.add("Category");
        headers.add("Status");
        headers.add("Model");
        headers.add("Owner");
        headers.add("Location");
        headers.add("Room");
        headers.add("PR Number");
        headers.add("Purchase Date");
        headers.add("Purchase Cost");
        headers.add("Qty");
        headers.add("Manufacturer");
        headers.add("Supplier");
        userManagementService.track(request,null,null,"Download ReportAccessory");
        export.init(a,headers,request.getUserPrincipal().getName()).export(response);
    }

    @Autowired
    private MsConsumableRepo msConsumableRepo;

    @GetMapping("/export-consumable")
    public void connsumable(HttpServletResponse response, String from, String to, HttpServletRequest request) throws IOException, ParseException {


        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=consumable_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);

        Collection<List<String>> a = msConsumableRepo.findJoinActive().stream().map(item->{
            List<String> newitem = new ArrayList<>();
            newitem.add(item.getName());
            newitem.add(item.getCategory());
            newitem.add(item.getModel());
            newitem.add(item.getLocation());
            newitem.add(item.getRoom());
            newitem.add(item.getPrNumber());
            newitem.add(item.getPurchaseDate().toString());
            newitem.add(String.valueOf(item.getPurchaseCost()));
            newitem.add(String.valueOf(item.getQty()));
            newitem.add(item.getManufacturer());
            return newitem;
        }).collect(Collectors.toList());

        List<String> headers = new ArrayList<>();
        headers.add("Name");
        headers.add("Category");
        headers.add("Model");
        headers.add("Location");
        headers.add("Room");
        headers.add("PR Number");
        headers.add("Purchase Date");
        headers.add("Purchase Cost");
        headers.add("Qty (Available)");
        headers.add("Manufacturer");

        Collection<List<String>> detail = msConsumableRepo.findJoinAndDetail().stream().map(item->{
            List<String> newitem = new ArrayList<>();
            newitem.add(String.valueOf(item.getId()));
            newitem.add(item.getName());
            newitem.add(item.getCategory());
            newitem.add(item.getModel());
            newitem.add(item.getLocation());
            newitem.add(item.getRoom());
            newitem.add(item.getPrNumber());
            newitem.add(item.getPurchaseDate().toString());
            newitem.add(String.valueOf(item.getPurchaseCost()));
            newitem.add(String.valueOf(item.getQty()));
            newitem.add(String.valueOf(item.getNote()));
            newitem.add(item.getManufacturer());
            return newitem;
        }).collect(Collectors.toList());

        List<String> headersdetail = new ArrayList<>();
        headersdetail.add("Consumable ID");
        headersdetail.add("Name");
        headersdetail.add("Category");
        headersdetail.add("Model");
        headersdetail.add("Location");
        headersdetail.add("Room");
        headersdetail.add("PR Number");
        headersdetail.add("Purchase Date");
        headersdetail.add("Purchase Cost");
        headersdetail.add("Qty (Used)");
        headersdetail.add("Note");
        headersdetail.add("Manufacturer");


        userManagementService.track(request,null,null,"Download ReportConsumable");

        export.init(a,headers,request.getUserPrincipal().getName()).addSheet(detail,headersdetail).export(response);
    }


    @Autowired
    private MsApplicationRepo msApplicationRepo;
    @GetMapping("/export-application")
    public void application(HttpServletResponse response, String from, String to, HttpServletRequest request) throws IOException, ParseException {


        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=apps_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);
        List<String> privileges = SecurityContextHolder.getContext().getAuthentication().getAuthorities() .stream().filter(e->e.toString().equals("ROLE_ASSET")).map(e->e.toString()).collect(Collectors.toList());

        Collection<List<String>> a = msApplicationRepo.findJoinActive().stream().map(item->{
            List<String> newitem = new ArrayList<>();
            newitem.add(item.getName());
            newitem.add(item.getApplicationStatus());
            newitem.add(item.getUrl());
            newitem.add(item.getLastDeploy().toString());
            newitem.add(item.getVersion());
            newitem.add(item.getServer());
            if (privileges.size()>0){

                newitem.add(item.getUserServer());
                newitem.add(item.getPasswordServer());
            }
            newitem.add(item.getOsVersion());
            newitem.add(item.getPath());
            if (privileges.size()>0){
                newitem.add(item.getUserDb());
                newitem.add(item.getPasswordDb());
            }
            newitem.add(item.getDbVersion());
            return newitem;
        }).collect(Collectors.toList());

        List<String> headers = new ArrayList<>();
        headers.add("Name");
        headers.add("Status");
        headers.add("URL");
        headers.add("Last Deploy");
        headers.add("Version");
        headers.add("Server");
        if (privileges.size()>0){
            headers.add("User Server");
            headers.add("Password Server");
        }
        headers.add("OS Version");
        headers.add("Path");
        if (privileges.size()>0){
            headers.add("User DB");
            headers.add("Password DB");
        }
        headers.add("DB Version");

        userManagementService.track(request,null,null,"Download ReportApplication");
        export.init(a,headers,request.getUserPrincipal().getName()).export(response);
    }


    @Autowired
    private MsRemoteAccessRepo msRemoteAccessRepo;
    @GetMapping("/export-remote-access")
    public void remoteAccess(HttpServletResponse response, String from, String to, HttpServletRequest request) throws IOException, ParseException {


        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=remoteaccess_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);
        List<String> privileges = SecurityContextHolder.getContext().getAuthentication().getAuthorities() .stream().filter(e->e.toString().equals("ROLE_ASSET")).map(e->e.toString()).collect(Collectors.toList());

        Collection<List<String>> a = msRemoteAccessRepo.findAllActive().stream().map(item->{
            List<String> newitem = new ArrayList<>();
            newitem.add(item.getApplication());
            newitem.add(item.getIp());
            newitem.add(item.getPort());
            newitem.add(item.getPurpose());
            if (privileges.size()>0){
                newitem.add(item.getUser());
                newitem.add(item.getPassword());

            }
            return newitem;
        }).collect(Collectors.toList());

        List<String> headers = new ArrayList<>();
        headers.add("Application");
        headers.add("IP");
        headers.add("Port");
        headers.add("Purpose");
        if (privileges.size()>0){
            headers.add("User");
            headers.add("Password");

        }

        userManagementService.track(request,null,null,"Download Report Remote Access");
        export.init(a,headers,request.getUserPrincipal().getName()).export(response);
    }


}
