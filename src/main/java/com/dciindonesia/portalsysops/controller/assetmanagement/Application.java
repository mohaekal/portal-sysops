package com.dciindonesia.portalsysops.controller.assetmanagement;

import com.dciindonesia.portalsysops.repository.assetmgmt.MsApplicationStatusRepo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(value = "/asset-management/application")
public class Application {

    @Autowired
    private MsApplicationStatusRepo msApplicationStatusRepo;
    private Logger log = LoggerFactory.getLogger("go");
    @GetMapping("")
    public String getAll(HttpServletRequest request, Model model) throws JsonProcessingException {
        ObjectMapper obj  = new ObjectMapper();
        model.addAttribute("privilege",obj.writeValueAsString(
                SecurityContextHolder.getContext().getAuthentication().getAuthorities()));
        model.addAttribute("statuses",msApplicationStatusRepo.findAllActive());

        return "application";
    };

}
