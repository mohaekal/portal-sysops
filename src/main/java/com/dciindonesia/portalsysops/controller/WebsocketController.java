package com.dciindonesia.portalsysops.controller;

import com.dciindonesia.portalsysops.model.ElasticResponse;
import com.dciindonesia.portalsysops.model.Response;
import com.dciindonesia.portalsysops.model.elastic.Source;
import com.dciindonesia.portalsysops.service.LastValueElastic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class WebsocketController {

//    @Autowired
//    LastValueElastic lastValueElastic;
//
//    @MessageMapping("/iot")
//    @SendTo("/data-stream/iot")
//    public Response getdata() throws IOException {
//        List<Source> source = lastValueElastic.get().hits.hits.stream()
//                .map(e->new Source(e))
//                .collect(Collectors.toList());
//
//        return new Response(true, source);
//    }

    @MessageMapping("/new-task")
    @SendTo("/request/task")
    public Response getdata(String jsongstring) throws IOException {


        return new Response(true, jsongstring);
    }
}
