package com.dciindonesia.portalsysops.controller.adminpanel;

import com.dciindonesia.portalsysops.repository.portalmanagement.MsRoleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/user-management")
public class User {


    @Autowired
    private MsRoleRepo msRoleRepo;
    @GetMapping("/user")
    public String getActiveUser(Model model){
        model.addAttribute("roles",msRoleRepo.findAllActive());
        return "user-management/user";
    }

    @GetMapping("/role")
    public String getActiveRole(){
        return "user-management/role";
    }

    @GetMapping("/access")
    public String getActiveAccess(){
        return "user-management/access";
    }

    @GetMapping("/role-access")
    public String getActiveRoleAccess(Model model){
        model.addAttribute("roles", msRoleRepo.findAllActive() );
        return "user-management/role-access";
    }

}
