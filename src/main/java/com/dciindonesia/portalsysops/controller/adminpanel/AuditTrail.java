package com.dciindonesia.portalsysops.controller.adminpanel;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/audit-trail")
public class AuditTrail {

    @GetMapping("")
    public String get(){
        return "user-management/log";
    }
}
