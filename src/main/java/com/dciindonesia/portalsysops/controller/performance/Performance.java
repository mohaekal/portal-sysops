package com.dciindonesia.portalsysops.controller.performance;

import com.dciindonesia.portalsysops.service.HttpConn;
import com.dciindonesia.portalsysops.service.UserManagementService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Controller
@CrossOrigin
public class Performance {
    @Autowired
    private HttpConn httpConn;
    @Autowired
    private UserManagementService userManagementService;
    @GetMapping("/windows-performance")
    public String windowsperf(HttpServletRequest request){

        userManagementService.track(request,null,null,"View Windows Performance ");

        return "performanceWindows";
    }

    @GetMapping("/linux-performance")
    public String linuxperf(HttpServletRequest request) throws IOException {

        userManagementService.track(request,null,null,"View Linux Performance ");
        return "performanceLinux";
    }

    @GetMapping("/mysql-performance")
    public String mysqlperf(HttpServletRequest request) throws IOException {

        userManagementService.track(request,null,null,"View MySQL Performance ");

        return "mysqlOverview";
    }

}
