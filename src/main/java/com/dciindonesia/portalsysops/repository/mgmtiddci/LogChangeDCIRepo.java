package com.dciindonesia.portalsysops.repository.mgmtiddci;

import com.dciindonesia.portalsysops.entity.mgmtiddci.LogChange;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LogChangeDCIRepo extends JpaRepository<LogChange,Integer> {
}
