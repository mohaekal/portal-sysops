package com.dciindonesia.portalsysops.repository.mgmtiddci;


import com.dciindonesia.portalsysops.entity.mgmtiddci.MsVariable;
import com.dciindonesia.portalsysops.model.InterfaceMsVariable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MsVariableDCIRepo extends JpaRepository<MsVariable,Long> {

//    @Query("SELECT new com.dbiot.managementid.model.MsVariableJoinan(mv,mg,md,mb, mu,ml) " +
//            " FROM MsVariable mv " +
//            " left join MsGroupVariable mg on mg.id = mv.groupId " +
//            " left join MsDevice md on md.id = mv.deviceId" +
//            " left join MsBuilding mb on mb.id = mv.buildingId " +
//            " left join MsUbidots mu on mu.variableId = mv.id" +
//            " left join MsLevel ml on ml.id = mv.levelId" +
//            " order by mv.id desc")
//    List<MsVariableJoinan> findJoinAllMapped();

    @Query(nativeQuery = true,value = " select " +
            "mv.id , " +
            "mv.variable_name variableName ," +
            "mv.variable_attribute variableAttribute," +
            "mv.device_id deviceId," +
            "md.device_name deviceName," +
            "mv.group_id groupId," +
            "mv.measurement,"+
            "mgv.group_nm groupNm," +
            "mb.id buildingIdx," +
            "mb.building_id buildingId," +
            "mu.ubidots_id ubidotsId," +
            "mu.mac_address macAddress," +
            "mu.`type` ," +
            "mu.`maxvaluee` ," +
            "mu.unit ," +
            "mu.last_update lastUpdate," +
            "mv.level_id levelId," +
            "ml.level_name levelName " +
            "from ms_variable mv " +
            "left join ms_group_variable mgv on mv.group_id  = mgv .id " +
            "left join ms_device md on md.id  = mv.device_id " +
            "left join ms_building mb on mv.building_id = mb.id " +
            "left join ms_ubidots mu on mu.variable_id  = mv.id " +
            "left join ms_level ml on ml.id = mv.level_id " +
            "order by mv.id desc ")
    List<InterfaceMsVariable> findJoinAllMapped();

}
