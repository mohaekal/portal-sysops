package com.dciindonesia.portalsysops.repository.mgmtiddci;

import com.dciindonesia.portalsysops.entity.mgmtiddci.MsDevice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MsDeviceDCIRepo extends JpaRepository<MsDevice, Long> {
}
