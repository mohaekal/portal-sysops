package com.dciindonesia.portalsysops.repository.mgmtiddci;

import com.dciindonesia.portalsysops.entity.mgmtiddci.MsGroupVariable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MsGroupVariableDCIRepo extends JpaRepository<MsGroupVariable, Long> {
}
