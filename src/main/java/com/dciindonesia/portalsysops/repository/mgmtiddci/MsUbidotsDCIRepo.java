package com.dciindonesia.portalsysops.repository.mgmtiddci;

import com.dciindonesia.portalsysops.entity.mgmtiddci.MsUbidots;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MsUbidotsDCIRepo extends JpaRepository<MsUbidots,Integer> {

    List<MsUbidots> findByVariableId(Integer var);
}
