package com.dciindonesia.portalsysops.repository.mgmtiddci;

import com.dciindonesia.portalsysops.entity.mgmtiddci.MsLevel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MsLevelDCIRepo extends JpaRepository<MsLevel,Long> {
}
