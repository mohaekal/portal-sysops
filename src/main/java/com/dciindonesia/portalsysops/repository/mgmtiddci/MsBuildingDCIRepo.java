package com.dciindonesia.portalsysops.repository.mgmtiddci;


import com.dciindonesia.portalsysops.entity.mgmtiddci.MsBuilding;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MsBuildingDCIRepo extends JpaRepository<MsBuilding,Integer> {
}
