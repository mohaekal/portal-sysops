package com.dciindonesia.portalsysops.repository.mgmtiddci;


import com.dciindonesia.portalsysops.entity.mgmtiddci.ManagementIdConfig;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ManagementIdConfigDCIRepo extends JpaRepository<ManagementIdConfig,Integer> {

}
