package com.dciindonesia.portalsysops.repository.portalmanagement;

import com.dciindonesia.portalsysops.entity.portalmanagement.MsRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MsRoleRepo extends JpaRepository<MsRole, Long> {


    @Query(nativeQuery=true, value = "select * from ms_role where is_active = 1 order by name asc")
    List<MsRole> findAllActive();
}
