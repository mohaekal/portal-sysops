package com.dciindonesia.portalsysops.repository.portalmanagement;

import com.dciindonesia.portalsysops.entity.portalmanagement.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRoleRepo extends JpaRepository<UserRole,Long> {


    @Query(nativeQuery=true, value="select * from user_role where user_id = :userid ")
    List<UserRole> findByUserId(@Param("userid") Integer userid);

}
