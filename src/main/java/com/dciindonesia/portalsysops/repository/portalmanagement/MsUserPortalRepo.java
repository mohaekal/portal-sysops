package com.dciindonesia.portalsysops.repository.portalmanagement;

import com.dciindonesia.portalsysops.entity.portalmanagement.MsUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MsUserPortalRepo extends JpaRepository<MsUser,Integer> {
    @Query(nativeQuery = true, value = "select * from ms_user where email = :email and is_active = 1 limit 0,1 ")
    MsUser findByEmail(@Param("email") String s);

    @Query(nativeQuery=true,value="select * from ms_user mu where " +
            " exists(select 1 from user_role ur where " +
            "   exists(select 1 from role_access ra where ra.access_id = :accessid and ra.role_id = ur.role_id) and ur.user_id =  mu.id" +
            ") and mu.is_active = 1")
    List<MsUser> findApproverByAccesID(@Param("accessid") Long accessid);
}
