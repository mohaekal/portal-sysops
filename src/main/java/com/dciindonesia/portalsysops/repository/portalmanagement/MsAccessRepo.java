package com.dciindonesia.portalsysops.repository.portalmanagement;

import com.dciindonesia.portalsysops.entity.portalmanagement.MsAccess;
import com.dciindonesia.portalsysops.entity.portalmanagement.RoleAccess;
import com.dciindonesia.portalsysops.model.InterfaceAccess;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MsAccessRepo extends JpaRepository<MsAccess,Long> {

    @Query(nativeQuery = true,value = "select CONCAT('ROLE_',UPPER(ma.name)) AS names from ms_access ma where exists(" +
            "select 1 from role_access ra where exists (select 1 from user_role ur where ra.role_id = ur.role_id and user_id = :userid ) and ma.id = ra.access_id " +
            ")" )
    List<String> findAuthorityByUserId(@Param("userid") Integer userid);

    @Query(nativeQuery=true,value = "select ma.*, IFNULL(z.dataexist,false) used from ms_access ma \n" +
            "left join ( select ra.access_id roleaccess, true as dataexist from role_access ra where ra.role_id = :roleid ) z on z.roleaccess = ma.id" +
            " order by ma.id desc")
    List<InterfaceAccess> findAllAccessByRole(@Param("roleid") Long roleid);
}
