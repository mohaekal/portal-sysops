package com.dciindonesia.portalsysops.repository.portalmanagement;

import com.dciindonesia.portalsysops.entity.portalmanagement.RoleAccess;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RoleAccessRepo extends JpaRepository<RoleAccess,Long> {


    @Query(nativeQuery=true,value="select * from role_access where role_id=:role and access_id=:access ")
    List<RoleAccess> findRoleAccessByRoleIdAndAccessId(@Param("role")Long role,@Param("access")Long access);
}
