package com.dciindonesia.portalsysops.repository.portalmanagement;

import com.dciindonesia.portalsysops.entity.portalmanagement.Logs;
import com.dciindonesia.portalsysops.model.InterfaceUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LogsRepo extends JpaRepository<Logs,Long> {

    @Query(nativeQuery=true,value="select l.*,mu.id userId,mu.email ,l.create_date createDate " +
            " from logs l inner join ms_user mu on mu.id = l.user_id order by l.id desc")
    List<InterfaceUser> findAllUser();
}
