package com.dciindonesia.portalsysops.repository.mgmtidh2;

import com.dciindonesia.portalsysops.entity.mgmtidh2.MsGroupVariable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MsGroupVariableH2Repo extends JpaRepository<MsGroupVariable, Long> {
}
