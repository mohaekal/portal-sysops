package com.dciindonesia.portalsysops.repository.mgmtidh2;

import com.dciindonesia.portalsysops.entity.mgmtidh2.MsDevice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MsDeviceH2Repo extends JpaRepository<MsDevice, Long> {
}
