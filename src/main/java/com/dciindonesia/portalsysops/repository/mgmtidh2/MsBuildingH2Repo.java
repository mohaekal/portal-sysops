package com.dciindonesia.portalsysops.repository.mgmtidh2;

import com.dciindonesia.portalsysops.entity.mgmtidh2.MsBuilding;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MsBuildingH2Repo extends JpaRepository<MsBuilding,Integer> {
}
