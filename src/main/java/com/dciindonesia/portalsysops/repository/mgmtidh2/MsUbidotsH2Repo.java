package com.dciindonesia.portalsysops.repository.mgmtidh2;

import com.dciindonesia.portalsysops.entity.mgmtidh2.MsUbidots;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MsUbidotsH2Repo extends JpaRepository<MsUbidots,Integer> {

    List<MsUbidots> findByVariableId(Integer var);
}
