package com.dciindonesia.portalsysops.repository.mgmtidh2;

import com.dciindonesia.portalsysops.entity.mgmtidh2.MsLevel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MsLevelH2Repo extends JpaRepository<MsLevel,Long> {
}
