package com.dciindonesia.portalsysops.repository.mgmtidh2;

import com.dciindonesia.portalsysops.entity.mgmtidh2.LogChange;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LogChangeH2Repo extends JpaRepository<LogChange,Integer> {
}
