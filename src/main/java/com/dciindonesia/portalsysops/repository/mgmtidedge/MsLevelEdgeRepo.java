package com.dciindonesia.portalsysops.repository.mgmtidedge;

import com.dciindonesia.portalsysops.entity.mgmtidedge.MsLevel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MsLevelEdgeRepo extends JpaRepository<MsLevel,Long> {
}
