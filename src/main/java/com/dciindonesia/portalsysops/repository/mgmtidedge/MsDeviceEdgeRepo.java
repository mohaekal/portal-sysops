package com.dciindonesia.portalsysops.repository.mgmtidedge;

import com.dciindonesia.portalsysops.entity.mgmtidedge.MsDevice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MsDeviceEdgeRepo extends JpaRepository<MsDevice, Long> {
}
