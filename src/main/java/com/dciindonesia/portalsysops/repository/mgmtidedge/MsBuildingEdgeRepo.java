package com.dciindonesia.portalsysops.repository.mgmtidedge;

import com.dciindonesia.portalsysops.entity.mgmtidedge.MsBuilding;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MsBuildingEdgeRepo extends JpaRepository<MsBuilding,Integer> {
}
