package com.dciindonesia.portalsysops.repository.mgmtidedge;

import com.dciindonesia.portalsysops.entity.mgmtidedge.MsGroupVariable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MsGroupVariableEdgeRepo extends JpaRepository<MsGroupVariable, Long> {
}
