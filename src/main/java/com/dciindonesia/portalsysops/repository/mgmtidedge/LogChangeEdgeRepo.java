package com.dciindonesia.portalsysops.repository.mgmtidedge;

import com.dciindonesia.portalsysops.entity.mgmtidedge.LogChange;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LogChangeEdgeRepo extends JpaRepository<LogChange,Integer> {
}
