package com.dciindonesia.portalsysops.repository.mgmtidedge;

import com.dciindonesia.portalsysops.entity.mgmtidedge.MsUbidots;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MsUbidotsEdgeRepo extends JpaRepository<MsUbidots,Integer> {

    List<MsUbidots> findByVariableId(Integer var);
}
