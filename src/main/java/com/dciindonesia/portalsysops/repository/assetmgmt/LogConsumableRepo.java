package com.dciindonesia.portalsysops.repository.assetmgmt;


import com.dciindonesia.portalsysops.entity.assetmgmt.LogConsumable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface LogConsumableRepo extends JpaRepository<LogConsumable,Long> {

    @Query(nativeQuery = true,value = "select * from log_consumable where consumable_id = :id  and is_active = 1 order by id desc")
    List<LogConsumable> findByConsumableId(@Param("id")Long id);
}
