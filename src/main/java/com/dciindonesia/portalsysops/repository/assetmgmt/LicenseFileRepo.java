package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.LicenseFile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LicenseFileRepo extends JpaRepository<LicenseFile,Long> {
}
