package com.dciindonesia.portalsysops.repository.assetmgmt;


import com.dciindonesia.portalsysops.entity.assetmgmt.MsVM;
import com.dciindonesia.portalsysops.model.InterfaceVM;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MsVMRepo extends JpaRepository<MsVM,Long> {

    @Query(nativeQuery= true, value="select * from ms_vm where is_active = 1 order by id desc")
    List<MsVM> findAllByActiveTrueOrderById();

    @Query(nativeQuery= true, value="select mv.* ," +
            "mv.high_availbility highAvailbility," +
            "mv.av_span_port avSpanPort," +
            "mv.av_ossec avOssec," +
            "mv.antivirus_installed antivirusInstalled," +
            "mv.antivirus_lastupdate antivirusLastupdate," +
            "mv.antivirus_lastupdate antivirusLastupdateStr," +
            "mv.prtg_icmp prtgIcmp," +
            "mv.prtg_service prtgService," +
            "mv.prtg_service_remark prtgServiceRemark," +
            "mc.name company,"+
            "mc.id companyId,"+
            "ma.id assetId, ml.id licenseId, ma.asset_name asset , ma.ip_host assetIPHost ,ml.name license from ms_vm mv \n" +
            "left join ms_asset ma on ma.id = mv.asset_id \n" +
            "left join ms_license ml on ml.id = mv.license_id " +
            "left join ms_company mc on mc.id = mv.company_id " +
            "where mv.is_active = 1 and mv.category_id = :category " +
            "order by mv.id desc")
    List<InterfaceVM> findByCategoryId(@Param("category")Long id);


    @Query(nativeQuery= true, value="select mv.* ," +
            "mv.high_availbility highAvailbility," +
            "mv.av_span_port avSpanPort," +
            "mv.av_ossec avOssec," +
            "mv.antivirus_installed antivirusInstalled," +
            "mv.antivirus_lastupdate antivirusLastupdate," +
            "mv.antivirus_lastupdate antivirusLastupdateStr," +
            "mv.prtg_icmp prtgIcmp," +
            "mv.prtg_service prtgService," +
            "mv.prtg_service_remark prtgServiceRemark," +
            "ma.id assetId, ml.id licenseId, ma.asset_name asset , ma.ip_host assetIPHost ,ml.name license, " +
            "trv.id requestId ," +
            "trv.create_date requestDate ," +
            "trv.request_type requestType ," +
            "trv.remark remark ," +
            "murequest.name requestBy ," +
            "mp.name progress ," +
            "mp.id progressid ," +
            "tar.id approvementId ," +
            "trp.remark evidenceRemark, " +
            "trp.id requestProgressId " +
            " from ms_vm mv \n" +
            "inner join tr_request_vm trv on trv.vm_id = mv.id " +
            "inner join ms_user murequest on trv.create_by = murequest.id " +
            "inner join tr_request_progress trp on trv.request_progress_id = trp.id " +
            "left join ms_progress mp on mp.id = trp.progress_id " +
            "left join tr_approvement_request tar on tar.request_progress_id = trp.id and tar.user_id = :userid " +
            "left join tr_evidence_task tet on tet.request_progress_id = trp.id and tet.user_id = :userid " +
            "left join ms_asset ma on ma.id = mv.asset_id \n" +
            "left join ms_license ml on ml.id = mv.license_id " +
            "where trv.is_active = :active and mv.category_id = :category" +
            " and ( " +
            "   ( trv.create_by = :userid )" +
            "   or ( mp.id = 2 and tar.is_approve is null and tar.user_id = :userid) " +
            "   or ( mp.id = 3 and tet.is_act is null and tet.user_id = :userid ) " +
            "   or ( mp.id = 4 and tar.is_approve is null and tar.user_id = :userid ) " +
            ") " +
            "order by mv.id desc")
    List<InterfaceVM> findRequestByCategoryId(@Param("category")Long id,@Param("userid") Integer userid, @Param("active") Integer active );



    @Query(nativeQuery= true, value="select mv.* ," +
            "mv.high_availbility highAvailbility," +
            "mv.av_span_port avSpanPort," +
            "mv.av_ossec avOssec," +
            "mv.antivirus_installed antivirusInstalled," +
            "mv.antivirus_lastupdate antivirusLastupdate," +
            "mv.antivirus_lastupdate antivirusLastupdateStr," +
            "mv.prtg_icmp prtgIcmp," +
            "mv.prtg_service prtgService," +
            "mv.prtg_service_remark prtgServiceRemark," +
            "ma.id assetId, ml.id licenseId, ma.asset_name asset , ma.ip_host assetIPHost ,ml.name license, " +
            "trv.id requestId ," +
            "trv.create_date requestDate ," +
            "trv.request_type requestType ," +
            "trv.remark remark ," +
            "murequest.name requestBy ," +
            "mp.name progress ," +
            "mp.id progressid ," +
            "tar.id approvementId ," +
            "trp.remark evidenceRemark, " +
            "trp.id requestProgressId " +
            " from ms_vm mv \n" +
            "inner join tr_request_vm trv on trv.vm_id = mv.id " +
            "inner join ms_user murequest on trv.create_by = murequest.id " +
            "inner join tr_request_progress trp on trv.request_progress_id = trp.id " +
            "left join ms_progress mp on mp.id = trp.progress_id " +
            "left join tr_approvement_request tar on tar.request_progress_id = trp.id and tar.user_id = :userid and tar.is_approve is null " +
            "left join tr_evidence_task tet on tet.request_progress_id = trp.id and tet.user_id = :userid " +
            "left join ms_asset ma on ma.id = mv.asset_id \n" +
            "left join ms_license ml on ml.id = mv.license_id " +
            "where trv.is_active = :active and mv.category_id = :category" +
            " and (     ( trv.create_by = :userid ) or ( mp.id > 1 )    ) " +
            "order by mv.id desc")
    List<InterfaceVM> findRequestAllActive(@Param("category")Long id,@Param("userid") Integer userid, @Param("active") Integer active );

    @Query(nativeQuery= true, value="select mv.* ," +
            "mv.high_availbility highAvailbility," +
            "mv.av_span_port avSpanPort," +
            "mv.av_ossec avOssec," +
            "mv.antivirus_installed antivirusInstalled," +
            "mv.antivirus_lastupdate antivirusLastupdate," +
            "mv.antivirus_lastupdate antivirusLastupdateStr," +
            "mv.prtg_icmp prtgIcmp," +
            "mv.prtg_service prtgService," +
            "mv.prtg_service_remark prtgServiceRemark," +
            "ma.id assetId, ml.id licenseId, ma.asset_name asset , ma.ip_host assetIPHost ,ml.name license, " +
            "trv.id requestId ," +
            "trv.create_date requestDate ," +
            "trv.request_type requestType ," +
            "trv.remark remark ," +
            "murequest.name requestBy ," +
            "mp.name progress ," +
            "mp.id progressid ," +
            "tar.id approvementId ," +
            "trp.remark evidenceRemark, " +
            "trp.id requestProgressId " +
            " from ms_vm mv \n" +
            "inner join tr_request_vm trv on trv.vm_id = mv.id " +
            "inner join ms_user murequest on trv.create_by = murequest.id " +
            "inner join tr_request_progress trp on trv.request_progress_id = trp.id " +
            "left join ms_progress mp on mp.id = trp.progress_id " +
            "left join tr_approvement_request tar on tar.request_progress_id = trp.id and tar.user_id = :userid " +
            "left join tr_evidence_task tet on tet.request_progress_id = trp.id and tet.user_id = :userid " +
            "left join ms_asset ma on ma.id = mv.asset_id \n" +
            "left join ms_license ml on ml.id = mv.license_id " +
            "where trv.is_active = 0 and mv.category_id = :category" +
            " and mp.id=5 " +
            "order by mv.id desc")
    List<InterfaceVM> findAllClosed(@Param("category")Long id,@Param("userid") Integer userid );

}
