package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsAccessoryCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MsAccessoryCategoryRepo extends JpaRepository<MsAccessoryCategory,Long> {
    @Query(nativeQuery = true,value = "select * from ms_accessory_category where is_active = 1 order by id desc")
    List<MsAccessoryCategory> findAllActive();
}
