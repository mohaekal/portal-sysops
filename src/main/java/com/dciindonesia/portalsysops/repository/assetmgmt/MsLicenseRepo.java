package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsLicense;
import com.dciindonesia.portalsysops.model.InterfaceLicense;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;

public interface MsLicenseRepo extends JpaRepository<MsLicense,Long> {

    @Query(nativeQuery = true,value = "select * from ms_license where is_active = 1 order by id desc")
    List<MsLicense> findAllActive();

    @Query(nativeQuery = true,value="select " +
            "ml.id," +
            "ml.license_category_id licenseCategoryId," +
            "mm.id manufacturerId," +
            "ms.id supplierId ," +
            "ml.name ,\n" +
            "ml.product_key productKey,\n" +
            "ml.qty,\n" +
            "mlc.name category,\n" +
            "ml.license_to_name licenseToName,\n" +
            "ml.license_to_email licenseToEmail,\n" +
            "ml.password,\n" +
            "ml.notes ,\n" +
            "ml.exp_date expDate,\n" +
            "ml.exp_date expDateString,\n" +
            "case when exp_date < now() then 1 else 0 end as expStats,\n" +
            "mm.name manufacturer,\n" +
            "ms.name supplier,\n" +
            "ml.pr_number prNumber,\n" +
            "ml.purchase_date purchaseDate,\n" +
            "ml.purchase_date purchaseDateStr,\n" +
            "ml.purchase_cost purchaseCost ," +
            "ml.license_file_id licenseFileId " +
            "from ms_license ml \n" +
            "left join ms_license_category mlc on ml.license_category_id = mlc.id\n" +
            "left join ms_manufacturer mm on mm.id = ml.manufacturer_id \n" +
            "left join ms_supplier ms on ms.id = ml.supplier_id \n" +
            "where ml.is_active = 1\n" +
            "order by ml.id desc")
    Collection<InterfaceLicense> getDetail();
}
