package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.IpVMDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IpVMDetailRepo extends JpaRepository<IpVMDetail,Long> {


    @Query(nativeQuery = true, value ="select * from ip_vm_detail where vm_id = :vmid")
    List<IpVMDetail> findAllByvmid(@Param("vmid") Long vmid);
}
