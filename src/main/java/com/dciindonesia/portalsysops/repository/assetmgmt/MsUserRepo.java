package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MsUserRepo extends JpaRepository<MsUser,Integer> {

    @Query(nativeQuery = true, value = "select * from ms_user where email = :email and enable = 1 limit 0,1 ")
    MsUser findByEmail(@Param("email") String s);
}
