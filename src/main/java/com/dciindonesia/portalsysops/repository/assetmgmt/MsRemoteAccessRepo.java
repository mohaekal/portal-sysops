package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsAsset;
import com.dciindonesia.portalsysops.entity.assetmgmt.MsRemoteAccess;
import com.dciindonesia.portalsysops.model.InterfaceRemoteAccess;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;

public interface MsRemoteAccessRepo extends JpaRepository<MsRemoteAccess,Long> {
    @Query(nativeQuery = true,value = "select mra.*,mra.is_active isActive,mra.create_date createDate,mra.create_by createBy" +
            ", mra.update_date updateDate,mra.update_by updateBy" +
            ",mra.location_id locationId, mra.room_id roomId" +
            ",ml.name location,mr.name room " +
            "  from ms_remote_access mra " +
            "left join ms_location ml on ml.id = mra.location_id " +
            "left join ms_room mr on mr.id = mra.room_id " +
            " where mra.is_active = 1 order by id desc ")
    List<InterfaceRemoteAccess> findAllActive();


}
