package com.dciindonesia.portalsysops.repository.assetmgmt;


import com.dciindonesia.portalsysops.entity.assetmgmt.MsApplicationStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MsApplicationStatusRepo extends JpaRepository<MsApplicationStatus,Long> {
    @Query(nativeQuery = true,value = "select * from ms_application_status where is_active = 1 order by id desc ")
    List<MsApplicationStatus> findAllActive();
}
