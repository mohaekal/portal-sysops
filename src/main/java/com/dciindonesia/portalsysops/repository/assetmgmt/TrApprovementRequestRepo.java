package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.TrApprovementRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TrApprovementRequestRepo extends JpaRepository<TrApprovementRequest,Long> {

    @Query(nativeQuery=true , value = "select * from tr_approvement_request where request_progress_id = :id  ")
    List<TrApprovementRequest> findByReqProgressId(@Param("id") Long id );


    @Query(nativeQuery=true , value = "select * from tr_approvement_request where request_progress_id = :id and user_id = :userid ")
    List<TrApprovementRequest> findByReqProgressIdAndApprover(@Param("id") Long progressid,@Param("userid") Integer approver);

    @Query(nativeQuery=true,value = "select * from tr_approvement_request tar where exists(" +
            " select 1 from tr_request_progress trp where trp.id = tar.request_progress_id and trp.request_id  = :reqid )" )
    List<TrApprovementRequest> findByRequestId(@Param("reqid")Long request_id );
}
