package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsConsumableCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MsConsumableCategoryRepo extends JpaRepository<MsConsumableCategory,Long> {

    @Query(nativeQuery = true,value = "select * from ms_consumable_category where is_active  = 1 order by id desc")
    List<MsConsumableCategory> findAllActive();
}
