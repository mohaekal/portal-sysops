package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsAssetStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MsAssetStatusRepo extends JpaRepository<MsAssetStatus, Long> {
    @Query(nativeQuery = true,value = "select * from ms_asset_status where is_active = 1 order by id desc")
    List<MsAssetStatus> findAllActive();
}
