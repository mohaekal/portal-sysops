package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsAccessoryStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MsAccessoryStatusRepo extends JpaRepository<MsAccessoryStatus,Long> {
    @Query(nativeQuery = true,value = "select * from ms_accessory_status where is_active = 1 order by id desc")
    List<MsAccessoryStatus> findAllActive();
}
