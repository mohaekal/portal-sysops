package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsManufacturer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MsManufacturerRepo extends JpaRepository<MsManufacturer,Long> {

    @Query(nativeQuery = true,value = "select * from ms_manufacturer where is_active  = 1 order by id desc ")
    List<MsManufacturer> findAllActive();
}
