package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.IpVm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IpVMRepo extends JpaRepository<IpVm,Long> {

    @Query(nativeQuery= true , value = "select * from ip_vm where is_active = 1 order by id desc")
    List<IpVm> findAllActive();


    @Query(nativeQuery= true , value = "select * from ip_vm ipvm where is_active = 1 and not exists(select 1 from ip_vm_detail ipd where ipd.ip_vm_id = ipvm.id ) order by id desc")
    List<IpVm> findAllActiveUnUsed();


    @Query(nativeQuery= true , value = "select * from ip_vm ipvm where is_active = 1 and not exists(select 1 from ip_vm_detail ipd where ipd.ip_vm_id = ipvm.id and vm_id != :vmid ) order by id desc")
    List<IpVm> findAllActiveUnUsedByVMId(@Param("vmid")Long id);


    @Query(nativeQuery= true , value = "select * from ip_vm where ip = :ip ")
    List<IpVm> findByIP(@Param("ip") String ip);
}
