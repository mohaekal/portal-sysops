package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.LogAccessory;
import org.springframework.data.jpa.repository.JpaRepository;


public interface LogAccessoryRepo extends JpaRepository<LogAccessory,Long> {

}
