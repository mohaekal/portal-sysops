package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsAssetCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MsAssetCategoryRepo extends JpaRepository<MsAssetCategory,Integer> {

    @Query(nativeQuery = true,value = "select * from ms_asset_category where is_active  = 1 order by id desc")
    List<MsAssetCategory> findAllActive();
}
