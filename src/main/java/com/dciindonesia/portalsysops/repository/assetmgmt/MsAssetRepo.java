package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsAsset;
import com.dciindonesia.portalsysops.model.InterfaceAsset;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.persistence.Column;
import java.util.Collection;
import java.util.List;

public interface MsAssetRepo extends JpaRepository<MsAsset,Long> {

    @Query(nativeQuery = true,value = "select * from ms_asset where is_active = 1 order by id desc ")
    List<MsAsset> findAllActive();

    @Query(nativeQuery = true,value = "select count(1) from ms_asset where asset_tag like %:category% and is_active = 1 ")
    Integer findCountAsset(@Param("category")String category);

    @Query(nativeQuery = true,value="select " +
            "ma.id, " +
            "ma.asset_category_id assetCategoryId," +
            "ma.asset_status_id assetStatusId, \n" +
            "ma.asset_name assetName, \n" +
            "ma.asset_tag assetTag,\n" +
            "ma.core,\n" +
            "ma.socket,\n" +
            "ma.type,\n" +
            "ma.storage,\n" +
            "ma.memory,\n" +
            "ma.purchase_date purchaseDate,\n" +
            "ma.purchase_date purchaseDateStr,\n" +
            "ms.name supplier,\n" +
            "pr_number prNumber,\n" +
            "mac.name assetCategory,\n" +
            "purchase_cost purchaseCost,\n" +
            "warranty ,\n" +
            "DATE_ADD(purchase_date,interval warranty month) as expDate,\n" +
            "DATE_ADD(purchase_date,interval warranty month) as expDateString,\n" +
            "notes,\n" +
            "case when DATE_ADD(purchase_date,interval warranty month) < now() then 1 else 0 end as expStats,\n" +
            "mr.id roomId,\n" +
            "mr.name room,\n" +
            "ml.id locationId,\n" +
            "ml.name location,\n" +
            "ms.name assetStatus,\n" +
            "ip_host ipHost ,\n" +
            "user_host userHost,\n" +
            "password_host passwordHost,\n" +
            "ip_management ipManagement ,\n" +
            "user_management userManagement,\n" +
            "password_management passwordManagement\n," +
            "ma.priority," +
            "ma.backup," +
            "ma.high_availbility highAvailbility," +
            "ma.av_spanport avSpanport," +
            "ma.av_ossec avOssec," +
            "ma.antivirus," +
            "ma.grafana," +
            "ma.prtg_icmp prtgIcmp," +
            "ma.prtg_service prtgService," +
            "mc.id companyId," +
            "mc.name company" +
            " from \n" +
            "ms_asset ma \n" +
            "left join ms_asset_status mas on ma.asset_status_id = mas.id \n" +
            "left join ms_supplier ms on ms.id = ma.supplier_id \n" +
            "left join ms_location ml on ml.id = ma.location_id \n" +
            "left join ms_room mr on mr.id = ma.room_id \n" +
            "left join ms_asset_category mac on mac.id = ma.asset_category_id \n" +
            "left join ms_company mc on mc.id = ma.company_id " +
            "where ma.is_active = 1 " +
            " order by ma.id desc ")
    Collection<InterfaceAsset> getAssetDetail();

    @Query(nativeQuery = true,value="select " +
            "ma.id, " +
            "ma.asset_category_id assetCategoryId," +
            "ma.asset_status_id assetStatusId, \n" +
            "ma.asset_name assetName, \n" +
            "ma.asset_tag assetTag,\n" +
            "ma.core,\n" +
            "ma.socket,\n" +
            "ma.type,\n" +
            "ma.storage,\n" +
            "ma.memory,\n" +
            "ma.purchase_date purchaseDate,\n" +
            "ma.purchase_date purchaseDateStr,\n" +
            "ms.name supplier,\n" +
            "pr_number prNumber,\n" +
            "mac.name assetCategory,\n" +
            "purchase_cost purchaseCost,\n" +
            "warranty ,\n" +
            "DATE_ADD(purchase_date,interval warranty month) as expDate,\n" +
            "DATE_ADD(purchase_date,interval warranty month) as expDateString,\n" +
            "notes,\n" +
            "case when DATE_ADD(purchase_date,interval warranty month) < now() then 1 else 0 end as expStats,\n" +
            "mr.id roomId,\n" +
            "mr.name room,\n" +
            "ml.id locationId,\n" +
            "ml.name location,\n" +
            "ms.name assetStatus,\n" +
            "ip_host ipHost ,\n" +
            "ip_management ipManagement ,\n" +
            "ma.priority," +
            "ma.backup," +
            "ma.high_availbility highAvailbility," +
            "ma.av_spanport avSpanport," +
            "ma.av_ossec avOssec," +
            "ma.antivirus," +
            "ma.grafana," +
            "ma.prtg_icmp prtgIcmp," +
            "ma.prtg_service prtgService" +
            " from \n" +
            "ms_asset ma \n" +
            "left join ms_asset_status mas on ma.asset_status_id = mas.id \n" +
            "left join ms_supplier ms on ms.id = ma.supplier_id \n" +
            "left join ms_location ml on ml.id = ma.location_id \n" +
            "left join ms_room mr on mr.id = ma.room_id \n" +
            "left join ms_asset_category mac on mac.id = ma.asset_category_id \n" +
            "where ma.is_active = 1" +
            " order by ma.id desc ")
    Collection<InterfaceAsset> getAssetDetailDataSensitive();

    @Query(nativeQuery = true,value="select " +
            "ma.id, " +
            "ma.asset_category_id assetCategoryId," +
            "ma.asset_status_id assetStatusId, \n" +
            "ma.asset_name assetName, \n" +
            "ma.asset_tag assetTag,\n" +
            "ma.purchase_date purchaseDate,\n" +
            "ma.purchase_date purchaseDateStr,\n" +
            "ms.name supplier,\n" +
            "pr_number prNumber,\n" +
            "mac.name assetCategory,\n" +
            "purchase_cost purchaseCost,\n" +
            "warranty ,\n" +
            "DATE_ADD(purchase_date,interval warranty month) as expDate,\n" +
            "DATE_ADD(purchase_date,interval warranty month) as expDateString,\n" +
            "notes,\n" +
            "case when DATE_ADD(purchase_date,interval warranty month) < now() then 1 else 0 end as expStats,\n" +
            "mr.id roomId,\n" + 
            "mr.name room,\n" +
            "ml.id locationId,\n" +
            "ml.name location,\n" +
            "mas.name assetStatus,\n" +
            "ip_host ipHost ,\n" +
            "ip_management ipManagement \n" +
            "from \n" +
            "ms_asset ma \n" +
            "left join ms_asset_status mas on ma.asset_status_id = mas.id \n" +
            "left join ms_supplier ms on ms.id = ma.supplier_id \n" +
            "left join ms_location ml on ml.id = ma.location_id \n" +
            "left join ms_room mr on mr.id = ma.room_id \n" +
            "left join ms_asset_category mac on mac.id = ma.asset_category_id \n" +
            "where ma.is_active = 1" +
            " and ma.id = :id " +
            " order by ma.id desc ")
    InterfaceAsset getAssetDetailById(@Param("id")Long id);
}
