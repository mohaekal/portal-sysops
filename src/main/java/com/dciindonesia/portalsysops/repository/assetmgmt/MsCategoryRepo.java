package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MsCategoryRepo extends JpaRepository<MsCategory,Integer> {
}
