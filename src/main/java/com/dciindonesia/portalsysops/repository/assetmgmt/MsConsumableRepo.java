package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsConsumable;
import com.dciindonesia.portalsysops.model.InterfaceConsumable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;

public interface MsConsumableRepo extends JpaRepository<MsConsumable,Long> {
    @Query(nativeQuery = true,value = "select * from ms_consumable where is_active = 1  order by id desc ")
    List<MsConsumable> findAllActive();


    @Query(nativeQuery = true,value = "select \n" +
            "mc.id,\n" +
            "mc.name ,\n" +
            "mc.consumable_category_id consumableCategoryId,\n" +
            "mcc.name category,\n" +
            "mc.location_id locationId,\n" +
            "ml.name location,\n" +
            "mc.manufacturer_id manufacturerId,\n" +
            "mm.name manufacturer,\n" +
            "mc.model ,\n" +
            "mc.pr_number prNumber,\n" +
            "mc.purchase_date purchaseDate,\n" +
            "mc.purchase_date purchaseDateString,\n" +
            "mc.purchase_cost purchaseCost,\n" +
            "mc.qty, " +
            "mc.room_id roomId," +
            "mr.name room,\n" +
            "qu.qtyUsed " +
            "from \n" +
            "ms_consumable mc \n" +
            "left join ms_consumable_category mcc on mcc.id = mc.consumable_category_id \n" +
            "left join ms_manufacturer mm on mm.id = mc.manufacturer_id \n" +
            "left join ms_location ml on ml.id  =  mc.location_id \n" +
            "left join ms_room mr on mr.id = mc.room_id \n" +
            "left join (select count(1) qtyused , consumable_id from log_consumable group by consumable_id) qu on qu.consumable_id = mc.id " +
            "where mc.is_active = 1\n" +
            "order by mc.id desc")
    Collection<InterfaceConsumable> findJoinActive();

    @Query(nativeQuery = true,value = "select \n" +
            "mc.id,\n" +
            "mc.name ,\n" +
            "mc.consumable_category_id consumableCategoryId,\n" +
            "mcc.name category,\n" +
            "mc.location_id locationId,\n" +
            "ml.name location,\n" +
            "mc.manufacturer_id manufacturerId,\n" +
            "mm.name manufacturer,\n" +
            "mc.model ,\n" +
            "mc.pr_number prNumber,\n" +
            "mc.purchase_date purchaseDate,\n" +
            "mc.purchase_date purchaseDateString,\n" +
            "mc.purchase_cost purchaseCost,\n" +
            "lc.qty ,mc.room_id roomId,mr.name room,\n" +
            "lc.note  " +
            "from \n" +
            "ms_consumable mc \n" +
            "left join ms_consumable_category mcc on mcc.id = mc.consumable_category_id \n" +
            "left join ms_manufacturer mm on mm.id = mc.manufacturer_id \n" +
            "left join ms_location ml on ml.id  =  mc.location_id \n" +
            "left join ms_room mr on mr.id = mc.room_id \n" +
            "left join log_consumable lc on lc.consumable_id = mc.id and lc.is_active = 1 \n" +
            "where mc.is_active = 1\n" +
            "order by mc.id desc")
    Collection<InterfaceConsumable> findJoinAndDetail();
}
