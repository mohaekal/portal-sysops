package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.TrEvidenceTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TrEvidenceTaskRepo extends JpaRepository<TrEvidenceTask,Long> {

    @Query(nativeQuery=true,value= "select * from tr_evidence_task tet where user_id = :userid and request_progress_id = :reqprogressid ")
    List<TrEvidenceTask> findTaskByUser(@Param("userid")Integer userid,@Param("reqprogressid")Long id);
}
