package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsAccessory;
import com.dciindonesia.portalsysops.model.AccessoryDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;

public interface MsAccessoryRepo extends JpaRepository<MsAccessory,Long> {
    @Query(nativeQuery = true,value = "select * from ms_accessory where is_active = 1 order by id desc")
    List<MsAccessory> findAllActive();

    @Query(nativeQuery = true,value = "select ma.id \n" +
            ", ma.accessory_category_id accessoryCategoryId , mac.name category\n" +
            ",ma.accessory_status_id accessoryStatusId , mas.name status\n" +
            ",ma.asset_tag assetTag \n" +
            ",ma.location_id locationId ,ml.name location\n" +
            ",ma.room_id roomId ,mr.name room\n" +
            ",ma.log_id logId \n" +
            ",ma.manufacturer_id manufacturerId ,mm.name manufacturer\n" +
            ",ma.model ,ma.name ,ma.owner \n" +
            ",ma.pr_number prNumber\n" +
            ",ma.purchase_cost purchaseCost\n" +
            ",ma.purchase_date purchaseDate \n" +
            ",ma.purchase_date purchaseDateStr \n" +
            ",ma.qty \n" +
            ",ma.supplier_id supplierId,ms.name supplier\n" +
            ",la.used_by usedBy\n" +
            "from \n" +
            "ms_accessory ma \n" +
            "left join ms_accessory_status mas on mas.id = ma.accessory_status_id \n" +
            "left join ms_accessory_category mac on mac.id = ma.accessory_category_id\n" +
            "left join ms_location ml on ml.id = ma.location_id \n" +
            "left join ms_manufacturer mm on mm.id = ma.manufacturer_id \n" +
            "left join ms_supplier ms on ms.id = ma.supplier_id \n" +
            "left join log_accessory la on la.id = ma.log_id \n" +
            "left join ms_room mr on mr.id = ma.room_id \n" +
            "where ma.is_active = 1\n" +
            "order by id desc ")
    Collection<AccessoryDetail> findJoinActive();

    @Query(nativeQuery = true,value = "select count(1) from ms_accessory where asset_tag like %:category% and is_active = 1 ")
    Integer findCountAsset(@Param("category")String category);

}
