package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsLicenseCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MsLicenseCategoryRepo extends JpaRepository<MsLicenseCategory,Long> {

    @Query(nativeQuery = true,value = "select * from ms_license_category where is_active = 1 order by id desc ")
    List<MsLicenseCategory> findAllActive();
}
