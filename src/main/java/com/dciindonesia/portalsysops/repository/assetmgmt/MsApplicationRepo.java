package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsApplication;
import com.dciindonesia.portalsysops.model.InterfaceApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MsApplicationRepo extends JpaRepository<MsApplication, Long> {

    @Query(nativeQuery = true,value = "select * from ms_application where is_active = 1 order by id desc")
    List<MsApplication> findAllActive();

    @Query(nativeQuery=true,value="select \n" +
            "ma.id,\n" +
            "ma.name ,\n" +
            "ma.os_version osVersion,\n" +
            "ma.db_version dbVersion,\n" +
            "ma.server ,\n" +
            "ma.`path` ,\n" +
            "ma.version ,\n" +
            "ma.password_server passwordServer,\n" +
            "ma.password_db passwordDb,\n" +
            "ma.user_db userDb,\n" +
            "ma.user_server userServer,\n" +
            "ma.last_deploy lastDeploy,\n" +
            "ma.application_status_id applicationStatusId,\n" +
            "mas.name applicationStatus,\n" +
            "ma.url \n" +
            "from ms_application ma \n" +
            "left join ms_application_status mas on mas.id = ma.application_status_id \n" +
            "where ma.is_active = 1\n" +
            "order by ma.id desc")
    List<InterfaceApplication> findJoinActive();
}
