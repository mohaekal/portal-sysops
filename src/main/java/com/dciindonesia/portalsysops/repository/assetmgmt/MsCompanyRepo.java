package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsCompany;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MsCompanyRepo extends JpaRepository<MsCompany, Long> {

    @Query(nativeQuery=true,value="select * from ms_company where is_active = 1 order by name asc ")
    List<MsCompany> findAllActive();
}
