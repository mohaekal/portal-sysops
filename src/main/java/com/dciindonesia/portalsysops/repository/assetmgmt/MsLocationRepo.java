package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsLocation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MsLocationRepo extends JpaRepository<MsLocation,Integer> {

    @Query(nativeQuery = true,value = "select * from ms_location where is_active = 1 order by id desc ")
    List<MsLocation> findAllActive();
}
