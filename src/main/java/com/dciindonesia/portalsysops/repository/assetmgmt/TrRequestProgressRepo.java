package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.TrRequestProgress;
import com.dciindonesia.portalsysops.model.InterfaceRequestProgress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TrRequestProgressRepo extends JpaRepository<TrRequestProgress,Long> {


    @Query(nativeQuery=true, value="select trp.id, trp.request_id requestId, trp.progress_id progressId," +
            "mp.name progress,trp.create_date createDate from tr_request_progress trp " +
            " inner join ms_progress mp on mp.id = trp.progress_id " +
            " where  trp.request_id = :reqid " +
            " order by id desc ")
    List<InterfaceRequestProgress> findHistoryByRequestId(@Param("reqid")Long request_id);
}
