package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.TrEvidenceFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TrEvidenceFileRep extends JpaRepository<TrEvidenceFile,Long> {

    @Query(nativeQuery= true,value = "select * from tr_evidence_file tf where " +
            " exists(select 1 from tr_request_progress trp where progress_id = 4 and request_id = :req_id and tf.request_progress_id = trp.id order by id limit 0,1) ")
    List<TrEvidenceFile> findLastRequestProgress(@Param("req_id")Long requestid);

    @Query(nativeQuery = true,value="select id,request_progress_id requestProgressId,create_by createBy,create_date createDate,null type,null data , null `size` ,null name from tr_evidence_file order by id desc")
    List<TrEvidenceFile> findAllLID();
}
