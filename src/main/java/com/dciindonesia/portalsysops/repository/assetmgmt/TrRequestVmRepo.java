package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.TrRequestVm;
import com.dciindonesia.portalsysops.entity.assetmgmt.TrVmUpdate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TrRequestVmRepo extends JpaRepository<TrRequestVm, Long> {


    @Query(nativeQuery=true,value="select * from tr_request_vm trv " +
            " where trv.is_active = 1 and vm_id = :vmid")
    List<TrRequestVm> findByVmId(@Param("vmid")Long vmid);
}
