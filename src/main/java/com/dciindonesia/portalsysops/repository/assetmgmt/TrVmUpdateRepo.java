package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.TrVmUpdate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TrVmUpdateRepo extends JpaRepository<TrVmUpdate,Long > {

    @Query(nativeQuery=true,value="select * from tr_vm_update tvu " +
            " where tvu.is_active = 1 and exists(select 1 from tr_request_vm trv where trv.is_active=1 and trv.request_type = 'MODIFICATION' and trv.id = tvu.request_id) ")
    List<TrVmUpdate> findAllActive();


    @Query(nativeQuery=true,value="select * from tr_vm_update tvu " +
            " where tvu.is_active = 1 and tvu.request_id = :reqid order by id desc limit 0 ,1 ")
    TrVmUpdate findByRequestId(@Param("reqid")Long reqid);


    @Query(nativeQuery=true,value="select * from tr_vm_update tvu " +
            " where tvu.is_active = 1 and exists(select 1 from tr_request_vm trv where trv.vm_id = :vmid and trv.is_active = 1 and trv.id = tvu.request_id )")
    List<TrVmUpdate> findByVmId(@Param("vmid")Long vmid);
}
