package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.LogChange;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LogChangeRepo extends JpaRepository<LogChange, Long> {
}
