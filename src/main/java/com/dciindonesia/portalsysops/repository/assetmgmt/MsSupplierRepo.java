package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsSupplier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MsSupplierRepo extends JpaRepository<MsSupplier,Integer> {
    @Query(nativeQuery = true,value = "select * from ms_supplier where is_active = 1 order by id desc")
    List<MsSupplier> findAllActive();
}
