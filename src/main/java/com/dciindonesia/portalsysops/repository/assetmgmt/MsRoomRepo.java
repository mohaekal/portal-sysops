package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsRoom;
import com.dciindonesia.portalsysops.entity.assetmgmt.MsSupplier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MsRoomRepo extends JpaRepository<MsRoom,Long> {
    @Query(nativeQuery = true,value = "select * from ms_room where is_active = 1 order by id desc")
    List<MsRoom> findAllActive();
}
