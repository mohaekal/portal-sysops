package com.dciindonesia.portalsysops.repository.assetmgmt;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsProgress;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MsProgressRepo extends JpaRepository<MsProgress,Long> {
}
