package com.dciindonesia.portalsysops.security;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsUser;
import com.dciindonesia.portalsysops.repository.portalmanagement.MsAccessRepo;
import com.dciindonesia.portalsysops.repository.portalmanagement.MsUserPortalRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomOAuth2UserService extends DefaultOAuth2UserService {

    @Autowired
    private MsUserPortalRepo  msUserPortalRepo;
    @Autowired
    private MsAccessRepo accessRepo;

    @Override
    public OAuth2User loadUser(OAuth2UserRequest userRequest) throws OAuth2AuthenticationException {
        OAuth2User user =  super.loadUser(userRequest);
        String email = user.getAttribute("email");
        com.dciindonesia.portalsysops.entity.portalmanagement.MsUser userDB = msUserPortalRepo.findByEmail(email);
        List<String> accesses = accessRepo.findAuthorityByUserId(userDB.getId());
        return new CustomOAuth2User(user,accesses);
    }
}
