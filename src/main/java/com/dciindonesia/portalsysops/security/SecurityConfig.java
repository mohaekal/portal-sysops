package com.dciindonesia.portalsysops.security;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsUser;
import com.dciindonesia.portalsysops.entity.portalmanagement.Logs;
import com.dciindonesia.portalsysops.model.UserModel;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.repository.portalmanagement.LogsRepo;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private MsUserRepo msUserRepo;

    @Autowired
    private LogsRepo logsRepo;

    @Qualifier("myUserDetailService")
    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    public PasswordEncoder passwordEncoder() {
        return new PasswordEncoder() {
            @Override
            public String encode(CharSequence charSequence) {
                return getMd5(charSequence.toString());
            }

            @Override
            public boolean matches(CharSequence charSequence, String s) {
                return getMd5(charSequence.toString()).equals(s);
            }
        };
    }

    public static String getMd5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger no = new BigInteger(1, messageDigest);
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        }
        catch (NoSuchAlgorithmException e) {
            System.out.println("Exception thrown for incorrect algorithm: " + e);
            return null;
        }
    }
//
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }


    private Logger log = LoggerFactory.getLogger("sec config");
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        // We don't need CSRF for this example
        httpSecurity
                .headers().frameOptions().sameOrigin().and()
                .csrf().disable()
                // dont authenticate this particular request
                .authorizeRequests().antMatchers("/login","/api/login",
                "/dist/**","/images/**","/plugins/**").permitAll().

                // all other requests need to be authenticated
                anyRequest().authenticated()
                .and().formLogin()
                .loginPage("/login")
                .failureUrl("/login?error=true")
                .permitAll()
                .successHandler(new AuthenticationSuccessHandler() {

                    @Override
                    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                                        Authentication authentication) throws IOException, ServletException {
                        // run custom logics upon successful login

                        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
                        String username = userDetails.getUsername();

                        MsUser usr = msUserRepo.findByEmail(username);
                        Logs tr = new Logs();
                        tr.setAfterchanges("");
                        tr.setBeforechanges("");
                        tr.setCreateDate(LocalDateTime.now());
                        tr.setDescription("Login");
                        tr.setUserId(usr.getId());
                        logsRepo.save(tr);

                        response.sendRedirect("/");
                    }
                })
                .and()
                .oauth2Login()
                .loginPage("/login")
                .userInfoEndpoint()
                .userService(oauthUserService)
                .and()
                .successHandler(new AuthenticationSuccessHandler() {

                    @Override
                    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                                        Authentication authentication) throws IOException, ServletException {


                        CustomOAuth2User oauthUser = (CustomOAuth2User) authentication.getPrincipal();
                        try {
                            MsUser usr = msUserRepo.findByEmail(oauthUser.getName());
                            Logs tr = new Logs();
                            tr.setAfterchanges("");
                            tr.setBeforechanges("");
                            tr.setCreateDate(LocalDateTime.now());
                            tr.setDescription("Login");
                            tr.setUserId(usr.getId());
                            logsRepo.save(tr);

                            response.sendRedirect("/");
                        }catch (NullPointerException e ){
                            response.sendRedirect("/logout");
                        }


                    }
                })
                .failureHandler((request, response, exception) -> {
                    ObjectMapper obj = new ObjectMapper();

                    System.out.println(obj.writeValueAsString(request)+obj.writeValueAsString(response)+obj.writeValueAsString(exception));
                })
                .and()
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessHandler(new LogoutSuccessHandler() {

                    @Override
                    public void onLogoutSuccess(HttpServletRequest request,
                                                HttpServletResponse response, Authentication authentication)
                            throws IOException, ServletException {
                        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
                        String username = userDetails.getUsername();

                        MsUser usr = msUserRepo.findByEmail(username);
                        Logs tr = new Logs();
                        tr.setAfterchanges("");
                        tr.setBeforechanges("");
                        tr.setCreateDate(LocalDateTime.now());
                        tr.setDescription("Logout");
                        tr.setUserId(usr.getId());
                        logsRepo.save(tr);

                        response.sendRedirect("/login");
                    }
                })
                .and()
                .httpBasic();
    }

    @Autowired
    private CustomOAuth2UserService oauthUserService;


}
