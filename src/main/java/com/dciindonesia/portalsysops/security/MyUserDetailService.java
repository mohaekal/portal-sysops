package com.dciindonesia.portalsysops.security;

import com.dciindonesia.portalsysops.entity.portalmanagement.MsAccess;
import com.dciindonesia.portalsysops.entity.portalmanagement.MsUser;
import com.dciindonesia.portalsysops.entity.portalmanagement.RoleAccess;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.repository.portalmanagement.MsAccessRepo;
import com.dciindonesia.portalsysops.repository.portalmanagement.MsUserPortalRepo;
import com.dciindonesia.portalsysops.repository.portalmanagement.RoleAccessRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class MyUserDetailService implements UserDetailsService {

    @Autowired
    private MsUserPortalRepo msUserRepo;

    @Autowired private MsUserRepo msUserRepoAsset;

    @Autowired
    private MsAccessRepo accessRepo;

    private Logger logger = LoggerFactory.getLogger("User Detail Service");

    public Collection<? extends GrantedAuthority> getAuthorities(MsUser user) {

        List<String> accesses = accessRepo.findAuthorityByUserId(user.getId());
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        for (String access : accesses) {
//            logger.info(access);
            authorities.add(new SimpleGrantedAuthority(access.toUpperCase()));
        }

        return authorities;
    }
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        com.dciindonesia.portalsysops.entity.portalmanagement.MsUser user = msUserRepo.findByEmail(s);


        if (user == null) {
            throw new UsernameNotFoundException("User not found with username: " + s);
        }
        try {
            Boolean isEnable = msUserRepoAsset.findByEmail(s).getEnable();
            if (!isEnable){
                throw new UsernameNotFoundException("User have no access: " + s);
            }
        }catch (Exception e){
            throw new UsernameNotFoundException("User not found with username: " + s);
        }


        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(),
                getAuthorities(user)    );
    }

    public String getMd5(String input) {
        try {
            // Static getInstance method is called with hashing SHA
            MessageDigest md = MessageDigest.getInstance("MD5");

            // digest() method called
            // to calculate message digest of an input
            // and return array of byte
            byte[] messageDigest = md.digest(input.getBytes());

            // Convert byte array into signum representation
            BigInteger no = new BigInteger(1, messageDigest);

            // Convert message digest into hex value
            String hashtext = no.toString(16);

            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }

            return hashtext;
        }

        // For specifying wrong message digest algorithms
        catch (NoSuchAlgorithmException e) {
            System.out.println("Exception thrown"
                    + " for incorrect algorithm: " + e);
            return null;
        }
    }
}
