package com.dciindonesia.portalsysops.security;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsUser;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.repository.portalmanagement.MsAccessRepo;
import com.dciindonesia.portalsysops.repository.portalmanagement.MsUserPortalRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class CustomOAuth2User implements OAuth2User {

    private OAuth2User oauth2User;

    private List<String> authorities;

    public CustomOAuth2User(OAuth2User oauth2User,List<String> authorities ) {
        this.authorities = authorities;
        this.oauth2User = oauth2User;
    }

    @Override
    public Map<String, Object> getAttributes() {
        return oauth2User.getAttributes();
    }

    private Logger log = LoggerFactory.getLogger("custom oauth");



    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {



        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        for (String access : this.authorities ) {
            authorities.add(new SimpleGrantedAuthority(access.toUpperCase()));
        }

        return authorities;

    }
    @Override
    public String getName() {
        return oauth2User.<String>getAttribute("email");
    }


    public String getUsername() {
        return oauth2User.<String>getAttribute("username");
    }
}
