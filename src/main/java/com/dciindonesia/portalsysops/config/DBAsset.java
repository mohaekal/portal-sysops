package com.dciindonesia.portalsysops.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@PropertySource({ "classpath:application.properties" })
@EnableJpaRepositories(basePackages = "com.dciindonesia.portalsysops.repository.assetmgmt", entityManagerFactoryRef = "AssetEntityManager", transactionManagerRef = "AssetTransactionManager")
public class DBAsset {
    @Autowired
    private Environment env;

    @Bean
    public EntityManagerFactory AssetEntityManager() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(AssetDataSource());
        em.setPackagesToScan(new String[]{"com.dciindonesia.portalsysops.entity.assetmgmt"});

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        HashMap<String, Object> properties = new HashMap<>();
        em.setJpaPropertyMap(properties);
        em.afterPropertiesSet();
        return em.getObject();
    }

    @Bean
    public DataSource AssetDataSource() {

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("spring.asset.datasource.driver-class-name"));
        dataSource.setUrl(env.getProperty("spring.asset.datasource.url"));
        dataSource.setUsername(env.getProperty("spring.asset.datasource.username"));
        dataSource.setPassword(env.getProperty("spring.asset.datasource.password"));

        return dataSource;
    }

    @Bean
    public PlatformTransactionManager AssetTransactionManager() {

        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(AssetEntityManager());
        return transactionManager;
    }
}
