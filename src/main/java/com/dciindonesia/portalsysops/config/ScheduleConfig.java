package com.dciindonesia.portalsysops.config;

import com.dciindonesia.portalsysops.model.Response;
import com.dciindonesia.portalsysops.model.elastic.Source;
import com.dciindonesia.portalsysops.service.LastValueElastic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

//@EnableScheduling
//@Configuration
public class ScheduleConfig {
    @Autowired
    SimpMessagingTemplate template;

    @Autowired
    LastValueElastic lastValueElastic;

//    @Scheduled(fixedDelay = 60000)
    public void sendAdhocMessages() throws IOException {
        List<Source> source = lastValueElastic.get().hits.hits.stream()
                .map(e->new Source(e))
                .collect(Collectors.toList());
        template.convertAndSend("/data-stream/iot", new Response(true, source));
    }
}
