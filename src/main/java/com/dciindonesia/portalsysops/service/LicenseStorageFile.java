package com.dciindonesia.portalsysops.service;

import com.dciindonesia.portalsysops.entity.assetmgmt.LicenseFile;
import com.dciindonesia.portalsysops.repository.assetmgmt.LicenseFileRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class LicenseStorageFile {
    @Autowired
    private LicenseFileRepo licenseFileRepo;


    public LicenseFile store(MultipartFile file, Long licenseId) throws IOException {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        LicenseFile FileDB = new LicenseFile(licenseId,fileName,file.getSize(), file.getContentType(), file.getBytes());
        return licenseFileRepo.save(FileDB);
    }
    public LicenseFile getFile(Long id) {
        return licenseFileRepo.findById(id).get();
    }

}
