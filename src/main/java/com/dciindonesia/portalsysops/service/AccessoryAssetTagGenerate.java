package com.dciindonesia.portalsysops.service;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsAccessoryCategory;
import com.dciindonesia.portalsysops.entity.assetmgmt.MsAssetCategory;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsAccessoryCategoryRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsAccessoryRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsAssetCategoryRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsAssetRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccessoryAssetTagGenerate {
    @Autowired
    private MsAccessoryRepo msAccessoryRepo;

    @Autowired
    private MsAccessoryCategoryRepo msAssetCategoryRepo;

    public String generateassettag(Integer categoryid){

        MsAccessoryCategory findcategory = msAssetCategoryRepo.findById(Long.valueOf(categoryid)).get();
        Integer countassettag = msAccessoryRepo.findCountAsset(findcategory.getName());
        return generatestring(findcategory.getSlug(),countassettag+1);
    }
    public String generatestring(String slug,Integer count){
        String generate = "IT-"+slug+"-"+count+"-"+"DCI";
        if(msAccessoryRepo.findCountAsset(generate)>0){
            return generatestring(slug,count+1) ;
        }
        return generate ;
    }
}

