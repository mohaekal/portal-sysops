package com.dciindonesia.portalsysops.service;

import com.dciindonesia.portalsysops.entity.portalmanagement.Logs;
import com.dciindonesia.portalsysops.entity.portalmanagement.MsUser;
import com.dciindonesia.portalsysops.entity.portalmanagement.UserRole;
import com.dciindonesia.portalsysops.model.RequestUser;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.repository.portalmanagement.*;
import com.dciindonesia.portalsysops.security.MyUserDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserManagementService {

    @Autowired
    private MsUserRepo msUserRepo;
    @Autowired
    private MsUserPortalRepo msUserPortalRepo;
    @Autowired
    private UserRoleRepo userRoleRepo;
    @Autowired
    private MyUserDetailService userDetailsService;


    public com.dciindonesia.portalsysops.entity.assetmgmt.MsUser changeProfile(RequestUser requestUser ){
        MsUser exisstUserPortal =  msUserPortalRepo.findById(requestUser.getId()).get();
        exisstUserPortal.setUpdateDate(LocalDateTime.now());
        exisstUserPortal.setName(requestUser.getName());
        if (requestUser.getPassword()!=null){
            exisstUserPortal.setPassword(userDetailsService.getMd5(requestUser.getPassword()));
        }
        msUserPortalRepo.save(exisstUserPortal);

        com.dciindonesia.portalsysops.entity.assetmgmt.MsUser useraset = msUserRepo.findById(requestUser.getId()).get();
        useraset.setName(requestUser.getName());
        useraset.setEnable(requestUser.getEnable());
        if (requestUser.getPassword()!=null){
            useraset.setPassword(userDetailsService.getMd5(requestUser.getPassword()));
        }
        return msUserRepo.save(useraset);
    }

    public MsUser addUser(RequestUser requestUser, HttpServletRequest request){


        MsUser userget = msUserPortalRepo.findByEmail(requestUser.getEmail());
        if(userget==null){

            MsUser newUserPortal =  new MsUser();
            newUserPortal.setCreateDate(LocalDateTime.now());
            newUserPortal.setIsActive(true);
            newUserPortal.setEmail(requestUser.getEmail());
            newUserPortal.setName(requestUser.getName());
            newUserPortal.setUsername(requestUser.getEmail());
            newUserPortal.setPassword(userDetailsService.getMd5(requestUser.getPassword()));
            MsUser userportal = msUserPortalRepo.save(newUserPortal);

            com.dciindonesia.portalsysops.entity.assetmgmt.MsUser useraset = new com.dciindonesia.portalsysops.entity.assetmgmt.MsUser();

            useraset.setId(userportal.getId());
            useraset.setName(requestUser.getName());
            useraset.setPassword(userDetailsService.getMd5(requestUser.getPassword()));
            useraset.setEmail(requestUser.getEmail());
            useraset.setEnable(requestUser.getEnable());
            useraset.setCreateDate(LocalDateTime.now());
            useraset.setCreateBy(msUserRepo.findByEmail(request.getUserPrincipal().getName()).getId());
            msUserRepo.save(useraset);

            List<UserRole> userRoles = new ArrayList<>();
            for (Long roleId: requestUser.getRole()
                 ) {
                UserRole userRole = new UserRole();
                userRole.setCreateBy(msUserPortalRepo.findByEmail(request.getUserPrincipal().getName()).getId());
                userRole.setCreateDate(LocalDateTime.now());
                userRole.setUserId(useraset.getId());
                userRole.setRoleId(roleId);
                userRoles.add(userRole);
            }
            userRoleRepo.saveAll(userRoles);

            return userportal;
        }

        return null;
    }
    private Logger log = LoggerFactory.getLogger("ininnni");
    public MsUser updateUser(RequestUser userchange, HttpServletRequest request){
        MsUser exisstUserPortal =  msUserPortalRepo.findById(userchange.getId()).get();
        exisstUserPortal.setUpdateDate(LocalDateTime.now());
        exisstUserPortal.setName(userchange.getName());
        if (userchange.getPassword()!=null && !userchange.getPassword().trim().equals("")){
            exisstUserPortal.setPassword(userDetailsService.getMd5(userchange.getPassword()));
        }
        MsUser userportal = msUserPortalRepo.save(exisstUserPortal);

        com.dciindonesia.portalsysops.entity.assetmgmt.MsUser useraset = msUserRepo.findById(userchange.getId()).get();
        useraset.setName(userchange.getName());
        useraset.setEnable(userchange.getEnable());
        if (userchange.getPassword()!=null && !userchange.getPassword().trim().equals("")){
            useraset.setPassword(userDetailsService.getMd5(userchange.getPassword()));
        }
        msUserRepo.save(useraset);
        userRoleRepo.deleteAll(userRoleRepo.findByUserId(userchange.getId()));
        List<UserRole> userRoles = new ArrayList<>();
        for (Long roleId:userchange.getRole()
        ) {
            UserRole userRole = new UserRole();
            userRole.setCreateBy(msUserPortalRepo.findByEmail(request.getUserPrincipal().getName()).getId());
            userRole.setCreateDate(LocalDateTime.now());
            userRole.setUserId(userchange.getId());
            userRole.setRoleId(roleId);
            userRoles.add(userRole);
        }
        userRoleRepo.saveAll(userRoles);
        return userportal;
    }

    public MsUser removeUser(RequestUser userchange, HttpServletRequest request){
        MsUser exisstUserPortal =  msUserPortalRepo.findById(userchange.getId()).get();
        exisstUserPortal.setUpdateDate(LocalDateTime.now());
        exisstUserPortal.setIsActive(false);
        MsUser userportal = msUserPortalRepo.save(exisstUserPortal);

        com.dciindonesia.portalsysops.entity.assetmgmt.MsUser useraset = msUserRepo.findById(userchange.getId()).get();
        useraset.setEnable(false);
        msUserRepo.save(useraset);

        return userportal;

    }
    @Autowired
    private LogsRepo logsRepo;

    public Logs track(HttpServletRequest request, String before,String after,String desc){
        try {
            Logs tr = new Logs();
            tr.setAfterchanges(after);
            tr.setBeforechanges(before);
            tr.setCreateDate(LocalDateTime.now());
            tr.setDescription(desc);
            tr.setUserId(msUserPortalRepo.findByEmail(request.getUserPrincipal().getName()).getId());
            return logsRepo.save(tr);

        }catch (Exception e){
            return null;
        }
    }
}
