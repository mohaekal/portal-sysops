package com.dciindonesia.portalsysops.service;

import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.net.ssl.*;
import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

@Service
public class Telegram {

    private Logger log = LoggerFactory.getLogger("TELEGRAM");

    @Value("${telegram.bot-token}")
    private String botToken ;

    @Value("${telegram.chat-id}")
    private String chatID ;

    @Value("${telegram.enable}")
    private String telegramEnable;

    @Autowired
    private MsUserRepo msUserRepo;

    public void send(String text )  {
        if (telegramEnable.toLowerCase().equals("yes")){
            try {
                String url = "http://10.2.222.129:1000?token=" + this.botToken + "&chatid=" + this.chatID + "&text=" + text ;

                OkHttpClient client = getUnsafeHttpClient();
                Request request = new Request.Builder()
                        .url(url)
                        .method("GET", null)
                        .build();

                Response response = client.newCall(request).execute();
                String codeTelegramMsg = response.body().string();
                log.info(codeTelegramMsg);
            }catch (Exception e){
                log.warn("Telegram Error : "+e.getMessage()+" "+e.getCause());
            }
        }
    }


    private static OkHttpClient getUnsafeHttpClient(){
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder()
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS);;
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager)trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
