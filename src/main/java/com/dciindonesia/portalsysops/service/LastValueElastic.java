package com.dciindonesia.portalsysops.service;

import com.dciindonesia.portalsysops.model.ElasticResponse;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class LastValueElastic extends ElasticLoad {

    public ElasticResponse get() throws IOException {
        return elasticResponse(this.query());
    }
    private String query() {

     return "{\n" +
                "  \"from\" : 0, \"size\" : 10000,\n" +
                "\"sort\": [\n" +
                "    {\n" +
                "      \"datetime\": {\n" +
                "        \"order\": \"desc\"\n" +
                "      }\n" +
                "    }\n" +
                "  ],  \n" +
                "  \"query\": {\n" +
                "    \"bool\": {\n" +
                "      \"must\": [\n" +
                "       {\n" +
                "          \"range\": {\n" +
                "            \"datetime\": {\n" +
                "              \"gte\": \"now-30d\",\n" +
                "              \"time_zone\": \"+07:00\",\n" +
                "              \"format\": \"dd/MM/yyyy||yyyy\"\n" +
                "            }\n" +
                "          }" +
                "       }" +
                "      ]\n" +
                "    }\n" +
                "  },\n" +
                "  \"collapse\": {\n" +
                "    \"field\": \"variable_id\"\n" +
                "  }\n" +
                "}";
    }
}
