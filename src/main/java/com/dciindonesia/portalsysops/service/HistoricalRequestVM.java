package com.dciindonesia.portalsysops.service;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsUser;
import com.dciindonesia.portalsysops.entity.assetmgmt.TrApprovementRequest;
import com.dciindonesia.portalsysops.entity.assetmgmt.TrRequestProgress;
import com.dciindonesia.portalsysops.model.History;
import com.dciindonesia.portalsysops.model.HistoryApprovement;
import com.dciindonesia.portalsysops.model.InterfaceRequestProgress;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsUserRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.TrApprovementRequestRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.TrRequestProgressRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class HistoricalRequestVM {

    @Autowired
    private TrApprovementRequestRepo trApprovementRequestRepo;

    @Autowired
    private TrRequestProgressRepo trRequestProgressRepo;
    @Autowired
    private MsUserRepo msUserRepo;

    private Logger log = Logger.getLogger("t");
    public List<History> getHistoryByRequestId(Long id){

        List<History> histories = new ArrayList<>();
        List<MsUser> users  = msUserRepo.findAll();
        List<InterfaceRequestProgress> progresses = trRequestProgressRepo.findHistoryByRequestId(id);
        List<TrApprovementRequest> approvementRequests = trApprovementRequestRepo.findByRequestId(id);
        log.info("progres:"+progresses.size());
        log.info("appr:"+approvementRequests.size());


        for (InterfaceRequestProgress progres:progresses
             ) {

            List<HistoryApprovement> approvement = approvementRequests.stream()
                    .filter(r->r.getRequestProgressId().equals(progres.getId()))
                    .map(e->new HistoryApprovement(e,users))
                    .collect(Collectors.toList());
            History history = new History(progres,approvement);
            histories.add(history);
        }
        return histories;
    }
}
