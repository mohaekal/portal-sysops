package com.dciindonesia.portalsysops.service;


import com.dciindonesia.portalsysops.model.ElasticResponse;
import com.google.gson.Gson;
import okhttp3.*;

import javax.net.ssl.*;
import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

public class ElasticLoad {
    private String Username = "yusron";
    private String Password = "zilong2021";
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    protected ElasticResponse elasticResponse(String queryelastic) throws IOException {
        String credential = Credentials.basic(this.Username, this.Password);
        String url = "https://10.2.222.216:9200/sensor-iot-activity/_search";
        OkHttpClient client = getUnsafeHttpClient();

        RequestBody body = RequestBody.create(JSON, queryelastic);

        Request request =
                new Request.Builder()
                        .url(url)
                        .post(body)
                        .header("Authorization", credential)
                        .build();

        try (
                Response response = client.newCall(request).execute()) {
            Gson a = new Gson();

            ElasticResponse p = a.fromJson(response.body().string(), ElasticResponse.class);
            return p;
        }
    }

    private static OkHttpClient getUnsafeHttpClient(){
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder()
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS);;
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager)trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}