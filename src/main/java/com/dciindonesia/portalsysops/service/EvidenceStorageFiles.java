package com.dciindonesia.portalsysops.service;

import com.dciindonesia.portalsysops.entity.assetmgmt.LicenseFile;
import com.dciindonesia.portalsysops.entity.assetmgmt.TrEvidenceFile;
import com.dciindonesia.portalsysops.repository.assetmgmt.TrEvidenceFileRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class EvidenceStorageFiles {

    @Autowired
    private TrEvidenceFileRep trEvidenceFileRepo;


    public List<TrEvidenceFile> store(MultipartFile[] files, Long reqProgressID) throws IOException {

        List<TrEvidenceFile> trsFiles = new ArrayList<>();
        for (MultipartFile file:files
             ) {

            String fileName = StringUtils.cleanPath(file.getOriginalFilename());
            TrEvidenceFile FileDB = new TrEvidenceFile(reqProgressID,fileName,file.getContentType(),  file.getBytes(), file.getSize(), LocalDateTime.now(),null);
            trsFiles.add(FileDB);
        }
        return trEvidenceFileRepo.saveAll(trsFiles);
    }

    public List<TrEvidenceFile> getFile(Long requestid) {
        return trEvidenceFileRepo.findLastRequestProgress(requestid);
    }

    public List<TrEvidenceFile> getFileId() {
        return trEvidenceFileRepo.findAllLID();
    }
    public TrEvidenceFile getFilee(Long id) {
        return trEvidenceFileRepo.findById(id).get();
    }


}
