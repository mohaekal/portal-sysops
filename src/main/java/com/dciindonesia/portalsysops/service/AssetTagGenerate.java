package com.dciindonesia.portalsysops.service;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsAssetCategory;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsAssetCategoryRepo;
import com.dciindonesia.portalsysops.repository.assetmgmt.MsAssetRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AssetTagGenerate {
    @Autowired
    private MsAssetRepo msAssetRepo;

    @Autowired
    private MsAssetCategoryRepo msAssetCategoryRepo;

    public String generateassettag(Integer categoryid){

        MsAssetCategory findcategory = msAssetCategoryRepo.findById(categoryid).get();
        Integer countassettag = msAssetRepo.findCountAsset(findcategory.getName());
        return generatestring(findcategory.getSlug(),countassettag+1);
    }
    public String generatestring(String slug,Integer count){
        String generate = "IT-"+slug+"-"+count+"-"+"DCI";
        if(msAssetRepo.findCountAsset(generate)>0){
            return generatestring(slug,count+1) ;
        }
        return generate ;
    }
}
