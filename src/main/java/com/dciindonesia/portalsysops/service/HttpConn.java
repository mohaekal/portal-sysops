package com.dciindonesia.portalsysops.service;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.springframework.stereotype.Service;

import javax.net.ssl.*;
import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;
@Service
public class HttpConn {

        public Response get(String url ) throws IOException {

            OkHttpClient client = getUnsafeHttpClient();

            Request request =
                    new Request.Builder()
                            .url(url)
                            .get()
                            .build();

            return client.newCall(request).execute();
        }

        public Response post(String url, RequestBody requestBody ) throws IOException {

            OkHttpClient client = getUnsafeHttpClient();

            Request request =
                    new Request.Builder()
                            .url(url)
                            .post(requestBody)
                            .build();

            return client.newCall(request).execute();
        }

        private static OkHttpClient getUnsafeHttpClient(){
            try {
                // Create a trust manager that does not validate certificate chains
                final TrustManager[] trustAllCerts = new TrustManager[] {
                        new X509TrustManager() {
                            @Override
                            public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                            }

                            @Override
                            public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                            }

                            @Override
                            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                                return new java.security.cert.X509Certificate[]{};
                            }
                        }
                };

                // Install the all-trusting trust manager
                final SSLContext sslContext = SSLContext.getInstance("SSL");
                sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
                // Create an ssl socket factory with our all-trusting manager
                final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

                OkHttpClient.Builder builder = new OkHttpClient.Builder();
                builder.sslSocketFactory(sslSocketFactory, (X509TrustManager)trustAllCerts[0]);
                builder.hostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        return true;
                    }
                });

                OkHttpClient okHttpClient = builder
                        .connectTimeout(600, TimeUnit.SECONDS)
                        .readTimeout(600,TimeUnit.SECONDS)
                        .writeTimeout(600,TimeUnit.SECONDS)
                        .build();
                return okHttpClient;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

}
