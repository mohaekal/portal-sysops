package com.dciindonesia.portalsysops.service;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

@Service
public class ExportReport {

    private SXSSFWorkbook workbook;
    private SXSSFSheet sheet;
    private Collection<List<String>> report;
    private List<String> headers;
    private String userDownload;
    private static final Logger log = LoggerFactory.getLogger("exportExcel");
    private Boolean multiplesheet = false;


    public ExportReport init(Collection<List<String>> report, List<String> headers, String userDownload) {
        this.headers = headers;
        this.report = report;
        this.userDownload = userDownload;
        workbook = new SXSSFWorkbook();

        writeHeaderLine();
        writeDataLines();
        return this;
    }

    public ExportReport addSheet(Collection<List<String>> report, List<String> headers) {
        this.headers = headers;
        this.report = report;

        writeHeaderLine();
        writeDataLines();
        return this;
    }


    private void writeHeaderLine() {
        sheet = workbook.createSheet();
        CellStyle style = workbook.createCellStyle();
        CellStyle styleCenter = workbook.createCellStyle();
        styleCenter.setAlignment(HorizontalAlignment.CENTER);

        Row row = sheet.createRow(0);
        int countcolumn = 0;
        for (String head:headers
             ) {
            createCell(row, countcolumn++, head, style);
        }
    }

    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
//        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        }else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    private void writeDataLines() {
        int rowCount = 1;
        int counter = 1;
        for (List<String> rp: report
        ) {
            log.info("Report Downloading by "+this.userDownload + " : "+rowCount+"/"+report.size());
            Row row = sheet.createRow(rowCount++);
            int colCount = 0;
            for (String coldata:rp
                 ) {
                createCell(row, colCount++, coldata, null);
            }
        }
    }


    public void export(HttpServletResponse response) throws IOException {

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();

        outputStream.close();

    }
}
