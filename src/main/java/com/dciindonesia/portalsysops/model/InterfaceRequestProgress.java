package com.dciindonesia.portalsysops.model;

import javax.persistence.Column;
import java.time.LocalDateTime;

public interface InterfaceRequestProgress {

    Long getId();
    Long getRequestId();
    Long getProgressId();
    Boolean getIsActive();
    LocalDateTime getCreateDate();
    Integer getCreateBy();
    String getRemark();
    String getProgress();

}
