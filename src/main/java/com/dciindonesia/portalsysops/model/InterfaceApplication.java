package com.dciindonesia.portalsysops.model;
import java.time.LocalDate;

public interface InterfaceApplication {
    Long getId();
    String getName();
    String getServer();
    String getUrl();
    String getUserServer();
    String getPasswordServer();
    String getPath();
    String getUserDb();
    String getPasswordDb();
    String getVersion();
    String getOsVersion();
    String getDbVersion();
    LocalDate getLastDeploy();
    Integer getApplicationStatusId();
    String getApplicationStatus();
}
