package com.dciindonesia.portalsysops.model;

public interface InterfaceAccess {

    Long getId();
    String getName();
    String getDescription();
    Integer getUsed();
}
