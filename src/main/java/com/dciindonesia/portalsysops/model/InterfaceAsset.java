package com.dciindonesia.portalsysops.model;

import javax.persistence.Column;
import java.time.LocalDate;
import java.time.LocalDateTime;

public interface InterfaceAsset {


    Long getId();
    String getAssetTag();
    String getAssetName();
    Integer getAssetStatusId();

    String getSocket();
    String getCore();
    String getType();
    String getStorage();
    String getMemory();


    String getAssetStatus();
    String getIpManagement();
    String getIpHost();
    String getUserManagement();
    String getPasswordManagement();

    String getUserHost();
    String getPasswordHost();
    LocalDate getPurchaseDate();
    String getPurchaseDateStr();
    String getSupplier();
    Integer getSupplierId();
    String getPrNumber();
    Integer getPurchaseCost();
    Integer getWarranty();
    LocalDate getExpDate();
    String getExpDateString();
    Integer getExpStats();
    String getNotes();
    String getLocation();
    Integer getLocationId();
    Long getRoomId();
    String getRoom();

    String getPriority();
    String getBackup();
    String getHighAvailbility();
    String getAvSpanport();
    String getAvOssec();
    String getAntivirus();
    String getGrafana();
    String getPrtgIcmp();
    String getPrtgService();

    Boolean getIsActive();
    Integer getAssetCategoryId();
    String getAssetCategory();
    LocalDateTime getCreateDate();
    Integer getCreateBy();
    LocalDateTime getUpdateDate();
    Integer getUpdateBy();
    Long getCompanyId();
    String getCompany();
}
