package com.dciindonesia.portalsysops.model;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsUser;
import com.dciindonesia.portalsysops.entity.assetmgmt.TrApprovementRequest;

import java.time.LocalDateTime;
import java.util.List;

public class HistoryApprovement {

    public HistoryApprovement(TrApprovementRequest tar, List<MsUser> users) {
        this.id = tar.getId();
        this.requestProgressId = tar.getRequestProgressId();
        this.userId = tar.getUserId();
        this.userString = users.stream().filter(e->e.getId().equals(tar.getUserId())).map(f->f.getName()).findFirst().get();
        this.isApprove = tar.getApprove();
        this.reason = tar.getReason();
        this.createDate = tar.getCreateDate();
        this.createBy = tar.getCreateBy();
        this.createByString = users.stream().filter(e->e.getId().equals(tar.getCreateBy())).map(f->f.getName()).findFirst().get();
        this.updateDate = tar.getUpdateDate();
        this.updateBy = tar.getUpdateBy();
        try {
            this.updateByString = users.stream().filter(e->e.getId().equals(tar.getUpdateBy())).map(f->f.getName()).findFirst().get();
        }catch (Exception e){}
    }

    private Long id;
    private Long requestProgressId;
    private Integer userId;
    private String userString;
    private Boolean isApprove;
    private String reason;
    private LocalDateTime createDate;
    private Integer createBy;
    private String createByString;
    private LocalDateTime updateDate;
    private Integer updateBy;
    private String updateByString;

    public String getUserString() {
        return userString;
    }

    public void setUserString(String userString) {
        this.userString = userString;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRequestProgressId() {
        return requestProgressId;
    }

    public void setRequestProgressId(Long requestProgressId) {
        this.requestProgressId = requestProgressId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Boolean getApprove() {
        return isApprove;
    }

    public void setApprove(Boolean approve) {
        isApprove = approve;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public Integer getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    public String getCreateByString() {
        return createByString;
    }

    public void setCreateByString(String createByString) {
        this.createByString = createByString;
    }

    public LocalDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Integer updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateByString() {
        return updateByString;
    }

    public void setUpdateByString(String updateByString) {
        this.updateByString = updateByString;
    }
}
