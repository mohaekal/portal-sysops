package com.dciindonesia.portalsysops.model;

public class Response {
    public Response() {
    }

    public Response(Boolean success, Object data) {
        this.success = success;
        this.data = data;
    }

    private Boolean success;
    private Object data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
