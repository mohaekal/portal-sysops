package com.dciindonesia.portalsysops.model;

import javax.persistence.Column;
import java.time.LocalDateTime;

public interface InterfaceRemoteAccess {

    Long getId();
    String getIp();
    String getPort();
    String getApplication();
    String getUser();
    String getPassword();
    String getPurpose();
    Boolean getIsActive();
    LocalDateTime getCreateDate();
    Integer getCreateBy();
    LocalDateTime getUpdateDate();
    Integer getUpdateBy();
    Long getRoomId();
    Long getLocationId();
    String getRoom();
    String getLocation();
}
