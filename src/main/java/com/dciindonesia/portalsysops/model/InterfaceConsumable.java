package com.dciindonesia.portalsysops.model;
import java.time.LocalDate;
import java.time.LocalDateTime;

public interface InterfaceConsumable {

    Long getId();
    String getName();
    Integer getConsumableCategoryId();
    String getCategory();
    Integer getManufacturerId();
    String getManufacturer();
    Integer getLocationId();
    String getLocation();
    Long getRoomId();
    String getRoom();
    String getModel();
    String getPrNumber();
    LocalDate getPurchaseDate();
    String getPurchaseDateString();
    Integer getPurchaseCost();
    Integer getQty();
    Integer getQtyUsed();
    String getNote();

}
