package com.dciindonesia.portalsysops.model;


import java.time.LocalDate;
import java.time.LocalDateTime;

public interface InterfaceVM {
    Long getId();
    Long getRequestId();
    String getRequestBy();
    Integer getSocket();
    String getName();
    String getIp();
    String getCpu();
    String getMemory();
    String getStorage();
    Long getLicenseId();
    Long getAssetId();
    String getLicense();
    String getAsset();
    String getAssetIPHost();
    String getState();
    String getDescription();
    Boolean getBackup();
    Boolean getHighAvailbility();
    String getPriority();
    Boolean getAvSpanPort();
    Boolean getAvOssec();
    String getAntivirus();
    Boolean getAntivirusInstalled();
    LocalDate getAntivirusLastupdate();
    String getAntivirusLastupdateStr();
    Boolean getPrtgIcmp();
    Boolean getPrtgService();
    String getPrtgServiceRemark();
    Boolean getGrafana();
    String getProgress();
    Long getProgressid();
    Long getApprovementId();
    String getEvidenceRemark();
    Long getRequestProgressId();
    LocalDateTime getRequestDate();
    String getCompany();
    Long getCompanyId();
    String getRemark();
    String getRequestType();
}
