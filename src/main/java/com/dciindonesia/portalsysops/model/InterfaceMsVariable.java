package com.dciindonesia.portalsysops.model;

import java.time.LocalDateTime;

public interface InterfaceMsVariable {

    public Long getId() ;

    public Integer getDeviceId() ;

    public String getDeviceName() ;

    public Integer getGroupId() ;

    public String getGroupNm() ;

    public String getVariableName() ;

    public String getVariableAttribute() ;

    public Integer getBuildingIdx() ;

    public String getBuildingId() ;

    public String getUbidotsId() ;

    public String getMacAddress() ;

    public String getType() ;

    public Double getMaxvaluee() ;

    public String getUnit() ;
    public String getMeasurement() ;

    public LocalDateTime getLastUpdate() ;

    public Integer getLevelId() ;

    public String getLevelName() ;
}
