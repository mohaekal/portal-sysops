package com.dciindonesia.portalsysops.model;

import java.io.Serializable;
import java.time.LocalDateTime;

public class JwtResponse implements Serializable {

    private static final long serialVersionUID = -8091879091924046844L;
    private final String jwttoken;
    private LocalDateTime jwtTime;

    public JwtResponse(String jwttoken,LocalDateTime jwttime) {
        this.jwttoken = jwttoken;
        this.jwtTime = jwttime;
    }

    public LocalDateTime getExpireDate(){return this.jwtTime;}

    public String getToken() {
        return this.jwttoken;
    }
}
