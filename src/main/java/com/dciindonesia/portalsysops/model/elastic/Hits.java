package com.dciindonesia.portalsysops.model.elastic;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Hits {

    public Hits(){}

    @SerializedName("total")
    @Expose
    public com.dciindonesia.portalsysops.model.elastic.Total total;
    @SerializedName("max_score")
    @Expose
    public Double maxScore;
    @SerializedName("hits")
    @Expose
    public List<Hit> hits = null;


    public com.dciindonesia.portalsysops.model.elastic.Total getTotal() {
        return total;
    }

    public void setTotal(com.dciindonesia.portalsysops.model.elastic.Total total) {
        this.total = total;
    }

    public Double getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(Double maxScore) {
        this.maxScore = maxScore;
    }

    public List<Hit> getHits() {
        return hits;
    }

    public void setHits(List<Hit> hits) {
        this.hits = hits;
    }
}