package com.dciindonesia.portalsysops.model.elastic;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Hit {

    public Hit(){}

    @SerializedName("_index")
    @Expose
    public String index;
    @SerializedName("_type")
    @Expose
    public String type;
    @SerializedName("_id")
    @Expose
    public String id;
    @SerializedName("_score")
    @Expose
    public Double score;
    @SerializedName("_source")
    @Expose
    public com.dciindonesia.portalsysops.model.elastic.Source source;

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public com.dciindonesia.portalsysops.model.elastic.Source getSource() {
        return source;
    }

    public void setSource(com.dciindonesia.portalsysops.model.elastic.Source source) {
        this.source = source;
    }
}
