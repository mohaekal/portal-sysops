package com.dciindonesia.portalsysops.model.elastic;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Shards {

    public Shards (){}

    @SerializedName("total")
    @Expose
    public Integer total;
    @SerializedName("successful")
    @Expose
    public Integer successful;
    @SerializedName("skipped")
    @Expose
    public Integer skipped;
    @SerializedName("failed")
    @Expose
    public Integer failed;

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getSuccessful() {
        return successful;
    }

    public void setSuccessful(Integer successful) {
        this.successful = successful;
    }

    public Integer getSkipped() {
        return skipped;
    }

    public void setSkipped(Integer skipped) {
        this.skipped = skipped;
    }

    public Integer getFailed() {
        return failed;
    }

    public void setFailed(Integer failed) {
        this.failed = failed;
    }
}