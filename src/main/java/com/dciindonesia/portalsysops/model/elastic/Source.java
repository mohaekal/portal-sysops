package com.dciindonesia.portalsysops.model.elastic;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Source {

    public Source(){}

    public Source(Hit h) {
        this.levelName = h.getSource().getLevelName();
        this.datetime = h.getSource().getDatetime();
        this.idNonIot = h.getSource().getIdNonIot();
        this.compressorId = h.getSource().getCompressorId();
        this.variableId = h.getSource().getVariableId();
        this.sensorName = h.getSource().getSensorName();
        this.timestamp = h.getSource().getTimestamp();
        this.powerStatus = h.getSource().getPowerStatus();
        this.value = h.getSource().getValue();
        this.highVibration1 = h.getSource().getHighVibration1();
        this.highVibration2 = h.getSource().getHighVibration2();
        this.valueNonIot = h.getSource().getValueNonIot();
        this.shortLevelName = h.getSource().getShortLevelName();
        this.datetimePowerStatus = h.getSource().getDatetimePowerStatus();
        this.datetimeNonIot = h.getSource().getDatetimeNonIot();
        this.location = h.getSource().getLocation();
        this.groupNm = h.getSource().getGroupNm();
        this.building = h.getSource().getBuilding();
        this.version = h.getSource().getVersion();
        this.variableName = h.getSource().getVariableName();
        this.id = h.getSource().getId();
        this.cposObjectName = h.getSource().getCposObjectName();
        this.cposChillerReference = h.getSource().getCposChillerReference();
        this.cposAlert = h.getSource().getCposAlert();
        this.cposDeviceName = h.getSource().getCposDeviceName();
        this.cposDeviceNameId = h.getSource().getCposDeviceNameId();
        this.frontendBuilding =h.getSource().getFrontendBuilding();
        this.frontendGroupVariable = h.getSource().getFrontendGroupVariable();
        this.frontendLevel = h.getSource().getFrontendLevel();
        this.frontendUrl = h.getSource().getFrontendUrl();
        this.frontEndIsUsed = h.getSource().getFrontEndIsUsed();
        this.unit = h.getSource().getUnit();
        this.maxValue = h.getSource().getMaxValue();
        this.measurement = h.getSource().getMeasurement();
        this.notation = h.getSource().getNotation();
        this.threshold = h.getSource().getThreshold();
    }

    @SerializedName("level_name")
    @Expose
    public String levelName;
    @SerializedName("datetime")
    @Expose
    public String datetime;
    @SerializedName("id_non_iot")
    @Expose
    public Integer idNonIot;
    @SerializedName("compressor_id")
    @Expose
    public Integer compressorId;
    @SerializedName("variable_id")
    @Expose
    public Integer variableId;
    @SerializedName("sensor_name")
    @Expose
    public String sensorName;
    @SerializedName("@timestamp")
    @Expose
    public String timestamp;
    @SerializedName("power_status")
    @Expose
    public Integer powerStatus;
    @SerializedName("value")
    @Expose
    public Double value;
    @SerializedName("value_non_iot")
    @Expose
    public Double valueNonIot;
    @SerializedName("high_vibration1")
    @Expose
    public Double highVibration1;
    @SerializedName("high_vibratoin2")
    @Expose
    public Double highVibration2;
    @SerializedName("short_level_name")
    @Expose
    public String shortLevelName;
    @SerializedName("datetime_power_status")
    @Expose
    public String datetimePowerStatus;
    @SerializedName("datetime_non_iot")
    @Expose
    public String datetimeNonIot;
    @SerializedName("location")
    @Expose
    public String location;
    @SerializedName("group_nm")
    @Expose
    public String groupNm;
    @SerializedName("building")
    @Expose
    public String building;
    @SerializedName("@version")
    @Expose
    public String version;
    @SerializedName("variable_name")
    @Expose
    public String variableName;
    @SerializedName("id")
    @Expose
    public Long id;

    @SerializedName("cpos_object_name")
    @Expose
    public String cposObjectName;

    @SerializedName("cpos_device_name")
    @Expose
    public String cposDeviceName;

    @SerializedName("cpos_device_name_id")
    @Expose
    public String cposDeviceNameId;

    @SerializedName("cpos_chiller_reference")
    @Expose
    public Integer cposChillerReference;

    @SerializedName("cpos_alert")
    @Expose
    public Boolean cposAlert;

    @SerializedName("frontend_is_used")
    @Expose
    public Boolean frontEndIsUsed;

    @SerializedName("frontend_building")
    @Expose
    public String frontendBuilding;

    @SerializedName("frontend_level")
    @Expose
    public String frontendLevel;

    @SerializedName("frontend_group_variable")
    @Expose
    public String frontendGroupVariable;

    @SerializedName("frontend_url")
    @Expose
    public String frontendUrl;

    @SerializedName("unit")
    @Expose
    public String unit;

    @SerializedName("max_value")
    @Expose
    public Double maxValue;

    @SerializedName("measurement")
    @Expose
    public String measurement;

    @SerializedName("notation")
    @Expose
    public String notation;

    @SerializedName("threshold")
    @Expose
    public Double threshold;

    @SerializedName("value1")
    @Expose
    public Double value1;

    @SerializedName("groupid")
    @Expose
    public Integer groupid;

    public Integer getGroupid() {
        return groupid;
    }

    public void setGroupid(Integer groupid) {
        this.groupid = groupid;
    }

    public void setValue1(Double value1) {
        this.value1 = value1;
    }

    public String getMeasurement() {
        return measurement;
    }

    public void setMeasurement(String measurement) {
        this.measurement = measurement;
    }

    public String getNotation() {
        return notation;
    }

    public void setNotation(String notation) {
        this.notation = notation;
    }

    public Double getThreshold() {
        return threshold;
    }

    public void setThreshold(Double threshold) {
        this.threshold = threshold;
    }

    public Double getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(Double maxValue) {
        this.maxValue = maxValue;
    }

    private LocalDateTime convertedDatetime;

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Boolean getFrontEndIsUsed() {
        return frontEndIsUsed;
    }

    public void setFrontEndIsUsed(Boolean frontEndIsUsed) {
        this.frontEndIsUsed = frontEndIsUsed;
    }

    public String getFrontendBuilding() {
        return frontendBuilding;
    }

    public void setFrontendBuilding(String frontendBuilding) {
        this.frontendBuilding = frontendBuilding;
    }

    public String getFrontendLevel() {
        return frontendLevel;
    }

    public void setFrontendLevel(String frontendLevel) {
        this.frontendLevel = frontendLevel;
    }

    public String getFrontendGroupVariable() {
        return frontendGroupVariable;
    }

    public void setFrontendGroupVariable(String frontendGroupVariable) {
        this.frontendGroupVariable = frontendGroupVariable;
    }

    public String getFrontendUrl() {
        return frontendUrl;
    }

    public void setFrontendUrl(String frontendUrl) {
        this.frontendUrl = frontendUrl;
    }

    public String getCposObjectName() {
        return cposObjectName;
    }

    public void setCposObjectName(String cposObjectName) {
        this.cposObjectName = cposObjectName;
    }

    public String getCposDeviceName() {
        return cposDeviceName;
    }

    public void setCposDeviceName(String cposDeviceName) {
        this.cposDeviceName = cposDeviceName;
    }

    public String getCposDeviceNameId() {
        return cposDeviceNameId;
    }

    public void setCposDeviceNameId(String cposDeviceNameId) {
        this.cposDeviceNameId = cposDeviceNameId;
    }

    public Integer getCposChillerReference() {
        return cposChillerReference;
    }

    public void setCposChillerReference(Integer cposChillerReference) {
        this.cposChillerReference = cposChillerReference;
    }

    public Boolean getCposAlert() {
        return cposAlert;
    }

    public void setCposAlert(Boolean cposAlert) {
        this.cposAlert = cposAlert;
    }

    public Double getHighVibration1() {
        return highVibration1;
    }

    public void setHighVibration1(Double highVibration1) {
        this.highVibration1 = highVibration1;
    }

    public Double getHighVibration2() {
        return highVibration2;
    }

    public void setHighVibration2(Double highVibration2) {
        this.highVibration2 = highVibration2;
    }

    public Integer getIdNonIot() {
        return idNonIot;
    }

    public void setIdNonIot(Integer idNonIot) {
        this.idNonIot = idNonIot;
    }

    public Integer getCompressorId() {
        return compressorId;
    }

    public void setCompressorId(Integer compressorId) {
        this.compressorId = compressorId;
    }

    public String getSensorName() {
        return sensorName;
    }

    public void setSensorName(String sensorName) {
        this.sensorName = sensorName;
    }

    public Double getValueNonIot() {
        return valueNonIot;
    }

    public void setValueNonIot(Double valueNonIot) {
        this.valueNonIot = valueNonIot;
    }

    public String getShortLevelName() {
        return shortLevelName;
    }

    public void setShortLevelName(String shortLevelName) {
        this.shortLevelName = shortLevelName;
    }

    public String getDatetimeNonIot() {
        return datetimeNonIot;
    }

    public void setDatetimeNonIot(String datetimeNonIot) {
        this.datetimeNonIot = datetimeNonIot;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getVariableName() {
        return variableName;
    }

    public void setVariableName(String variableName) {
        this.variableName = variableName;
    }

    public String getDatetimePowerStatus() {
        return datetimePowerStatus;
    }

    public void setDatetimePowerStatus(String datetimePowerStatus) {
        this.datetimePowerStatus = datetimePowerStatus;
    }

    public Double getValue() {
        return value;
    }


    public Double getValue1() {
        return value1;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getGroupNm() {
        return groupNm;
    }

    public void setGroupNm(String groupNm) {
        this.groupNm = groupNm;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public LocalDateTime getConvertedDatetime() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime convertedDatetime = LocalDateTime.parse(this.getDatetime().replace("T"," ").replace(".000Z",""), formatter);
        return convertedDatetime.plusHours(7);
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPowerStatus() {
        return powerStatus;
    }

    public void setPowerStatus(Integer powerStatus) {
        this.powerStatus = powerStatus;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getVariableId() {
        return variableId;
    }

    public void setVariableId(Integer variableId) {
        this.variableId = variableId;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }
}