package com.dciindonesia.portalsysops.model.elastic;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Total {

    public Total(){}

    @SerializedName("value")
    @Expose
    public Integer value;
    @SerializedName("relation")
    @Expose
    public String relation;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }
}