package com.dciindonesia.portalsysops.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

public interface AccessoryDetail {

    Long getId();
    String getName();
    String getAssetTag();
    Integer getAccessoryStatusId();
    String getStatus();
    Integer getAccessoryCategoryId();
    String getCategory();
    LocalDate getPurchaseDate();
    String getPurchaseDateStr();
    Integer getSupplierId();
    String getSupplier();
    Integer getManufacturerId();
    String getManufacturer();
    String getOwner();
    String getModel();
    String getPrNumber();
    Integer getPurchaseCost();
    Integer getLocationId();
    String getLocation();
    String getRoom();
    Integer getQty();
    Long getLogId();
    String getUsedBy();

}
