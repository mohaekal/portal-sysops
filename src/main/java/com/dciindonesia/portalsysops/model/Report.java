package com.dciindonesia.portalsysops.model;

import java.util.Collection;
import java.util.List;

public class Report {

    private Collection<List<String>> report;
    private List<String> header;

    public Collection<List<String>> getReport() {
        return report;
    }

    public void setReport(Collection<List<String>> report) {
        this.report = report;
    }

    public List<String> getHeader() {
        return header;
    }

    public void setHeader(List<String> header) {
        this.header = header;
    }
}
