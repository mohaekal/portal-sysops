package com.dciindonesia.portalsysops.model;

import com.dciindonesia.portalsysops.model.elastic.Hits;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ElasticResponse {

    public ElasticResponse(){}

    @SerializedName("took")
    @Expose
    public Integer took;
    @SerializedName("timed_out")
    @Expose
    public Boolean timedOut;
    @SerializedName("_shards")
    @Expose
    public com.dciindonesia.portalsysops.model.elastic.Shards shards;
    @SerializedName("hits")
    @Expose
    public Hits hits;

    public Integer getTook() {
        return took;
    }

    public void setTook(Integer took) {
        this.took = took;
    }

    public Boolean getTimedOut() {
        return timedOut;
    }

    public void setTimedOut(Boolean timedOut) {
        this.timedOut = timedOut;
    }

    public com.dciindonesia.portalsysops.model.elastic.Shards getShards() {
        return shards;
    }

    public void setShards(com.dciindonesia.portalsysops.model.elastic.Shards shards) {
        this.shards = shards;
    }

    public Hits getHits() {
        return hits;
    }

    public void setHits(Hits hits) {
        this.hits = hits;
    }
}
