package com.dciindonesia.portalsysops.model;

import com.dciindonesia.portalsysops.entity.assetmgmt.IpVMDetail;
import com.dciindonesia.portalsysops.entity.assetmgmt.IpVm;
import com.dciindonesia.portalsysops.entity.assetmgmt.MsVM;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

public class RequestMsVM {

    public RequestMsVM() {
    }
    public RequestMsVM(InterfaceVM msVM, List<IpVMDetail> ips, List<IpVm> msIpVm) {
        this.id = msVM.getId();
        this.requestId = msVM.getRequestId();
        this.requestBy = msVM.getRequestBy();
        this.requestProgressId = msVM.getRequestProgressId();
        this.name = msVM.getName();
        try {
            this.ips = ips.stream().filter(e->e.getVmId().equals(msVM.getId())).map(s->s.getIpVmId()).collect(Collectors.toList());
        }catch (Exception e){}
        try {
            this.ipsStr = msIpVm.stream().filter(e->this.ips.stream().anyMatch(f->f.equals(e.getId()))).map(e->e.getIp()).collect(Collectors.toList());
        }catch (Exception e){}
        this.cpu = msVM.getCpu();
        this.socket = msVM.getSocket();
        this.memory = msVM.getMemory();
        this.storage = msVM.getStorage();
        this.licenseId = msVM.getLicenseId();
        this.assetId = msVM.getAssetId();
        this.state = msVM.getState();
        this.description = msVM.getDescription();
        this.backup = msVM.getBackup();
        this.highAvailbility = msVM.getHighAvailbility();
        this.priority = msVM.getPriority();
        this.avSpanPort = msVM.getAvSpanPort();
        this.avOssec = msVM.getAvOssec();
        this.antivirus = msVM.getAntivirus();
        this.antivirusInstalled = msVM.getAntivirusInstalled();
        this.antivirusLastupdate = msVM.getAntivirusLastupdate();
        this.prtgIcmp = msVM.getPrtgIcmp();
        this.prtgService = msVM.getPrtgService();
        this.prtgServiceRemark = msVM.getPrtgServiceRemark();
        this.grafana = msVM.getGrafana();
        this.license = msVM.getLicense();
        this.asset = msVM.getAsset();
        this.assetIPHost = msVM.getAssetIPHost();
        this.antivirusLastupdateStr = msVM.getAntivirusLastupdateStr();
        this.progress = msVM.getProgress();
        this.progressid = msVM.getProgressid();
        this.approvementId = msVM.getApprovementId();
        this.evidenceRemark = msVM.getEvidenceRemark();
        this.company = msVM.getCompany();
        this.companyId = msVM.getCompanyId();
        this.requestDate = msVM.getRequestDate();
        this.remark = msVM.getRemark();
        this.requestType = msVM.getRequestType();
    }
    private Long requestId;
    private String requestBy;
    private LocalDateTime requestDate;
    private Long requestProgressId;
    private List<Long> evidenceFiles;
    private String evidenceRemark;
    private MultipartFile[] evidenceFileData;
    private String name;
    private Long id;
    private String ip;
    private List<Long> ips;
    private List<String> ipsStr;
    private String cpu;
    private Integer socket;
    private String memory;
    private String storage;
    private Long licenseId;
    private Long assetId;
    private Long categoryId;
    private String license;
    private String asset;
    private String assetIPHost;
    private String category;
    private String state;
    private String description;
    private Boolean backup;
    private Boolean highAvailbility;
    private String priority;
    private Boolean avSpanPort;
    private Boolean avOssec;
    private String antivirus;
    private Boolean antivirusInstalled;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate antivirusLastupdate;
    private String antivirusLastupdateStr;
    private Boolean prtgIcmp;
    private Boolean prtgService;
    private String prtgServiceRemark;
    private Boolean grafana;
    private String progress;
    private Long progressid;
    private Long approvementId;
    private String company;
    private Long companyId;
    private String remark;
    private String requestType;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public LocalDateTime getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(LocalDateTime requestDate) {
        this.requestDate = requestDate;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getRequestBy() {
        return requestBy;
    }

    public void setRequestBy(String requestBy) {
        this.requestBy = requestBy;
    }

    public Long getRequestProgressId() {
        return requestProgressId;
    }

    public void setRequestProgressId(Long requestProgressId) {
        this.requestProgressId = requestProgressId;
    }

    public Long getApprovementId() {
        return approvementId;
    }

    public void setApprovementId(Long approvementId) {
        this.approvementId = approvementId;
    }

    public Long getProgressid() {
        return progressid;
    }

    public void setProgressid(Long progressid) {
        this.progressid = progressid;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public MultipartFile[] getEvidenceFileData() {
        return evidenceFileData;
    }

    public void setEvidenceFileData(MultipartFile[] evidenceFileData) {
        this.evidenceFileData = evidenceFileData;
    }

    public List<Long> getEvidenceFiles() {
        return evidenceFiles;
    }

    public void setEvidenceFiles(List<Long> evidenceFiles) {
        this.evidenceFiles = evidenceFiles;
    }

    public String getEvidenceRemark() {
        return evidenceRemark;
    }

    public void setEvidenceRemark(String evidenceRemark) {
        this.evidenceRemark = evidenceRemark;
    }

    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    public List<String> getIpsStr() {
        return ipsStr;
    }

    public void setIpsStr(List<String> ipsStr) {
        this.ipsStr = ipsStr;
    }

    public String getAssetIPHost() {
        return assetIPHost;
    }

    public void setAssetIPHost(String assetIPHost) {
        this.assetIPHost = assetIPHost;
    }


    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getAsset() {
        return asset;
    }

    public void setAsset(String asset) {
        this.asset = asset;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAntivirusLastupdateStr() {
        return antivirusLastupdateStr;
    }

    public void setAntivirusLastupdateStr(String antivirusLastupdateStr) {
        this.antivirusLastupdateStr = antivirusLastupdateStr;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public List<Long> getIps() {
        return ips;
    }

    public void setIps(List<Long> ips) {
        this.ips = ips;
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public Integer getSocket() {
        return socket;
    }

    public void setSocket(Integer socket) {
        this.socket = socket;
    }

    public String getMemory() {
        return memory;
    }

    public void setMemory(String memory) {
        this.memory = memory;
    }

    public String getStorage() {
        return storage;
    }

    public void setStorage(String storage) {
        this.storage = storage;
    }

    public Long getLicenseId() {
        return licenseId;
    }

    public void setLicenseId(Long licenseId) {
        this.licenseId = licenseId;
    }

    public Long getAssetId() {
        return assetId;
    }

    public void setAssetId(Long assetId) {
        this.assetId = assetId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getBackup() {
        return backup;
    }

    public void setBackup(Boolean backup) {
        this.backup = backup;
    }

    public Boolean getHighAvailbility() {
        return highAvailbility;
    }

    public void setHighAvailbility(Boolean highAvailbility) {
        this.highAvailbility = highAvailbility;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public Boolean getAvSpanPort() {
        return avSpanPort;
    }

    public void setAvSpanPort(Boolean avSpanPort) {
        this.avSpanPort = avSpanPort;
    }

    public Boolean getAvOssec() {
        return avOssec;
    }

    public void setAvOssec(Boolean avOssec) {
        this.avOssec = avOssec;
    }

    public String getAntivirus() {
        return antivirus;
    }

    public void setAntivirus(String antivirus) {
        this.antivirus = antivirus;
    }

    public Boolean getAntivirusInstalled() {
        return antivirusInstalled;
    }

    public void setAntivirusInstalled(Boolean antivirusInstalled) {
        this.antivirusInstalled = antivirusInstalled;
    }

    public LocalDate getAntivirusLastupdate() {
        return antivirusLastupdate;
    }

    public void setAntivirusLastupdate(LocalDate antivirusLastupdate) {
        this.antivirusLastupdate = antivirusLastupdate;
    }

    public Boolean getPrtgIcmp() {
        return prtgIcmp;
    }

    public void setPrtgIcmp(Boolean prtgIcmp) {
        this.prtgIcmp = prtgIcmp;
    }

    public Boolean getPrtgService() {
        return prtgService;
    }

    public void setPrtgService(Boolean prtgService) {
        this.prtgService = prtgService;
    }

    public String getPrtgServiceRemark() {
        return prtgServiceRemark;
    }

    public void setPrtgServiceRemark(String prtgServiceRemark) {
        this.prtgServiceRemark = prtgServiceRemark;
    }

    public Boolean getGrafana() {
        return grafana;
    }

    public void setGrafana(Boolean grafana) {
        this.grafana = grafana;
    }
}
