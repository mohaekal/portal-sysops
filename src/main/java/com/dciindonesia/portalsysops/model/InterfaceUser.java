package com.dciindonesia.portalsysops.model;

import java.time.LocalDateTime;

public interface InterfaceUser {

    Long getId();
    Integer getUserId();
    String getEmail();
    String getBeforechanges();
    String getAfterchanges();
    String getDescription();
    LocalDateTime getCreateDate();
    String getCreateDateStr();

}
