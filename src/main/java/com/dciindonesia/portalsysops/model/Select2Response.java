package com.dciindonesia.portalsysops.model;

public class Select2Response {


    public Select2Response(Object id, Object text) {
        this.id = id;
        this.text = text;
    }

    private Object id;
    private Object text;

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getText() {
        return text;
    }

    public void setText(Object text) {
        this.text = text;
    }
}
