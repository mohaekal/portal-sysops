package com.dciindonesia.portalsysops.model;

import com.dciindonesia.portalsysops.entity.assetmgmt.MsUser;
import com.dciindonesia.portalsysops.entity.portalmanagement.UserRole;

import java.util.List;
import java.util.stream.Collectors;

public class RequestUser {
    public RequestUser() {
    }

    public RequestUser(MsUser msUser, List<UserRole> userRole) {
        this.id = msUser.getId();
        this.name = msUser.getName();
        this.email = msUser.getEmail();
        this.role = userRole.stream().filter(e->e.getUserId().equals(msUser.getId())).map(f->f.getRoleId()).collect(Collectors.toList());
        this.enable = msUser.getEnable();
    }

    private Integer id;
    private String name;
    private String email;
    private String password;
    private List<Long> role;
    private Boolean enable;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Long> getRole() {
        return role;
    }

    public void setRole(List<Long> role) {
        this.role = role;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }
}
