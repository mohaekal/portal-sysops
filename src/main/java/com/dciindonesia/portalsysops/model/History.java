package com.dciindonesia.portalsysops.model;
import java.time.LocalDateTime;
import java.util.List;

public class History {

    public History(InterfaceRequestProgress requestProgress, List<HistoryApprovement> approvement) {
        this.requestId = requestProgress.getRequestId();
        this.requestProgressId = requestProgress.getProgressId();
        this.progressId = requestProgress.getProgressId();
        this.progressName = requestProgress.getProgress();
        this.progressTime = requestProgress.getCreateDate();
        this.approvement = approvement;
    }

    private Long requestId;
    private Long requestProgressId;
    private Long progressId;
    private String progressName;
    private LocalDateTime progressTime;
    private List<HistoryApprovement> approvement;

    public List<HistoryApprovement> getApprovement() {
        return approvement;
    }

    public void setApprovement(List<HistoryApprovement> approvement) {
        this.approvement = approvement;
    }

    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    public Long getRequestProgressId() {
        return requestProgressId;
    }

    public void setRequestProgressId(Long requestProgressId) {
        this.requestProgressId = requestProgressId;
    }

    public Long getProgressId() {
        return progressId;
    }

    public void setProgressId(Long progressId) {
        this.progressId = progressId;
    }

    public String getProgressName() {
        return progressName;
    }

    public void setProgressName(String progressName) {
        this.progressName = progressName;
    }

    public LocalDateTime getProgressTime() {
        return progressTime;
    }

    public void setProgressTime(LocalDateTime progressTime) {
        this.progressTime = progressTime;
    }
}
