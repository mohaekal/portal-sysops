package com.dciindonesia.portalsysops.model;

public class Select2 {

    public Select2() {
    }

    public Select2(Long id, String text) {
        this.id = id;
        this.text = text;
    }

    private Long id;
    private String text;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
