package com.dciindonesia.portalsysops.model;
import java.time.LocalDate;
import java.time.LocalDateTime;

public interface InterfaceLicense {

    Long getId();
    String getName();
    String getProductKey();
    Integer getQty();
    String getSupplierId();
    String getSupplier();
    String getManufacturerId();
    String getManufacturer();
    String getLicenseCategoryId();
    String getCategory();
    String getPrNumber();
    String getLicenseToName();
    String getLicenseToEmail();
    String getPassword();
    Integer getPurchaseCost();
    LocalDate getPurchaseDate();
    String getPurchaseDateStr();
    LocalDate getExpDate();
    String getExpDateString();
    Integer getExpStats();
    String getNotes();
    Long getLicenseFileId();
}
