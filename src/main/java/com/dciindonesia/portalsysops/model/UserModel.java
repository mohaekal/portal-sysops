package com.dciindonesia.portalsysops.model;

public class UserModel {

    public UserModel() {
    }

    public String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
