package com.dciindonesia.portalsysops.entity.mgmtidedge;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "ms_ubidots")
public class MsUbidots {
    public MsUbidots(){}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ubidots_id")
    private String ubidotsId;

    @Column(name = "variable_id")
    private Integer variableId;
    @Column(name = "ubidots_name")
    private String ubidotsName;
    @Column(name = "ubidots_desc")
    private String ubidotsDesc;
    private String type;
    private Double maxvaluee;
    private String unit;
    @Column(name = "mac_address")
    private String macAddress;

    @Column(name = "last_update")
    private LocalDateTime lastUpdate = LocalDateTime.now();

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getMaxvaluee() {
        return maxvaluee;
    }

    public void setMaxvaluee(Double maxvalue) {
        this.maxvaluee = maxvalue;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUbidotsId() {
        return ubidotsId;
    }

    public void setUbidotsId(String ubidotsId) {
        this.ubidotsId = ubidotsId;
    }

    public Integer getVariableId() {
        return variableId;
    }

    public void setVariableId(Integer variableId) {
        this.variableId = variableId;
    }

    public String getUbidotsName() {
        return ubidotsName;
    }

    public void setUbidotsName(String ubidotsName) {
        this.ubidotsName = ubidotsName;
    }

    public String getUbidotsDesc() {
        return ubidotsDesc;
    }

    public void setUbidotsDesc(String ubidotsDesc) {
        this.ubidotsDesc = ubidotsDesc;
    }
}
