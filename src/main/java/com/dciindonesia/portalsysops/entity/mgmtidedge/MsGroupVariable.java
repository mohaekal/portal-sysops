package com.dciindonesia.portalsysops.entity.mgmtidedge;


import javax.persistence.*;

@Table(name = "ms_group_variable")
@Entity
public class MsGroupVariable {

    public MsGroupVariable(){}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "group_nm")
    private String groupNm;
    @Column(name = "group_desc")
    private String groupDesc;

    private String note;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGroupNm() {
        return groupNm;
    }

    public void setGroupNm(String groupNm) {
        this.groupNm = groupNm;
    }

    public String getGroupDesc() {
        return groupDesc;
    }

    public void setGroupDesc(String groupDesc) {
        this.groupDesc = groupDesc;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
