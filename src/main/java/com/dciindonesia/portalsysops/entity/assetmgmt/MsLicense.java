package com.dciindonesia.portalsysops.entity.assetmgmt;

import com.dciindonesia.portalsysops.model.RequestLicense;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Table(name = "ms_license")
@Entity
public class MsLicense {

    public MsLicense() {
    }

    public MsLicense(RequestLicense requestLicense) {
        this.id = requestLicense.getId();
        this.name = requestLicense.getName();
        this.productKey = requestLicense.getProductKey();
        this.qty = requestLicense.getQty();
        this.supplierId = requestLicense.getSupplierId();
        this.manufacturerId = requestLicense.getManufacturerId();
        this.licenseCategoryId = requestLicense.getLicenseCategoryId();
        this.prNumber = requestLicense.getPrNumber();
        this.licenseToName = requestLicense.getLicenseToName();
        this.licenseToEmail = requestLicense.getLicenseToEmail();
        this.password = requestLicense.getPassword();
        this.purchaseCost = requestLicense.getPurchaseCost();
        this.purchaseDate = requestLicense.getPurchaseDate();
        this.expDate = requestLicense.getExpDate();
        this.notes = requestLicense.getNotes();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @Column(name = "product_key")
    private String productKey;
    private Integer qty;

    @Column(name = "supplier_id")
    private Integer supplierId;
    @Column(name = "manufacturer_id")
    private Integer manufacturerId;
    @Column(name = "license_category_id")
    private Integer licenseCategoryId;
    @Column(name = "pr_number")
    private String prNumber;

    @Column(name = "license_to_name")
    private String licenseToName;

    @Column(name = "license_to_email")
    private String licenseToEmail;
    private String password;
    @Column(name = "purchase_cost")
    private Integer purchaseCost;

    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "purchase_date")
    private LocalDate purchaseDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "exp_date")
    private LocalDate expDate;
    private String notes;
    @Column(name = "is_active")
    private Boolean isActive;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @Column(name = "create_date")
    private LocalDateTime createDate;
    @Column(name = "create_by")
    private Integer createBy;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @Column(name = "update_date")
    private LocalDateTime updateDate;
    @Column(name = "update_by")
    private Integer updateBy;
    @Column(name = "license_file_id")
    private Long licenseFileId;

    public Long getLicenseFileId() {
        return licenseFileId;
    }

    public void setLicenseFileId(Long licenseFileId) {
        this.licenseFileId = licenseFileId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPrNumber() {
        return prNumber;
    }

    public void setPrNumber(String prNumber) {
        this.prNumber = prNumber;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Integer getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(Integer manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public Integer getLicenseCategoryId() {
        return licenseCategoryId;
    }

    public void setLicenseCategoryId(Integer licenseCategoryId) {
        this.licenseCategoryId = licenseCategoryId;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public Integer getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    public LocalDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Integer updateBy) {
        this.updateBy = updateBy;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProductKey() {
        return productKey;
    }

    public void setProductKey(String productKey) {
        this.productKey = productKey;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Integer getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }

    public String getLicenseToName() {
        return licenseToName;
    }

    public void setLicenseToName(String licenseToName) {
        this.licenseToName = licenseToName;
    }

    public String getLicenseToEmail() {
        return licenseToEmail;
    }

    public void setLicenseToEmail(String licenseToEmail) {
        this.licenseToEmail = licenseToEmail;
    }

    public Integer getPurchaseCost() {
        return purchaseCost;
    }

    public void setPurchaseCost(Integer purchaseCost) {
        this.purchaseCost = purchaseCost;
    }

    public LocalDate getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(LocalDate purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public LocalDate getExpDate() {
        return expDate;
    }

    public void setExpDate(LocalDate expDate) {
        this.expDate = expDate;
    }
}
