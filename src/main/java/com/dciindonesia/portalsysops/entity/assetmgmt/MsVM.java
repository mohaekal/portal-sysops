package com.dciindonesia.portalsysops.entity.assetmgmt;

import com.dciindonesia.portalsysops.model.RequestMsVM;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Table(name = "ms_vm")
@Entity
public class MsVM {

    public MsVM() {
    }

    public MsVM(RequestMsVM requestMsVM) {
        this.id = requestMsVM.getId();
        this.name = requestMsVM.getName();
        this.cpu = requestMsVM.getCpu();
        this.socket = requestMsVM.getSocket();
        this.memory = requestMsVM.getMemory();
        this.storage = requestMsVM.getStorage();
        this.licenseId = requestMsVM.getLicenseId();
        this.assetId = requestMsVM.getAssetId();
        this.categoryId = requestMsVM.getCategoryId();
        this.state = requestMsVM.getState();
        this.description = requestMsVM.getDescription();
        this.backup = requestMsVM.getBackup();
        this.highAvailbility = requestMsVM.getHighAvailbility();
        this.priority = requestMsVM.getPriority();
        this.avSpanPort = requestMsVM.getAvSpanPort();
        this.avOssec = requestMsVM.getAvOssec();
        this.antivirus = requestMsVM.getAntivirus();
        this.antivirusInstalled = requestMsVM.getAntivirusInstalled();
        this.antivirusLastupdate = requestMsVM.getAntivirusLastupdate();
        this.prtgIcmp = requestMsVM.getPrtgIcmp();
        this.prtgService = requestMsVM.getPrtgService();
        this.prtgServiceRemark = requestMsVM.getPrtgServiceRemark();
        this.grafana = requestMsVM.getGrafana();
        this.companyId = requestMsVM.getCompanyId();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String ip;
    private String cpu;
    private Integer socket;
    private String memory;
    private String storage;
    @Column(name = "license_id")
    private Long licenseId;
    @Column(name = "asset_id")
    private Long assetId;
    @Column(name = "category_id")
    private Long categoryId;
    private String state;
    private String description;
    private Boolean backup;
    @Column(name = "high_availbility")
    private Boolean highAvailbility;
    private String priority;
    @Column(name = "av_span_port")
    private Boolean avSpanPort;
    @Column(name = "av_ossec")
    private Boolean avOssec;
    private String antivirus;
    @Column(name = "antivirus_installed")
    private Boolean antivirusInstalled;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "antivirus_lastupdate")
    private LocalDate antivirusLastupdate;
    @Column(name = "prtg_icmp")
    private Boolean prtgIcmp;
    @Column(name = "prtg_service")
    private Boolean prtgService;
    @Column(name = "prtg_service_remark")
    private String prtgServiceRemark;
    private Boolean grafana;
    @Column(name = "create_by")
    private Integer createBy;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @Column(name = "create_date")
    private LocalDateTime createDate;

    @Column(name = "update_by")
    private Integer updateBy;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @Column(name = "update_date")
    private LocalDateTime updateDate;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "company_id")
    private Long companyId = Long.valueOf(1);

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Boolean getGrafana() {
        return grafana;
    }

    public void setGrafana(Boolean grafana) {
        this.grafana = grafana;
    }

    public Integer getSocket() {
        return socket;
    }

    public void setSocket(Integer socket) {
        this.socket = socket;
    }

    public Integer getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Integer updateBy) {
        this.updateBy = updateBy;
    }

    public LocalDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getBackup() {
        return backup;
    }

    public void setBackup(Boolean backup) {
        this.backup = backup;
    }

    public Boolean getHighAvailbility() {
        return highAvailbility;
    }

    public void setHighAvailbility(Boolean highAvailbility) {
        this.highAvailbility = highAvailbility;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public Boolean getAvSpanPort() {
        return avSpanPort;
    }

    public void setAvSpanPort(Boolean avSpanPort) {
        this.avSpanPort = avSpanPort;
    }

    public Boolean getAvOssec() {
        return avOssec;
    }

    public void setAvOssec(Boolean avOssec) {
        this.avOssec = avOssec;
    }

    public String getAntivirus() {
        return antivirus;
    }

    public void setAntivirus(String antivirus) {
        this.antivirus = antivirus;
    }

    public Boolean getAntivirusInstalled() {
        return antivirusInstalled;
    }

    public void setAntivirusInstalled(Boolean antivirusInstalled) {
        this.antivirusInstalled = antivirusInstalled;
    }

    public LocalDate getAntivirusLastupdate() {
        return antivirusLastupdate;
    }

    public void setAntivirusLastupdate(LocalDate antivirusLastupdate) {
        this.antivirusLastupdate = antivirusLastupdate;
    }

    public Boolean getPrtgIcmp() {
        return prtgIcmp;
    }

    public void setPrtgIcmp(Boolean prtgIcmp) {
        this.prtgIcmp = prtgIcmp;
    }

    public Boolean getPrtgService() {
        return prtgService;
    }

    public void setPrtgService(Boolean prtgService) {
        this.prtgService = prtgService;
    }

    public String getPrtgServiceRemark() {
        return prtgServiceRemark;
    }

    public void setPrtgServiceRemark(String prtgServiceRemark) {
        this.prtgServiceRemark = prtgServiceRemark;
    }

    public Long getLicenseId() {
        return licenseId;
    }

    public void setLicenseId(Long licenseId) {
        this.licenseId = licenseId;
    }

    public Long getAssetId() {
        return assetId;
    }

    public void setAssetId(Long assetId) {
        this.assetId = assetId;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public String getMemory() {
        return memory;
    }

    public void setMemory(String memory) {
        this.memory = memory;
    }

    public String getStorage() {
        return storage;
    }

    public void setStorage(String storage) {
        this.storage = storage;
    }


    public Integer getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }
}
