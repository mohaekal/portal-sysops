package com.dciindonesia.portalsysops.entity.assetmgmt;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Table(name = "ms_asset")
@Entity
public class MsAsset {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "asset_tag")
    private String assetTag;

    @Column(name = "asset_name")
    private String assetName;

    @Column(name = "asset_status_id")
    private Integer assetStatusId;


    private String socket;
    private String core;
    private String type;
    private String storage;
    private String memory;

    @Column(name = "ip_management")
    private String ipManagement;
    @Column(name = "ip_host")
    private String ipHost;
    @Column(name = "user_management")
    private String userManagement;
    @Column(name = "password_management")
    private String passwordManagement;
    @Column(name = "user_host")
    private String userHost;
    @Column(name = "password_host")
    private String passwordHost;


    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "purchase_date")
    private LocalDate purchaseDate;

    @Column(name = "supplier_id")
    private Integer supplierId;

    @Column(name = "pr_number")
    private String prNumber;

    @Column(name = "purchase_cost")
    private Integer purchaseCost;
    private Integer warranty;
    private String notes;

    @Column(name = "location_id")
    private Integer locationId;

    @Column(name = "room_id")
    private Long roomId;
    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "asset_category_id")
    private Integer assetCategoryId;

    private String priority;
    private String backup;
    @Column(name = "high_availbility")
    private String highAvailbility;
    @Column(name = "av_spanport")
    private String avSpanport;
    @Column(name = "av_ossec")
    private String avOssec;
    private String antivirus;
    private String grafana;
    @Column(name = "prtg_icmp")
    private String prtgIcmp;
    @Column(name = "prtg_service")
    private String prtgService;

    @Column(name = "company_id")
    private Long companyId;

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @Column(name = "create_date")
    private LocalDateTime createDate;

    @Column(name = "create_by")
    private Integer createBy;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @Column(name = "update_date")
    private LocalDateTime updateDate;

    @Column(name = "update_by")
    private Integer updateBy;

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getBackup() {
        return backup;
    }

    public void setBackup(String backup) {
        this.backup = backup;
    }

    public String getHighAvailbility() {
        return highAvailbility;
    }

    public void setHighAvailbility(String highAvailbility) {
        this.highAvailbility = highAvailbility;
    }

    public String getAvSpanport() {
        return avSpanport;
    }

    public void setAvSpanport(String avSpanport) {
        this.avSpanport = avSpanport;
    }

    public String getAvOssec() {
        return avOssec;
    }

    public void setAvOssec(String avOssec) {
        this.avOssec = avOssec;
    }

    public String getAntivirus() {
        return antivirus;
    }

    public void setAntivirus(String antivirus) {
        this.antivirus = antivirus;
    }

    public String getGrafana() {
        return grafana;
    }

    public void setGrafana(String grafana) {
        this.grafana = grafana;
    }

    public String getPrtgIcmp() {
        return prtgIcmp;
    }

    public void setPrtgIcmp(String prtgIcmp) {
        this.prtgIcmp = prtgIcmp;
    }

    public String getPrtgService() {
        return prtgService;
    }

    public void setPrtgService(String prtgService) {
        this.prtgService = prtgService;
    }

    public String getSocket() {
        return socket;
    }

    public void setSocket(String socket) {
        this.socket = socket;
    }

    public String getCore() {
        return core;
    }

    public void setCore(String core) {
        this.core = core;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStorage() {
        return storage;
    }

    public void setStorage(String storage) {
        this.storage = storage;
    }

    public String getMemory() {
        return memory;
    }

    public void setMemory(String memory) {
        this.memory = memory;
    }

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public String getIpManagement() {
        return ipManagement;
    }

    public void setIpManagement(String ipManagement) {
        this.ipManagement = ipManagement;
    }

    public String getIpHost() {
        return ipHost;
    }

    public void setIpHost(String ipHost) {
        this.ipHost = ipHost;
    }

    public String getUserManagement() {
        return userManagement;
    }

    public void setUserManagement(String userManagement) {
        this.userManagement = userManagement;
    }

    public String getPasswordManagement() {
        return passwordManagement;
    }

    public void setPasswordManagement(String passwordManagement) {
        this.passwordManagement = passwordManagement;
    }

    public String getUserHost() {
        return userHost;
    }

    public void setUserHost(String userHost) {
        this.userHost = userHost;
    }

    public String getPasswordHost() {
        return passwordHost;
    }

    public void setPasswordHost(String passwordHost) {
        this.passwordHost = passwordHost;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public Integer getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    public LocalDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Integer updateBy) {
        this.updateBy = updateBy;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public Integer getAssetCategoryId() {
        return assetCategoryId;
    }

    public void setAssetCategoryId(Integer assetCategoryId) {
        this.assetCategoryId = assetCategoryId;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAssetTag() {
        return assetTag;
    }

    public void setAssetTag(String assetTag) {
        this.assetTag = assetTag;
    }

    public Integer getAssetStatusId() {
        return assetStatusId;
    }

    public void setAssetStatusId(Integer assetStatusId) {
        this.assetStatusId = assetStatusId;
    }

    public LocalDate getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(LocalDate purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public Integer getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }

    public String getPrNumber() {
        return prNumber;
    }

    public void setPrNumber(String prNumber) {
        this.prNumber = prNumber;
    }

    public Integer getPurchaseCost() {
        return purchaseCost;
    }

    public void setPurchaseCost(Integer purchaseCost) {
        this.purchaseCost = purchaseCost;
    }

    public Integer getWarranty() {
        return warranty;
    }

    public void setWarranty(Integer warranty) {
        this.warranty = warranty;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }
}
