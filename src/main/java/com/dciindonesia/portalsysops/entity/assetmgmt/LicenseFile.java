package com.dciindonesia.portalsysops.entity.assetmgmt;

import javax.persistence.*;

@Entity
@Table(name = "license_file")
public class LicenseFile {

    public LicenseFile() {
    }

    public LicenseFile(Long licenseId, String name, Long size, String type, byte[] data) {
        this.licenseId = licenseId;
        this.name = name;
        this.size = size;
        this.type = type;
        this.data = data;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "license_id")
    private Long licenseId;
    private String name;
    private Long size;
    private String type;
    private byte[] data;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLicenseId() {
        return licenseId;
    }

    public void setLicenseId(Long licenseId) {
        this.licenseId = licenseId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}
