package com.dciindonesia.portalsysops.entity.mgmtiddci;

import javax.persistence.*;

@Table(name = "log_change")
@Entity
public class LogChange {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "log_before")
    private String logBefore;
    @Column(name = "log_after")
    private String logAfter;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogBefore() {
        return logBefore;
    }

    public void setLogBefore(String logBefore) {
        this.logBefore = logBefore;
    }

    public String getLogAfter() {
        return logAfter;
    }

    public void setLogAfter(String logAfter) {
        this.logAfter = logAfter;
    }
}
